INSERT INTO `site_config` (`id`, `param`, `value`, `default`, `label`, `type`) VALUES
	(1, 'SITE_IS-OFFLINE', '0', '0', 'Сайт не доступен', 'checkbox'),
	(2, 'SITE_TITLE', 'Заголовок', 'Заголовок', 'Site title', 'text'),
	(3, 'SITE_DESCRIPTION', '', '', 'Seo-описание', 'text'),
	(4, 'SITE_KEYWORDS', '', ', aachen', 'Seo-ключевики', 'text'),
	(5, 'SITE_ADMIN_EMAIL', '', '', 'Емайл админа', 'text'),
	(6, 'SITE_NEW_MESSAGES_COUNT', '3', '0', 'Счетчик новых сообщений', 'text'),
	(7, 'SITE_GOOGLE_SCRIPT', '', '<script></script>', 'Google скрипты', 'textarea'),
	(8, 'SITE_YANDEX_SCRIPT', '', '<script></script>', 'Yandex скрипты', 'textarea');
	(8, 'SITE_JS_SCRIPT', '', '<script></script>', 'Доп. скрипты скрипты', 'textarea');
