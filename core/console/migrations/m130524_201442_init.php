<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'name' => $this->string(),
            'phone' => $this->string(),
            'role' => $this->integer()->defaultValue(10),
            'roles' => $this->string(),
            'utm' => $this->text(),
            'group' => $this->string(),
            'crm_name' => $this->string(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),

            'status_role' => $this->string()->defaultValue('client'),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'status_drop' => $this->smallInteger()->notNull()->defaultValue(5),
            'status_opt' => $this->smallInteger()->notNull()->defaultValue(5),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
