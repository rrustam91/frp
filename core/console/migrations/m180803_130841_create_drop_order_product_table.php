<?php

use yii\db\Migration;

/**
 * Handles the creation of table `drop_order_product`.
 */
class m180803_130841_create_drop_order_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%drop_order_product}}', [
            'id' => $this->primaryKey(),
            'drop_order_id'=> $this->integer()->notNull(),

            'product_id' => $this->integer()->notNull(),
            'product_name' => $this->string(),
            'price' => $this->integer()->defaultValue(0),
            'price_new' => $this->integer()->defaultValue(0),
            'price_total' => $this->integer()->defaultValue(0),
            'price_new_total' => $this->integer()->defaultValue(0),
            'quantity' => $this->integer()->defaultValue(1),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'status_append' => $this->smallInteger()->notNull()->defaultValue(10),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),

        ]);



        $this->createIndex('idx-drop_order_product-drop_order_id','{{%drop_order_product}}','drop_order_id');
        $this->addForeignKey('fk-drop_order_product-drop_order_id','{{%drop_order_product}}','drop_order_id','{{%drop_order}}','id','CASCADE');
        $this->createIndex('idx-drop_order_product-product_id','{{%drop_order_product}}','product_id');
        $this->addForeignKey('fk-drop_order_product-product_id','{{%drop_order_product}}','product_id','{{%products}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-drop_order_product-product_id','{{%drop_order_product}}');
        $this->dropIndex('idx-drop_order_product-product_id','{{%drop_order_product}}');
        $this->dropForeignKey('fk-drop_order_product-drop_order_id','{{%drop_order_product}}');
        $this->dropIndex('idx-drop_order_product-drop_order_id','{{%drop_order_product}}');

        $this->dropTable('{{%drop_order_product}}');
    }
}
