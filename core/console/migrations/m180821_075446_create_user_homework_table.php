<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_homework`.
 */
class m180821_075446_create_user_homework_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {/*
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_homework}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer()->notNull(),
            'lesson_id'=>$this->integer()->notNull(),
            'course_id'=>$this->integer()->notNull(),
            'category_id'=>$this->integer()->notNull(),
            'status'=>$this->integer()->defaultValue(10),
            'text'=>$this->text(),
            'comment_teach'=>$this->text(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex('idx-user_homework-lesson_id','{{%user_homework}}','lesson_id');
        $this->addForeignKey('fk-user_homework-lesson_id','{{%user_homework}}','lesson_id','{{%courses_lessons}}','id','CASCADE');

        $this->createIndex('idx-user_homework-course_id','{{%user_homework}}','course_id');
        $this->addForeignKey('fk-user_homework-course_id','{{%user_homework}}','course_id','{{%courses}}','id','CASCADE');

        $this->createIndex('idx-user_homework-category_id','{{%user_homework}}','category_id');
        $this->addForeignKey('fk-user_homework-category_id','{{%user_homework}}','category_id','{{%course_category}}','id','CASCADE');

        $this->createIndex('idx-user_homework-user_id','{{%user_homework}}','user_id');
        $this->addForeignKey('fk-user_homework-user_id','{{%user_homework}}','user_id','{{%user}}','id','CASCADE');*/


    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      /*  $this->dropForeignKey('fk-user_homework-user_id','{{%user_homework}}');
        $this->dropIndex('idx-user_homework-user_id','{{%user_homework}}');
        $this->dropForeignKey('fk-user_homework-lesson_id','{{%user_homework}}');
        $this->dropIndex('idx-user_homework-lesson_id','{{%user_homework}}');

        $this->dropTable('{{%user_homework}}');*/
    }
}
