<?php

use yii\db\Migration;

/**
 * Handles the creation of table `courses_order`.
 */
class m180803_131112_create_courses_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%courses_order}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'price'=> $this->integer()->notNull()->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'comment' => $this->text(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-courses_order-user_id','{{%courses_order}}','user_id');
        $this->addForeignKey('fk-courses_order-user_id','{{%courses_order}}','user_id','{{%user}}','id','CASCADE');

        $this->createIndex('idx-courses_order-course_id','{{%courses_order}}','course_id');
        $this->addForeignKey('fk-courses_order-course_id','{{%courses_order}}','course_id','{{%courses}}','id','CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-courses_order-course_id','{{%courses_order}}');
        $this->dropIndex('idx-courses_order-course_id','{{%courses_order}}');

        $this->dropForeignKey('fk-courses_order-user_id','{{%courses_order}}');
        $this->dropIndex('idx-courses_order-user_id','{{%courses_order}}');

        $this->dropTable('{{%courses_order}}');
    }
}
