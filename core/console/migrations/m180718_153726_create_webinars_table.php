<?php

use yii\db\Migration;

/**
 * Handles the creation of table `webinar`.
 */
class m180718_153726_create_webinars_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%webinars}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string()->notNull(),
            'alias'         => $this->string()->notNull(),
            'status'        => $this->smallInteger()->notNull()->defaultValue(10),

            'video_url'     => $this->string(),
            'title'     => $this->string(),

            'date_start'    => $this->integer(),
            'date_end'      => $this->integer(),

            'created_at'    => $this->integer()->notNull(),
            'updated_at'    => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%webinars}}');
    }
}
