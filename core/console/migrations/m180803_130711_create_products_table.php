<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products`.
 */
class m180803_130711_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->defaultValue(0),
            'name' => $this->text()->notNull(),
            'alias' => $this->string(),
            'article'=> $this->string()->notNull(),
            'price' => $this->float()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),

            'status_drop' => $this->smallInteger()->notNull()->defaultValue(5),
            'status_opt' => $this->smallInteger()->notNull()->defaultValue(5),

            'xml_id'=> $this->string(),
            'xml_name'=> $this->string(),
            'xml_category'=> $this->string(),
            'xml_price'=> $this->string(),
            'xml_article'=> $this->string(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex('idx-products-category_id','{{%products}}','category_id');
        $this->addForeignKey('fk-products-category_id','{{%products}}','category_id','{{%products_categories}}','id','SET NULL','RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk-products-category_id','{{%products}}');
        $this->dropIndex('idx-products-category_id','{{%products}}');

        $this->dropTable('{{%products}}');
    }
}
