<?php

use yii\db\Migration;

/**
 * Class m180911_115738_opt_request
 */
class m180911_115738_opt_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%opt_request}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer()->notNull(),
            'link'=>$this->string(),

            'status'=>$this->integer()->defaultValue(5),
            'append'=>$this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-opt_request-user_id','{{%opt_request}}','user_id');
        $this->addForeignKey('fk-opt_request-user_id','{{%opt_request}}','user_id','{{%user}}','id','CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-opt_request-user_id','{{%opt_request}}');
        $this->dropIndex('idx-opt_request-user_id','{{%opt_request}}');

        $this->dropTable('{{%opt_request}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180911_115738_opt_request cannot be reverted.\n";

        return false;
    }
    */
}
