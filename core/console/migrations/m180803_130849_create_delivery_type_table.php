<?php

use yii\db\Migration;

/**
 * Handles the creation of table `delivery_type`.
 */
class m180803_130849_create_delivery_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%delivery_type}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->notNull(),
            'alias'=>$this->string()->notNull(),
            'delivery_price' => $this->integer()->notNull()->defaultValue(0),

            'status'=>$this->string()->notNull()->defaultValue(10),
            'status_opt'=>$this->string()->defaultValue(5),
            'status_drop'=>$this->string()->defaultValue(5),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%delivery_type}}');
    }
}
