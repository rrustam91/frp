<?php

use yii\db\Migration;

/**
 * Handles the creation of table `courses_lessons`.
 */
class m180803_130405_create_courses_lessons_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%courses_lessons}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer()->defaultValue(NULL),
            'category_id' => $this->integer()->defaultValue(NULL),
            'name' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'content' => $this->text(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'opened' => $this->smallInteger()->notNull()->defaultValue(10),
            'step_by' => $this->integer()->notNull()->defaultValue(0),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex('idx-courses_lessons-course_id','{{%courses_lessons}}','course_id');
        $this->addForeignKey('fk-courses_lessons-course_id','{{%courses_lessons}}','course_id','{{%courses}}','id','CASCADE');

        $this->createIndex('idx-courses_lessons-category_id','{{%courses_lessons}}','category_id');
        $this->addForeignKey('fk-courses_lessons-category_id','{{%courses_lessons}}','category_id','{{%course_category}}','id','SET NULL');


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk-courses_lessons-category_id','{{%courses_lessons}}');
        $this->dropIndex('idx-courses_lessons-category_id','{{%courses_lessons}}');


        $this->dropForeignKey('fk-courses_lessons-course_id','{{%courses_lessons}}');
        $this->dropIndex('idx-courses_lessons-course_id','{{%courses_lessons}}');

        $this->dropTable('{{%courses_lessons}}');
    }
}
