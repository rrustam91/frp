<?php

use yii\db\Migration;

class m170328_191926_site_config extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%site_config}}', [
            'id' => $this->primaryKey(),
            'param' => $this->string()->notNull()->unique(),
            'value' => $this->text(),
            'default' => $this->text()->notNull(),
            'label' => $this->string(),
            'type' => $this->string()->notNull(),
        ], $tableOptions);
        //наполняем таблицу заготовленными данными в sql
        $this->execute(file_get_contents(__DIR__ . '/sql/site_config.sql'));
    }

    public function safeDown()
    {
        $this->dropTable('{{%site_config}}');
    }
}