<?php

use yii\db\Migration;

/**
 * Handles the creation of table `drop_deliver`.
 */
class m180803_130850_create_drop_deliver_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%drop_deliver}}', [
            'id' => $this->primaryKey(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),

            'drop_order_id' => $this->integer()->notNull(),
            'delivery_type_id' => $this->integer()->defaultValue(0),

            'delivery_type_code' => $this->string()->notNull(),
            'region' => $this->string(),
            'city' => $this->string(),
            'subway' => $this->string(),
            'street' => $this->string(),
            'zipcode' => $this->string(),
            'building' => $this->string(),
            'block' => $this->string(),
            'house' => $this->string(),

            'flat' => $this->string(),
            'float' => $this->string(),
            'floor' => $this->string(),
            'note' => $this->string(),
            'cost' => $this->integer(), // стоимость доставки
            'delivery_price' => $this->string(), // стоимость доставки

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);



        $this->createIndex('idx-drop_deliver-drop_order_id','{{%drop_deliver}}','drop_order_id');
        $this->addForeignKey('fk-drop_deliver-drop_order_id','{{%drop_deliver}}','drop_order_id','{{%drop_order}}','id','CASCADE');
        $this->createIndex('idx-drop_deliver-delivery_type_id','{{%drop_deliver}}','delivery_type_id');
        $this->addForeignKey('fk-drop_deliver-delivery_type_id','{{%drop_deliver}}','delivery_type_id','{{%delivery_type}}','id','CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk-drop_deliver-delivery_type_id','{{%drop_deliver}}');
        $this->dropIndex('idx-drop_deliver-delivery_type_id','{{%drop_deliver}}');
        $this->dropForeignKey('fk-drop_deliver-drop_order_id','{{%drop_deliver}}');
        $this->dropIndex('idx-drop_deliver-drop_order_id','{{%drop_deliver}}');

        $this->dropTable('{{%drop_deliver}}');
    }
}
