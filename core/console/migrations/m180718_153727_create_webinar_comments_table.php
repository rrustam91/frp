<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comments`.
 */
class m180718_153727_create_webinar_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%webinar_comments}}', [
            'id'                => $this->primaryKey(),
            'webinar_id'       => $this->integer()->notNull(),
            'name'              => $this->string()->notNull(),
            'text'              => $this->text(),
            'time_send'         => $this->integer()->defaultValue(0),

            'created_at'        => $this->integer()->notNull(),
            'updated_at'        => $this->integer()->notNull(),
        ], $tableOptions);



        $this->createIndex('idx-webinar_comments-webinar_id','{{%webinar_comments}}','webinar_id');
        $this->addForeignKey('fk-webinar_comments-webinar_id','{{%webinar_comments}}','webinar_id','{{%webinars}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-webinar_comments-webinar_id','{{%webinar_comments}}');
        $this->dropIndex('idx-webinar_comments-webinar_id','{{%webinar_comments}}');
        $this->dropTable('{{%webinar_comments}}');
    }
}
