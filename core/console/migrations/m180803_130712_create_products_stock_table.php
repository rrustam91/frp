<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products_stock`.
 */
class m180803_130712_create_products_stock_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%products_stock}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'count'=>   $this->integer()->notNull()->defaultValue(0),

            'created_at'    => $this->integer()->notNull(),
            'updated_at'    => $this->integer()->notNull(),

        ], $tableOptions);

        $this->createIndex('idx-products_stock-product_id','{{%products_stock}}','product_id');
        $this->addForeignKey('fk-products_stock-product_id','{{%products_stock}}','product_id','{{%products}}','id','CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {


        $this->dropForeignKey('fk-products_stock-product_id','{{%products_stock}}');
        $this->dropIndex('idx-products_stock-product_id','{{%products_stock}}');
        $this->dropTable('{{%products_stock}}');
    }
}
