<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course_category`.
 */
class m180803_130404_create_course_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      /*  $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%course_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'content' => $this->text(),

            'course_id' => $this->integer()->defaultValue(null),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'opened' => $this->smallInteger()->notNull()->defaultValue(10),
            'step_by' => $this->integer()->notNull()->defaultValue(0),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);



        $this->createIndex('idx-course_category-course_id','{{%course_category}}','course_id');
        $this->addForeignKey('fk-course_category-course_id','{{%course_category}}','course_id','{{%courses}}','id','CASCADE');*/


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {


        $this->dropForeignKey('fk-course_category-course_id','{{%course_category}}');
        $this->dropIndex('idx-course_category-course_id','{{%course_category}}');

        $this->dropTable('{{%course_category}}');
    }
}
