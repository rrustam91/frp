<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dropship`.
 */
class m180803_130023_create_user_opt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_opt}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'status' => $this->integer()->defaultValue(\common\models\MainModel::STATUS_USER_DROP_WAIT),


            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex('idx-user_opt-user_id','{{%user_opt}}','user_id');
        $this->addForeignKey('fk-user_opt-user_id','{{%user_opt}}','user_id','{{%user}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-user_opt-user_id','{{%user_opt}}');
        $this->dropIndex('idx-user_opt-user_id','{{%user_opt}}');
        $this->dropTable('{{%user_opt}}');
    }
}
