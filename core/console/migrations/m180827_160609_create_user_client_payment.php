<?php

use yii\db\Migration;

/**
 * Class m180827_160609_create_user_drop_payment
 */
class m180827_160609_create_user_client_payment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /*$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_client_payment}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer()->notNull(),
            'manager_id'=>$this->integer()->notNull(),
            'sum' => $this->integer()->notNull(),
            'status'=>$this->integer()->defaultValue(10),
            'comment'=>$this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-user_client_payment-user_id','{{%user_client_payment}}','user_id');
        $this->addForeignKey('fk-user_client_payment-user_id','{{%user_client_payment}}','user_id','{{%user}}','id','CASCADE');

        $this->createIndex('idx-user_client_payment-manager_id','{{%user_client_payment}}','manager_id');
        $this->addForeignKey('fk-user_client_payment-manager_id','{{%user_client_payment}}','manager_id','{{%user}}','id','CASCADE');*/
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

       // $this->dropTable('{{%user_client_payment');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180827_160609_create_user_drop_payment cannot be reverted.\n";

        return false;
    }
    */
}
