<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_courses`.
 */
class m180803_130404_create_user_courses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_courses}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),


            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),


        ], $tableOptions);

        $this->createIndex('idx-user_courses-user_id','{{%user_courses}}','user_id');
        $this->addForeignKey('fk-user_courses-user_id','{{%user_courses}}','user_id','{{%user}}','id','CASCADE');

        $this->createIndex('idx-user_courses-course_id','{{%user_courses}}','course_id');
        $this->addForeignKey('fk-user_courses-course_id','{{%user_courses}}','course_id','{{%courses}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-user_courses-user_id','{{%user_courses}}');
        $this->dropIndex('idx-user_courses-user_id','{{%user_courses}}');

        $this->dropForeignKey('fk-user_courses-course_id','{{%user_courses}}');
        $this->dropIndex('idx-user_courses-course_id','{{%user_courses}}');


        $this->dropTable('{{%user_courses}}');
    }
}
