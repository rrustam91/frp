<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_img`.
 */
class m180815_091253_create_product_img_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%product_img}}', [
            'id' => $this->primaryKey(),
            'product_id'=>$this->integer()->notNull(),
            'url' => $this->string(),
            'title' => $this->string(),


            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-product_img-product_id','{{%product_img}}','product_id');
        $this->addForeignKey('fk-product_img-product_id','{{%product_img}}','product_id','{{%products}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_img-product_id','{{%product_img}}');
        $this->dropIndex('idx-product_img-product_id','{{%product_img}}');
        $this->dropTable('{{%product_img}}');
    }
}
