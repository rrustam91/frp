<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_messages`.
 */
class m180815_104431_create_user_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_messages}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer()->notNull(),
            'type'=>$this->string(),

            'text'=>$this->text(),
            'status' => $this->smallInteger()->notNull()->defaultValue(5),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),

        ], $tableOptions);

        $this->createIndex('idx-user_messages-user_id','{{%user_messages}}','user_id');
        $this->addForeignKey('fk-user_messages-user_id','{{%user_messages}}','user_id','{{%user}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-user_messages-user_id','{{%user_messages}}');
        $this->dropIndex('idx-user_messages-user_id','{{%user_messages}}');
        $this->dropTable('{{%user_messages}}');
    }
}
