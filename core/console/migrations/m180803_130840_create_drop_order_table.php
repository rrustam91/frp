<?php

use yii\db\Migration;

/**
 * Handles the creation of table `drop_order`.
 */
class m180803_130840_create_drop_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%drop_order}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'client_id' => $this->integer()->notNull(),
            'crm_id' => $this->integer()->defaultValue(0),

            'products' => $this->text(),
            'comment' => $this->text(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'price'=>$this->integer()->defaultValue(0),
            'price_total'=>$this->integer()->defaultValue(0),
            'price_new'=>$this->integer()->defaultValue(0),
            'price_new_total'=>$this->integer()->defaultValue(0),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex('idx-drop_order-user_id','{{%drop_order}}','user_id');
        $this->addForeignKey('fk-drop_order-user_id','{{%drop_order}}','user_id','{{%user}}','id','CASCADE');
        $this->createIndex('idx-drop_order-client_id','{{%drop_order}}','client_id');
        $this->addForeignKey('fk-drop_order-client_id','{{%drop_order}}','client_id','{{%user}}','id','CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk-drop_order-client_id','{{%drop_order}}');
        $this->dropIndex('idx-drop_order-client_id','{{%drop_order}}');
        $this->dropForeignKey('fk-drop_order-user_id','{{%drop_order}}');
        $this->dropIndex('idx-drop_order-user_id','{{%drop_order}}');

        $this->dropTable('{{%drop_order}}');
    }
}
