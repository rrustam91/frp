<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chat`.
 */
class m180718_153816_create_webinar_chats_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%webinar_chats}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'webinar_id' => $this->integer()->notNull(),

            'text'             => $this->text(),
            'time_video'            => $this->integer()->defaultValue(0),

            'created_at'        => $this->integer()->notNull(),
            'updated_at'        => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex('idx-webinar_chats-webinar_id','{{%webinar_chats}}','webinar_id');
        $this->addForeignKey('fk-webinar_chats-webinar_id','{{%webinar_chats}}','webinar_id','{{%webinars}}','id','CASCADE');

        $this->createIndex('idx-webinar_chats-user_id','{{%webinar_chats}}','user_id');
        $this->addForeignKey('fk-webinar_chats-user_id','{{%webinar_chats}}','user_id','{{%user}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk-webinar_chats-webinar_id','{{%webinar_chats}}');
        $this->dropIndex('idx-webinar_chats-webinar_id','{{%webinar_chats}}');
        $this->dropForeignKey('fk-webinar_chats-user_id','{{%webinar_chats}}');
        $this->dropIndex('idx-webinar_chats-user_id','{{%webinar_chats}}');
        $this->dropTable('{{%webinar_chats}}');
    }
}
