<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders_status`.
 */
class m180828_065429_create_orders_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /*$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%orders_status_group}}', [
            'id' => $this->primaryKey(),
            'alias'=> $this->string()->notNull(),
            'name'=>$this->string()->notNull(),
            'code'=> $this->string()->notNull(),
            'active'=> $this->integer()->defaultValue(1),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%orders_status}}', [
            'id' => $this->primaryKey(),
            'group'=> $this->integer(),
            'alias'=> $this->string()->notNull(),
            'name'=>$this->string()->notNull(),
            'code'=> $this->string()->notNull(),
            'status_drop'=> $this->integer()->defaultValue(5),
            'drop_name'=> $this->string(),
            'active'=> $this->integer()->defaultValue(1),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);


        $this->createIndex('idx-orders_status-group','{{%orders_status}}','group');
        $this->addForeignKey('fk-orders_status-group','{{%orders_status}}','group','{{%orders_status_group}}','id','SET NULL');*/

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
/*        $this->dropTable('{{%orders_status}}');
        $this->dropTable('{{%orders_status_group}}');*/
    }
}
