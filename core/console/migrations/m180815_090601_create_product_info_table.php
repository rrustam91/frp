<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_info`.
 */
class m180815_090601_create_product_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%product_info}}', [
            'id' => $this->primaryKey(),
            'product_id'=>$this->integer()->notNull(),
            'text'=> $this->text(),


            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-product_info-product_id','{{%product_info}}','product_id');
        $this->addForeignKey('fk-product_info-product_id','{{%product_info}}','product_id','{{%products}}','id','CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_info-product_id','{{%product_info}}');
        $this->dropIndex('idx-product_info-product_id','{{%product_info}}');
        $this->dropTable('{{%product_info}}');
    }
}
