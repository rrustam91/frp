<?php

use yii\db\Migration;

/**
 * Handles the creation of table `courses`.
 */
class m180803_130021_create_courses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%courses}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'parent_id' => $this->integer()->defaultValue(NULL),
            'price' => $this->float()->notNull()->defaultValue(0),

            'date_start' => $this->integer(),
            'date_end' => $this->integer(),

            'content' => $this->text(),
            'img' => $this->string(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),


            'seo_title' => $this->string()->defaultValue(NULL),
            'seo_keywords' => $this->string()->defaultValue(NULL),
            'seo_description' => $this->string()->defaultValue(NULL),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),


        ], $tableOptions);


        $this->createIndex('idx-courses-parent_id','{{%courses}}','parent_id');
        $this->addForeignKey('fk-courses-parent_id','{{%courses}}','parent_id','{{%courses}}','id','SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {


        $this->dropForeignKey('fk-courses-parent_id','{{%courses}}');
        $this->dropIndex('idx-courses-parent_id','{{%courses}}');

        $this->dropTable('{{%courses}}');
    }
}
