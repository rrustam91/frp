<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_categories`.
 */
class m180803_130710_create_products_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%products_categories}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'alias' => $this->text()->notNull(),
            'content' => $this->text(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'status_drop' => $this->smallInteger()->notNull()->defaultValue(5),
            'status_opt' => $this->smallInteger()->notNull()->defaultValue(5),

            'xml_id'=> $this->string(),
            'xml_parent_id'=> $this->string(),
            'xml_name'=> $this->string(),


            'seo_keywords' => $this->string()->defaultValue(NULL),
            'seo_description' => $this->string()->defaultValue(NULL),



            'img' => $this->string(),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),

        ], $tableOptions);

        $this->createIndex('idx-products_categories-parent_id','{{%products_categories}}','parent_id');
        $this->addForeignKey('fk-products_categories-parent_id','{{%products_categories}}','parent_id','{{%products_categories}}','id','SET NULL','RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk-products_categories-parent_id','{{%products_categories}}');
        $this->dropIndex('idx-products_categories-parent_id','{{%products_categories}}');

        $this->dropTable('{{%products_categories}}');
    }
}
