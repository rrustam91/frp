<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_step_lesson`.
 */
class m180821_112136_create_user_step_lesson_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       /* $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_step_lesson}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer()->notNull(),
            'lesson_id'=>$this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'course_category_id'=>$this->integer()->notNull(),

            'step'=>$this->integer()->notNull(),
            'status'=>$this->integer()->defaultValue(10),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex('idx-user_step_lesson-lesson_id','{{%user_step_lesson}}','lesson_id');
        $this->addForeignKey('fk-user_step_lesson-lesson_id','{{%user_step_lesson}}','lesson_id','{{%courses_lessons}}','id','CASCADE');

        $this->createIndex('idx-user_step_lesson-course_id','{{%user_step_lesson}}','course_id');
        $this->addForeignKey('fk-user_step_lesson-course_id','{{%user_step_lesson}}','course_id','{{%courses}}','id','CASCADE');

        $this->createIndex('idx-user_step_lesson-course_category_id','{{%user_step_lesson}}','course_category_id');
        $this->addForeignKey('fk-user_step_lesson-course_category_id','{{%user_step_lesson}}','course_category_id','{{%course_category}}','id','CASCADE');

        $this->createIndex('idx-user_step_lesson-user_id','{{%user_step_lesson}}','user_id');
        $this->addForeignKey('fk-user_step_lesson-user_id','{{%user_step_lesson}}','user_id','{{%user}}','id','CASCADE');*/

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        $this->dropTable('user_step_lesson');
    }
}
