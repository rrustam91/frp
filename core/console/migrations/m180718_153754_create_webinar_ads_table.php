<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ads`.
 */
class m180718_153754_create_webinar_ads_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%webinar_ads}}', [
            'id'            => $this->primaryKey(),
            'webinar_id'          => $this->integer()->notNull(),
            'status'        => $this->smallInteger()->notNull()->defaultValue(10),

            'img'     => $this->string(),
            'text'     => $this->string(),
            'url'     => $this->string(),

            'date_start'    => $this->integer(),
            'date_end'      => $this->integer(),

            'created_at'    => $this->integer()->notNull(),
            'updated_at'    => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-ads-webinar_id','{{%webinar_ads}}','webinar_id');
        $this->addForeignKey('fk-ads-webinar_id','{{%webinar_ads}}','webinar_id','{{%webinars}}','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-ads-webinar_id','{{%webinar_ads}}');
        $this->dropIndex('idx-ads-webinar_id','{{%webinar_ads}}');
        $this->dropTable('{{%webinar_ads}}');
    }
}
