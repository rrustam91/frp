<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_profile`.
 */
class m180805_095430_create_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_profile}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'surname'=>$this->string(),
            'phone'=>$this->string(),


            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);


        $this->createIndex('idx-user_profile-user_id','{{%user_profile}}','user_id');
        $this->addForeignKey('fk-user_profile-user_id','{{%user_profile}}','user_id','{{%user}}','id','CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-user_profile-user_id','{{%user_profile}}');
        $this->dropIndex('idx-user_profile-user_id','{{%user_profile}}');
        $this->dropTable('{{%user_profile}}');
    }
}
