<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23.08.2018
 * Time: 12:27
 */

namespace common\widgets;

use common\helpers\myHellpers;
use common\models\courses\CourseCategory;
use common\models\courses\Courses;
use common\models\courses\CoursesHellper;
use common\models\courses\CoursesLessons;
use common\models\MainModel;
use Yii;
use yii\base\Widget;

class LessonMenuWidget extends Widget
{
    const TPL = 'menu.php';
    const CACHE_TPL = 'lesson_menu';
    public $tpl;
    public $model;
    public $data;
    public $tree;
    public $menuHtml;
    public $menutype;
    public $course;
    public $category;
    public $lessons;

    public function init(){
        parent::init();
        if($this->tpl === null) $this->tpl = 'menu';
        $this->tpl .= '.php';
    }

    public function run()
    {
        //Yii::$app->cache->delete(self::CACHE_TPL);
        if($this->tpl == self::TPL){
            $menu = Yii::$app->cache->get(self::CACHE_TPL);
            if($menu) return $menu;
        }
        $this->tree = $this->getTree();
        $this->menuHtml = $this->getMenuHtml($this->tree);

        if($this->tpl == self::TPL){
            Yii::$app->cache->set(self::CACHE_TPL, $this->menuHtml, 60);
        }

        return $this->menuHtml;

    }

    protected function getTree(){
        $tree = [];
        $category = CoursesHellper::getModelByCourseId(CourseCategory::className(),$this->course->id, 'all');
        $complete = CoursesHellper::getLastLessonUser($this->course->id);
        $cat_cmplt = $complete->courseCategory;
        $lsn_cmplt = $complete->lesson;

        foreach ($category as $id=>&$cat ){
            $com = MainModel::STATUS_LESSON_INACTIVE;
            $tree[$id] =[];
            $tree[$id]['id'] = $cat->id;
            $tree[$id]['name'] = $cat->name;
            $tree[$id]['alias'] = $cat->alias;
            $tree[$id]['course'] = $cat->course->alias;
            foreach ($cat->coursesLessons as $lid=>$lsn){
                $com = MainModel::STATUS_LESSON_INACTIVE;
                if($id<=$cat_cmplt->step_by-1){
                    if($lid<=$lsn_cmplt->step_by-1){
                        $com = MainModel::STATUS_LESSON_ACTIVE;
                    }
                }
                $tree[$id]['lsn'][$lid]['id'] = $lsn->id;
                $tree[$id]['lsn'][$lid]['name'] = $lsn->name;
                $tree[$id]['lsn'][$lid]['alias'] = $lsn->alias;
                $tree[$id]['lsn'][$lid]['status'] = $lsn->status;
                $tree[$id]['lsn'][$lid]['opened'] =  $lsn->opened;
                $tree[$id]['lsn'][$lid]['complete'] = MainModel::STATUS_LESSON_ACTIVE;
            }
            $tree[$id]['complete'] = $com;
            $tree[$id]['current'] = $complete->lesson_id;
        }
        return $tree;
    }


    protected function getMenuHtml($tree, $tab = ''){
        $str = '';
        foreach ($tree as $item) {
            $str .= $this->setTemplate($item, $tab);
        }
        return $str;
    }

    protected function setTemplate($item, $tab){
        ob_start();
        include __DIR__ . '/lesson_menu_tpl/' . $this->tpl;
        return ob_get_clean();
    }

}