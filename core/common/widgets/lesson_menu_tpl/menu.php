<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23.08.2018
 * Time: 12:27
 */

?>
    <div class="lesson__sidebar-block  open">
        <div class="lesson__sidebar-header"  data-target="les-menu-<?=$item['id']?>">
            <span>
                <?=$item['name']?>
            </span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </div>
        <ul class="lesson__sidebar-menu " id="les-menu-<?=$item['id']?>">
            <?if(isset($item['lsn'])){
                foreach ($item['lsn'] as $lsn) {
                    $lurl = '/cp/courses/'.$item['course'].'/'.$lsn['alias'];
            ?>
                    <li class="lesson__sidebar-list
                        <?= ($lsn['complete'] == \common\models\MainModel::STATUS_LESSON_ACTIVE) ? "complete" : ""; ?>
                        <?= ($lsn['opened'] != \common\models\MainModel::STATUS_LESSON_INACTIVE) ? "" : "deactive"; ?> "
                        data-href="<?=$lurl?>"
                    >
                        <a
                            <?if($lsn['opened'] != \common\models\MainModel::STATUS_LESSON_INACTIVE){ ?>
                            href="<?= \yii\helpers\Url::toRoute(['/courses/'.$item['course'].'/'.$lsn['alias']], false) ?>"
                            <?}?>
                        >
                            <i class="fa <?= ($lsn['complete'] == \common\models\MainModel::STATUS_LESSON_ACTIVE) ? "fa-check-circle-o" : "fa-circle-o"; ?> ">&nbsp;</i> <?=$lsn['name']?>
                        </a>
                    </li>

            <? }
            }?>

        </ul>
    </div>


<?
/*** template **
 *
 *
    <li class="lesson__sidebar-list complete">
        <a href="">
            <span class="fa fa-check-circle-o">&nbsp;</span> <?=$lsn['name']?>
        </a>
    </li>
    <li class="lesson__sidebar-list active">
        <a href="#">
            <span class="fa fa-circle-o">&nbsp;</span> Урок 2
        </a>
    </li>
    <li class="lesson__sidebar-list">
        <a href="#">
            <span class="fa fa-circle-o">&nbsp;</span> Урок 3
        </a>
    </li>
    <li class="lesson__sidebar-list deactive">
        <a href="#">
            <span class="fa fa-circle-o">&nbsp;</span> Урок 5
        </a>
    </li>
 *
 *
 */

