<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.08.2018
 * Time: 12:01
 */

namespace common\models;


use common\models\products\Products;
use common\models\products\ProductsCategories;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class MainModel extends ActiveRecord
{

    const COM_FIZ  = 0.97;
    const COM_YUR  = 0.87;
    const DEBUG_M = true;

    const STATUS_TYPE_ACTIVE = 5;
    const STATUS_TYPE_INACTIVE = 10;
    const STATUS_TYPE_DELETED = -1;

    const STATUS_ACTIVE = 10;
    const STATUS_COMPLETE = 11;
    const STATUS_EDIT = 6;
    const STATUS_INACTIVE = 5;
    const STATUS_DELETED = -1;

    const STATUS_YES = 10;
    const STATUS_NO = 0;

    /** PRODUCTS DEFAULT STATUS */

    const STATUS_BADGE = [
        self::STATUS_ACTIVE => 'label bg-blue flat',
        self::STATUS_COMPLETE => 'label bg-orange',
        self::STATUS_EDIT => 'label bg-primary',
        self::STATUS_INACTIVE => 'label bg-navy',
        self::STATUS_DELETED => 'label bg-red',
    ];
    const STATUS_DROP = [
        self::STATUS_EDIT => 'Обработанно',
        self::STATUS_COMPLETE => 'Завершено',
        self::STATUS_ACTIVE => 'В процессе',
        self::STATUS_INACTIVE => 'Не активно',
        self::STATUS_DELETED => 'Удаленно',
    ];

    const STATUS_SHOW = 10;
    const STATUS_DEFAULT_LIST = [
        self::STATUS_ACTIVE => 'Активно',
        self::STATUS_EDIT => 'Редактируется',
        self::STATUS_INACTIVE => 'Не активно',
        self::STATUS_DELETED => 'Удалить',
    ];
    const STATUS_PAY = 11;
    const STATUS_ORDER_USER__LIST = [
        self::STATUS_PAY => 'Оплаченно',
        self::STATUS_ACTIVE => 'В процессе',
        self::STATUS_EDIT => 'Не оплаченно',
        self::STATUS_INACTIVE => 'Что то еще',
        self::STATUS_DELETED => 'Не хочет',
    ];

    const STATUS_SIMPLE_LIST = [
        self::STATUS_ACTIVE => 'Активно',
        self::STATUS_INACTIVE => 'Не активно',
    ];



    /** PRODUCTS STATUS */
    const STATUS_DROP_ACTIVE = 5;
    const STATUS_DROP_INACTIVE = 10;
    const STATUS_DROP_DELETED = -1;

    const STATUS_PRODUCT_ACTIVE = 10;
    const STATUS_PRODUCT_INACTIVE = 5;
    const STATUS_PRODUCT_DELETED = -1;

    const STATUS_PRODUCT_DROP_ACTIVE = 10;
    const STATUS_PRODUCT_DROP_INACTIVE = 5;
    const STATUS_PRODUCT_DROP_DELETED = -1;

    const STATUS_PRODUCT_DROP_LIST = [
        self::STATUS_PRODUCT_DROP_ACTIVE => 'Активно',
        self::STATUS_PRODUCT_DROP_INACTIVE => 'Не активно',
        self::STATUS_PRODUCT_DROP_DELETED => 'Удаленно',
    ];


    /** PRODUCTS STATUS END */

    const BOOLEAN_LIST = array(
        'value'=> self::STATUS_YES,
        'uncheckValue'=> self::STATUS_NO
    );
    /** STATUS USER  */
    const STATUS_USER_DROP_ACTIVE = 5;
    const STATUS_USER_DROP_WAIT = 10;
    const STATUS_USER_DROP_INACTIVE = 3;
    const STATUS_USER_DROP_DELETED = -1;



    const STATUS_USER_DROPSHIP_WAIT = 5;

    const STATUS_ROLE_GUEST         = "guest";
    const STATUS_ROLE_CLIENT        = "client";
    const STATUS_ROLE_DROP          = "drop";
    const STATUS_ROLE_OPT           = "opt";
    const STATUS_ROLE_STUDENT       = "student";
    const STATUS_ROLE_MANAGER       = "manager";
    const STATUS_ROLE_ADMIN         = "admin";
    const STATUS_ROLE_SUPERMANAGER  = "supermanager";
    const STATUS_ROLE_SUPERADMIN    = "superadmin";


    const STATUS_ROLE_LIST = [
        self::STATUS_ROLE_GUEST         => "Гость",
        self::STATUS_ROLE_CLIENT        => "Клиент",
        self::STATUS_ROLE_DROP          => "Дропшипер",
        self::STATUS_ROLE_OPT           => "Оптовик",
        self::STATUS_ROLE_STUDENT       => "Студент",
        self::STATUS_ROLE_MANAGER       => "Менеджер",
        self::STATUS_ROLE_ADMIN         => "Админ",
        self::STATUS_ROLE_SUPERMANAGER  => "Супер-Менеджер",
        self::STATUS_ROLE_SUPERADMIN    => "СуперАдмин",
    ];



    const STATUS_MODERATE = [
        self::STATUS_USER_DROP_WAIT => 'На модерации',
        self::STATUS_USER_DROP_INACTIVE => 'Не активно',
        self::STATUS_USER_DROP_ACTIVE => 'Активен',
        self::STATUS_USER_DROP_DELETED => 'Удалить',
    ];



    const STATUS_BADGE_USER = [
        self::STATUS_USER_DROP_ACTIVE => 'label bg-blue flat',
        self::STATUS_USER_DROP_WAIT => 'label bg-success',
        self::STATUS_EDIT => 'label bg-primary',
        self::STATUS_USER_DROP_WAIT => 'label bg-red',
        self::STATUS_USER_DROP_DELETED => 'label bg-red',
    ];



    const STATUS_MESSAGES_CONSULT = 33;
    const STATUS_MESSAGES_PRODUCT = 44;
    const STATUS_MESSAGES_SENDUS = 55;
    const STATUS_MESSAGES_LIST = [
        self::STATUS_MESSAGES_CONSULT => 'Консультация',
        self::STATUS_MESSAGES_PRODUCT  => 'Предложить продукт',
        self::STATUS_MESSAGES_SENDUS  => 'Напишите нам',
    ];

    const STATUS_LESSON_ACTIVE = 10;
    const STATUS_LESSON_INACTIVE = 5;
    const STATUS_LESSON_EDIT = 6;
    const STATUS_LESSON_LIST = [
        self::STATUS_LESSON_INACTIVE => 'Не активно',
        self::STATUS_LESSON_ACTIVE => 'Активен',
        self::STATUS_LESSON_EDIT => 'Удалить',
    ];


    const STATUS_HOMEWORK_NEW = 5;
    const STATUS_HOMEWORK_READ = 10;


    const STATUS_LIST_DROP_ORDERS = [
        'dv' =>         'Выплачен',
        'provierien' => 'Ожидает выплаты',
        'otpravlen' =>  'Отправленно клиенту',
        'novyi' =>      'Новый',
        'vozvrat' =>    'Возврат',
        'oformlen' =>   'Принят',
        'spam' =>       'СПАМ',
        'v-procc' =>    'В процессе',
    ];


    const STATUS_LIST_DROP_ORDERS_BADGE = [
        'dv' =>            'label bg-blue flat',
        'provierien' =>    'label bg-orange',
        'otpravlen' =>      'label bg-primary',
        'novyi' =>         'label bg-navy',
        'vozvrat' =>       'label bg-red',
        'oformlen' =>   'label bg-primary',
        'v-procc' =>    'label bg-primary',
        'any' =>    'label bg-primary',
        'spam' =>    'label bg-red',


    ];


    public static function getSelectList($class, $from = 'id', $to = 'name')
    {
        return ArrayHelper::map($class::find()->all(), $from, $to);
    }


    public static function getDropProducts($from = 'id', $to = 'xml_id')
    {
        return ArrayHelper::map(
            Products::find()
                ->where(['status_drop' => self::STATUS_PRODUCT_DROP_ACTIVE])
                ->orderBy('name')
                ->asArray()
                ->all(),
            $from, $to);
    }
    public static function getDropCategories($from = 'id', $to = 'xml_id')
    {
        return ArrayHelper::map(
            ProductsCategories::find()
                ->where(['status_drop' => self::STATUS_PRODUCT_DROP_ACTIVE])
                ->orderBy('name')
                ->asArray()
                ->all(),
            $from, $to);
    }

    public static function getDropCategoriesParent($from = 'id', $to = 'xml_id',$prnt)
    {
        return ArrayHelper::map(
            ProductsCategories::find()
                ->where(['status_drop' => self::STATUS_PRODUCT_DROP_ACTIVE])
                ->where( ['parent_id' => $prnt])
                ->orderBy('name')
                ->asArray()
                ->all(),
            $from, $to);
    }

    public static function getModelByName($class, $name)
    {
        $model = $class::findOne(['name' => $name]);
        if (isset($model)) {
            return $model->id;
        }
        return 1;
    }


    public static function setArrayToString($array, $in = 'id', $to = 'name')
    {
        return implode(',', ArrayHelper::map($array, $in, $to));
    }

    //метод для автоматического определения глубины категории, основывается на depth родительской категории
    protected function setDepth()
    {
        if ($this->parent_id != 0) {
            $this->depth = self::findOne($this->parent_id)->depth + 1;
        } else {
            $this->depth = 0;
        }
    }

    //метод для автоматического определения урла категории, основывается на урл родительской категории и алиас категории
    protected function setPath()
    {
        //todo сделать норм
        if ($this->parent_id == 0 || $this->parent_id == 1) {
            $this->path = $this->alias;
        } else {
            $this->path = self::findOne($this->parent_id)->path . '/' . $this->alias;
        }
    }

    public function trash(){
        $this->status = MainModel::STATUS_DELETED;
        return $this->save();
    }

}