<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.08.2018
 * Time: 11:04
 */

namespace common\models\dropship;

use common\models\crm\ShopCrm;
use common\models\retail_crm\RetailCrm;
use Yii;
use common\helpers\myHellpers;
use common\models\delivery\DeliveryType;
use common\models\MainModel;
use common\models\products\Products;
use common\models\user\User;
use yii\base\Model;

class SendDropOrder extends Model
{
    private $data;
    private $user;
    private $order;
    private $client;
    private $deliver_type;

    public function __construct($user,  $data = [], array $config = []){
        $this->user = $user;
        $this->data = $data;
        parent::__construct($config);
    }

    public function sendToCrmFromUser(){
        foreach ($this->data as $item){
            if(!empty($item)){
                $this->addCrm($item);
            }else{
                continue;
            }
        }
    }

    public function send($items){
        $this->addCrm($items);
        $this->addNewCrm($items);
    }

    public function addCrm($item){


        $crm = $this->getClientData($item);
        $crm['items'] = self::getItemProducts($item->id);
        $crm['delivery'] = self::getDeliveryData($item->id);
        $send = new RetailCrm();
        if($crm_id = $send->addOrder($crm)){
            self::updateOrder($item->id, $crm_id);
        }
    }

    public function addNewCrm($item){
        $crm = $this->getClientData($item);
        $crm['items'] = self::getItemNewProducts($item->id);
        $crm['delivery'] = self::getDeliveryData($item->id);
        $send = new ShopCrm();
        $send->send($crm);

        if($crm_id = $send->addOrder($crm)){
            self::updateOrder($item->id, $crm_id);
        }
    }


    protected  function getClientData($data){
        $dropman = $this->user->crm_name;
        $client = self::getUserById($data->client_id);

        $_item = [];
        $_item['firstName'] = $client->name;
        $_item['phone'] = $client->phone;
        $_item['dropshipper'] = $this->user->crm_name_new;
        $_item['status'] = 'confirmdrop';
        $_item['customFields']['second_manager'] = $dropman;
        $_item['customFields']["landing"] = "Заявка с FRP - кабинер дропшипера";
        $_item['customerComment'] = $data->comment;
        return $_item;
    }



    protected static function getDeliveryData($id){
        $delivery = self::getDelivery($id);
        $_item = [];
        if($delivery){
            $d =  self::getDeliverType($delivery->delivery_type_id);
            $_item = [];
            $_item['code'] = $d->alias;
            $_item['address']['region'] = $delivery->region;
            $_item['address']['city'] = $delivery->city;
            $_item['address']['metro'] = $delivery->subway;
            $_item['address']['street'] = $delivery->street;
            $_item['address']['index'] = $delivery->zipcode;
            $_item['address']['building'] = $delivery->building;
            $_item['address']['flat'] = $delivery->flat;
            $_item['address']['house'] = $delivery->house;
            $_item['address']['block'] = $delivery->block;
            $_item['address']['floor'] = $delivery->floor;
            $_item['address']['notes'] = $delivery->note;
            $_item['cost'] = $delivery->cost;

        }
        return $_item;
    }

    protected static function getItemProducts($id){
        $_item = [];
        $products = self::getProducts($id);
        if(!empty($products)){
            $i = 0;
            foreach ($products as $product){
                $_item[$i]['productName'] = $product->product_name;
                $_item[$i]['initialPrice'] = $product->price_new;
                $_item[$i]['quantity'] = $product->quantity;
                $_item[$i]['offer']['xmlId'] = $product->product->xml_id;
                $_item[$i]['offer']['externalId'] = $product->product->xml_id;
                $i++;
            }
            return $_item;
        }
        return $_item;
    }


    protected static function getItemNewProducts($id){
        $_item = [];
        $products = self::getProducts($id);
        if(!empty($products)){
            $i = 0;
            foreach ($products as $product){
                $_item[$i]['productName'] = $product->product_name;
                $_item[$i]['initialPrice'] = $product->price_new;
                $_item[$i]['quantity'] = $product->quantity;
                $_item[$i]['externalId'] = $product->product->xml_id;
                $i++;
            }
            return $_item;
        }
        return $_item;
    }




    protected static function updateOrder($id, $crm_id){
        if($o = self::getOrder($id)){
            $o->crm_id = $crm_id;
            return $o->save();
        }
        return false;
    }
    protected static function getUserOrders($id){
        return DropOrder::find()->where(['user_id'=>$id, 'status'=>MainModel::STATUS_DROP_INACTIVE])->all();
    }

    protected static function getOrder($id){
        return DropOrder::find()->where(['id'=>$id])->one();
    }

    protected static function getDelivery($id){
        return DropDeliver::find()->where(['drop_order_id'=>$id])->one();
    }

    protected static function getProducts($id){
        return DropOrderProduct::find()->where(['drop_order_id'=>$id])->all();
    }

    protected static function checkProduct($product){
        return Products::findProductByIdXmlDrop($product['id'],$product['xid']);
    }
    protected static function getDeliverType($id){
        $d = DeliveryType::find()->where(['id'=>$id])->one();
        return ($d) ? $d : 1;
    }

    protected static function getUserById($id){
        if($u = User::find()->where(['id'=>$id])->one()){
            return $u;
        }
        return null;
    }

    protected function getUser($phone){
        if($u = User::findByPhone($phone)){
            return $u;
        }
        return null;
    }

}