<?php

namespace common\models\dropship;

use Yii;

/**
 * This is the model class for table "dropship".
 *
 * @property int $id
 */
class Dropship extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dropship';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return DropshipQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DropshipQuery(get_called_class());
    }
}
