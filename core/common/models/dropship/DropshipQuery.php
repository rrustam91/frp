<?php

namespace common\models\dropship;

/**
 * This is the ActiveQuery class for [[Dropship]].
 *
 * @see Dropship
 */
class DropshipQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Dropship[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Dropship|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
