<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 05.08.2018
 * Time: 22:01
 */

namespace common\models\dropship;

use common\helpers\myHellpers;
use common\models\delivery\DeliveryType;
use common\models\MainModel;
use common\models\products\Products;
use common\models\retail_crm\RetailCrm;
use common\models\user\User;
use Yii;
use yii\base\Model;

class CreateDropOrder extends Model
{
    private $data;
    private $user;
    private $order;
    private $client;
    private $deliver_type;
    private $delivers;

    private $delivery_price;
    private $delivery_start;
    private $commission_products;
    private $commission_full;

    public function __construct($user, $data = [], array $config = []){
        $this->data = $data;
        $this->user = $user;
        parent::__construct($config);
    }

    public function createOrder(){
        $this->order = $this->addToDb();
        return true;
    }

    protected function addToDb(){
        $this->client = $this->addClient();
        $this->order = $this->addDropOrder();
        $products = $this->addProductItem();
        $delivers = $this->addDeliver();

        if($products){ self::updateOrder($this->order->id, $products, $delivers); }


        if(Yii::$app->user->getIdentity()->status_drop == MainModel::STATUS_DROP_ACTIVE){
            $crm = new SendDropOrder(Yii::$app->user->getIdentity());
            $crm->send(self::getUserOrder($this->order->id, Yii::$app->user->id));
        }
        return true;

    }

    protected static function updateOrder($id, $prices, $delivers){
        $o = self::getUserOrder($id, Yii::$app->user->id);
        $o->price_total = $prices['price_total'];
        $o->price_new_total = $prices['price_total_new'];
        $o->commission_products = (($prices['price_total_new']+$delivers)*MainModel::COM_FIZ) - $prices['price_buy_drop'];
        $o->commission_full = (($prices['price_total_new']+$delivers)*MainModel::COM_FIZ) - $prices['price_buy_drop'];
        $o->delivery_price = $delivers;
        $o->delivery_start = $delivers;
        $o->status_crm = 'noviy';
        $o->status_crm_drop = 'noviy';
        return $o->update();
    }



    protected function addToCrm(){
        $retailCrm = Yii::$app->retailCrm;
        $crm = $this->getDataToCrm();
        $send = $retailCrm->client->request->ordersCreate([
            'firstName' =>  $crm['firstName'],
            'phone' => $crm['phone'],
            "customFields"=> [
                "second_manager" => Yii::$app->user->getIdentity()->crm_name,
                "landing" => "Заявка с FRP - кабинер дропшипера"
            ],
            "customerComment" => $crm['customerComment'],
            "delivery" => $crm['delivery'],
            'items' => $crm['items']
        ],'dropshop');

        $send = true;
        return $send;

    }


    protected function getDataToCrm(){
        $crm = $this->getClientData();
        $crm['items'] = $this->getItemProducts();
        $crm['delivery'] = $this->getDeliveryData();
        return $crm;
    }

    protected function getClientData(){
        $dropman = Yii::$app->user->getIdentity()->crm_name;
        $_item = [];
        $_item['firstName'] = $this->data['name'];
        $_item['phone'] = $this->data['phone'];
        $_item['status'] = 'confirmdrop';
        $_item['customFields']['second_manager'] = $dropman;
        $_item['customerComment'] = $this->data['comment'];
        return $_item;
    }



    protected function getDeliveryData(){
        $d =  self::getDeliverType($this->data['delivery_type_id']);
        $_item = [];
        $_item['code'] = $d->alias;
        $_item['address']['region'] = $this->data['region'];
        $_item['address']['city'] = $this->data['city'];
        $_item['address']['metro'] = $this->data['subway'];
        $_item['address']['street'] = $this->data['street'];
        $_item['address']['index'] = $this->data['zipcode'];
        $_item['address']['building'] = $this->data['building'];
        $_item['address']['flat'] = $this->data['flat'];
        $_item['address']['house'] = $this->data['house'];
        $_item['address']['block'] = $this->data['block'];
        $_item['address']['floor'] = $this->data['floor'];
        $_item['address']['notes'] = $this->data['note'];
        $_item['cost'] = $this->data['cost'];

        return $_item;
    }

    protected function getItemProducts(){
        $_item = [];
        if(!empty($this->data['products_h'])){
            $i=0;
            foreach ($this->data['products_h'] as $products){
                if($_prd = self::checkProduct($products)){
                    $_item[$i]['productName'] = $_prd->name;
                    $_item[$i]['initialPrice'] = $products['ap_price'];
                    $_item[$i]['quantity'] = $products['qnt'];
                    $_item[$i]['productName'] = $_prd->name;
                    $_item[$i]['offer']['xmlId'] = $_prd->xml_id;
                    $_item[$i]['offer']['externalId'] = $_prd->xml_id;
                    $i++;
                }
                continue;
            }
        }
        return $_item;
    }

    protected function addClient(){
        if(!$user = User::findByPhone($this->data['phone'])){
            $user = new User();
            $user->name = $this->data['name'];
            $user->phone = $this->data['phone'];
            $user->roles = 'clients';
            $user->setRandomEmail(myHellpers::generateRandomWord().'@frp.ru');
            $user->setRandomUsername( myHellpers::generateRandomWord());
            $user->setPassword('xtgjxtv');
            $user->generateAuthKey();
            $user->save();
        }
        return $user->id;
    }
    protected function addDropOrder(){
        $drop_order = new DropOrder();
        $drop_order->crm_id = 0;
        $drop_order->user_id = $this->user->id;
        $drop_order->client_id = $this->client;
        $drop_order->comment = $this->data['comment'];


        if($drop_order->save()){
            return $drop_order;
        }
        myHellpers::show($drop_order->errors,'product');
    }
    protected function addProductItem(){

        $data['price_buy_drop']= 0;
        $data['price_buy_opt']= 0;
        $data['price_total']= 0;
        $data['price_total_new'] = 0;
        $data['commission_products'] = 0;
        if(!empty($this->data['products_h'])){
            $i = 0;
            foreach ($this->data['products_h'] as $products){
                if($_prd = self::checkProduct($products)){

                    $drop_product = new DropOrderProduct();
                    $drop_product->drop_order_id = $this->order->id;
                    $drop_product->product_id = $_prd->id;
                    $drop_product->product_name = $_prd->name;
                    $drop_product->price = $_prd->price;
                    $drop_product->price_new = $products['ap_price'];
                    $drop_product->quantity = $products['qnt'];


                    $drop_product->price_total = $_prd->price * $products['qnt'];
                    $drop_product->price_new_total = $products['ap_price'] * $products['qnt'];


                    $data['price_buy_drop'] += $_prd->price_drop * $products['qnt'];
                    $data['price_buy_opt'] += $_prd->price_opt * $products['qnt'];
                    $data['price_total_new'] += $drop_product->price_new_total;
                    $data['price_total'] += $drop_product->price_total;
                    if(!$drop_product->save()){
                        myHellpers::show($drop_product->errors,'product');
                    }
                }
                continue;
            }
            return $data;
        }
    }
    protected function addDeliver(){

        $dtype =  self::getDeliverType($this->data['delivery_type_id']);
        $deliver = new DropDeliver();
        $deliver->drop_order_id = $this->order->id;
        $deliver->delivery_type_id = $dtype->id;
        $deliver->delivery_type_code = $dtype->alias;
        $deliver->region = $this->data['region'];
        $deliver->city = $this->data['city'];
        $deliver->subway = $this->data['subway'];
        $deliver->street = $this->data['street'];
        $deliver->zipcode = $this->data['zipcode'];
        $deliver->building = $this->data['building'];
        $deliver->flat = $this->data['flat'];
        $deliver->block = $this->data['block'];
        $deliver->house = $this->data['house'];
        $deliver->floor = $this->data['floor'];
        $deliver->note = $this->data['note'];
        $deliver->cost = $this->data['cost'];
        $this->delivery_start = $this->data['cost'];
        $this->delivery_price = $this->data['cost'];
        $deliver->save();

        return ($deliver->save()) ? $deliver->cost : myHellpers::show($deliver->errors, 'deliver');



    }

    protected static function checkProduct($product){
        return Products::findProductByIdXmlDrop($product['id'],$product['xid']);
    }
    protected static function getDeliverType($id){
        $d = DeliveryType::find()->where(['id'=>$id, 'status'=>MainModel::STATUS_TYPE_ACTIVE])->one();
        return ($d) ? $d : 1;
    }
    protected function getUser($phone){
        if($u = User::findByPhone($phone)){
            return $u;
        }
        return null;
    }


    protected static function getUserOrder($order_id, $user_id){
        return DropOrder::find()->where(['id'=>$order_id,'user_id'=>$user_id, 'status'=>MainModel::STATUS_DROP_INACTIVE])->one();
    }






}