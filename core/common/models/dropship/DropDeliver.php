<?php

namespace common\models\dropship;

use common\models\delivery\DeliveryType;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "drop_deliver".
 *
 * @property int $id
 * @property int $status
 * @property int $drop_order_id
 * @property int $delivery_type_id
 * @property string $delivery_type_code
 * @property string $region
 * @property string $city
 * @property string $subway
 * @property string $street
 * @property string $zipcode
 * @property string $building
 * @property string $block
 * @property string $house
 * @property string $flat
 * @property string $float
 * @property string $floor
 * @property string $note
 * @property int $cost
 * @property string $delivery_price
 * @property int $created_at
 * @property int $updated_at
 *
 * @property DeliveryType $deliveryType
 * @property DropOrder $dropOrder
 */
class DropDeliver extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'drop_deliver';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'drop_order_id', 'delivery_type_id', 'cost', 'created_at', 'updated_at'], 'integer'],
            [['drop_order_id', 'delivery_type_code'], 'required'],
            [['delivery_type_code', 'region', 'city', 'subway', 'street', 'zipcode', 'building', 'block', 'house', 'flat', 'float', 'floor', 'note', 'delivery_price'], 'string', 'max' => 255],
            [['delivery_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DeliveryType::className(), 'targetAttribute' => ['delivery_type_id' => 'id']],
            [['drop_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => DropOrder::className(), 'targetAttribute' => ['drop_order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Status'),
            'drop_order_id' => Yii::t('app', 'Drop Order ID'),
            'delivery_type_id' => Yii::t('app', 'Delivery Type ID'),
            'delivery_type_code' => Yii::t('app', 'Delivery Type Code'),
            'region' => Yii::t('app', 'Region'),
            'city' => Yii::t('app', 'City'),
            'subway' => Yii::t('app', 'Subway'),
            'street' => Yii::t('app', 'Street'),
            'zipcode' => Yii::t('app', 'Zipcode'),
            'building' => Yii::t('app', 'Building'),
            'block' => Yii::t('app', 'Block'),
            'house' => Yii::t('app', 'House'),
            'flat' => Yii::t('app', 'Flat'),
            'float' => Yii::t('app', 'Float'),
            'floor' => Yii::t('app', 'Floor'),
            'note' => Yii::t('app', 'Note'),
            'cost' => Yii::t('app', 'Cost'),
            'delivery_price' => Yii::t('app', 'Delivery Price'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(DeliveryType::className(), ['id' => 'delivery_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDropOrder()
    {
        return $this->hasOne(DropOrder::className(), ['id' => 'drop_order_id']);
    }

    /**
     * {@inheritdoc}
     * @return DropshipQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DropshipQuery(get_called_class());
    }
}
