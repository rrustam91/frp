<?php

namespace common\models\dropship;

use common\helpers\myHellpers;
use common\models\MainModel;
use common\models\products\Products;
use common\models\user\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "drop_order".
 *
 * @property int $id
 * @property int $user_id
 * @property int $client_id
 * @property int $crm_id
 * @property string $products
 * @property string $comment
 * @property string $comment_manager
 * @property string $track_number
 * @property int $status
 * @property string $status_crm
 * @property string $status_crm_drop
 * @property int $price
 * @property int $price_total
 * @property int $price_new
 * @property int $price_new_total
 * @property double $commission_products
 * @property int $delivery_price
 * @property int $delivery_start
 * @property double $commission_full
 * @property int $created_at
 * @property int $updated_at
 *
 * @property DropDeliver[] $dropDelivers
 * @property User $client
 * @property User $user
 * @property DropOrderProduct[] $dropOrderProducts
 */
class DropOrder extends MainModel
{
 /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'drop_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'client_id'], 'required'],
            [['user_id', 'client_id', 'crm_id', 'status', 'price', 'price_total', 'price_new', 'price_new_total', 'delivery_price', 'delivery_start', 'created_at', 'updated_at'], 'integer'],
            [['products', 'comment', 'comment_manager'], 'string'],
            [['commission_products', 'commission_full'], 'number'],
            [['track_number', 'status_crm', 'status_crm_drop'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'client_id' => Yii::t('app', 'Client ID'),
            'crm_id' => Yii::t('app', 'Crm ID'),
            'products' => Yii::t('app', 'Products'),
            'comment' => Yii::t('app', 'Comment'),
            'comment_manager' => Yii::t('app', 'Comment Manager'),
            'track_number' => Yii::t('app', 'Track Number'),
            'status' => Yii::t('app', 'Status'),
            'status_crm' => Yii::t('app', 'Status Crm'),
            'status_crm_drop' => Yii::t('app', 'Status Crm Drop'),
            'price' => Yii::t('app', 'Price'),
            'price_total' => Yii::t('app', 'Price Total'),
            'price_new' => Yii::t('app', 'Price New'),
            'price_new_total' => Yii::t('app', 'Price New Total'),
            'commission_products' => Yii::t('app', 'Commission Products'),
            'delivery_price' => Yii::t('app', 'Delivery Price'),
            'delivery_start' => Yii::t('app', 'Delivery Start'),
            'commission_full' => Yii::t('app', 'Commission Full'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(DropDeliver::className(), ['drop_order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(User::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDropOrderProducts()
    {
        return $this->hasMany(DropOrderProduct::className(), ['drop_order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(DropOrderProduct::className(), ['drop_order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasMany(DropOrderProduct::className(), ['drop_order_id' => 'id'])->with('product');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusCrmDrop()
    {
        return $this->hasOne(OrdersStatus::className(), ['code'=>'status_crm_drop']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusCrm()
    {
        return $this->hasOne(OrdersStatus::className(), ['code'=>'status_crm']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['id' => 'product_id'])
            ->via('list');
    }

    public function getCommission(){
        $_pc = $this->commission_products;
        $_dp = $this->delivery_price;
        $_sum = $_pc - $_dp;
        return $_sum;
    }

    public function updateCommission(){
        $_pc = $this->commission_products;
        $_dp = $this->delivery_price;
        $_sum = $_pc - $_dp;
        return $_sum;
    }

    public static function getUserCountOrder($id){
        return self::find()->where(['user_id'=>$id])->asArray()->count();
    }


    public static function getUserOrdersWait($id){
        return self::find()->where(['user_id'=>$id, 'status'=>self::STATUS_DROP_INACTIVE])->all();
    }
}
