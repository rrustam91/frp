<?php

namespace common\models\dropship\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\dropship\DropDeliver;

/**
 * DropDeliverSearch represents the model behind the search form of `common\models\dropship\DropDeliver`.
 */
class DropDeliverSearch extends DropDeliver
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'drop_order_id', 'delivery_type_id', 'cost', 'created_at', 'updated_at'], 'integer'],
            [['delivery_type_code', 'region', 'city', 'subway', 'street', 'zipcode', 'building', 'block', 'house', 'floor', 'note', 'delivery_price'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DropDeliver::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'drop_order_id' => $this->drop_order_id,
            'delivery_type_id' => $this->delivery_type_id,
            'cost' => $this->cost,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'delivery_type_code', $this->delivery_type_code])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'subway', $this->subway])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'zipcode', $this->zipcode])
            ->andFilterWhere(['like', 'building', $this->building])
            ->andFilterWhere(['like', 'block', $this->block])
            ->andFilterWhere(['like', 'house', $this->house])
            ->andFilterWhere(['like', 'floor', $this->floor])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'delivery_price', $this->delivery_price]);

        return $dataProvider;
    }
}
