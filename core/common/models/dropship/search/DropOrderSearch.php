<?php

namespace common\models\dropship\search;

use common\helpers\myHellpers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\dropship\DropOrder;

/**
 * DropOrderSearch represents the model behind the search form of `common\models\dropship\DropOrder`.
 */
class DropOrderSearch extends DropOrder
{
    /**
     * @inheritdoc
     */
    public $quantity;
    public $status_crm_drop;
    public $total_price;
    public function rules()
    {
        return [
            [['id', 'user_id', 'client_id', 'crm_id', 'quantity', 'commission_full', 'status',  'updated_at'], 'integer'],
            [['status_crm_drop'],'string'],
            [['products', 'comment','created_at','total_price'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {

        $u = Yii::$app->user;
        $query = DropOrder::find();


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $u->id,
            'status' => $this->status,
            'price' => $this->price,
            'price_total' => $this->price_total,
            'price_new' => $this->price_new,
            'price_new_total' => $this->price_new_total,
        ]);
        //myHellpers::show($this->created_at);
        if($this->created_at){
            $filter_time = explode(' / ' ,$this->created_at );
            $date_start = strtotime($filter_time[0]);
            $date_end = strtotime($filter_time[1]);
            $query->andFilterWhere(['between', 'created_at', $date_start, $date_end]);

            //myHellpers::show(strtotime($filter_time[1]));

            /*$date =  new \DateTime($this->created_at);
            $start = $date->getTimestamp();
            //константа SECONDS_IN_DAY содержит в себе 86400 и объявлена заранее
            $query->andFilterWhere(['between', 'created_at', $start, $start + self::SECONDS_IN_DAY]);*/
        }
        $query->andFilterWhere(['like', 'products', $this->products])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'crm_id', $this->crm_id])
            ->andFilterWhere(['like', 'status_crm_drop', $this->status_crm_drop]);
        $query->with('user','client','products','statusCrmDrop', 'statusCrm');

        if(!$u->can('manager')){
            $query->andWhere(['user_id'=>$u->id]);
        }

        $query->orderBy(['created_at'=>SORT_DESC]);
        return $dataProvider;
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search1($params)
    {
        $u = Yii::$app->user;
        $query = DropOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $u->user_id,
            'client_id' => $this->client_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        myHellpers::show($this);
        $query->andFilterWhere(['like', 'products', $this->products])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'crm_id', $this->crm_id]);


        if(!$u->can('manager')){
            $query->andWhere(['user_id'=>$u->id]);
        }
        $query->all();
        $query->with('user','client','products','statusCrmDrop', 'statusCrm');
        $query->orderBy(['created_at'=>SORT_DESC]);
        return $dataProvider;
    }
}
