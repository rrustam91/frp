<?php

namespace common\models\dropship\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\products\ProductsCategories;

/**
 * ProductsCategoriesSearch represents the model behind the search form of `common\models\products\ProductsCategories`.
 */
class ProductsCategoriesSearch extends ProductsCategories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'alias', 'content', 'xml_id', 'xml_parent_id', 'xml_name', 'seo_keywords', 'seo_description', 'img'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductsCategories::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            //->andFilterWhere(['like', 'alias', $this->alias])
            //->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'xml_id', $this->xml_id])
            ->andFilterWhere(['like', 'xml_parent_id', $this->xml_parent_id])
            ->andFilterWhere(['like', 'xml_name', $this->xml_name])
            ->andFilterWhere(['like', 'seo_keywords', $this->seo_keywords])
            ->andFilterWhere(['like', 'seo_description', $this->seo_description])
            ->andFilterWhere(['like', 'img', $this->img]);
        $query->with('products','parent');
        return $dataProvider;
    }
}
