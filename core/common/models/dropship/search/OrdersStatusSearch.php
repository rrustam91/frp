<?php

namespace common\models\dropship\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\dropship\OrdersStatus;

/**
 * OrdersStatusSearch represents the model behind the search form of `common\models\dropship\OrdersStatus`.
 */
class OrdersStatusSearch extends OrdersStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group', 'status_drop', 'active', 'created_at', 'updated_at'], 'integer'],
            [['alias', 'name', 'code', 'drop_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdersStatus::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'group' => $this->group,
            'status_drop' => $this->status_drop,
            'active' => $this->active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'drop_name', $this->drop_name]);

        return $dataProvider;
    }
}
