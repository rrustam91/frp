<?php

namespace common\models\dropship;

use common\models\MainModel;
use Yii;

/**
 * This is the model class for table "orders_status_group".
 *
 * @property int $id
 * @property string $alias
 * @property string $name
 * @property string $code
 * @property int $active
 * @property int $created_at
 * @property int $updated_at
 *
 * @property OrdersStatus[] $ordersStatuses
 */
class OrdersStatusGroup extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_status_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias', 'name', 'code'], 'required'],
            [['active', 'created_at', 'updated_at'], 'integer'],
            [['alias', 'name', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'alias' => Yii::t('app', 'Alias'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'active' => Yii::t('app', 'Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersStatuses()
    {
        return $this->hasMany(OrdersStatus::className(), ['group' => 'id']);
    }
}
