<?php

namespace common\models\dropship;

use common\models\MainModel;
use common\models\products\Products;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "drop_order_product".
 *
 * @property int $id
 * @property int $drop_order_id
 * @property int $product_id
 * @property string $product_name
 * @property int $price
 * @property int $price_new
 * @property int $price_total
 * @property int $price_new_total
 * @property int $price_commission
 * @property int $price_commission_total
 * @property int $quantity
 * @property int $status
 * @property int $status_append
 * @property int $created_at
 * @property int $updated_at
 *
 * @property DropOrder $dropOrder
 * @property Products $product
 */
class DropOrderProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'drop_order_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['drop_order_id', 'product_id',], 'required'],
            [['drop_order_id', 'product_id', 'price', 'price_new', 'price_total', 'price_new_total', 'price_commission', 'price_commission_total', 'quantity', 'status', 'status_append', 'created_at', 'updated_at'], 'integer'],
            [['product_name'], 'string', 'max' => 255],
            [['drop_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => DropOrder::className(), 'targetAttribute' => ['drop_order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'drop_order_id' => Yii::t('app', 'Drop Order ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'product_name' => Yii::t('app', 'Product Name'),
            'price' => Yii::t('app', 'Price'),
            'price_new' => Yii::t('app', 'Price New'),
            'price_total' => Yii::t('app', 'Price Total'),
            'price_new_total' => Yii::t('app', 'Price New Total'),
            'price_commission' => Yii::t('app', 'Price Commission'),
            'price_commission Total' => Yii::t('app', 'Price Commission Total'),
            'quantity' => Yii::t('app', 'Quantity'),
            'status' => Yii::t('app', 'Status'),
            'status_append' => Yii::t('app', 'Status Append'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDropOrder()
    {
        return $this->hasOne(DropOrder::className(), ['id' => 'drop_order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    /**
     * {@inheritdoc}
     * @return DropshipQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DropshipQuery(get_called_class());
    }
}
