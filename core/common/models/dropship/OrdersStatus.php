<?php

namespace common\models\dropship;

use common\models\MainModel;
use Yii;

/**
 * This is the model class for table "orders_status".
 *
 * @property int $id
 * @property int $group
 * @property string $alias
 * @property string $name
 * @property string $code
 * @property int $status_drop
 * @property string $drop_name
 * @property int $active
 * @property int $created_at
 * @property int $updated_at
 *
 * @property OrdersStatusGroup $group0
 */
class OrdersStatus extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group', 'status_drop', 'active', 'created_at', 'updated_at'], 'integer'],
            [['alias', 'name', 'code'], 'required'],
            [['alias', 'name', 'code', 'drop_name'], 'string', 'max' => 255],
            [['group'], 'exist', 'skipOnError' => true, 'targetClass' => OrdersStatusGroup::className(), 'targetAttribute' => ['group' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'group' => Yii::t('app', 'Group'),
            'alias' => Yii::t('app', 'Alias'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'status_drop' => Yii::t('app', 'Status Drop'),
            'drop_name' => Yii::t('app', 'Drop Name'),
            'active' => Yii::t('app', 'Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup0()
    {
        return $this->hasOne(OrdersStatusGroup::className(), ['id' => 'group']);
    }
}
