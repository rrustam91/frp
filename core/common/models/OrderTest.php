<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.09.2018
 * Time: 10:29
 */

namespace common\models;


use yii\base\Model;

class OrderTest extends Model
{

    public function getTotalPrice(){
        return 120;
    }

    public function getId(){
        return 1;
    }

    public function setInvoiceId($invoiceId) {
        $this->invoice_id = $invoiceId;
    }


    public function getInvoiceId() {
        return $this->invoice_id;
    }


    public function getPaymentAmount() {
        return $this->amount;
    }


    public function findByInvoiceId($invoiceId) {
        return self::find()->where(['invoice_id' => $invoiceId]);
    }

    public function findById($id) {
        return self::findOne($id);
    }

}