<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.09.2018
 * Time: 13:01
 */

namespace common\models\crm;


use common\helpers\myHellpers;
use yii\base\Model;
use Yii;

class ShopCrm extends Model
{

    public $d;
    const SHOP_KEY = 'wrLPNPcK8TY7wnEfqmwxGo7QzKoH1k5e';

    public function __construct(array $config = [])
    {

        parent::__construct($config);
    }
    public function send($crm){
        $data['apiKey'] = 'AAE9B9AMIupsIICV33xe0skXvDD0gGQU5Kg';
        $data =[
            'apiKey'=>'AAE9B9AMIupsIICV33xe0skXvDD0gGQU5Kg',
            'name' =>$crm['firstName'],

            'dropshipper' => 'alex_drop-3453',
            'phone' => $crm['phone'],
            'items'=>serialize($crm['items']),
            'customFields'=> [
                'landing'=>'Заказ от дропера Fank Retail Partners',
            ],
            'shop' => 'drop',

        ]; 
        $result = $this->addOrder($data);

        return $result;

    }
    public static function callMethod($data, $method='dropshippers/create'){
        $domain = 'https://9-market.ru';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $domain . '/v1/api/'.$method.'?apiKey='.self::SHOP_KEY);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        $result = json_decode(curl_exec($curl), false);
        curl_close($curl);
        if($result->error){
            return $result->error;
        }
        return $result;
    }



    protected function sendToCrm($method='dropshippers/create',$data){
        $domain = 'https://9-market.ru';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $domain . '/v1/api/'.$method.'?apiKey='.self::SHOP_KEY);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        $result = json_decode(curl_exec($curl), false);
        curl_close($curl);
        return $result;
    }

    public function addOrder($data){
        $domain = 'https://9-market.ru';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $domain . '/v1/api/orders/create?apiKey='.$data['apiKey']);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        $result = json_decode(curl_exec($curl), false);
        curl_close($curl);
        return $result;
    }

}