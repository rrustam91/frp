<?php

namespace common\models\delivery;

/**
 * This is the ActiveQuery class for [[DeliveryType]].
 *
 * @see DeliveryType
 */
class DeliveryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return DeliveryType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return DeliveryType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
