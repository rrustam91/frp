<?php

namespace common\models\delivery;

use common\components\AliasComponent;
use common\models\dropship\DropDeliver;
use common\models\MainModel;
use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "delivery_type".
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property int $delivery_price
 * @property string $status
 * @property string $status_opt
 * @property string $status_drop
 * @property int $created_at
 * @property int $updated_at
 *
 * @property DropDeliver[] $dropDelivers
 */
class DeliveryType extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'alias',], 'required'],
            [['delivery_price', 'created_at', 'updated_at'], 'integer'],
            [['name', 'alias', 'status', 'status_opt', 'status_drop'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'alias' => Yii::t('app', 'Alias'),
            'delivery_price' => Yii::t('app', 'Delivery Price'),
            'status' => Yii::t('app', 'Status'),
            'status_opt' => Yii::t('app', 'Status Opt'),
            'status_drop' => Yii::t('app', 'Status Drop'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            AliasComponent::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDropDelivers()
    {
        return $this->hasMany(DropDeliver::className(), ['delivery_type_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return DeliveryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DeliveryQuery(get_called_class());
    }
}
