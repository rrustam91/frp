<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.08.2018
 * Time: 10:06
 */

namespace common\models\forms;

use common\helpers\myHellpers;
use common\models\courses\Courses;
use common\models\courses\CoursesOrder;
use common\models\user\User;
use common\models\user\UserAuth;
use Yii;
use yii\base\Model;

class OrderCourseForm extends Model{


    public $name;
    public $email;
    public $phone;
    public $course;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone'], 'required'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
            [['course'], 'integer'],
            [['email'], 'unique'],
            [['email'], 'email'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Partner Email'),
            'name' => Yii::t('app', 'Partner Name'),
            'phone' => Yii::t('app', 'Partner Phone'),
        ];
    }

    public function addOrder(){
        $u = $this->getUser();

        $chats = [
            461704559,
            302980258,
            396497806,
        ];
        Yii::$app->telegram->sendMessage([
                'chat_id' => $chats[0],
                'text' => "Имя: {$u->name}\n Тел: {$u->phone}\n Емайл: {$u->email}\n, Крус: {$this->course} ",
            ]
        );


        $order = new CoursesOrder();
        $order->user_id = $u->id;
        $order->course_id = $this->course;
        $order->p_course_id = ($pid = Courses::getCourse($this->course)->parent_id) ? $pid  : '';
        $order->price = Courses::getCourse($this->course)->price;
        $order->status = CoursesOrder::STATUS_ACTIVE;

        if($order->save()){
            return $order;
        }

        return false;
    }

    private function getUser(){
        if(UserAuth::findByEmail($this->email)){
            $u = UserAuth::findByEmail($this->email);

            $roles = $u->roles;
            $u->roles =$roles.', Заполнил форму покупки курса '.Courses::getCourse($this->course)->name. '. Цена: '.Courses::getCourse($this->course)->price.' Руб.';
            $u->save();
        }else if(UserAuth::findByPhone($this->phone)){
            $u = UserAuth::findByPhone($this->phone);

            $roles = $u->roles;
            $u->roles = $roles.', Заполнил форму покупки курса '.Courses::getCourse($this->course)->name. '. Цена: '.Courses::getCourse($this->course)->price.' Руб.';
            $u->save();
        }else{
            $u = new UserAuth();
            $u->username = myHellpers::generateRandomWord();
            $u->name = $this->name;
            if(myHellpers::isEmail($this->email)){
                $u->email = $this->email;
            }else{
                $u->phone = $this->phone;
            }

            $roles = $u->roles;
            $u->roles = $roles.', Заполнил форму покупки курса '.Courses::getCourse($this->course)->name. '. Цена: '.Courses::getCourse($this->course)->price.' Руб.';
            $u->setPassword($u->username);
            $u->generateAuthKey();

            $u->save();
        }
        return $u;
    }

}