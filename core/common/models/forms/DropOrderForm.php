<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 05.08.2018
 * Time: 14:18
 */

namespace common\models\forms;

use Yii;
use yii\base\Model;

class DropOrderForm extends Model
{
    public $name;
    public $phone;
    public $comment;
    public $delivery_type_id;
    public $region;
    public $city;
    public $subway;
    public $street;
    public $zipcode;
    public $building;
    public $flat;
    public $block;
    public $house;
    public $floor;
    public $note;
    public $cost;

    public $products;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone', 'name', 'city','region', 'delivery_type_id'], 'required'],
            [['cost', 'zipcode', 'floor'], 'integer'],
            [['index','name', 'phone'], 'string', 'max' => 255],
            [['comment','note','region','city','subway','street','building','block','house','floor','flat',],'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Client delivery'),
            'phone' => Yii::t('app', 'Phone delivery'),
            'comment' => Yii::t('app', 'Comment delivery'),
            'products' => Yii::t('app', 'Products delivery'),
            'delivery_type_id' => Yii::t('app', 'Delivery Type ID'),
            'region' => Yii::t('app', 'region delivery'),
            'city' => Yii::t('app', 'city delivery'),
            'subway' => Yii::t('app', 'subway delivery'),
            'street' => Yii::t('app', 'street delivery'),
            'zipcode' => Yii::t('app', 'zipcode delivery'),
            'building' => Yii::t('app', 'building delivery'),
            'flat' => Yii::t('app', 'flat delivery'),
            'block' => Yii::t('app', 'block delivery'),
            'house' => Yii::t('app', 'house delivery'),
            'floor' => Yii::t('app', 'floor delivery'),
            'note' => Yii::t('app', 'note delivery'),
            'cost' => Yii::t('app', 'cost delivery'),
        ];
    }
}