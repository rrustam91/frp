<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.08.2018
 * Time: 13:42
 */

namespace common\models\forms;

use common\helpers\modelHellpers;
use common\helpers\myHellpers;
use common\models\products\ProductImg;
use common\models\products\ProductInfo;
use common\models\products\Products;
use common\models\products\ProductsCategories;
use common\models\upload\UploadImage;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ProductsForm extends Model
{
    public $product_id;
    public $category_id;
    public $status;
    public $status_drop;
    public $status_opt;
    public $name;
    public $article;
    public $text;
    public $price;
    public $image;
    public $imageFiles;
    public $alias;
    public $xml_name;
    public $xml_id;
    public $xml_category;
    public $xml_price;
    public $xml_article;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'status', 'status_drop', 'status_opt'], 'integer'],
            [['name', 'price','category_id'], 'required'],
            [['name'], 'string'],

            [['image, imageFiles'],'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['text'], 'string'],
            [['price'], 'number'],
            [['alias', 'article', 'xml_id', 'xml_name', 'xml_category', 'xml_price', 'xml_article'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductsCategories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'name' => Yii::t('app', 'Name'),
            'alias' => Yii::t('app', 'Alias'),
            'article' => Yii::t('app', 'Article'),
            'price' => Yii::t('app', 'Price'),
            'status' => Yii::t('app', 'Status'),
            'status_drop' => Yii::t('app', 'Status Drop'),
            'status_opt' => Yii::t('app', 'Status Opt'),
            'text' => Yii::t('app', 'Text'),
            'xml_id' => Yii::t('app', 'Xml ID'),
            'xml_name' => Yii::t('app', 'Xml Name'),
            'xml_category' => Yii::t('app', 'Xml Category'),
            'xml_price' => Yii::t('app', 'Xml Price'),
            'xml_article' => Yii::t('app', 'Xml Article'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'imageFiles' => Yii::t('app', 'Image Files'),
        ];
    }

    public function saveProduct(){

        $p = new Products();
        $p->name = $this->name;
        $p->category_id = $this->category_id;
        $p->price = $this->price;
        $p->article = myHellpers::generateRandomWord();
        $p->status = $this->status;
        $p->status_opt = $this->status_opt;
        $p->status_drop = $this->status_drop;

        if($p->save()){
            if( $this->saveProductInfo($p->id) && $this->saveProductImg($p->id, $p->alias)) {
                return $p->id;
            }
        }
        return true;


    }

    public function updateProduct($data){
        if($p =  Products::find()->where(['id'=>$data['product_id']])->one()){
            $this->text = $data['text'];
            $this->product_id = $data['product_id'];
            $p->name = $data['name'];
            $p->category_id = $data['category_id'];
            $p->price = $data['price'];
            $p->status = $data['status'];
            $p->status_opt = $data['status_opt'];
            $p->status_drop = $data['status_drop'];
            if($p->save()){
                if( $this->saveProductInfo($p->id) && $this->saveProductImg($p->id, $p->alias)) {
                    return $p->id;
                }else{
                    return $p->id;
                }
            }else{
                myHellpers::show($p->errors, 'p errors');
            }


        }
        return false;


    }
    protected function saveProductInfo($id){
        ProductInfo::deleteAll('product_id = '.$id);
        $pinfo = new ProductInfo();
        $pinfo->product_id = $id;
        $pinfo->text = $this->text;
        return $pinfo->save();
        //return $pinfo->save();
    }

    protected function saveProductImg($id,$alias){
        $image = new UploadImage();
        $image->imageFiles = $this->imageFiles;

        if($image->uploadProduct($id,$alias)){
            return true;
        }
    }
    public static function findFull($id){
        $product = Products::find()->where(['id'=>$id])->one();
        $text = ProductInfo::find()->where(['product_id'=>$id])->select('text')->one();
        $product->text = $text->text;
        $product->imageFiles = [];

        return $product;
    }
}