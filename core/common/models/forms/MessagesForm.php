<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.08.2018
 * Time: 13:53
 */

namespace common\models\forms;


use Yii;
use yii\base\Model;
use common\helpers\myHellpers;
use common\models\User;
use common\models\user\UserMessages;

class MessagesForm extends Model
{
    public $user_id;
    public $type;
    public $text;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id','text'], 'required'],
            [['user_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'string'],
            [['type'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'type' => Yii::t('app', 'Type'),
            'text' => Yii::t('app', 'Text'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function addMessages(){
        $msg = new UserMessages();

        $msg->user_id = Yii::$app->user->id;
        $msg->text = $this->text;
        $msg->type = $this->type;
        return $msg->save();

    }

}