<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.08.2018
 * Time: 10:02
 */

namespace common\models\forms;

use common\helpers\myHellpers;
use common\models\webinars\WebinarChats;
use Yii;
use yii\base\Model;
use yii\helpers\Html;

class ChatMessagesForm extends Model
{

    public $msg;
    public $webinar;
    public $time_video;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['msg',], 'string' ],
            [['webinar','time_video'], 'integer' ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

            'msg' => Yii::t('app', 'Webinar Chat Msg'),
            'webinar' => Yii::t('app', 'Webinar Chat Msg'),

        ];
    }

    public function addMsg(){
        $msg = new WebinarChats();
        $msg->user_id = Yii::$app->user->getIdentity()->id;
        $msg->text = Html::decode($this->msg);
        $msg->webinar_id = $this->webinar;
        $msg->time_video = (int)$this->time_video;
        if(!empty($msg->text)){
            return $msg->save();
        }
        return true;

    }

}