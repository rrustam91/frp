<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.08.2018
 * Time: 19:29
 */

namespace common\models\forms;
use common\models\MainModel;
use Yii;
use common\helpers\myHellpers;
use common\models\user\UserHomework;
use common\models\courses\CoursesLessons;
use yii\base\Model;


class CabinetLessonForm extends Model{

    public $text;
    public $user_id;
    public $lesson_id;
    public $course_id;
    public $status;
    public $comment_teach;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text','comment_teach'], 'string' ],
            [['status', 'user_id', 'lesson_id'], 'integer' ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

            'text' => Yii::t('app', 'Homework Text'),
            'comment_teach' => Yii::t('app', 'Comment Teach'),
            'status' => Yii::t('app', 'Comment Teach'),
            'user_id' => Yii::t('app', 'Comment Teach'),
            'lesson_id' => Yii::t('app', 'Comment Teach'),

        ];
    }
    public function addHomeWork(){
        if(!$homework = UserHomework::find()->where(['lesson_id'=>$this->lesson_id,'user_id'=>Yii::$app->user->id])->one()){
            $homework = new UserHomework();
        }
        $homework->lesson_id = $this->lesson_id;
        $homework->user_id = Yii::$app->user->id;
        $homework->status = MainModel::STATUS_HOMEWORK_NEW;
        $homework->text = $this->text;

        if($homework->save()){

        }else{
            myHellpers::show($homework->errors);
        }
        myHellpers::show($this,'add');
    }

    public function addHomeWorkComment(){

    }
    public static function getHomeWork($lesson_id){
        if($model = UserHomework::find()->where(['lesson_id'=>$lesson_id,'user_id'=>Yii::$app->user->id])->one()){
            return $model;
        }else{
            return new self();
        }
    }





}