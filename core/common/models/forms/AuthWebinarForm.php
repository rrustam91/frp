<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.08.2018
 * Time: 10:02
 */

namespace common\models\forms;

use Yii;
use yii\base\Model;

class AuthWebinarForm extends Model
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email_or_phone'], 'required'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

            'name' => Yii::t('app', 'User Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'email_or_phone' => Yii::t('app', 'Phone'),

        ];
    }

}