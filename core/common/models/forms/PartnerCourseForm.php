<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.08.2018
 * Time: 10:06
 */

namespace common\models\forms;

use common\helpers\myHellpers;
use common\models\MainModel;
use common\models\user\User;
use common\models\user\UserAdd;
use common\models\user\UserAuth;
use Yii;
use yii\base\Model;

class PartnerCourseForm extends Model{

    public $username;
    public $name;
    public $email;
    public $phone;
    public $type;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone'], 'required'],
            [['type'],'integer'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],

            [['email'], 'unique'],
            [['email'], 'email'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Partner Email'),
            'name' => Yii::t('app', 'Partner Name'),
            'phone' => Yii::t('app', 'Partner Phone'),
        ];
    }
    public function addPartner(){


        if(!$u = UserAdd::findUser($this->email, $this->phone)){
            $u = new UserAdd();
            $u->username = myHellpers::generateRandomWord();
            $u->name = $this->name;
            if(myHellpers::isEmail($this->email)) $u->email = $this->email;
            $u->phone = $this->phone;
            $u->setPassword($u->username);
            $u->generateAuthKey();
        }

        $u->status_drop = MainModel::STATUS_USER_DROP_WAIT;
        $u->status_opt = MainModel::STATUS_USER_DROP_WAIT;
        $u->status_role = $u::getType($this->type);

        ($u::getType($this->type)=='opt') ? $status =  " оптовик" : $status = 'дропшипер'; 
        $roles = $u->roles;
        $u->roles = $roles.', Зарегистировался как '. $status;

        if($u->save()){
            if($u->setUserRole()){

            };
        }
        $this->username = $u->username;
        return $u;
    }

}