<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11.09.2018
 * Time: 14:20
 */

namespace common\models\forms;
use common\models\retail_crm\RetailCrm;
use common\models\user\User;
use Yii;
use common\helpers\myHellpers;
use common\models\opt\OptRequest;
use yii\base\Model;


class OptForm extends Model
{
    public $append;
    public $user_id;
    public $status;
    public $email;
    public $phone;
    public $link;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email','phone'], 'required' ],
            [['email','link','phone'], 'string' ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'link' => Yii::t('app', 'Link'),
            'status' => Yii::t('app', 'Status'),
            'append' => Yii::t('app', 'Append'),
        ];
    }

    public function addOptSubscribe(){

        $opt = new OptRequest();
        if(!$user = User::findByEmail($this->email)){
            $user = new User();
            $user->email = $this->email;
            $user->phone = $this->phone;
            $user->roles = 'clients';

            $user->setRandomUsername( myHellpers::generateRandomWord());
            $user->setPassword($user->username);
            $user->generateAuthKey();
            $user->save();
        }
        $opt->user_id = $user->id;
        $opt->link = $this->link;
        if($opt->save()){
            $this->sendToCrm($opt,$user);
            return $opt;
        }
        return false;
    }

    private function sendToCrm($opt, $user){
        $ref = myHellpers::getUtm(Yii::$app->request->getReferrer());
        $crm = new RetailCrm();
        $data['firstName'] = $user->name;
        $data['phone'] = $user->phone;
        $data['email'] = $user->email;
        $data['status'] = 'novyi';
        $data['customFields']["landing"] = "Заявка с FRP - ОПТ";
        $data['customFields']["utm"] = myHellpers::encodeJson($ref);
        $data['customerComment'] =  ($opt->link) ? 'Ссылка на товар: '. $opt->link : '';

        $data['customFields']['search_system'] = $ref['utm_source'];
        $data['customFields']['utm_medium'] = $ref['utm_medium'];
        $data['customFields']['utm_campaign'] = $ref['utm_campaign'];
        $data['customFields']['utm_announcement'] = $ref['utm_content'];
        $data['customFields']['utm_keyword'] = $ref['utm_term'];
        $data['customFields']['yclid'] = $ref['yclid'];

        return $crm->addOpt($data);
    }




}