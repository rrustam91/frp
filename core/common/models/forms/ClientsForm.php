<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 05.08.2018
 * Time: 22:10
 */

namespace common\models\forms;

use Yii;
use yii\base\Model;
class ClientsForm extends Model
{


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'auth_key', 'password_hash'], 'required'],
            [['role', 'status', 'created_at', 'updated_at'], 'integer'],
            [['utm', 'username', 'email', 'name', 'phone', 'roles', 'password_hash', 'password_reset_token', 'group'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'role' => Yii::t('app', 'Role'),
            'roles' => Yii::t('app', 'Roles'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'group' => Yii::t('app', 'Group'),
        ];
    }


}