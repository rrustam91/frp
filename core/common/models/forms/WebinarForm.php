<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.08.2018
 * Time: 11:16
 */

namespace common\models\forms;
use common\helpers\myHellpers;
use common\models\user\UserAuth;
use Yii;
use yii\base\Model;

class WebinarForm extends Model
{
    public $name;
    public $email_or_phone;

    public $rememberMe = true;
    public function rules()
    {
        return [
            // username and password are both required,
            [['email_or_phone'], 'required'],
            [['name',  'email_or_phone'], 'string'],


        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Webinar Nickname'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'email_or_phone' => Yii::t('app', 'Email Or Phone'),

        ];
    }


    public function login()
    {
        if ($this->validate()) {
            if(UserAuth::findByEmail($this->email_or_phone)){ 
                $u = UserAuth::findByEmail($this->email_or_phone);
                $roles = $u->roles;
                $u->roles = $roles.', Смотрит вебинар';
                $u->save();
            }else if(UserAuth::findByPhone($this->email_or_phone)){
                $u = UserAuth::findByPhone($this->email_or_phone);

                $roles = $u->roles;
                $u->roles = $roles.', Смотрит вебинар';
                $u->save();
            }else{
                $u = new UserAuth();
                $u->username = myHellpers::generateRandomWord();
                $u->name = $this->name;

                if(myHellpers::isEmail($this->email_or_phone)){
                    $u->email = $this->email_or_phone;
                }else{
                    $u->phone = $this->email_or_phone;
                }
                $roles = $u->roles;
                $u->roles = $roles.', Смотрит вебинар';
                $u->setPassword('newUser1');
                $u->generateAuthKey();
                if($u->save()){
                    return Yii::$app->user->login($u, $this->rememberMe ? 3600 * 24 * 30 : 0);
                }
            }
            return Yii::$app->user->login($u, $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }




}