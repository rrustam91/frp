<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.08.2018
 * Time: 11:05
 */

namespace common\models\retail_crm;

use Yii;
use common\helpers\myHellpers;
use yii\base\Model;

class RetailCrm extends Model
{


    protected $retailCrm;

    public function __construct(array $config = [])
    {
        $this->retailCrm = Yii::$app->retailCrm;
        parent::__construct($config);
    }

    /**
     * Set User Drop
    **/
    public function setUserDrop(){

    }

    /**
     * Get User Drop
     **/
    public function getUserDrop(){

    }


    /**
     * set User Drop
     **/

    public function setUserOpt(){

    }


    /**
     * Get User Drop
     **/

    public function getUserOpt(){

    }



    /**
     * add User Drop order
     **/

    public function addOrder($data){
        $send = $this->retailCrm->client->request->ordersCreate(
            $data
        , 'dropshop');
        if($send->success ==1){
            return $send->id;
        }else{
            myHellpers::show($send, 'error crm');
        }
        return false;
    }
    public function addOpt($data){
        $send = $this->retailCrm->client->request->ordersCreate(
            $data
        , 'www-smart-microcam-ru');
        if($send->success ==1){
            return $send->id;
        }else{
            myHellpers::show($send, 'error crm');
        }
        return false;
    }


    /**
     * Get User Drop
     **/
    public function getOrders(){

    }

}