<?php

namespace common\models\products;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_info".
 *
 * @property int $id
 * @property int $product_id
 * @property string $text
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Products $product
 */
class ProductInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id',], 'required'],
            [['product_id', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'string'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'text' => Yii::t('app', 'Text'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
