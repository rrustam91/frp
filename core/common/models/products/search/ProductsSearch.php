<?php

namespace common\models\products\search;

use common\helpers\myHellpers;
use common\models\MainModel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\products\Products;

/**
 * ProductsSearch represents the model behind the search form of `common\models\products\Products`.
 */
class ProductsSearch extends Products
{
    /**
     * @inheritdoc
     */
    public $drop = 'not';

    public function __construct($drop='',array $config = [])
    {
        $this->drop = $drop;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['id', 'category_id', 'status', 'status_drop', 'created_at', 'updated_at'], 'integer'],
            [['name', 'alias', 'article', 'xml_id', 'xml_name', 'xml_category', 'xml_price', 'xml_article'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'price' => $this->price,
            'status' => $this->status,
            'status_drop' => $this->status_drop,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'article', $this->article])
            ->andFilterWhere(['like', 'xml_id', $this->xml_id])
            ->andFilterWhere(['like', 'xml_name', $this->xml_name])
            ->andFilterWhere(['like', 'xml_category', $this->xml_category])
            ->andFilterWhere(['like', 'xml_price', $this->xml_price])
            ->andFilterWhere(['like', 'xml_article', $this->xml_article]);
        if($this->drop=='drop'){
            $query->andWhere(['status_drop'=>MainModel::STATUS_PRODUCT_DROP_ACTIVE]);
        }
        $query->with('category');
        return $dataProvider;
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchCategory($params)
    {
        $query = Products::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([

            'category_id' => $this->category_id,
            'price' => $this->price,
            'status' => $this->status,
            'status_drop' => $this->status_drop,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'article', $this->article])
            ->andFilterWhere(['like', 'xml_id', $this->xml_id])
            ->andFilterWhere(['like', 'xml_name', $this->xml_name])
            ->andFilterWhere(['like', 'xml_category', $this->xml_category])
            ->andFilterWhere(['like', 'xml_price', $this->xml_price])
            ->andFilterWhere(['like', 'xml_article', $this->xml_article]);
        $query->andWhere(['category_id'=>$params['category_id']]);
        if($this->drop == 'drop'){
            $query->andWhere(['status_drop'=>MainModel::STATUS_PRODUCT_ACTIVE]);
        }

        $query->with('category');
        return $dataProvider;
    }
}
