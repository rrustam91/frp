<?php

namespace common\models\products;

use common\components\AliasComponent;
use common\helpers\myHellpers;
use common\models\dropship\DropOrderProduct;
use common\models\forms\ProductsForm;
use common\models\MainModel;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $alias
 * @property string $article
 * @property double $price
 * @property int $price_drop
 * @property int $price_opt
 * @property int $status
 * @property int $status_drop
 * @property int $status_opt
 * @property string $xml_id
 * @property string $xml_name
 * @property string $xml_category
 * @property string $xml_price
 * @property string $xml_article
 * @property int $created_at
 * @property int $updated_at
 *
 * @property DropOrderProduct[] $dropOrderProducts
 * @property ProductImg[] $productImgs
 * @property ProductInfo[] $productInfos
 * @property ProductsCategories $category
 * @property ProductsStock[] $productsStocks
 */
class Products extends MainModel
{
    public $text;
    public $imageFiles;
    public $product_id;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            AliasComponent::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'price_drop', 'price_opt', 'status', 'status_drop', 'status_opt', 'created_at', 'updated_at'], 'integer'],
            [['name', 'article', 'price', 'created_at', 'updated_at'], 'required'],
            [['name'], 'string'],
            [['price'], 'number'],
            [['alias', 'article', 'xml_id', 'xml_name', 'xml_category', 'xml_price', 'xml_article'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductsCategories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'name' => Yii::t('app', 'Name'),
            'alias' => Yii::t('app', 'Alias'),
            'article' => Yii::t('app', 'Article'),
            'price' => Yii::t('app', 'Price'),
            'price_drop' => Yii::t('app', 'Price Drop'),
            'price_opt' => Yii::t('app', 'Price Opt'),
            'status' => Yii::t('app', 'Status'),
            'status_drop' => Yii::t('app', 'Status Drop'),
            'status_opt' => Yii::t('app', 'Status Opt'),
            'xml_id' => Yii::t('app', 'Xml ID'),
            'xml_name' => Yii::t('app', 'Xml Name'),
            'xml_category' => Yii::t('app', 'Xml Category'),
            'xml_price' => Yii::t('app', 'Xml Price'),
            'xml_article' => Yii::t('app', 'Xml Article'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDropOrderProducts()
    {
        return $this->hasMany(DropOrderProduct::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductsCategories::className(), ['id' => 'category_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsStocks()
    {
        return $this->hasMany(ProductsStock::className(), ['product_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfo()
    {
        return $this->hasOne(ProductInfo::className(), ['product_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImgs()
    {
        return $this->hasMany(ProductImg::className(), ['product_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function findProductByXml($xml_id)
    {
        return self::find()->where(['xml_id'=>$xml_id])->one();
    }

    /**
     * @return array
     */
    public static function findProductByIdXmlDrop($id, $xml_id)
    {
        //return self::find()->where(['id'=>$id,'xml_id'=>$xml_id])->andWhere(['status_drop'=>self::STATUS_PRODUCT_DROP_ACTIVE])->one();
        return self::find()->where(['id'=>$id,'xml_id'=>$xml_id])->andWhere(['status_drop'=>self::STATUS_PRODUCT_DROP_ACTIVE])->one();
    }

    /**
     * @return array
     */
    public static function getDrop($prm = ['id', 'name', 'article', 'price','price_drop','price_opt', 'xml_id', 'xml_name', 'xml_price', 'xml_article'])
    {
        return self::find()->select($prm)->where(['status_drop'=>self::STATUS_PRODUCT_DROP_ACTIVE])->asArray()->all();
    }

    /**
     * @return array
     */
    public static function getDropMain()
    {
        return self::find()->where(['status_drop'=>self::STATUS_PRODUCT_DROP_ACTIVE])->andWhere(['NOT IN','category_id',[25]])->asArray()->all();
    }
    /**
     * @return array
     */
    public static function getDropAppend()
    {
        return self::find()->where(['status_drop'=>self::STATUS_PRODUCT_DROP_ACTIVE, 'category_id'=>25])->asArray()->all();
    }

    /**
     * {@inheritdoc}
     * @return ProductsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductsQuery(get_called_class());
    }
    public function updateProduct(){
        //myHellpers::show($this);
        $up = new ProductsForm();
        $up->load($this);
        myHellpers::show($up->updateProduct());

    }



}
