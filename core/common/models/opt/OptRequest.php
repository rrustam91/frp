<?php

namespace common\models\opt;

use common\models\MainModel;
use common\models\user\User;
use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "opt_request".
 *
 * @property int $id
 * @property int $user_id
 * @property string $link
 * @property int $status
 * @property string $append
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class OptRequest extends MainModel
{

    public $email;
    public $phone;
    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'opt_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['append'], 'string'],
            [['link'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'link' => Yii::t('app', 'Link'),

            'status' => Yii::t('app', 'Status'),
            'append' => Yii::t('app', 'Append'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }



    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
