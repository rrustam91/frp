<?php

namespace common\models\opt;

use common\models\user\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\opt\OptRequest;

/**
 * OptRequestSearch represents the model behind the search form of `common\models\opt\OptRequest`.
 */
class OptRequestSearch extends OptRequest
{

    public $email;
    public $phone;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'email','phone', 'status', 'created_at', 'updated_at'], 'integer'],
            [['link', 'append'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OptRequest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);
        if(!empty($email)){
            $user = User::findByEmail($this->email);
        }
        if(!empty($phone)){
            $user = User::findByEmail($this->phone);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $user->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'append', $this->append]);

        $query->orderBy(['id'=>SORT_DESC]);
        $query->with('user');
        return $dataProvider;
    }
}
