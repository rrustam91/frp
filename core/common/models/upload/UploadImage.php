<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.08.2018
 * Time: 13:17
 */

namespace common\models\upload;

use common\helpers\myHellpers;
use common\models\products\ProductImg;
use Faker\Provider\File;
use Yii;
use yii\base\Model;

use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class UploadImage extends Model{

    public $imageFiles;

    public function rules(){
        return[
            [['imageFiles'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    public function upload()
    {
            foreach ($this->imageFiles as $file) {
                $filename=Yii::$app->getSecurity()->generateRandomString(15);
                $file->saveAs(Yii::getAlias('@uploads').'/' . $filename . '.' . $file->extension);
                return $filename . '.' . $file->extension;
            }
            return true;

    }
    public function uploadProduct($id,$alias)
    {
        ProductImg::deleteAll('product_id = '.$id);
        $path = $this->issetDir(Yii::getAlias('@uploads'),$alias);
        $path1 = $this->issetDir(Yii::getAlias('@admin_uploads'),$alias);
        $path2 = $this->issetDir(Yii::getAlias('@cp_uploads'),$alias);

        foreach ($this->imageFiles as $file) {
            $filename=Yii::$app->getSecurity()->generateRandomString(15);
            $file_path = $path .'/'.$filename . '.' . $file->extension;

            $this->fileSave($file, $file_path);
            FileHelper::copyDirectory($path, $path1 );
            FileHelper::copyDirectory($path, $path2 );

            $pimg = new ProductImg();
            $pimg->product_id = $id;
            $pimg->url = '/'.$alias. '/'. $filename . '.' . $file->extension;
            if($pimg->save()){

            }else{
                myHellpers::show($pimg->errors, 'pimg er');
            }
        }

    }
    protected function fileSave($file, $filePath){
        return $file->saveAs($filePath);
    }
    protected function issetDir($path, $alias){
        $dir = $path.'/' . $alias;
        FileHelper::removeDirectory($dir);
        if(!is_dir($dir)){
            FileHelper::createDirectory($dir, $mode = 0775);
        }



        return $dir;
    }
}
