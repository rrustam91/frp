<?php

namespace common\models\webinars\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\webinars\WebinarComments;

/**
 * WebinarCommentsSearch represents the model behind the search form of `common\models\webinars\WebinarComments`.
 */
class WebinarCommentsSearch extends WebinarComments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'webinar_id', 'time_send', 'created_at', 'updated_at'], 'integer'],
            [['name', 'text'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WebinarComments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'webinar_id' => $this->webinar_id,
            'time_send' => $this->time_send,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'text', $this->text]);
        $query->with('webinar');
        return $dataProvider;
    }
}
