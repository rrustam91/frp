<?php

namespace common\models\webinars\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\webinars\WebinarAds;

/**
 * WebinarAdsSearch represents the model behind the search form of `common\models\webinars\WebinarAds`.
 */
class WebinarAdsSearch extends WebinarAds
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'webinar_id', 'status', 'date_start', 'date_end', 'created_at', 'updated_at'], 'integer'],
            [['img', 'text', 'url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WebinarAds::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'webinar_id' => $this->webinar_id,
            'status' => $this->status,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'img', $this->img])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
