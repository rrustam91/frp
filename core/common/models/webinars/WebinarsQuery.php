<?php

namespace common\models\webinars;

/**
 * This is the ActiveQuery class for [[Webinars]].
 *
 * @see Webinars
 */
class WebinarsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Webinars[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Webinars|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
