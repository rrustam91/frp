<?php

namespace common\models\webinars;

use common\models\MainModel;
use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "webinar_chats".
 *
 * @property int $id
 * @property int $user_id
 * @property int $webinar_id
 * @property string $text
 * @property int $time_video
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 * @property Webinars $webinar
 */
class WebinarChats extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'webinar_chats';
    }



    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'webinar_id'], 'required'],
            [['user_id', 'webinar_id', 'time_video', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['webinar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Webinars::className(), 'targetAttribute' => ['webinar_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'webinar_id' => Yii::t('app', 'Webinar ID'),
            'text' => Yii::t('app', 'Text'),
            'time_video' => Yii::t('app', 'Time Video'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebinar()
    {
        return $this->hasOne(Webinars::className(), ['id' => 'webinar_id']);
    }

    /**
     * {@inheritdoc}
     * @return WebinarsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WebinarsQuery(get_called_class());
    }
}
