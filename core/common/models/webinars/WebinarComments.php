<?php

namespace common\models\webinars;

use common\models\MainModel;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "webinar_comments".
 *
 * @property int $id
 * @property int $webinar_id
 * @property string $name
 * @property string $text
 * @property int $time_send
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Webinars $webinar
 */
class WebinarComments extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'webinar_comments';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['webinar_id', 'name'], 'required'],
            [['webinar_id', 'time_send', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['webinar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Webinars::className(), 'targetAttribute' => ['webinar_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'webinar_id' => Yii::t('app', 'Webinar ID'),
            'name' => Yii::t('app', 'Name'),
            'text' => Yii::t('app', 'Text'),
            'time_send' => Yii::t('app', 'Time Send'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebinar()
    {
        return $this->hasOne(Webinars::className(), ['id' => 'webinar_id']);
    }


    /**
     * {@inheritdoc}
     * @return WebinarsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WebinarsQuery(get_called_class());
    }
}
