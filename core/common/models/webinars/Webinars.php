<?php

namespace common\models\webinars;

use common\components\AliasComponent;
use common\models\MainModel;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "webinars".
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property int $status
 * @property string $video_url
 * @property string $title
 * @property int $date_start
 * @property int $date_end
 * @property int $created_at
 * @property int $updated_at
 *
 * @property WebinarAds[] $webinarAds
 * @property WebinarChats[] $webinarChats
 * @property WebinarComments[] $webinarComments
 */
class Webinars extends MainModel
{
    const STATUS_ACTIVE = 10;
    const STATUS_EDIT = 5;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETED = -1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'webinars';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            AliasComponent::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'alias' ], 'required'],
            [['status',  'created_at', 'updated_at'], 'integer'],
            [['name', 'alias', 'video_url','date_start', 'date_end', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'alias' => Yii::t('app', 'Alias'),
            'status' => Yii::t('app', 'Status'),
            'video_url' => Yii::t('app', 'Video Url'),
            'title' => Yii::t('app', 'Title'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_end' => Yii::t('app', 'Date End'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebinarAds()
    {
        return $this->hasMany(WebinarAds::className(), ['webinar_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebinarChats()
    {
        return $this->hasMany(WebinarChats::className(), ['webinar_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebinarComments()
    {
        return $this->hasMany(WebinarComments::className(), ['webinar_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return WebinarsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WebinarsQuery(get_called_class());
    }
}
