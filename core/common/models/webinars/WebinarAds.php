<?php

namespace common\models\webinars;

use common\models\MainModel;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "webinar_ads".
 *
 * @property int $id
 * @property int $webinar_id
 * @property int $status
 * @property string $img
 * @property string $text
 * @property string $url
 *
 * @property int $time_start
 * @property int $date_start
 * @property int $date_end
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Webinars $webinar
 */
class WebinarAds extends MainModel
{
    public $time_start;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'webinar_ads';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['webinar_id'], 'required'],
            [['webinar_id', 'status', 'date_start', 'date_end', 'time_start', 'created_at', 'updated_at'], 'integer'],
            [['img', 'text', 'url'], 'string', 'max' => 255],
            [['webinar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Webinars::className(), 'targetAttribute' => ['webinar_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'webinar_id' => Yii::t('app', 'Webinar ID'),
            'status' => Yii::t('app', 'Status'),
            'img' => Yii::t('app', 'Img'),
            'text' => Yii::t('app', 'Text'),
            'url' => Yii::t('app', 'Url'),
            'time_start' => Yii::t('app', 'Time Start'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_end' => Yii::t('app', 'Date End'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebinar()
    {
        return $this->hasOne(Webinars::className(), ['id' => 'webinar_id']);
    }

    /**
     * {@inheritdoc}
     * @return WebinarsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WebinarsQuery(get_called_class());
    }
}
