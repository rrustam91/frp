<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.08.2018
 * Time: 14:45
 */

namespace common\models\courses;

use common\helpers\myHellpers;
use common\models\MainModel;
use common\models\User;
use Yii;
use common\models\user\UserStepLesson;
use yii\base\Model;

class CoursesHellper extends Model
{
    public static function getLastLessonUser($course_id='', $lesson_id = '',$course_category_id=''){

        $query = UserStepLesson::find();
        $query->where(['user_id'=>Yii::$app->user->id]);
        if(!empty($course_id)){
            $query->andWhere(['course_id'=>$course_id]);
        }
        if(!empty($lesson_id)){
            $query->andWhere(['lesson_id'=>$lesson_id]);
        }
        if(!empty($course_category_id)){
            $query->andWhere(['course_id'=>$course_category_id]);
        }
        $query->orderBy(['updated_at'=>SORT_DESC]);
        $data = $query->one();
        if(empty($data)){
            $data = self::createNewLastLessonUser($course_id);
        }

        return $data;
    }

    public static function createNewLastLessonUser($course_id=''){
        if($lesson = self::getModelByCourseId(CoursesLessons::className(),$course_id)){
            $step = new UserStepLesson();
            $step->user_id = Yii::$app->user->id;
            $step->course_id = $course_id;
            $step->lesson_id = $lesson->id;
            $step->course_category_id = $lesson->category_id;
            $step->step = $lesson->step_by;
            if($step->save()){
                return $step;
            }else{
                myHellpers::show($step->errors,'error');
            }
        }
    }
    public static function updateLastLessonUser($lsn_id=''){
        if($lesson = self::findModel(CoursesLessons::className(),$lsn_id)){
            if(!$step = self::findStepLesson($lesson->course->id)){
                $step = new UserStepLesson();
            }
            $step->user_id = Yii::$app->user->id;
            $step->course_id = $lesson->course_id;
            $step->lesson_id = $lesson->id;
            $step->course_category_id = $lesson->category_id;
            $step->step = $lesson->step_by;

            if($step->save()){
                return $step;
            }else{
                myHellpers::show($step->errors,'error');
            }
        }
    }

    public static function findModelByAlias($model,$alias){
        return $model::find()->where(['alias'=>$alias])->one();
    }
    public static function findLessonByStep($ctg_id,$step){
        return CoursesLessons::find()->where([
            'category_id'=>$ctg_id,
            'step_by'=>$step,
            'opened'=>MainModel::STATUS_LESSON_ACTIVE
        ])->one();
    }

    public static function findCategoryByStep($ctg_id,$step){
        return CourseCategory::find()->where(['course_id'=>$ctg_id,'step_by'=>$step])->one();
    }

    public static function findStepLesson($course_id){
        return UserStepLesson::find()->where([
            'user_id'=>Yii::$app->user->id,
            'course_id'=>$course_id,
        ])->one();
    }


    public static function findModel($model,$id){
        return $model::find()->where(['id'=>$id])->one();
    }


    public static function getModelByCourseId($model, $course_id, $type='one'){

//        myHellpers::show([$model, $course_id]);
        $query = $model::find();
        $query->where([
            'course_id'=>$course_id,
            'status'=>MainModel::STATUS_LESSON_ACTIVE,
            'opened'=>MainModel::STATUS_LESSON_ACTIVE,
        ]);
        if($type=='all'){
            $data = $query->all();
        }else{
            $data = $query->one();
        }
        return $data;
    }

    public static function getLessonById($id){
        $query = CoursesLessons::find();
        $query->where([
            'id'=>$id,
            'status'=>MainModel::STATUS_LESSON_ACTIVE,
            'opened'=>MainModel::STATUS_LESSON_ACTIVE,
        ]);
        $data = $query->one();

        return $data;
    }

}