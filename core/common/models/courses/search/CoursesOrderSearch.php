<?php

namespace common\models\courses\search;

use common\helpers\myHellpers;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\courses\CoursesOrder;
use yii\helpers\ArrayHelper;

/**
 * CoursesOrderSearch represents the model behind the search form of `common\models\courses\CoursesOrder`.
 */
class CoursesOrderSearch extends CoursesOrder
{
    /**
     * @inheritdoc
     */

    public $user_name;
    public $user_email;
    public $user_phone;
    public function rules()
    {
        return [
            [['id', 'user_id', 'course_id', 'price', 'status', 'created_at', 'updated_at'], 'integer'],
            [['user_name','user_email','user_phone'],'string'],
            [['user_name','user_email','user_phone','comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoursesOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $ids = [];
        if(!empty($this->user_name)){
            $u = User::find()->select('id')->where(['like','name',$this->user_name])->asArray()->all();
            $ids =  ArrayHelper::getColumn($u, 'id');
            //myHellpers::show($ids);
        }
        if(!empty($this->user_email)){
            $u = User::find()->select('id')->where(['like','email',$this->user_email])->asArray()->all();
            $ids = ArrayHelper::getColumn($u, 'id');
        }
        if(!empty($this->user_phone)){
            $u = User::find()->select('id')->where(['like','phone',$this->user_phone])->asArray()->all();
            $ids = ArrayHelper::getColumn($u, 'id');
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id'=> $ids,
            'course_id' => $this->course_id,
            'price' => $this->price,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        $query->andFilterWhere(['like', 'comment', $this->comment]);
        $query->with('user','course');
        return $dataProvider;
    }
}
