<?php

namespace common\models\courses;

use common\components\AliasComponent;
use common\models\MainModel;
use common\models\user\UserStepLesson;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "course_category".
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property string $content
 * @property int $course_id
 * @property int $status
 * @property int $opened
 * @property int $step_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Courses $course
 * @property CoursesLessons[] $coursesLessons
 * @property UserStepLesson[] $userStepLessons
 */
class CourseCategory extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'alias','course_id'], 'required'],
            [['content'], 'string'],
            [['course_id', 'status', 'opened', 'step_by', 'created_at', 'updated_at'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 255],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course_id' => 'id']],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'alias' => Yii::t('app', 'Alias'),
            'content' => Yii::t('app', 'Content'),
            'course_id' => Yii::t('app', 'Course ID'),
            'status' => Yii::t('app', 'Status'),
            'opened' => Yii::t('app', 'Opened'),
            'step_by' => Yii::t('app', 'Step By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            AliasComponent::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoursesLessons()
    {
        return $this->hasMany(CoursesLessons::className(), ['category_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoursesActiveLessons()
    {
        return $this->hasMany(CoursesLessons::className(), ['category_id' => 'id'])->where(['opened'=>MainModel::STATUS_LESSON_ACTIVE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserStepLessons()
    {
        return $this->hasMany(UserStepLesson::className(), ['course_category_id' => 'id']);
    }
}
