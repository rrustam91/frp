<?php

namespace common\models\courses;

use common\components\AliasComponent;
use common\models\MainModel;
use common\models\user\UserCourses;
use common\models\user\UserHomework;
use common\models\user\UserLessons;
use common\models\user\UserStepLesson;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "courses".
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property int $parent_id
 * @property double $price
 * @property int $date_start
 * @property int $date_end
 * @property string $content
 * @property string $img
 * @property int $status
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CourseCategory[] $courseCategories
 * @property Courses $parent
 * @property Courses[] $courses
 * @property CoursesLessons[] $coursesLessons
 * @property CoursesOrder[] $coursesOrders
 * @property UserCourses[] $userCourses
 * @property UserHomework[] $userHomeworks
 * @property UserStepLesson[] $userStepLessons
 */
class Courses extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * {@inheritdoc}
     */
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'alias', ], 'required'],
            [['parent_id', 'date_start', 'date_end', 'status', 'created_at', 'updated_at'], 'integer'],
            [['price'], 'number'],
            [['content'], 'string'],
            [['name', 'alias', 'img', 'seo_title', 'seo_keywords', 'seo_description'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Course Name'),
            'alias' => Yii::t('app', 'Alias'),
            'parent_id' => Yii::t('app', 'Course Parent ID'),
            'price' => Yii::t('app', 'Price'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_end' => Yii::t('app', 'Date End'),
            'content' => Yii::t('app', 'Course Content'),
            'img' => Yii::t('app', 'Img'),
            'status' => Yii::t('app', 'Status'),
            'seo_title' => Yii::t('app', 'Seo Title'),
            'seo_keywords' => Yii::t('app', 'Seo Keywords'),
            'seo_description' => Yii::t('app', 'Seo Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            AliasComponent::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseCategories()
    {
        return $this->hasMany(CourseCategory::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Courses::className(), ['id' => 'parent_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function checkParent()
    {

        return $this->hasOne(Courses::className(), ['id' => 'parent_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public static function checkParentByAlias($alias)
    {
        $current = self::find()->where(['alias'=>$alias])->one()->id;
        $parent = self::find()->where(['id'=>$current]);


        //return $this->hasOne(Courses::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Courses::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoursesLessons()
    {
        return $this->hasMany(CoursesLessons::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoursesOrders()
    {
        return $this->hasMany(CoursesOrder::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCourses()
    {
        return $this->hasMany(UserCourses::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserStepLessons()
    {
        return $this->hasMany(UserStepLesson::className(), ['course_id' => 'id']);
    }

    public static function getCourse($id){
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function getCourseParent($id){
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function getCourseIdByAlias($alias){
        return static::find()->where(['alias'=>$alias, 'status' => self::STATUS_ACTIVE])->select('id')->one()->id;
    }

    public static function getCoursesList($parent_id=4){
        return static::find()->where(['parent_id'=>$parent_id, 'status'=>self::STATUS_ACTIVE])->asArray()->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHomeworks()
    {
        return $this->hasMany(UserHomework::className(), ['course_id' => 'id']);
    }


    /**
     * {@inheritdoc}
     * @return CoursesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CoursesQuery(get_called_class());
    }
}
