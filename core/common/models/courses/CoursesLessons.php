<?php

namespace common\models\courses;

use common\components\AliasComponent;
use common\models\MainModel;
use common\models\user\UserHomework;
use common\models\user\UserStepLesson;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "courses_lessons".
 *
 * @property int $id
 * @property int $course_id
 * @property int $category_id
 * @property string $name
 * @property string $alias
 * @property string $content
 * @property int $status
 * @property int $opened
 * @property int $step_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CourseCategory $category
 * @property Courses $course
 * @property UserHomework[] $userHomeworks
 * @property UserStepLesson[] $userStepLessons
 */
class CoursesLessons extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses_lessons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'category_id', 'status', 'opened', 'step_by', 'created_at', 'updated_at'], 'integer'],
            [['name', 'category_id','course_id'], 'required'],
            [['content'], 'string'],
            [['name', 'alias'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CourseCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'course_id' => Yii::t('app', 'Course ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'name' => Yii::t('app', 'Name'),
            'alias' => Yii::t('app', 'Alias'),
            'content' => Yii::t('app', 'Content'),
            'status' => Yii::t('app', 'Status'),
            'opened' => Yii::t('app', 'Opened'),
            'step_by' => Yii::t('app', 'Step By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            AliasComponent::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CourseCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHomeworks()
    {
        return $this->hasMany(UserHomework::className(), ['lesson_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserStepLessons()
    {
        return $this->hasMany(UserStepLesson::className(), ['lesson_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CoursesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CoursesQuery(get_called_class());
    }
}
