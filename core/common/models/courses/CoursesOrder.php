<?php

namespace common\models\courses;

use common\helpers\myHellpers;
use common\models\MainModel;
use common\models\user\User;
use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "courses_order".
 *
 * @property int $id
 * @property int $user_id
 * @property int $course_id
 * @property int $p_course_id
 * @property int $price
 * @property int $status
 * @property string $comment
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Courses $course
 * @property User $user
 */
class CoursesOrder extends MainModel
{
    const STATUS_UNACTIVE = 5;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses_order';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'course_id',], 'required'],
            [['user_id', 'course_id', 'p_course_id', 'price', 'status', 'created_at', 'updated_at'], 'integer'],
            [['comment'], 'string'],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'course_id' => Yii::t('app', 'Course ID'),
            'p_course_id' => 'P Course ID',
            'price' => Yii::t('app', 'Price'),
            'status' => Yii::t('app', 'Status'),
            'comment' => Yii::t('app', 'Comment'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentCourse()
    {
        return $this->hasOne(Courses::className(), ['id' => 'p_course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserName()
    {
        $user = $this->user;
        return $user ? $user->name : '';
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserEmail()
    {
        $user = $this->user;
        return $user ? $user->email : '';
    }

    /**
     * {@inheritdoc}
     * @return CoursesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CoursesQuery(get_called_class());
    }

    public static function findByPay($user,$order){
        return self::find()->where(['id'=>$order['id'], 'created_at'=>$order['date'],'user_id'=>$user])->one();

    }

    public static function changeStatus($user,$order,$status = MainModel::STATUS_INACTIVE){
        $order = self::findByPay($user,$order);
        $order->status = $status;
        return $order->save();
    }

    public static function getUrlPay($order,$form){
        $config = Yii::$app->yakassa;
        $URL = $config->paymentAction;
        $fields = array(
            'shopId'=>$config->shopId,
            'scid'=>$config->scId,
            'sum'=>$order->price,
            'customerNumber'=>$form->name . ' ' .$order->user_id,
            'paymentType'=>'AC',
            'orderNumber'=>$order->created_at.$order->id,
            'cps_name'=>$form->name,
            'cps_phone'=>$form->phone,
            'cps_email'=>$form->email,
        );

        $data= self::sendCurl($URL,$fields);
        $string = $data;
        preg_match('/htt(.*?)$/', $string,$smod);
        $sm = 'htt'. $smod[1];
        return $sm;
    }


    private static function sendCurl($url,$fields){
        $fields_str = http_build_query($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_str);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }



}
