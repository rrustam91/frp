<?php

namespace common\models\user;

use common\components\AliasComponent;
use common\helpers\myHellpers;
use common\models\courses\CoursesHellper;
use common\models\crm\ShopCrm;
use common\models\dropship\SendDropOrder;
use common\models\MainModel;
use common\models\opt\OptRequest;
use common\models\rbac\AuthAssignment;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use common\models\courses\CoursesOrder;
use common\models\dropship\DropOrder;
use common\models\webinars\WebinarChats;


/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $name
 * @property string $phone
 * @property int $role
 * @property string $roles
 * @property string $utm
 * @property string $group
 * @property string $crm_name
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $status_role
 * @property int $status
 * @property int $status_drop
 * @property int $status_opt
 * @property string $comment
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CoursesOrder[] $coursesOrders
 * @property DropOrder[] $dropOrders
 * @property DropOrder[] $dropOrders0
 * @property UserClientPayment[] $userClientPayments
 * @property UserClientPayment[] $userClientPayments0
 * @property UserCourses[] $userCourses
 * @property UserDropship[] $userDropships
 * @property UserHomework[] $userHomeworks
 * @property UserMessages[] $userMessages
 * @property UserOpt[] $userOpts
 * @property UserProfile[] $userProfiles
 * @property UserStepLesson[] $userStepLessons
 * @property WebinarChats[] $webinarChats
 */
class User extends MainModel
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    public $password_new;
    public $password_confirm;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'required'],
            [['role', 'status', 'created_at','status_drop', 'status_opt', 'updated_at'], 'integer'],
            [['username', 'email', 'name', 'phone', 'crm_name','crm_name_new','status_role', 'password_hash', 'password_reset_token', 'password_new', 'password_confirm', 'status_role'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['utm', 'comment'], 'string'],
            ['password_new', 'validatePassword'],
            ['password_confirm', 'compare', 'compareAttribute' => 'password_new'],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'role' => Yii::t('app', 'Role'),
            'roles' => Yii::t('app', 'Roles'),
            'crm_name' => Yii::t('app', 'Crm Name'),
            'crm_name_new' => Yii::t('app', 'Crm Name'),
            'utm' => Yii::t('app', 'Utm'),
            'status' => Yii::t('app', 'Status'),
            'status_role' => Yii::t('app', 'Status Role'),
            'group' => Yii::t('app', 'Group'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'comment' => Yii::t('app', 'Comment'),
            'password_new' => Yii::t('app', 'User Password'),
            'password_confirm' => Yii::t('app', 'User Password Confirm'),
        ];
    }



    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoursesOrders()
    {
        return $this->hasMany(CoursesOrder::className(), ['user_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserManagerPayments()
    {
        return $this->hasMany(UserClientPayment::className(), ['manager_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserClientPayments()
    {
        return $this->hasMany(UserClientPayment::className(), ['user_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoursesOrdersId()
    {
        return $this->hasMany(CoursesOrder::className(), ['user_id' => 'id'])->select('course_id');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoursesPOrdersId()
    {
        return $this->hasMany(CoursesOrder::className(), ['user_id' => 'id'])->select('p_course_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDropOrderClients()
    {
        return $this->hasMany(DropOrder::className(), ['client_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDropOrderUsers()
    {
        return $this->hasMany(DropOrder::className(), ['user_id' => 'id']);
    }


    public function getOrdersSum()
    {
        return $this->hasMany(DropOrder::className(), ['user_id' => 'id'])->sum('commission_full');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCourses()
    {
        return $this->hasMany(UserCourses::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLessons()
    {
        return $this->hasMany(UserLessons::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebinarChats()
    {
        return $this->hasMany(WebinarChats::className(), ['user_id' => 'id']);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }


    public function setPasswordHash($password){
        return Yii::$app->security->generatePasswordHash($password);
    }




    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptRequests()
    {
        return $this->hasMany(OptRequest::className(), ['user_id' => 'id']);
    }
    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    public static function findByPhone($phone){
        return static::findOne(['phone' => $phone, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email){
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    public static function checkUserEmailPhone($email, $phone){
        return static::find()->where(['email'=>$email])->orWhere(['phone' => $phone])->where([ 'status' => self::STATUS_ACTIVE])->one();
    }

    public static function checkEmailPhone($data){
        return static::find()->where(['email'=>$data])->orWhere(['phone' => $data])->where([ 'status' => self::STATUS_ACTIVE])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }


    public function setRandomUsername($username){
        $this->username = $username;
    }

    public function setRandomEmail($email){
        $this->email = $email;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }



    /**** new methods */


    public function addClient(){

    }

    /**
     * @return bool
     */
    public function setUser(){
        if(Yii::$app->user->can('supermanager')){
            $this->generateAuthKey();
            $this->setPassword($this->password_new);

        }
        if($this->save()){
            $role = $this->setUserRole();
            if($this->status_drop == MainModel::STATUS_USER_DROP_ACTIVE){
                if(empty($this->crm_name)){
                    if($crm = $this->addUserDroperToCrm()){
                        $this->crm_name = $crm;
                    }
                }
                if(empty($this->crm_name_new)){

                    if($crm_new = ShopCrm::callMethod([
                        'name'=>$this->name,
                        'email'=>$this->email,
                        'phone'=>$this->phone,
                    ])){
                        $this->crm_name_new = $crm_new->username;
                    }else{
                        myHellpers::show($crm_new);
                    }

                }
                self::checkOrders();

            }
           return $this->save();
        }

    }


    public function checkOrders(){
        if($orders = DropOrder::getUserOrdersWait($this->id)){
            $send = new SendDropOrder($this, $orders);
            $send->sendToCrmFromUser();
        }

    }

    /**
     * @return bool
     */
    protected function addUserDroperToCrm(){

        $retailCrm = Yii::$app->retailCrm;
        $u['code'] = '';
        $u['elements'] = '';
        //$retailCrm
        $old = $retailCrm->client->request->customDictionariesGet('managers');
        $old = $old['customDictionary']['elements'];
        $code = myHellpers::translit($this->name) . '_drop-'.$this->id;
        $name = $this->name . ' '.$this->id.'-дроп';
        $new = [
            'name'=> $name,
            'code' =>  $code,
            'ordering'=> 1,
        ];
        if(!myHellpers::findArray($old,$code, ['code'])){ array_unshift ($old, $new); }

        $req = $retailCrm->client->request->customDictionariesEdit([
            'code' =>  'managers',
            'name'=> 'Дропшипeры',
            'elements' => $old,
        ]);

        return ($req->success==1) ? $code : false ;
    }

    public function setUserRole(){

        if(!$role_assigment = AuthAssignment::findUser($this->id)){
            $role_assigment = new AuthAssignment();
            $role_assigment->user_id = strval($this->id);
        }
        $role_assigment->item_name = $this->status_role;
        if($role_assigment->validate()){
           if($role_assigment->save()){
               return $role_assigment;
           }
        }

        return false;
    }

    public function trash(){
        $this->status = MainModel::STATUS_DELETED;
        return $this->save();
    }


    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }


    public function getLastLesson(){
        if($course = CoursesHellper::getLastLessonUser()){
            return $course;
        }else{
            return null;
        };

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHomeworks()
    {
        return $this->hasMany(UserHomework::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserMessages()
    {
        return $this->hasMany(UserMessages::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles()
    {
        return $this->hasMany(UserProfile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserStepLessons()
    {
        return $this->hasMany(UserStepLesson::className(), ['user_id' => 'id']);
    }


}
