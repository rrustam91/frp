<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.08.2018
 * Time: 11:04
 */

namespace common\models\user;


use common\helpers\myHellpers;
use common\models\rbac\AuthAssignment;
use common\models\user\User;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;


class UserAuth extends User  implements IdentityInterface
{
    public $group;


    const STATUS_ACTIVE = 10;
    const STATUS_EDIT = 5;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETED = -1;

    const ROLE_ADMIN = 10;
    const ROLE_MODER = 5;
    const ROLE_SUBSCRIBE = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }



    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    public function setUserRole(){
        if(!$role_assigment = AuthAssignment::findUserRole($this->id, $this->status_role)){

            $role_assigment = new AuthAssignment();
            $role_assigment->user_id = strval($this->id);
            $role_assigment->item_name = $this->status_role;

        }
        $role_assigment->item_name = $this->status_role;

        if($role_assigment->validate()){
            if($role_assigment->save()){
                return $role_assigment;
            };
            return false;
        }else{
            myHellpers::show($role_assigment->errors);
        }


        return false;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($login)
    {
        return static::findOne(['login' => $login, 'status' => self::STATUS_ACTIVE]);
    }
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByPhone($phone)
    {
        return static::findOne(['phone' => $phone, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }



    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }


    public function addSubscribe($data){
        if(!$user = self::findByEmail($data['email'])){
            $this->username = myHellpers::generateRandomWord();
            $this->email = $data['email'];
            $this->name = $data['name'];
            $this->phone = $data['phone'];
            $this->utm = serialize($data['utm']);
            $this->password_hash = $this->setPasswordHash('passToFPR');
            self::generateAuthKey();

            $this->role = self::ROLE_SUBSCRIBE;

            $this->roles = "Зарегистрировался на вебинар";
            $this->status = self::STATUS_ACTIVE;
            return $this->save();
        }
        return false;
    }
}