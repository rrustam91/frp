<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.08.2018
 * Time: 9:47
 */

namespace common\models\user;
use common\helpers\myHellpers;
use common\models\MainModel;
use common\models\rbac\AuthAssignment;
use Yii;
use common\models\user\User;


class UserAdd extends User
{
 
    public static function findUser($email, $phone){
        if(!$u = self::find()->where(['email'=>$email,'status'=>self::STATUS_ACTIVE])->one()){
            return self::find()->where(['phone'=>$phone,'status'=>self::STATUS_ACTIVE])->one();
        }
        return $u;
    }
    public function addSubscribe($data){
        if(!$user = self::findByEmail($data['email'])){
            $this->username = myHellpers::generateRandomWord();
            $this->email = $data['email'];
            $this->name = $data['name'];
            $this->phone = $data['phone'];
            $this->utm = serialize($data['utm']);
            $this->password_hash = $this->setPasswordHash('passToFPR');
            self::generateAuthKey();

            $this->role = self::ROLE_SUBSCRIBE;
            $this->status = self::STATUS_ACTIVE;
            return $this->save();
        }
        return false;
    }

    public function setUser(){
        if(Yii::$app->user->can('supermanager')){
            $this->generateAuthKey();
            $this->setPassword($this->password_new);
        }
        if($this->save()){
            $role = $this->setUserRole();
            if($this->status_drop == MainModel::STATUS_USER_DROP_ACTIVE){
                if(empty($this->crm_name)){
                    $crm = self::addUserDroperToCrm();
                    $this->crm_name = $crm;
                }
            }

            die();
            return $this->save();
        }

    }



    public static function getType($type){
        $t = 'undefined';
        if($type == 1){
            $t = 'drop';
        }else if($type == 2){
            $t =  'opt';
        }
        return $t;


    }

}