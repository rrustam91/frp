<?php

namespace common\models\user;

use common\helpers\myHellpers;
use common\models\courses\CourseCategory;
use common\models\courses\Courses;
use common\models\courses\CoursesLessons;
use common\models\MainModel;
use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "user_homework".
 *
 * @property int $id
 * @property int $user_id
 * @property int $lesson_id
 * @property int $course_id
 * @property int $category_id
 * @property int $status
 * @property string $text
 * @property string $comment_teach
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CourseCategory $category
 * @property Courses $course
 * @property CoursesLessons $lesson
 * @property User $user
 */
class UserHomework extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_homework';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'lesson_id', 'course_id', 'category_id'], 'required'],
            [['user_id', 'lesson_id', 'course_id', 'category_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['text', 'comment_teach'], 'string'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CourseCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => CoursesLessons::className(), 'targetAttribute' => ['lesson_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'lesson_id' => Yii::t('app', 'Lesson ID'),
            'course_id' => Yii::t('app', 'Course ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'status' => Yii::t('app', 'Status'),
            'text' => Yii::t('app', 'Text'),
            'comment_teach' => Yii::t('app', 'Comment Teach'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(CoursesLessons::className(), ['id' => 'lesson_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getHomeWork($lesson_id){
        if($model = self::find()->where(['lesson_id'=>$lesson_id,'user_id'=>Yii::$app->user->id])->one()){
            return $model;
        }else{
            return new self();
        }
    }

    public function addHomeWork(){
       $this->user_id = Yii::$app->user->id;
       return $this->save();
    }

    public static function updateHomework(){

    }
}
