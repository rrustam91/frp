<?php

namespace common\models\user;

use common\helpers\myHellpers;
use common\models\courses\CourseCategory;
use common\models\courses\Courses;
use common\models\courses\CoursesLessons;
use common\models\MainModel;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_step_lesson".
 *
 * @property int $id
 * @property int $user_id
 * @property int $lesson_id
 * @property int $course_id
 * @property int $course_category_id
 * @property int $step
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CourseCategory $courseCategory
 * @property Courses $course
 * @property CoursesLessons $lesson
 * @property User $user
 */
class UserStepLesson extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_step_lesson';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'lesson_id', 'course_id', 'course_category_id', 'step'], 'required'],
            [['user_id', 'lesson_id', 'course_id', 'course_category_id', 'step', 'status', 'created_at', 'updated_at'], 'integer'],
            [['course_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CourseCategory::className(), 'targetAttribute' => ['course_category_id' => 'id']],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['lesson_id'], 'exist', 'skipOnError' => true, 'targetClass' => CoursesLessons::className(), 'targetAttribute' => ['lesson_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'lesson_id' => Yii::t('app', 'Lesson ID'),
            'course_id' => Yii::t('app', 'Course ID'),
            'course_category_id' => Yii::t('app', 'Course Category ID'),
            'step' => Yii::t('app', 'Step'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseCategory()
    {
        return $this->hasOne(CourseCategory::className(), ['id' => 'course_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(CoursesLessons::className(), ['id' => 'lesson_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveLesson()
    {
        return $this->hasOne(CoursesLessons::className(), ['id' => 'lesson_id'])->where(['opened'=>self::STATUS_LESSON_ACTIVE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


}
