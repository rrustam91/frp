<?php
/**
 * Created by PhpStorm.
 * User: Rustam
 * Date: 02.09.2017
 * Time: 12:46
 */

namespace common\helpers;


use common\models\Category;
use common\models\dropship\OrdersStatus;
use dosamigos\transliterator\TransliteratorHelper;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;
use common\models\MainModel;


class myHellpers
{
    public static function show($value = '',$text = '',$v = 'print')
    {
        echo "<pre>";

            echo " ==========>> ";
            print_r($text);
            echo " <<========== ";
            echo "<br>";
            ($v != 'print') ? print_r($value) : var_dump($value);;

        echo '</pre>';
    }

    public static function code($value,$text = '')
    {
        echo "<code>";
            print_r($value);
        echo '</code>';
    }

    public static function isEmail($email){
        $_p = "/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i";
        return preg_match($_p, $email);
    }
    public static function isPhone($phone){

    }
    public static function generateRandomWord($len = 10) {
        $word = array_merge(range('a', 'z'), range('A', 'Z'));
        shuffle($word);
        return substr(implode($word), 0, $len);
    }
    public static function getUtm($http_referer){
        $output = [];
        $referrer = urldecode(iconv("utf-8", "windows-1251", $http_referer));
        $url = substr($referrer, 0, strrpos($referrer, '/?'));
        $utm = str_replace('/?', '', substr($referrer, strrpos($referrer, '/?')));
        parse_str($utm, $output);
        return $output;
    }

    public static function encodeJson($ref){
        return json_encode($ref, JSON_UNESCAPED_UNICODE);
    }
    public function decodeJson($ref){
        return json_decode($ref, JSON_UNESCAPED_UNICODE);
    }

    public static function unsrl($msg){
        return unserialize($msg);
    }

    public static function setMeta($title = null, $keywords = null, $description = null){

        //if(empty($title)) $title = \Yii::$app->siteconfig->get('SITE_TITLE');
        if(empty($keywords)) $keywords = \Yii::$app->siteconfig->get('SITE_DESCRIPTION');
        if(empty($description)) $description = \Yii::$app->siteconfig->get('SITE_KEYWORDS');
        (empty($title)) ? $title = "" : $title =  $title . ' | ';
        \Yii::$app->view->title = $title . \Yii::$app->siteconfig->get('SITE_TITLE');
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
    }

    public static function getStatusLabel($status, $status_info = [], $badge = MainModel::STATUS_BADGE ){
        $class = ArrayHelper::getValue($badge, $status);
        $status = ArrayHelper::getValue($status_info, $status);
        $badge = "<span class='{$class}'>{$status}</span>";
        return $badge;
    }


    public static function getStatusLabelDropOrder($status, $type = 'drop', $badge = MainModel::STATUS_LIST_DROP_ORDERS_BADGE ){
        //$status = OrdersStatus::find()->where(['code'=>$status])->one()->name;

        //myHellpers::show($status);
/*        $class = ArrayHelper::getValue($badge, $status);
        $status = ArrayHelper::getValue(MainModel::STATUS_LIST_DROP_ORDERS, $status);
        myHellpers::show($class);
        myHellpers::show($status);*/
        //$badge = "<span class='{$class}'>{$status}</span>";
        return '';
    }


    /**
     * @param $ar - сам большой массив по которому осуществляется поиск
     * @param $findValue -  Value который надо найти (в примере ANTONOVKA)
     * @param $executeKeys - массив с перечнем ключей $executeKeys = array ('name1','name2','name3','name4','name5')
     * @return array
     */
    public static function findArray ($ar, $findValue, $executeKeys){
        $result = array();

        foreach ($ar as $k => $v) {
            if (is_array($ar[$k])) {
                $second_result = self::findArray($ar[$k], $findValue, $executeKeys);
                $result = array_merge($result, $second_result);
                continue;
            }
            if ($v === $findValue) {
                foreach ($executeKeys as $val){
                    $result[] = $ar[$val];
                }

            }
        }
        return $result;
    }

    public static function checkArray($ar=[], $key, $val ){

        foreach ($ar as $i){
            if($i->$key == $val){
                return true;
            }
        }
    }


    public static function translit($alias){
        return Inflector::slug( TransliteratorHelper::process( $alias ), '-', true );
    }
}