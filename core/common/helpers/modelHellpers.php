<?php
/**
 * Created by PhpStorm.
 * User: Rustam
 * Date: 02.09.2017
 * Time: 12:46
 */

namespace common\helpers;


use common\models\courses\Courses;
use common\models\MainModel;
use common\models\products\ProductsCategories;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\products\Products;
use yii\helpers\ArrayHelper;


class modelHellpers
{
    public static function getSelectList($class, $from = 'id', $to = 'name')
    {
        return ArrayHelper::map($class::find()->all(), $from, $to);
    }

    public static function getSelectListWithParent($class, $from = 'id', $to = 'name'){
        $_d =[];
        $_m = $class::find()->select([
            'id',
            'name',
            'alias',
            'parent_id'
        ])->indexBy('id')->orderBy(['parent_id'=>SORT_ASC])->asArray()->all();
        $tree = [];
        $tree = self::form_tree($_m);

        return $tree;
    }

    protected static function form_tree($mess)
    {
        if (!is_array($mess)) return false;
        $tree = array();
        foreach ($mess as $value) {
            if(!empty($value['parent_id'])){
                $tree[$value['parent_id']][$value['id']] = $value['name'];
            }
        }
        return $tree;
    }

    //$parent_id - какой parentid считать корневым
    //по умолчанию 0 (корень)
    protected static function build_tree($cats, $parent_id)
    {
        if (is_array($cats) && isset($cats[$parent_id])) {
            $tree = '<ul>';
            foreach ($cats[$parent_id] as $cat) {
                $tree .= '<li>' . $cat['name'];
                $tree .= self::build_tree($cats, $cat['id']);
                $tree .= '</li>';
            }
            $tree .= '</ul>';
        } else {
            return false;
        }
        return $tree;
    }


    public static function getDropSelectList($class, $from = 'id', $to = 'name')
    {
        return ArrayHelper::map($class::find()->where(['status_drop'=>MainModel::STATUS_ACTIVE])->all(), $from, $to);
    }
    public static function getChildrenRecursive($id, $children = [], $with_parent = true)
    {
        if($model = Courses::find()->where(['parent_id' => $id])->all()){
            foreach ($model as $item) {
                $children[] = $item->id;
                $children = self::getChildrenRecursive($item->id, $children);
            }
            return $children;
        }
    }


    public static function getDropProducts($from = 'id', $to = 'xml_id')
    {
        return ArrayHelper::map(
            Products::find()
                ->where(['status_drop' => self::STATUS_PRODUCT_DROP_ACTIVE])
                ->all(),
            $from, $to);
    }
    public static function getDropProductsCat($from = 'id', $to = 'xml_id')
    {
        return ArrayHelper::map(
            ProductsCategories::find()
                ->where(['status_drop' => self::STATUS_PRODUCT_DROP_ACTIVE])
                ->all(),
            $from, $to);
    }

    public static function getModelByName($class, $name)
    {
        $model = $class::findOne(['name' => $name]);
        if (isset($model)) {
            return $model->id;
        }
        return 1;
    }


    public static function setArrayToString($array, $in = 'id', $to = 'name')
    {
        return implode(',', ArrayHelper::map($array, $in, $to));
    }

    //метод для автоматического определения глубины категории, основывается на depth родительской категории
    protected function setDepth()
    {
        if ($this->parent_id != 0) {
            $this->depth = self::findOne($this->parent_id)->depth + 1;
        } else {
            $this->depth = 0;
        }
    }

    //метод для автоматического определения урла категории, основывается на урл родительской категории и алиас категории
    protected function setPath()
    {
        //todo сделать норм
        if ($this->parent_id == 0 || $this->parent_id == 1) {
            $this->path = $this->alias;
        } else {
            $this->path = self::findOne($this->parent_id)->path . '/' . $this->alias;
        }
    }
}