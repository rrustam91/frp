<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@cp', dirname(dirname(__DIR__)) . '/cp');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('@uploads', dirname(dirname(dirname(__DIR__))) . '/public_html/uploads');

Yii::setAlias('@admin_uploads', dirname(dirname(dirname(__DIR__))) . '/public_html/admin/uploads');

Yii::setAlias('@cp_uploads', dirname(dirname(dirname(__DIR__))) . '/public_html/cp/uploads');
