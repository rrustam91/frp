<?php
$params = array_merge(
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);


return [

    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@common/runtime/cache',
        ],
        'image' => [
            'class' => 'yii\image\ImageDriver',
            'driver' => 'Imagick',
        ],
        'user' => [

            'class' => 'yii\web\User',
            'identityClass' => 'common\models\Auth',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity', 'httpOnly' => true, 'domain' => $params['cookieDomain']],
        ],
        'session' => [
            'name' => '_session',
            'cookieParams' => [
                'domain' => $params['cookieDomain'],
                'httpOnly' => true,
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\PhpManager'
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'ru',
                    'fileMap' => [
                        'app'       => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],

        'bot' => [
            'class' => 'SonkoDmitry\Yii\TelegramBot\Component',
            'apiToken' => '602854001:AAG-Z5rPqvnh0ZiaNtJDaiAXtbvqWxeUFCY',
        ],
        'telegram' => [
            'class' => 'aki\telegram\Telegram',
            'botToken' => '648690691:AAEALsuNN54fSPFns7TcbGVnC_0zrbXFDKM',
        ],
        'sendpulse' => [
            'class' => \sevenfloor\sendpulse\SendPulse::className(),
            'userId' => '36cbd7967d3239cc44a6aaeb6897bd6a',
            'secret' => 'fb187fe0ddb27c962fcece1a0b04d868',
            'storageType' => 'session'
        ],
    ],
];
