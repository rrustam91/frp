<?php
return [
    'Save'=> 'Сохранить',

    'Send Reset'=> 'Сбросить пароль',

    'Projects'=> 'Проекты',
    'Project'=> 'Проект',
    'Project ID'=> 'Проект',
    'ID'=> 'ID',
    'Name'=> 'Название',
    'Title'=> 'Заголовок',
    'Status'=> 'Статус',
    'Date Start'=> 'Дата начала',
    'Date End'=> 'Дата завершения',
    'Url'=> 'Ссылка',
    'Img'=> 'Изображение',
    'Created At'=> 'Создана',
    'Updated At'=> 'Отредактированно',
    'Video Url'=> 'Ссылка на видео',
    'Alias'=> 'Путь',
    'User ID'=> 'ID Пользователя',
    'Time Video'=> 'Время видео',
    'Time Send'=> 'Время отправки',
    'Login'=> 'Логин',
    'Phone'=> 'Телефон',
    'Email'=> 'Е-майл',
    'Role'=> 'Роль',
    'User Name'=> 'Имя',
    'Text'=> 'Текст',
    'Search'=> 'Найти',
    'Reset'=> 'Сбросить фильтр',
    'Update {modelClass}: '=> 'Редатирование: ',
    'Price'=> 'Цена',
    'Price Opt'=> 'Цена для оптов',
    'Price Drop'=> 'Цена для дропшипа',

    'Status Drop' => 'Статус для дропа',
    'Status Opt' => 'Статус для опта',
    'Content' => 'Контент',
    'Image Files' => 'Изображение',

    'Delivery Types' => 'Тип доставки',
    'Create Delivery Type' => 'Новый тип доставки',
    'Create Delivery Types' => 'Новый тип доставки',
    'Delivery Price' => 'Цена доставки',
    'Remember Me'=> 'Запомнить меня',
    'Email Or Phone'=> 'Е-майл или телефон',
    'Webinar Nickname'=> 'Ваше имя',

    /**** Webinar translate ****/

    'Webinars'=> 'Вебинары',
    'Create Webinars'=> 'Новый вебинар',
    'Update Webinars'=> 'Редактировать вебинар',
    'Webinar ID'=> 'ID Вебинара',
    'Select Webinar'=> 'Выберите Вебинар',
    'Time Start'=> 'Время старта ',
    'Webinar Ads'=> 'Баннеры/Страницы',
    'Create Webinar Ads'=> 'Новый банер',
    'Webinar Chats' => 'Вопрос от пользователей',
    'Create Webinar Chats' => 'Новый вопрос',
    'Webinar Comments'=> 'Авто-чат на вебинаре',
    'Create Webinar Comments'=> 'Новое сообщение',
    /*** ***/
    'Text chat'=>'Тест сообщения',
    /*** Select ***/
    'Select user'=>'Выберите пользователя',

    /***** Users ********/
    'Users' => 'Пользователи',
    'Create User' => 'Новый пользователь',
    'Roles' => 'Роли',
    'User Name'=> 'ФИО пользователя',
    'User Email'=> 'Емайл пользователя',
    'User Phone'=> 'Телефон пользователя',
    'User Password'=> 'Новый пароль',
    'User Password Confirm'=> 'Повторите пароль',
    'User name'=> 'Имя пользователя',
    'User name viewer'=> 'Имя участика',
    'User Status Role'=> 'Роль пользователя',


    'Select User '=> 'Выберите пользователя',

    /** */
    'Products' => 'Товары',
    'Create Products' => 'Новый товар',
    'Create Products Categories' => 'Новая категория товаров',
    'Products Stocks' =>'Склад товаров',
    'Create Products Stock' =>'Новый товар',
    'Product ID' =>'Товар',
    'Count' =>'Количество',
    /**** Delivery order sample****/

    'Client delivery'=> 'ФИО клиента',
    'Phone delivery'=> 'Контактный номер клиента',
    'Delivery Type ID'=> 'Способ доставки',

    'Comment delivery'=> 'Комментарий клиента',
    'Products delivery'=> 'Товар',
    'region delivery'=> 'Регион',
    'city delivery'=> 'Город',
    'subway delivery'=> 'Метро',
    'street delivery'=> 'Улица',
    'zipcode delivery'=> 'Почтовый индекс',
    'building delivery'=> 'Дом',
    'flat delivery'=> 'Квартира',
    'block delivery'=> 'Строение/Корпус',
    'house delivery'=> 'Подъезд',
    'floor delivery'=> 'Этаж',
    'note delivery'=> 'Доп. информация',
    'cost delivery'=> 'Стоимость доставки',


    /**** ****/
    'Create Projects'=> 'Создать проект',
    'Update'=> 'Редактировать',

    'Update Projects: '=> 'Редактировать проект',
    'Delete'=> 'Удалить',
    'Are you sure you want to delete this item?'=> 'Вы точно хотите удалить ?',

    'Ads'=> 'Рекламный баннер',
    'Create Ads'=> 'Создать баннер',
    'Update Ads'=> 'Редактировать баннер',

    'Comments'=> 'Авто-сообщения',
    'Create Comments'=> 'Создать авто-сообщение',
    'Update Comments'=> 'Ред. авто-сообщение',


    /***** */

    'Create New Drop Order' => 'Новая заявка',
    'My Drop Orders' => 'Ваши заявки',
    'My Drop Statistics' => 'Ваши заявки',
    'My Panel' => 'Моя статистика',
    'Create New Drop' => 'Создать',
    'View' =>'Подробнее',
    'Create Drop Order' => 'Новая заявка',
    'Drop Order' => 'Заявка',
    'Drop Products' => 'Товары',
    'Drop Orders' => 'Заявки',
    'Drop Products Categories' => 'Категории товаров',
    'Products Categories' => 'Категории товаров',
    'User Messages' => 'Ваши сообщения',
    'Create User Messages' => 'Новое сообщение',
    'User Message Type' => 'Выберите тип сообщения',

    /*** */
    'Content' => 'Контент',
    'Parent ID'=> 'Родительская',
    'Opened'=> 'Открыта',
    'Step By'=> 'Последовательность',
    'Course ID'=> 'Курс',
    'Category ID'=> 'Категория',
    /*** ***/

    'Courses'=> 'Курсы',
    'Courses Orders'=> 'Заявки по курсам',
    'Create Courses'=> 'Новый курс',
    'Create Courses Order'=> 'Новая заявка',
    'Course Parent ID'=> 'Основной курс',
    'Course Content'=> 'Контент',
    'Courses Lessons'=> 'Уроки курсов',
    'Create Courses Lessons'=> 'Новый урок',
    'Course Categories'=> 'Модули курсов',
    'Create Course Category'=> 'Новый модуль',





];

