<?php

use yii\helpers\Html;
use kartik\grid\GridView;

//use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\datetime\DateTimePicker;
use common\models\dropship\DropOrder;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\dropship\search\DropOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My Drop Orders');
$this->params['breadcrumbs'][] = $this->title;
$cnt = 0;
$pay_cmp = 0;
$pay_wait = 0;
if(!Yii::$app->user->isGuest){

    $full_com = sprintf('%0.2f', (Yii::$app->user->getIdentity()->ordersSum));

    //$pay_cmp = Yii::$app->user->getIdentity()->ordersSum;
    //$pay_wait = Yii::$app->user->getIdentity()->ordersSum;

    $cnt = DropOrder::getUserCountOrder(Yii::$app->user->id);
}

?>

<div class="box-body">
    <div class="box-group" id="accordion">
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <div class="panel box box-primary ">
            <div class="box-header with-border no-pd" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                <h4 class="box-title" >
                    Фильтр
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                <div class="box-body row   ">
                    <div class="col-md-4">
                        <?
                        $addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;
                        echo '<div class="input-group drp-container">';
                        echo \kartik\daterange\DateRangePicker::widget([
                                'name'=>'kvdate3',
                                'value' => '2018-10-04 - 2018-11-14',
                                'useWithAddon'=>true,
                                'convertFormat'=>true,
                                'startAttribute' => 'from_date',
                                'endAttribute' => 'to_date',
                                'pluginOptions'=>[
                                    'locale'=>['format' => 'Y-m-d'],
                                ]
                            ]) . $addon;
                        echo '</div>';
                        ?>



                    </div>
                    <div class="col-md-4">
                        <?
                        echo DateTimePicker::widget([
                            'name' => 'dp_1',
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'd-m-Y h:i'
                            ]
                        ]);?>

                    </div>

                    <div class="col-md-4">
                        <div class="btn bg-navy">применить фильтр</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa  fa-drivers-license-o"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Заказы</span>
                <span class="info-box-number">
                    <?=$cnt ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>

        <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Коммисия Выплаченно</span>
                <span class="info-box-number">
                   <span class="price-num">
                       <?= $pay_cmp ?>
                    </span>
                    <i class="fa fa-rub" style="font-size: 20px; font-weight: 300"></i>

                </span>
            </div>
            <!-- /.info-box-content -->
        </div>

        <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Коммисия к выплате</span>
                <span class="info-box-number">
                   <span class="price-num">
                       <?= $pay_wait ?>
                    </span>
                    <i class="fa fa-rub" style="font-size: 20px; font-weight: 300"></i>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>


        <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green disabled "><i class="fa fa-percent"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Коммисия Всего </span>
                <span class="info-box-number">
                  <span class="price-num">
                        <?= $full_com?>
                    </span>
                    <i class="fa fa-rub" style="font-size: 20px; font-weight: 300"></i>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>


        <!-- /.info-box -->
    </div>
</div>
<div class="row">
    <?
    if(!Yii::$app->user->getIdentity()->crm_name){
        ?>
        <div class="col-md-12">
            <div class="callout callout-danger ">
                <h4>Ваш аккаунт не активен</h4>

                <p>
                    Ожидайте Ваш аккаунт проходит модерацию
                </p>
            </div>
        </div>
    <?}?>
    <div class="col-md-7">

        <?if(empty(!$model->crm_name)){?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>
                    Добро пожаловать в FankRetailPartners
                </h4>
                Ваш аккаунт активен, для  начала работы
                <a href="/orders/new">
                    добавьте заказ
                </a>
            </div>
        <?}?>
    </div>
</div>
<div class="drop-order-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create New Drop Order'), ['new'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>

    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <? /*= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute'=>'crm_id',
                    'label'=>'Номер заказа',
                ],
                [
                    'attribute'=>'products',
                    'value'=>function ($model, $key, $index, $widget) {

                        $items = $model->list;
                        $list = '';
                        foreach ($items as $item){
                            $list .= $item->product_name . ', кол-во:'.$item->quantity.'<br>';
                        }
                        return $list;

                    },

                    'format'=>'html',
                    'label'=>'Список товаров',
                ],
               /* [
                    'attribute'=>'quantity',
                    'value'=>function ($model, $key, $index, $widget) {
                        $items = $model->list;
                        $q = 0;
                        foreach ($items as $item){
                            $q += $item->quantity;
                        }
                        return $q;

                    },

                    'format'=>'html',
                    'label'=>'Общ. кол-во'
                ],* /
                [
                    'attribute'=>'total_price',
                    'value'=>function ($model, $key, $index, $widget) {
                        $items = $model->list;
                        $price = 0;
                        foreach ($items as $item){ $price += $item->price_total; }
                        return $price . ' руб.';
                    },
                    'label'=>'Общая стоимость',
                ],
                [
                    'attribute'=>'commission_full',
                    'value'=>function ($model, $key, $index, $widget) {


                        return $model->commission_full . ' руб.';
                    },

                    'label'=>'Коммисия за заказ',
                    'format'=>['html'],
                ],
                [
                    'label'=>'Статус',
                    'attribute'=>'status_crm_drop',
                    'value'=> function ($model){
                        return "<span class=\"pull-right\"><span class=\"label   bg-flat bg-green\">".  $model->statusCrmDrop->name."</span>";
                    },
                    'format'=>'html',
                    'filter' => \common\models\MainModel::STATUS_LIST_DROP_ORDERS,




                ],

                [

                    'attribute' => 'created_at',
                    'value' => 'created_at',
                    'format' => 'date',

                    'filter' =>   DateTimePicker::widget([
                        'name' => 'created_at',
                        'type' => DateTimePicker::TYPE_INPUT,
                        'pluginOptions' => [
                        //    'autoclose'=>true,
                            'format' => 'd-m-Y h:i'
                        ]
                    ]),

                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} ',
                    'buttons' => [
                        'view' => function ($url,$model) {
                            return Html::a('<span class="fa fa-search"></span> Подробнее', $url, [
                                'title' => Yii::t('app', 'View'),
                                'id'=>'',
                                'class'=>'btn bg-blue btn-xs  btn-flat',
                            ]);
                        },

                    ],
                ],
            ],
        ]); */?>
        <?= GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>false,
            'striped'=>true,
            'hover'=>true,
            'export'=>false,
            //'panel'=>['type'=>'info', 'heading'=>'Таблица ваших заявок'],
            'columns'=>[

                [
                    'class'=>'kartik\grid\SerialColumn',
                    'hAlign'=>'center',
                ],

                [
                    'attribute'=>'crm_id',
                    'width'=> '50px',
                    'filter'=>false,
                    'label'=>'Номер заказа',
                    'hAlign'=>'center',
                ],
                [
                    'attribute'=>'products',
                    'value'=>function ($model, $key, $index, $widget) {

                        $items = $model->list;
                        $list = '';
                        foreach ($items as $item){
                            $list .= $item->product_name . ', кол-во:'.$item->quantity.'<br>';
                        }
                        return $list;

                    },

                    'format'=>'html',
                    'label'=>'Список товаров',

                    'width'=>'300px',
                ],
                [
                    'attribute'=>'commission_full',
                    'value'=>function ($model, $key, $index, $widget) {


                        return $model->commission_full;
                    },

                    'width'=>'200px',
                    'label'=>'Коммисия за заказ',
                    'width'=>'150px',
                    'hAlign'=>'right',
                    'pageSummary'=>true,
                    'format'=>['decimal'],
                ],
                [
                    'attribute'=>'status_crm_drop',
                    'value'=> function ($model){
                        return "<span class=\"pull-right\"><span class=\"label   bg-flat bg-green\">".  $model->statusCrmDrop->name."</span>";
                    },
                    'format'=>'html',

                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>\common\models\MainModel::getDropCategories('id','name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'format'=>'html',
                    'filterInputOptions'=>['placeholder'=>'Название категории'],
                    'width'=>'50px',
                    'hAlign'=>'center',
                    'label'=>'Статус',


                ],
                [
                    'attribute'=>'created_at',
                    'format'=>'datetime',
                    'width'=>'250px',
                    'filter'=>'',
                    'filterType'=>GridView::FILTER_DATE_RANGE,

                    'filterWidgetOptions'=>[
                        'name'=>'date_range_3',
                        'convertFormat'=>true,
                        'hideInput'=>true,
                        'pluginOptions'=>[
                            'timePickerIncrement'=>15,
                            'locale'=>[
                                'format'=>'Y-m-d',
                                'separator'=>' / ',
                            ],
                        ],

                        'options'=>['class'=>'drp-container form-group']
                    ],
                    'hAlign'=>'center'
                ],


                [

                    'class' => '\kartik\grid\ActionColumn',
                    'hiddenFromExport' => true,
                    'template' => '{view}',
                    'buttons' => [

                        //view button
                        'view' => function ($url, $model) {
                            return Html::a('<span class="fa fa-search"></span> Подробнее', $url, [
                                'title' => Yii::t('app', 'View'),
                                'id'=>'',
                                'class'=>'btn bg-blue btn-xs  btn-flat',
                            ]);
                        },
                    ],

                ],

            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>



<script>
    var prices = document.querySelectorAll(".price-num");
    Array.from(prices).forEach(function(elem){
        elem.innerHTML = elem.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    });
</script>