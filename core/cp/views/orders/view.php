<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\dropship\DropOrder */

$this->title = 'Заказ: '.$model->crm_id ;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drop Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title ;


?>



<section class="content">

    <div class="row">
        <div class="col-md-12">

        </div>
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">


                    <h3 class="profile-username text-center">Заказ: <?= $model->crm_id?>А</h3>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Фио Покупатель</b> <span class="pull-right"><?=$model->client->name?></span>
                        </li>
                        <li class="list-group-item">
                            <b>Конт. номер</b> <span class="pull-right"><?=$model->client->phone?></span>
                        </li>
                        <li class="list-group-item">
                            <b class="">Статус</b> <span class="pull-right"><span class="label label-info"><?=$model->statusCrmDrop->name?></span></span>
                        </li>

                        <li class="list-group-item">
                            <b>Трек номер: </b> <span class="pull-right"><?=$model->track_number ?></span>
                        </li>
                        <br><br>

                        <li class="list-group-item">
                            <b>Сумма заказа</b> <span class="pull-right"><?=$model->price_new_total?> руб.</span>
                        </li>
                        <li class="list-group-item">
                            <b>Сумма коммисси</b> <span class="pull-right"><?=$model->commission_full?> руб.</span>
                        </li>

                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#timeline" data-toggle="tab" aria-expanded="false">Доставка</a></li>
                    <li class=""><a href="#activity" data-toggle="tab" aria-expanded="true">Выбранные товары</a></li>
                    <li class=""><a href="#settings" data-toggle="tab" aria-expanded="false">Доп. инфа</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane " id="activity" style="min-height: 500px;">

                        <div class="table-responsive">
                            <table class="table table-hover product__table product__table--drop " id="listProducts">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Артикул</th>
                                    <th>Наименование товара</th>
                                    <th>Цена товара закупки</th>
                                    <th>Цена товара</th>
                                    <th align="center">Озвученная цена</th>
                                    <th align="center">Кол-во</th>

                                    <th align="center">Общая сумма</th>

                                </tr>
                                </thead>


                                <tbody class="dropProductListTable">
                                <?if($items = $model->list){
                                    $full_price = 0;
                                    foreach ($items as $item){
                                        ?>
                                        <tr>

                                            <td>
                                                <?=
                                                $item->product->article
                                                ?>
                                            </td>
                                            <td>
                                                <?=
                                                $item->product_name
                                                ?>
                                            </td>
                                            <td>
                                                <?=
                                                $item->product->price_drop
                                                ?>
                                            </td>
                                            <td>
                                                <?=
                                                $item->product->price
                                                ?>
                                            </td>

                                            <td>
                                                <?=
                                                $item->price_new
                                                ?>
                                            </td>
                                            <td>
                                                <?=
                                                $item->quantity
                                                ?>
                                            </td>

                                            <td>
                                                <?=
                                                $item->price_new_total
                                                ?>
                                            </td>
                                        </tr>
                                        <?
                                    }?>
                                    <tr>
                                        <td colspan="12">
                                            <h4 align="center">
                                                <b>
                                                    Общая сумма заказа: <span class="price-num"><?=$model->price_new_total?> </span>руб.
                                                </b>
                                            </h4>
                                        </td>
                                    </tr>
                                    <?

                                }else{?>
                                    <tr >
                                        <td >
                                            <div class="empty-table">
                                                Товар не выбран
                                            </div>
                                        </td>
                                    </tr>
                                <?};?>

                                </tbody>
                            </table>

                        </div>

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane active" id="timeline" style="min-height: 500px;" >
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <b>Тип доставки: </b> <span class="pull-right">
                                         <?=($model->delivery->delivery_type_id) ?$model->delivery->type->name : 'Не указано' ?></span>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Регион:</b> <span class="pull-right"><?=($model->delivery->region) ?$model->delivery->region : 'Не указано' ?></span>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Город:</b> <span class="pull-right">
                            <?=($model->delivery->city) ?$model->delivery->city : 'Не указано' ?></span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Сумма доставки:</b>  <span class="pull-right"><?=$model->delivery_price?></span>
                                    </li>
                                </ul>

                            </div>



                            <div class="col-md-6">
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <b>Метро:</b> <span class="pull-right">
                            <?=($model->delivery->subway) ?$model->delivery->subway : 'Не указано' ?>
                                        </span>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Улица:</b> <span class="pull-right">
                            <?=($model->delivery->street) ?$model->delivery->street : 'Не указано' ?></span>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Почтовый индекс: </b> <span class="pull-right">
                            <?=($model->delivery->zipcode) ?$model->delivery->zipcode : 'Не указано' ?></span>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Дом :</b> <span class="pull-right">
                            <?=($model->delivery->house) ?$model->delivery->house : 'Не указано' ?></span>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Квартира:</b> <span class="pull-right">
                            <?=($model->delivery->building) ?$model->delivery->building : 'Не указано' ?></span>
                                    </li>

                                    <li class="list-group-item">
                                        <b>Строение/Корпус:</b> <span class="pull-right">
                            <?=($model->delivery->flat) ?$model->delivery->flat : 'Не указано' ?>.</span>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Подъезд:</b> <span class="pull-right">
                            <?=($model->delivery->block) ?$model->delivery->block : 'Не указано' ?></span>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Этаж:</b> <span class="pull-right">
                            <?=($model->delivery->floor) ?$model->delivery->floor : 'Не указано' ?></span>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div >
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="settings" style="min-height: 500px;">
                        <div class="row">
                            <div class="col-md-12">
                                <strong><i class="fa fa-file-text-o margin-r-5"></i> Коментарий менеджера</strong>
                                <p><?=$model->comment_manager?></p>
                                <hr>
                                <strong><i class="fa fa-file-text-o margin-r-5"></i> Коментарий </strong>
                                <p><?=$model->comment?></p>
                                <hr>

                                <strong><i class="fa fa-file-text-o margin-r-5"></i> Коментарий Клиента</strong>
                                <p><?=$model->delivery->note?></p>
                                <hr>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div>
    </div>
</section>


