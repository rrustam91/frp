<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\products\search\ProductsCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Drop Products Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-categories-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'parent_id',
                'name',
                'alias:ntext',
                'content:ntext',
                // 'status',
                // 'status_drop',
                // 'xml_id',
                // 'xml_parent_id',
                // 'xml_name',
                // 'seo_keywords',
                // 'seo_description',
                // 'img',
                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
