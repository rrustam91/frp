<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\products\search\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Drop Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",

            'options' => [
                'style'=>'overflow: auto; word-wrap: break-word;'
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute'=>'article',

                    'label'=>'Ариткул',
                    'value' => function($model){
                        return $model->article ;
                    },

                    'contentOptions'=>['style'=>'width: 40px; '],
                    'format'=>'raw',
                ],


                [
                    'attribute' => 'name',
                    'value' => function ($model) {

                        return  Html::a($model->name, ['products/view', 'id' => $model->id]);
                    },
                    'format' => 'html',
                    'label' => 'Название продукта',
                ],

                [
                    'attribute'=>'price',
                    'value'=>function ($model, $key, $index, $widget) {
                        return $model->price .' руб.';
                    },

                    'label'=>"Рыночная цена"

                ],
                [
                    'attribute'=>'price_drop',
                    'value'=>function ($model, $key, $index, $widget) {
                        return $model->price_drop .' руб.';
                    },

                    'label'=>"Цена для партнеров "

                ],
                [
                    'attribute'=>'price_marj',
                    'value'=>function ($model, $key, $index, $widget) {
                        return ($model->price - $model->price_drop) .' руб.';
                    },

                    'label'=>"Маржинальность "

                ],
                [
                    'attribute' => 'category_id',
                    'value' => function ($model) {

                        return  Html::a($model->category->name, ['products-categories/info', 'category_id' => $model->category->id]);
                    },
                    'format' => 'html',
                    'label' => 'Категория',
                ],
               /* [
                    'attribute'=>'status',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->status, \common\models\MainModel::STATUS_SIMPLE_LIST);
                    },
                    'label'=>'Статус',
                    'format'=>'html'

                ],
                [
                    'attribute'=>'status_drop',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->status_drop, \common\models\MainModel::STATUS_SIMPLE_LIST);
                    },
                    'label'=>'Статус для Дроперов',
                    'format'=>'html'

                ],*/
                // 'price',
                // 'status',
                // 'status_drop',
                // 'xml_id',
                // 'xml_name',
                // 'xml_category',
                // 'xml_price',
                // 'xml_article',
                // 'created_at',
                // 'updated_at',
                [

                    'class' => '\kartik\grid\ActionColumn',
                    'hiddenFromExport' => true,
                    'template' => '{view}',
                    'buttons' => [

                        //view button
                        'view' => function ($url, $model) {
                            return Html::a('<span class="fa fa-search"></span> Подробнее', $url, [
                                'title' => Yii::t('app', 'View'),
                                'class'=>'btn bg-blue btn-xs  btn-flat',
                            ]);
                        },
                    ],

                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = '/cp/products/view?id=' . $model->id;
                            return $url;
                        }
                    }
                ],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
