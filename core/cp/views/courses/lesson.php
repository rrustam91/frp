<?php


use yii\widgets\ActiveForm;
use yii\helpers\Html;
use common\widgets\LessonMenuWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $lesson \common\models\courses\CoursesLessons*/
/* @var $course \common\models\courses\Courses*/

$curl = Yii::$app->request->url;
$this->title = $lesson->name;

?>


<section class="content" style="padding: 0px 0">
    <div class="container-fluid" style="position: relative">
        <div class="row" style="padding: 0;">
            <div class="col-md-4 col-sm-4 col-lg-3" style="padding: 0; margin: 0;position: relative">

                <div class="lesson__sidebar" style="width: 100%">

                    <?try {
                        echo  LessonMenuWidget::widget(['course' => $course]);
                    } catch (Exception $e) {
                        \common\helpers\myHellpers::show($e);
                    } ?>
                </div>
            </div>
            <div class="col-md-8  col-sm-8 col-lg-9">


                <?if($lesson->opened == \common\models\MainModel::STATUS_LESSON_ACTIVE){?>
                    <h1>
                        <?=$lesson->name?>
                    </h1>
                    <div style="max-width: 1000px;">
                        <div style="border-bottom: 1px solid rgba(0,0,0,.2)">
                            <style>
                                .lesson__video{
                                    padding: 20px 0;
                                }
                                .lesson__video video{
                                    width: 100% !important;
                                    max-width: 100%;
                                }
                                .lesson__video .video-js{
                                    width: 100% !important;
                                    max-width: 100%;
                                }
                                @media{

                                }
                            </style>
                            <div style="" class="lesson__video">
                                <?
                                $video = '/uploads/pltcmdbltj/'.$lesson->video .'.mp4';
                                echo \kato\VideojsWidget::widget([
                                    'options' => [
                                        'class' => 'video-js vjs-default-skin vjs-big-play-centered',
                                        'controls' => true,
                                        'preload' => 'auto',
                                        'height' => '500',
                                        'data-setup' => '{ "plugins" : { "resolutionSelector" : { "default_res" : "1024" } } }',
                                    ],
                                    'tags' => [
                                        'source' => [
                                            ['src' =>  $video, 'type' => 'video/mp4', 'data-res' => '360'],
                                            ['src' =>  $video, 'type' => 'video/mp4', 'data-res' => '720'],
                                        ],
                                    ],
                                    'multipleResolutions' => true,
                                ]);
                                ?>
                            </div>
                            <?=$lesson->content?>
                        </div>
                        <h3>
                            Напишите что вы сделали:
                        </h3>

                        <?php $form = ActiveForm::begin([
                        ]); ?>
                        <?= $form->field($homework, 'text')->widget(\dosamigos\tinymce\TinyMce::className(), [
                            'options' => ['rows' => 12],
                            'language' => 'ru',
                            'clientOptions' => [
                                'plugins' => [
                                    "autolink lists link",
                                    "media contextmenu paste image"
                                ],
                                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media "
                            ]
                        ])->label('');?>


                        <?= $form->field($homework, 'lesson_id')->textInput(['type'=>'hidden','value'=>$lesson->id])->label(false) ?>
                        <?= $form->field($homework, 'course_id')->textInput(['type'=>'hidden','value'=>$lesson->course_id])->label(false) ?>
                        <?= $form->field($homework, 'category_id')->textInput(['type'=>'hidden','value'=>$lesson->category_id])->label(false) ?>


                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
                        <?php ActiveForm::end(); ?>
                        <br><br>
                    </div>

                <?}else{?>
                    <h1>
                        Не унывайте, урок пока-что не доступен !:)
                    </h1>
                    <p>

                    </p>
                <?}?>


            </div>
        </div>



    </div>


</section>
  
