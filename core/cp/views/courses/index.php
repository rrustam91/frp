<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\helpers\myHellpers;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses');
$this->params['breadcrumbs'][] = $this->title;
$user_courses = Yii::$app->user->getIdentity()->coursesPOrdersId;
//myHellpers::show($user_courses);
//myHellpers::show(Yii::$app->user->getIdentity()->coursesOrders)

?>

<section class="content" style="padding: 20px 0">
    <div class="container">
        <?php Pjax::begin(); ?>
        <div class="row">
            <?foreach ($model as $item){ 
                    foreach($user_courses as $uc){
                        if($check = ($uc['p_course_id'] == $item->id)){
                                break;
                        }
                    }
                ?>
                <div class="col-md-12">
                    <div class="thumbnail disable" style="padding: 0; position: relative">
                        <?if(!$check){?>
                            <div class="disable_fade" style="display: flex; align-items: center;justify-content: center;position: absolute; z-index: 2; width: 100%; height: 100%;background:  rgba(255,255,255,.4)">
                                <a href="http://fankretailpartners.ru/courses" class="btn btn-flat btn-lg bg-maroon" role="button">
                                    Приобрести курс от <?=$item->price?> р.
                                </a>
                                <!--<a href="buy-course\<?/*=$item->alias*/?>" class="btn btn-flat btn-lg bg-maroon" role="button">
                                    Приобрести курс за <?/*=$item->price*/?> р.
                                </a>-->
                            </div>
                        <?}?>
                        <?= Html::img('/img/bnr_b.png', ['alt' => 'My logo', 'class'=>'img-responsive']) ?>
                        <div class="caption" style="padding: 10px 20px ">
                            <h3 style="margin: 0; padding: 0"> <?=$item->name;?></h3>
                            <p>
                                <?=$item->content;?>
                            </p>
                            <p>
                                <a href="<?if($check){?>courses\<?=$item->alias;}?>" class="btn btn-flat btn-md bg-green <?if(!$check){ ?>disabled<?}?>" role="button">
                                    Перейти к обучению
                                </a>
                            </p>
                        </div>
                    </div>
                </div>

            <?} ?>

        </div>
        <?php Pjax::end(); ?>
    </div>
</section>