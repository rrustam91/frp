<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
        <?= Alert::widget() ?>
        <?= $content ?>
</div>
