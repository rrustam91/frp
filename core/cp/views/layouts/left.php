
<aside class="main-sidebar">
    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/img/user.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?
                    if(!empty(Yii::$app->user->getIdentity()->name)){
                        echo Yii::$app->user->getIdentity()->name;
                    }else{?>

                        <a href="cp/profile">Мой кабинет</a>
                    <?};?>
                </p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <?


        if (Yii::$app->user->can('drop') || Yii::$app->user->can('opt')) {

            $items = [
                ['label' => 'Заявки', 'options' => ['class' => 'header']],
                ['label' => 'Новая заявка', 'url' => ['/orders/new'], 'icon' => 'plus', 'options' => ['class' => '']],
                ['label' => 'Мои заявки', 'icon' => 'list-ol', 'url' => ['/orders']],
                ['label' => 'Статистика', 'icon' => 'pie-chart', 'url' => ['/']],
                [
                    'label' => 'Доступные товары',
                    'icon' => 'shopping-cart',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Доступные товары', 'icon' => 'shopping-cart', 'url' => ['/products']],
                        ['label' => 'Категории товаров', 'icon' => 'clone', 'url' => ['/products/categories']],
                        ['label' => 'Предложить свой товар', 'icon' => 'cart-plus', 'url' => ['/send-messages?type=product']],
                        //['label' => 'Калькулятор', 'icon' => 'calculator', 'url' => ['/products/calculator']],

                    ],
                ],

                ['label' => 'Обратная связь', 'options' => ['class' => 'header']],
                ['label' => 'Получить консультацию', 'icon' => 'plus', 'url' => ['/send-messages?type=consult']],
                ['label' => 'Напишите нам', 'icon' => 'comments-o', 'url' => ['/send-messages?type=send-msg']],
            ];
        }
        $items[] = ['label' => 'Доп.', 'options' => ['class' => 'header']];
        $items[] = ['label' => 'Профиль', 'icon' => 'user', 'url' => ['/profile']];
        $items[] = ['label' => 'Курсы','icon'=>'graduation-cap', 'url' => ['/courses'] ];
        $items[] = ['label' => 'Выйти', 'url' => ['/logout'] ];
        ?>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $items
            ]
        ) ?>

    </section>

</aside>
