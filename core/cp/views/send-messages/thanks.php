<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\user\UserMessages */

$this->title = "Спасибо ваша заявка принята";


?>
<div class="user-messages-create">
    <h1>
        Спасибо ваша заявка принята, скоро мы свяжемся с вами
    </h1>
    <div>
        <a  href="/" class="btn btn-success">Вернуться обратно</a>
    </div>
</div>
