<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\user\UserMessages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-messages-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">


        <?= $form->field($model, 'type')->dropDownList(\common\models\MainModel::STATUS_MESSAGES_LIST)->label(Yii::t('app', 'User Message Type')) ?>

        <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>


    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
