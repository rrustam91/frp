<?php
use yii\helpers\Html;
use kartik\grid\GridView;

use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\dropship\search\DropOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My Panel');
$this->params['breadcrumbs'][] = $this->title;

if(!Yii::$app->user->isGuest){
    $cnt = \common\models\dropship\DropOrder::getUserCountOrder(Yii::$app->user->id);
}
?>



<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-navy">
            <span class="info-box-icon">
                <a href="#" style="color:white">
                    <span class="info-box-icon"><i class="fa  fa-drivers-license-o"></i></span>
                </a>
            </span>

            <div class="info-box-content">
                <span class="info-box-text">Заказы</span>
                <span class="info-box-number">
                    <?=
                    $cn
                    ?>
                </span>

                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description"></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-green">
            <a href="#" style="color:white">
                <span class="info-box-icon"><i class="fa fa-money"></i></span>
            </a>
            <div class="info-box-content">
                <span class="info-box-text">Коммисия Выплаченно</span>
                <span class="info-box-number">
                   <span class="price-num">
                        123456
                    </span>
                    <i class="fa fa-rub" style="font-size: 20px; font-weight: 300"></i>
                </span>

                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">

                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-red">

            <span class="info-box-icon">
                <a href="#" style="color:white">
                    <span class="info-box-icon"><i class="fa fa-rub"></i></span>
                </a>
            </span>

            <div class="info-box-content">
                <span class="info-box-text">Коммисия к выплате</span>
                <span class="info-box-number">
                   <span class="price-num">
                        123456
                    </span>
                    <i class="fa fa-rub" style="font-size: 20px; font-weight: 300"></i>
                </span>

                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description"></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-blue">

            <span class="info-box-icon">
                <a href="#" style="color:white">
                    <span class="info-box-icon"><i class="fa fa-percent"></i></span>
                </a>
            </span>

            <div class="info-box-content">
                <span class="info-box-text">Коммисия Всего </span>
                <span class="info-box-number">
                  <span class="price-num">
                        123456
                    </span>
                    <i class="fa fa-rub" style="font-size: 20px; font-weight: 300"></i>
                </span>

                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description"></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="callout callout-danger ">
            <h4>Ваш аккаунт не активен</h4>

            <p>
                Ожидайте Ваш аккаунт проходит модерацию
            </p>
        </div>
    </div>
    <div class="col-md-7">
        <?if(empty(!$model->crm_name)){?>
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>
              Добро пожаловать в FankRetailPartners
            </h4>
            Ваш аккаунт активен, для  начала работы
                <a href="/orders/new">
                    добавьте заказ
                </a>
        </div>
        <?}?>
    </div>
</div>
<div class="drop-order-index box box-primary ">

    <?php Pjax::begin(); ?>
    <div class="box-header with-border" >
        <?= Html::a(Yii::t('app', 'Create New Drop Order'), ['/orders/new'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider'=>$dataProvider,

            'showPageSummary'=>true,
            'pjax'=>false,
            'striped'=>true,
            'hover'=>true,
            'panel'=>false,
            'export'=>false,
            'panel'=>['type'=>'info', 'heading'=>'Таблица ваших заявок'],
            'columns'=>[

                [
                    'class'=>'kartik\grid\SerialColumn',
                    'hAlign'=>'center',
                ],
                [
                    'attribute'=>'crm_id',
                    'width'=> '50px',
                    'filter'=>false,
                    'label'=>'Номер заказа',
                    'hAlign'=>'center',
                ],
                [
                    'attribute'=>'products',
                    'value'=>function ($model, $key, $index, $widget) {
                        $items = $model->list;
                        $list = '';
                        foreach ($items as $item){
                            $list .= $item->product_name . ', кол-во:'.$item->quantity.'<br>';
                        }
                        return $list;

                    },

                    'format'=>'html',
                    'filter'=>false,
                    'filterInputOptions'=>['placeholder'=>'Название продукта'],
                    'label'=>'Список товаров',

                    'width'=>'300px',
                ],
                [
                    'attribute'=>'quantity',
                    'value'=>function ($model, $key, $index, $widget) {
                        $items = $model->list;
                        $q = 0;
                        foreach ($items as $item){
                            $q += $item->quantity;
                        }
                        return $q;

                    },

                    'hAlign'=>'center',
                    'width'=>'70px',
                    'format'=>'html',
                    'label'=>'Общ. кол-во'
                ],
                [
                    'attribute'=>'total_price',
                    'value'=>function ($model, $key, $index, $widget) {
                        $items = $model->list;
                        $price = 0;
                        foreach ($items as $item){ $price += $item->price_total; }
                        return $price;
                    },

                    'width'=>'200px',
                    'label'=>'Общая стоимость',
                    'width'=>'150px',
                    'hAlign'=>'right',
                    'pageSummary'=>true,
                    'format'=>['decimal'],
                ],
                [
                    'attribute'=>'status',
                    'value'=> function ($model){
                        return \common\helpers\myHellpers::getStatusLabel($model->status, $model::STATUS_DROP);
                    },
                    'format'=>'html',
                    'filter'=>false,
                    'width'=>'50px',
                    'hAlign'=>'center'
                ],

                [
                    'attribute'=>'created_at',
                    'format'=>'datetime',
                    'width'=>'150px',

                    'hAlign'=>'center'
                ],


                [

                    'class' => '\kartik\grid\ActionColumn',
                    'hiddenFromExport' => true,
                    'template' => '{view}',
                    'buttons' => [

                        //view button
                        'view' => function ($url, $model) {
                            return Html::a('<span class="fa fa-search"></span> Подробнее', $url, [
                                'title' => Yii::t('app', 'View'),
                                'class'=>'btn bg-blue btn-xs  btn-flat',
                            ]);
                        },
                    ],

                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = '/cp/orders/view?id=' . $model->id;
                            return $url;
                        }
                    }
                ],

            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>



<script>
</script>