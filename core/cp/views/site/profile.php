<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\user\UserMessages */

$this->title = Yii::t('app', 'User Profile');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Profile'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-messages-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
