<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\user\User */
/* @var $form yii\widgets\ActiveForm */
$role_list = \common\models\rbac\AuthItem::getRolesList();
?>

<div class="user-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive row">
        <div class="col-md-6">

            <label class="control-label" >Ваш емайл</label>
            <input type="text" value="<?=$model->email?>" class="form-control" disabled>
            <br>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Ваше имя') ?>
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true])->label('Ваш телефон') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'comment')->textarea(['row' => 6])->label('Доп. информация') ?>
        </div>

        <div class="col-md-12">

            <hr>
            <?= $form->field($model, 'password_new')->textInput(['type'=>'password']) ?>
            <?= $form->field($model, 'password_confirm')->textInput(['type'=>'password']) ?>
        </div>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
