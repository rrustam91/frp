<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\products\search\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Drop Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class=" ">
    <?php Pjax::begin(); ?>
    <div class=" ">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>false,
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>false,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'Доступные продукты для дропшипинга'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],

                [
                    'attribute'=>'article',
                    'width'=>'100px',
                    'label'=>'Артикул'
                ],
                [
                    'attribute'=>'name',
                    'value'=>function ($model, $key, $index, $widget) {
                        return Html::a($model->name, ['products/view', 'id' => $model->id]);
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>\yii\helpers\ArrayHelper::map(\common\models\products\Products::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'format'=>'html',
                    'filterInputOptions'=>['placeholder'=>'Название продукта'],
                    'label'=>'Название продукта'
                ],


                [
                    'attribute'=>'category_id',
                    'value'=>function ($model, $key, $index, $widget) {
                        return $model->category->name;
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>\yii\helpers\ArrayHelper::map(\common\models\products\ProductsCategories::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Название категории'],
                    'label'=>"Категория"
                ],
                [
                    'attribute'=>'price',
                    'value'=>function ($model, $key, $index, $widget) {
                        return $model->price .' руб.';
                    },

                    'label'=>"Рыночная цена"

                ],
                [
                    'attribute'=>'price_drop',
                    'value'=>function ($model, $key, $index, $widget) {
                        return $model->price_drop .' руб.';
                    },

                    'label'=>"Цена для партнеров "

                ],
                [
                    'attribute'=>'price_marj',
                    'value'=>function ($model, $key, $index, $widget) {
                        return ($model->price - $model->price_drop) .' руб.';
                    },

                    'label'=>"Маржинальность "

                ],
                //'alias',

                // 'price',
                // 'status',
                // 'status_drop',
                // 'xml_id',
                // 'xml_name',
                // 'xml_category',
                // 'xml_price',
                // 'xml_article',
                // 'created_at',
                // 'updated_at',

            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
