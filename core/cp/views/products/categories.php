<?php

use yii\helpers\Html;
use kartik\grid\GridView;

use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\products\ProductsCategories;

/* @var $this yii\web\View */
/* @var $searchModel common\models\products\search\ProductsCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Drop Products Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-categories-index ">
    <?php Pjax::begin(); ?>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search_categories', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>false,
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>false,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'info', 'heading'=>'Доступные категории для дропшипинга'],
            'columns'=>[

                ['class'=>'kartik\grid\SerialColumn'],
                [
                    'attribute'=>'name',
                    'value'=>function ($model, $key, $index, $widget) {
                        return Html::a($model->name, ['products-categories/info', 'category_id' => $model->id]);
                        return $model->name;
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>\common\models\MainModel::getDropCategories('id','name'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'format'=>'html',
                    'filterInputOptions'=>['placeholder'=>'Название категории'],
                    'label'=>'Название категории'
                ],
                [
                    'attribute'=>'parent_id',
                    'value'=>function ($model, $key, $index, $widget) {
                        return $model->parent->name;
                    },
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>\common\models\MainModel::getDropCategoriesParent('id','name',$model->parent->id),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Основная категории'],
                    'label'=>'Основная категории'
                ],

            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
