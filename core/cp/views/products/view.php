<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\products\Products */

$this->title = 'Товар: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drop Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view box box-primary">
    <div class="box-body table-responsive no-padding">
        <div class="col-md-12">
            <h1>Товар: <?=$model->name?></h1>
        </div>
        <div class="col-md-4">
            <h4>Категория: <?= Html::a($model->category->name, ['products-categories/info', 'category_id' => $model->category->id]);?></h4>
            <h4>Рыночная цена: <?=$model->price?> руб.</h4>
            <h4>Цена для партнеров: <?=$model->price_drop?> руб.</h4>
            <h4>Маржинальность: <?=$model->price-$model->price_drop?> руб.</h4>
            <h4>Артикул: <?=$model->article?> </h4>
        </div>
        <div class="col-md-8">
            <h4> Описание</h4>
            <p>
                <?=$model->info->text?>
            </p>
        </div>

        <div class="col-md-12">
            <hr>
            <h4> Изображения</h4>
            <?
            if(!empty($model->imgs) && isset($model->imgs)){
                foreach ($model->imgs as $img){
                    ?>
                    <div class="col-md-1">
                        <img src="/uploads/<?=$img->url?>" alt="" class="img-responsive">
                    </div>
                    <?
                }

            }
            ?>
            <br clear="all">
            <br><br>
        </div>
    </div>
</div>
