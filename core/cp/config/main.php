<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-cp',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'cp\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ]

    ],
    'components' => [

        'request' => [
            'csrfParam' => '_csrf-cp',
            'cookieValidationKey' => $params['cookieValidationKey'],
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        /*'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'user' => [

            'class' => 'yii\web\User',
            'identityClass' => 'common\models\Auth',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity', 'httpOnly' => true, 'domain' => $params['cookieDomain']],
        ],*/
        'session' => [
            'name' => '_session',
            'cookieParams' => [
                'domain' => $params['cookieDomain'],
                'httpOnly' => true,
            ],
        ],
        'backendUrlManager' => require __DIR__ . '/../../backend/config/urlManager.php',
        'frontendUrlManager' => require __DIR__ . '/../../frontend/config/urlManager.php',
        'cpUrlManager' => require __DIR__ . '/urlManager.php',

        'urlManager' => function () {
            return Yii::$app->get('cpUrlManager');
        },
    ],


    'as access' => [
        'class' => 'yii\filters\AccessControl',
        'except' => ['site/login', 'site/error'],
        'rules' => [
            [
                'allow' => true,
                'roles' => ['@'],
            ],
            [
                'actions' => ['login', 'error','password-reset','reset-password'],
                'allow' => true,
            ],

        ],
        'denyCallback' => function () { 
            if( ! Yii::$app->user->isGuest ) {
                Yii::$app->user->logout();
            }
            return Yii::$app->response->redirect(['site/login']);
        },
    ],
    'params' => $params,
];
