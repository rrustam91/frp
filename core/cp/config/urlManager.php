<?php

/** @var array $params */

return [
    'class' => 'yii\web\UrlManager',
    'hostInfo' => $params['cpHostInfo'],
    'baseUrl' => '/',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [

        '/' => 'orders/statistics',
        '/profile' => 'site/profile',
        '/reset-password' => 'site/reset-password',
        '/password-reset' => 'site/password-reset',
        '<_a:login|logout>' => 'site/<_a>',
        'finish/<course:[\w-]+>' => 'courses/finish',
        'courses/<course:[\w-]+>/<lesson:[\w-]+>' => 'courses/lesson',
        'courses/<course:[\w-]+>' => 'courses/course',
        '<_c:[\w\-]+>' => '<_c>/index',
        '<_c:[\w\-]+>/<id:\d+>' => '<_c>/view',
        '<_c:[\w\-]+>/<_a:[\w-]+>' => '<_c>/<_a>',
        '<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_c>/<_a>',
    ],
];
