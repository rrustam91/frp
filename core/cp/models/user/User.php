<?php

namespace app\models\user;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $name
 * @property string $phone
 * @property int $role
 * @property string $roles
 * @property string $utm
 * @property string $group
 * @property string $crm_name
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $status_role
 * @property int $status
 * @property int $status_drop
 * @property int $status_opt
 * @property string $comment
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CoursesOrder[] $coursesOrders
 * @property DropOrder[] $dropOrders
 * @property DropOrder[] $dropOrders0
 * @property OptRequest[] $optRequests
 * @property UserClientPayment[] $userClientPayments
 * @property UserClientPayment[] $userClientPayments0
 * @property UserCourses[] $userCourses
 * @property UserDropship[] $userDropships
 * @property UserHomework[] $userHomeworks
 * @property UserMessages[] $userMessages
 * @property UserOpt[] $userOpts
 * @property UserProfile[] $userProfiles
 * @property UserStepLesson[] $userStepLessons
 * @property WebinarChats[] $webinarChats
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'auth_key', 'password_hash', 'comment', 'created_at', 'updated_at'], 'required'],
            [['role', 'status', 'status_drop', 'status_opt', 'created_at', 'updated_at'], 'integer'],
            [['roles', 'utm', 'comment'], 'string'],
            [['username', 'email', 'name', 'phone', 'group', 'crm_name', 'password_hash', 'password_reset_token', 'status_role'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'name' => 'Name',
            'phone' => 'Phone',
            'role' => 'Role',
            'roles' => 'Roles',
            'utm' => 'Utm',
            'group' => 'Group',
            'crm_name' => 'Crm Name',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'status_role' => 'Status Role',
            'status' => 'Status',
            'status_drop' => 'Status Drop',
            'status_opt' => 'Status Opt',
            'comment' => 'Comment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoursesOrders()
    {
        return $this->hasMany(CoursesOrder::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDropOrders()
    {
        return $this->hasMany(DropOrder::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDropOrders0()
    {
        return $this->hasMany(DropOrder::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptRequests()
    {
        return $this->hasMany(OptRequest::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserClientPayments()
    {
        return $this->hasMany(UserClientPayment::className(), ['manager_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserClientPayments0()
    {
        return $this->hasMany(UserClientPayment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCourses()
    {
        return $this->hasMany(UserCourses::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDropships()
    {
        return $this->hasMany(UserDropship::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHomeworks()
    {
        return $this->hasMany(UserHomework::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserMessages()
    {
        return $this->hasMany(UserMessages::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOpts()
    {
        return $this->hasMany(UserOpt::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles()
    {
        return $this->hasMany(UserProfile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserStepLessons()
    {
        return $this->hasMany(UserStepLesson::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebinarChats()
    {
        return $this->hasMany(WebinarChats::className(), ['user_id' => 'id']);
    }
}
