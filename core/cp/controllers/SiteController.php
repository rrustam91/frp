<?php
namespace cp\controllers;

use common\helpers\myHellpers;
use common\models\auth\PasswordResetRequestForm;
use common\models\auth\ResetPasswordForm;
use common\models\dropship\search\DropOrderSearch;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\auth\LoginForm;
use yii\web\NotFoundHttpException;
use common\models\user\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                     
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
           
            return true;
        } else {
            return false;
        }
    }
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){


        $searchModel = new DropOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProfile(){
        $user = Yii::$app->user;
        $model = $this->findUser($user->id);

        if(Yii::$app->request->post()){
            if ($model->load(Yii::$app->request->post()) && $model->setUser()) {
                return $this->redirect('/');
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }


        }

        return $this->render('profile', [
            'model'=>$model
        ]);
    }


    protected function findUser($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionOldIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'main-login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->redirect('/cp/orders');
        } else {
            $model->password = '';

            Yii::$app->user->logout();
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionPasswordReset()
    {

        $this->layout = 'main-login';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Проверьте вашу почту, с подробной инструкцией для сброса пароля.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Простите, мы сброс пароля не возможен.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }


    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {

        $this->layout = 'main-login';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль сохранен.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
