<?php

namespace cp\controllers;

use common\helpers\myHellpers;
use common\models\MainModel;
use common\models\products\search\ProductsCategoriesSearch;
use common\models\forms\MessagesForm;
use Yii;
use common\models\products\Products;
use common\models\products\search\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class SendMessagesController extends Controller
{

    public function actionIndex($type=''){
        $model = new MessagesForm();
        switch($type) {
            case 'consult':
                $model->type = MainModel::STATUS_MESSAGES_CONSULT ;
                break;
            case 'msg':
                $model->type = MainModel::STATUS_MESSAGES_SENDUS ;
                break;
            case 'product':
                $model->type = MainModel::STATUS_MESSAGES_PRODUCT ;
                break;
            default:
                $model->type = MainModel::STATUS_MESSAGES_SENDUS ;
                break;
        }
        if(Yii::$app->request->isPost){
            if($model->load(Yii::$app->request->post()) && $model->addMessages()){
                return $this->render('thanks');
            }
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }
}