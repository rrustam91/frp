<?php

namespace cp\controllers;

use common\helpers\myHellpers;
use common\models\dropship\CreateDropOrder;
use common\models\dropship\DropDeliver;
use common\models\forms\DropOrderForm;
use common\models\User;
use Yii;
use common\models\dropship\DropOrder;
use common\models\dropship\search\DropOrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrdersController implements the CRUD actions for DropOrder model.
 */
class OrdersController extends SiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DropOrder models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new DropOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionStatistics()
    {
        $searchModel = new DropOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all DropOrder models.
     * @return mixed
     */
    public function actionNew()
    {
        $model_form = new DropOrderForm(); 
        $drop_delivery = new DropDeliver();

        if(Yii::$app->request->isPost){
            if($model_form->load(Yii::$app->request->post())){
                $data = Yii::$app->request->post();
                $drop_order = new CreateDropOrder(Yii::$app->user, $data['DropOrderForm']);
                if($drop_order->createOrder()){
                    return $this->redirect('/orders');
                };
            }
        } else {
            return $this->render('create', [
                'model_form' => $model_form,
                'drop_delivery' => $drop_delivery
            ]);
        }
    }

    public function actionView($id)
    {
        if($m = $this->findModel($id)) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
        return $this->redirect('/orders');
    }

    protected function findModel($id)
    {
        $_u = Yii::$app->user->id;
        if (($model = DropOrder::find()->where(['id'=>$id])->andWhere(['user_id'=>$_u])->one()) !== null) {
            return $model;
        } else {
            return null;
        }
    }
}
