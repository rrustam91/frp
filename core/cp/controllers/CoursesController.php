<?php

namespace cp\controllers;

use common\helpers\myHellpers;
use common\models\courses\CourseCategory;
use common\models\courses\CoursesHellper;
use common\models\courses\CoursesLessons;
use common\models\forms\CabinetLessonForm;
use common\models\user\UserHomework;
use common\models\user\UserStepLesson;
use common\widgets\Alert;
use Yii;
use common\models\courses\Courses;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CoursesController implements the CRUD actions for Courses model.
 */
class CoursesController extends Controller
{
    public $layout = 'courses';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }


    /**
     * Lists all Courses models.
     * @return mixed
     */
    public function actionIndex()
    {

        $model = Courses::find()->where(['parent_id'=>null])->all();

        return $this->render('index', [
            'model' => $model,
        ]);
    }
    /**
     * Lists all Courses models.
     * @return mixed
     */
    public function actionCourse($course)
    {
        if ($course = $this->findModelByAlias(Courses::className(), $course)) {
            $course = CoursesHellper::findModelByAlias(Courses::className(), $course);
            $lesson = CoursesHellper::getModelByCourseId(CoursesLessons::className(), $course->id);
            if ($user_lesson = CoursesHellper::getLastLessonUser($course->id)) {
                $lesson = CoursesHellper::findModel(CoursesLessons::className(), $user_lesson->lesson_id);
            }
            if($lesson){
                return $this->redirect(["/courses/{$course->alias}/{$lesson->alias}"]);
            }
        }
    }

    public function buy($course){
        return 'hi';
    }

    public function actionFinish($course){

        if($course = CoursesHellper::findModelByAlias(Courses::className(), $course)){
            return $this->render('finish', [
                'model'=>$course
            ]);
        }
        return $this->redirect('/courses');

    }
    /**
     * Lists all Courses models.
     * @return mixed
     */
    public function actionLesson($course, $lesson='')
    {

        $course = CoursesHellper::findModelByAlias(Courses::className(), $course);
        $lesson = CoursesHellper::findModelByAlias(CoursesLessons::className(), $lesson);
        $homework = UserHomework::getHomeWork($lesson->id); 
        if(Yii::$app->request->isPost){
            if($homework->load(Yii::$app->request->post()) && $homework->addHomeWork()){
                $this->nextLesson($homework);
            }else{
                myHellpers::show($homework->errors);
            }
        }

        CoursesHellper::updateLastLessonUser($lesson->id);
        if($course && $lesson) {
            return $this->render('lesson', [
                'course' => $course,
                'lesson' => $lesson,
                'homework' => $homework
            ]);
        }
       return $this->redirect('/courses');


    }

    protected function nextLesson($lesson){
        $crnt_lsn = CoursesHellper::findModel(CoursesLessons::className(), $lesson->lesson_id);
        $crnt_cat = CoursesHellper::findModel(CourseCategory::className(), $lesson->category_id);
        $course = $crnt_lsn->course;
        if($crnt_lsn->step_by+1 <= count($crnt_cat->coursesActiveLessons)){
            if($crnt_lsn = CoursesHellper::findLessonByStep($crnt_lsn->category_id, $crnt_lsn->step_by+1)){
                $crs = $course->alias;
                $lsn = $crnt_lsn->alias;
                return $this->redirect(["/courses/{$crs}/{$lsn}"]);
            }
        }else{
            $step = $crnt_lsn->category->step_by;
            if($crnt_cat = CoursesHellper::findCategoryByStep($crnt_lsn->course->id,$step+1)){
                $crs = $crnt_cat->course->alias;
                $lsn = CoursesHellper::findLessonByStep($crnt_cat->id, 1);
                return $this->redirect(["/courses/{$crs}/{$lsn->alias}"]);
            }
        }
        return $this->redirect(["/finish/{$course->alias}"]);

    }


    protected function getLastLessonUser($course_id, $lesson_id = ''){


    }



    /**
     * Finds the Courses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Courses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Courses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    protected function findModelByAlias($model,$alias){
        if($model = $model::find()->where(['alias'=>$alias])->one()){
            return $model;
        }
    }
}
