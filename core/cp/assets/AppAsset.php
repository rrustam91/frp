<?php

namespace cp\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'libs/datetimepicker/jquery.datetimepicker.min.css',

    ];
    public $js = [
        'libs/datetimepicker/jquery.datetimepicker.full.min.js',
        'js/scripts.js',
        'js/lesson.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',

    ];
}