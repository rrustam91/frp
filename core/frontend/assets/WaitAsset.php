<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class WaitAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/pages/main.min.css',
        'https://use.fontawesome.com/releases/v5.1.1/css/all.css'
    ];
    public $js = [
        'js/pages/common.js',
        'js/pages/scripts.min.js',
        'js/wait.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
