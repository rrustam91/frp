<?php
$link = 'http://fankretailpartners.ru/webinar';
?>
<style>
    .thanks{
        width: 100%;
        max-width: 800px;
        margin: 20px auto;
        text-align: center;
        padding: 20px;
    }
    .thanks__title{
        margin: 0 auto;
    }
    .thanks__desc{

    }
    .thanks__link{
        display: block;
        width: 100%;
        min-height: 50px;
        padding: 20px;
        color: #333;
        font-size: 24px;
        background: #e5e5e5;
        border-radius: 50px;
    }
    .thanks__link:hover,.thanks__link:active, .thanks__link:focus  {
        color: #333;
        text-decoration: none;
    }
    .thanks-social{
        margin: 30px auto;
    }
    .thanks-social__wrap{
        max-width: 300px;
        margin: 0 auto;
        display: flex;
        flex-direction: row;


    }
    .thanks-social__item{
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 10px;
        background: #5546ba;
        margin: 0 auto;
        min-width: 50px;
        min-height: 50px;
        color: white;
        transition: .5s all ease;

    }
    .thanks-social__item:hover,.thanks-social__item:active,.thanks-social__item:focus {
        text-decoration: none;
        color: white;
        background: #7867ec;
    }
</style>

<!-- Главная -->

<section class="section head">
    <header class="head__header">
        <div class="head__img-wrap">
                <img src="/img/logo.jpg" alt="FRP" class="head__img">
        </div>
        <p class="head__live">Онлайн трансляция по всему миру</p>
    </header>
    <div class="thanks">
        <div>
            <p class="thanks__desc" style="font-weight: 300; font-size: 28px;">Вебинар стартует <br><span class="date_start" style="display: inline-block;font-weight: 500;">6 августа</span> в <span class="time_start"  style="display: inline-block;font-weight: 500;">20:00 по Мск</span></p>

            <div style="display: flex; flex-direction: row; max-width: 500px; margin: 20px auto;  " align="center" id="clockdiv">
                <div style="max-width: 100px; margin: 20px auto">
                    <h1 class="_td">00</h1>
                    <div>День</div>
                </div>
                <div style="max-width: 100px; margin: 20px auto">
                    <h1 class="_th">00</h1>
                    <div>Часов</div>
                </div>
                <div style="max-width: 100px; margin: 20px auto">
                    <h1 class="_tm">00</h1>
                    <div>Минут</div>
                </div>
                <div style="max-width: 100px; margin: 20px auto">
                    <h1 class="_ts">00</h1>
                    <div>Сек</div>
                </div>
            </div>

        </div>
        <div class="thanks-social" style="display:none;">
            <p class="thanks-social__title">Всю актуальную информацию вы можете узнать здесь:</p>
            <div class="thanks-social__wrap">
                <a href="#" class="thanks-social__item"><i class="fab fa-vk"></i></a>
                <a href="#" class="thanks-social__item"><i class="fab fa-telegram-plane"></i></a>
                <a href="#" class="thanks-social__item"><i class="fab fa-whatsapp"></i></a>
                <a href="#" class="thanks-social__item"><i class="fab fa-facebook"></i></a>
            </div>
        </div>
    </div>
    <div class="thanks-social">

    </div>
</section>

<script>
    var _r = true;
</script>