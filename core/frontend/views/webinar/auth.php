<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\forms\WebinarForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация на вебинар';
?>

<style>
    .register,.register__form-block{
        background: #333;
    }
</style>


<section class="register " >

    <div class="register__form-block">
        <div class="register__form-wrap" style="max-width: 500px;">

            <div align="center" style="max-width: 300px; margin: 50px auto;">
                <img src="/img/logo.png" style="width: 100%;">
            </div>
            <p class="register__action">Для входа в вебинар-комнату</p>
            <p class="register__action">Пожалуйста авторизуйтесь</p>


            <br>


            <?php $form_partner = ActiveForm::begin(['id'=>'partner-form','fieldConfig' => [
                'template' => "{input}",
                'options' => [
                    'tag'=>'span',
                ]
            ]]); ?>


            <?= $form_partner->field($model, 'name', [
                'options' => [
                    'tag' => false, // Don't wrap with "form-group" div
                ],
            ])->textInput(['class'=>'form__input register__input','placeholder'=>'Введите Имя...', 'required'=>'required'])->label(false) ?>

            <?= $form_partner->field($model, 'email_or_phone', [
                'options' => [
                    'tag' => false, // Don't wrap with "form-group" div
                ],
            ])->textInput(['class'=>'form__input register__input','placeholder'=>'Введите Е-майл...','type'=>'email', 'required'=>'required'])->label(false) ?>

            <button class="register__btn" onclick="yaCounter49725970.reachGoal('pg_auth_form'); return true;">Войти</button>
            <?php ActiveForm::end(); ?>



        </div>
    </div>
</section>