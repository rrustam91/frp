
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $chat \common\models\forms\ChatMessagesForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;



?>


<section class="main">
        <div class="main__wrap">
            <img src="img/course/logo-white.png" alt="FRP" class="main__logo">
            <span class="main__name">Образовательный курс</span>
            <div class="main__img-wrap">
                <img src="img/course/main.svg" alt="Товарный бизнес с 0 до 100000 руб." class="main__img main__img--desc">
                <img src="img/course/main-tel.svg" alt="Товарный бизнес с 0 до 100000 руб." class="main__img main__img--tel">
            </div>
            <div class="main__titles">
                <h1 class="main__title main__title--head">Запусти свой собственный бизнес</h1>
                <h1 class="main__title main__title--bottom">С доходом от 100 000 ₽</h1>
            </div>
            <div class="main__price">
                <span class="main__price-name">Всего за</span>
                <span class="main__price-numb">990 ₽</span>
            </div>
            <!--                <a data-fancybox data-src="#hidden-main" data-product="Заказ курса | Header" data-pprice="990" href="javascript:;" class="main__btn btn btn--yellow">Получить доступ к курсу</a>-->
            <a class="goto main__btn btn btn--yellow" href="#order-form">Получить доступ к курсу</a>
        </div>
    </section>
    <!-- Блок 2 -->
    <section class="map">
        <h2 class="h2 map__title">В данном курсе вы изучите</h2>
        <div class="map__wrap">
            <div class="map__item map__item--mark">
                <div class="map__content">
                    <div class="map__header">
                        <div class="map__img-wrap">
                            <img src="img/course/mark.png" alt="Марк" class="map__img">
                        </div>
                        <h3 class="h3 map__name">Основы<br>упаковки бизнеса</h3>
                    </div>
                    <p class="map__text">Узнайте как проводить анализ ниши, выбрать подходящий товар, изучить конкурентов, найти сильные стороны продукта. Даже новичок сможет за один день выбрать несколько интересных ниш.<br>Как правильно оформить сайт, который будет продавать? Составление продающих заголовков, офферов и интересного описания товара или услуги</p>
                </div>
                <div class="map__abs map__abs--mark"></div>
            </div>
            <div class="map__item map__item--kirill">
                <div class="map__abs map__abs--kirill"></div>
                <div class="map__content">
                    <div class="map__header">
                        <h3 class="h3 map__name">Настройка релевантного трафика и привлечение клиентов</h3>
                        <div class="map__img-wrap">
                            <img src="img/course/kirill.png" alt="Кирилл" class="map__img">
                        </div>
                    </div>
                    <p class="map__text">Как рекламировать свой сайт. Какие эффективные каналы привлечения клиентов использовать. Как правильно настроить rконтекстную рекламу в Яндекс.Директ и Google.Adwords<br>Продажи в социальных сетях - какие лучше использовать. Как настроить рекламу. Авито - составление эффективных и продающих объявлений. Как получать заявки без вложений.</p>
                </div>
            </div>
            <div class="map__item map__item--max">
                <div class="map__content">
                    <div class="map__header">
                        <div class="map__img-wrap">
                            <img src="img/course/max.png" alt="Максим" class="map__img">
                        </div>
                        <h3 class="h3 map__name">Продвижение и обработка заказов. Увеличение среднего чека</h3>
                    </div>
                    <p class="map__text">Что нужно знать про клиента. Как написать скрипт подходящий по для вашего продукта. Продажа по технике SPIN. Как не допустить возражений.</p>
                </div>
                <div class="map__abs map__abs--max"></div>
            </div>
        </div>
    </section>
    <!-- Блок 3 -->
    <section class="man">
        <h2 class="h2 man__title">Кому подойдет этот курс</h2>
        <div class="man__wrap">
            <div class="man__item">
                <div class="man__img-wrap">
                    <img src="img/course/man1.svg" alt="Начинающий" class="man__img">
                </div>
                <div class="man__text">
                    <h3 class="h3 man__name">Начинающим предпринимателям</h3>
                    <p class="man__desc">Научитесь всем основам заработка в интернете</p>
                </div>
            </div>
            <div class="man__item">
                <div class="man__img-wrap">
                    <img src="img/course/man2.svg" alt="Опытный" class="man__img">
                </div>
                <div class="man__text">
                    <h3 class="h3 man__name">Владельцам существующего бизнеса</h3>
                    <p class="man__desc">Увеличьте доход своего бизнеса</p>
                </div>
            </div>
            <div class="man__item">
                <div class="man__img-wrap">
                    <img src="img/course/man3.svg" alt="Работник" class="man__img">
                </div>
                <div class="man__text">
                    <h3 class="h3 man__name">Наемным сотрудникам</h3>
                    <p class="man__desc">Получайте дополнительный доход не тратя много времени</p>
                </div>
            </div>
            <div class="man__item">
                <div class="man__img-wrap">
                    <img src="img/course/man4.svg" alt="Студент" class="man__img">
                </div>
                <div class="man__text">
                    <h3 class="h3 man__name">Школьникам и студентам</h3>
                    <p class="man__desc">Запустите свой первый бизнес без вложений</p>
                </div>
            </div>
        </div>
        <!--
        <a data-fancybox data-src="#hidden-main" data-product="Заказ курса | Header" data-pprice="199" href="javascript:;" class="man__btn btn btn--yellow">Мне подходит <span>- Хочу заказать</span></a>-->
        <a class="goto man__btn btn btn--yellow" href="#order-form">Мне подходит <span>- Хочу заказать</span></a>
    </section>
    <!-- Доп блок -->
    <section class="money">
        <div class="money__wrap">
            <h2 class="h2 money__title">Гарантируем окумаемость стоимости курса!</h2>
            <p class="money__text">Все ученики окупили стоимоть данного курса уже в первую неделю работы.</p>
            <p class="money__text">Если предоставленные нами знания не помогут Вам окупить стоимость курса, мы гарантируем возврат денег!</p>
            <!--                <a data-fancybox data-src="#hidden-main" data-product="Заказ курса" data-pprice="199" href="javascript:;" class="money__btn btn btn--yellow">Получить курс</span></a>-->
            <a   class=" goto money__btn btn btn--yellow" href="#order-form">Получить курс</span></a>
        </div>
        <img src="img/course/money.svg" alt="Возврат" class="money__img">
    </section>
    <section class="trener">
        <div class="terner__wrap">
            <div class="trener__img-wrap">
                <img src="/img/course/trener.png" alt="Эксперт" class="trener__img">
                <div class="terner__inst-wrap">
                    <span class="trener__inst-text">Страница инстаграм:</span>
                    <!--                        <a class="trener__inst-icon" href="https://www.instagram.com/mark_fankuhin/" target="_blank"><img class="trener__inst-img" src="/img/course/instagram.svg" alt="Инстаграм"></a>-->
                    <a class="trener__inst-icon" href="https://www.instagram.com/mark_fankuhin/" target="_blank"><img class="trener__inst-img" src="/img/course/instagram.svg" alt="Инстаграм"></a>
                </div>
            </div>
            <div class="trener__content">
                <h2 class="h2 trener__title"><span class="trener__subtitle sub2">Бонус всем участникам курса</span></h2>
                <h2 class="h2 trener__title">Персональный разбор <span class="trener__subtitle">От основателя Fank Retail Partners</span></h2>
                <p class="trener__text">Ответит на все вопросы по запуску товарного бизнеса. Работа над ошибками по выполнению заданий. Рекомендации и советы по достижению цели "Доход от 100 000 рублей ежемесячно"</p>
                <!--                    <a data-fancybox data-src="#hidden-consult" data-product="Консультация" data-pprice="0" href="javascript:;" class="trener__btn btn btn--blue">Получить консультацию</a>-->
                <a  class=" goto trener__btn btn btn--blue" href="#order-form">Получить консультацию</a>
            </div>
        </div>
    </section>
    <!-- Блок 5 -->


    <!-- Тарифы -->


    <div class="tarif__wrap">
        <h2 class="h2 tarif__title">Начните сейчас</h2>
        <div class="tarif">
            <div class="tarif__item tarif__item--a">
                <h3 class="tarif__name">Стандартный</h3>
                <ul class="tarif__list">
                    <li class="tarif__text">Доступ к образовательной платформе</li>
                    <li class="tarif__text">Обучающий видеокурс для запуска товарного бизнеса (самостоятельно)</li>
                </ul>
                <p class="tarif__price">990<span> ₽</span></p>
                <a href="#order-form" class="tarif__btn btn" data-pprice="990" data-target="1">Выбрать</a>
            </div>
            <div class="tarif__item tarif__item--b tarif__item--active">
                <h3 class="tarif__name">Персональньный</h3>
                <ul class="tarif__list tarif__list--active">
                    <li class="tarif__text">Доступ к обучающей платформе</li>
                    <li class="tarif__text">Обучающий видеокурс для запуска товарного бизнеса</li>
                    <li class="tarif__text"><b>Домашние задания</b></li>
                    <li class="tarif__text"><b>Проверка домашних заданий</b></li>
                    <li class="tarif__text"><b>Рекомендации по улучшению сайта, рекламных кампаний, скриптов продаж</b></li>
                </ul>
                <p class="tarif__price">2990<span> ₽</span></p>
                <a href="#order-form" class="tarif__btn btn tarif__btn--active" data-pprice="2990" data-target="2">Выбран</a>
            </div>
            <div class="tarif__item tarif__item--c">
                <h3 class="tarif__name">VIP</h3>
                <ul class="tarif__list">
                    <li class="tarif__text">Доступ к образовательной платформе</li>
                    <li class="tarif__text">Обучающий видеокурс для запуска товарного бизнеса</li>
                    <li class="tarif__text">Домашние задания</li>
                    <li class="tarif__text">Проверка домашних заданий</li>
                    <li class="tarif__text">Рекомендации по улучшению сайта, рекламных кампаний, скриптов продаж</li>
                    <li class="tarif__text"><b>Персональный разбор от основателя FRP, Марка Фанкухина</b></li>
                    <li class="tarif__text"><b>Персональное ведение до результата 5 продаж</b></li>
                </ul>
                <p class="tarif__price">9990<span> ₽</span></p>
                <a href="#order-form" class="tarif__btn btn" data-pprice="9990" data-target="3">Выбрать</a>
            </div>
        </div>
    </div>

    <section class="footer" id="order-form">
        <div class="footer__wrap">
            <h2 class="h2 footer__title">Запишись на курс<span class="footer__subtitle">и начни зарабатывать на своем бизнесе от 100000 ₽</span></h2>




            <?php $form_order = ActiveForm::begin(['id'=>'course-form','action'=>'/webinar/add-course', 'fieldConfig' => [
                'template' => "{input}",
                'options' => [
                    'tag'=>'span'
                ]
            ]]); ?>
            <div class="footer__input-cont">

                <div class="footer__input-wrap">
                    <?= $form_order->field($course, 'name', [
                        'options' => [
                            'tag' => false, // Don't wrap with "form-group" div
                        ],
                    ])->textInput(['class'=>'footer__input','placeholder'=>'Введите Имя...', 'required'=>'required'])->label(false) ?>

                </div>
                <div class="footer__input-wrap">


                    <?= $form_order->field($course, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7 (999) 999-99-99',
                        'options' => [
                            'class' => 'footer__input mask',
                            'id' => 'phone2',
                            'placeholder' => ('Введите телефон...'),
                            'type'=>'phone',
                            'required'=>'required',
                            'tag'=>false
                        ],
                        'clientOptions' => [
                            'clearIncomplete' => true
                        ]
                    ])->label(false) ?>


                </div>
                <div class="footer__input-wrap">
                    <?= $form_order->field($course, 'email', [
                        'options' => [
                            'tag' => false, // Don't wrap with "form-group" div
                        ],
                    ])->textInput(['class'=>'footer__input', 'type'=>'email','placeholder'=>'Введите Емайл...', 'required'=>'required'])->label(false) ?>
                </div>

            </div>

            <input type="hidden" id="type_course" class="type_course" name="OrderCourseForm[course]" value="1">
            <button class="footer__btn btn btn--yellow">Начать обучение за 990 ₽</button>

            <?php ActiveForm::end(); ?>


        </div>
    </section>

    <!-- Доступ к курсу -->


    <div style="display: none;" id="hidden-main" class="h-form">
        <h3 class="h3 h-form__title">Для получения доступа к курсу заполните форму ниже</h3>

        <form class="h-form__form" action="/send.php" method="post">
            <input class="h-form__input" type="text" name='name' required placeholder="Введите Имя...">
            <input class="h-form__input mask" type="text" name='phone' required placeholder="Введите Номер телефона...">
            <input type="hidden" name='variant' value="">
            <input type="hidden" name='landing' value="Видеоглазки" >
            <input type="hidden" name='category_title' value="Заказ">
            <input type="hidden" name='product_title' value="Видеоглазки">
            <button class="btn btn--yellow h-form__btn">Получить курс</button>
        </form>

    </div>


    <!-- Консультация -->


    <div style="display: none;" id="hidden-consult" class="h-form">
        <h3 class="h3 h-form__title">Для получения консультации заполните форму ниже</h3>

        <form class="h-form__form" action="/send.php" method="post">
            <input class="h-form__input" type="text" name='name' required placeholder="Введите Имя...">
            <input class="h-form__input mask" type="text" name='phone' required placeholder="Введите Номер телефона...">
            <input type="hidden" name='variant' value="">
            <input type="hidden" name='landing' value="Видеоглазки" >
            <input type="hidden" name='category_title' value="Заказ">
            <input type="hidden" name='product_title' value="Видеоглазки">
            <button class="btn btn--blue h-form__btn">Получить консультацию</button>
        </form>

    </div>