
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $chat \common\models\forms\ChatMessagesForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$u = Yii::$app->user->getIdentity();
$course->course = 2;
$course->name = $u->name;
$course->phone = $u->phone;
$course->email = $u->email;
$partner->name = $u->name;
$partner->phone = $u->phone;
$partner->email = $u->email;

$chat->webinar = 1;
$this->title = '';
?>

<section class="video">
    <div class="container-fluid video__container">
        <div class="row">
            <div class="col-md-9 p0">
                <section class="head">
                    <header class="container-fluid">
                        <div class="head__header row">
                            <div class="col-md-4">
                                <a href="#">
                                    <img src="img/logo.png" alt="FRP" class="head__img">
                                </a>
                            </div>
                            <div class="col-md-8">
                                <p class="head__live">Онлайн трансляция <span class="head__live--dot">●</span></p>
                            </div>

                        </div>
                    </header>


                </section>

                <div class="video__player-wrap">
                    <div class="video__player-video">
                        <video id="player" playsinline autoplay  controls preload="auto" class="video__player-video video-js vjs-default-skin vjs-16-9" data-setup='{ "controls": true, "autoplay": true, "preload": "auto" }'></video>
                        <style>
                            .vjs-progress-control, .vjs-live-control,.vjs-remaining-time{
                                display: none !important;
                            }
                        </style>
                    </div>
                    <div class="video__player-bnr">
                        <a href="#course" class="tab-btn__btn" onclick="yaCounter49725970.reachGoal('pg_translation_baner'); return true;">
                            <img src="/img/course_d1.png" alt="" class="video__player-course video__player-course--d">
                            <img src="/img/course_m1.png" alt="" class="video__player-course video__player-course--m">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 p0">
                <div class="chat">
                    <div class="chat__head">
                        Кол-во участников:&nbsp; <span id="viewer">1713</span>
                    </div>
                    <ul class="chat__msg" id="chat">

                    </ul>

                    <div class="chat__message" id="chat__msg">

                        <?php $form = ActiveForm::begin(['id'=>'send-msg', 'fieldConfig' => [
                            'template' => "{input}",
                            'options' => [
                                'tag'=>'span'
                            ]
                        ]]); ?>
                        <div class="chat__message-form">

                            <div class="chat__message-wrap">
                                <div class="chat__message-block--input">
                                    <?= $form->field($chat, 'msg', [
                                        'options' => [
                                            'tag' => false, // Don't wrap with "form-group" div
                                        ],
                                    ])->textInput(['class'=>'chat__message-input', 'placeholder'=>'Введите сообщение'])->label(false) ?>
                                    <?=$form->field($chat,'time_video')->textInput(['type'=>'hidden','value'=>0, 'class'=>'tvt'])->label(false)?>
                                </div>
                                <div class="chat__message-block--btn">

                                    <button class="submit chat__message-btn"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>


                        <?= $form->field($chat, 'webinar',  [
                            'options' => [
                                'tag' => false, // Don't wrap with "form-group" div
                            ],
                        ])->textInput(['type'=>'hidden'])->label(false) ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<div class="add_block">
    <section class="section tab-btn">
        <div class="container">
            <div class="row">

                <div class="col-md-6 p-0">
                    <a class="tab-btn__btn main__btn btn btn--yellow"  href="#course"  onclick="yaCounter49725970.reachGoal('pg_translation_knopka_dostyp1'); return true;">
                        Получить доступ к курсу
                    </a>
                </div>

                <div class="col-md-6 p-0">
                    <a class="tab-btn__btn main__btn btn  btn--blue"  href="#reg"  onclick="yaCounter49725970.reachGoal('pg_translation_knopka_partner'); return true;">
                        Стать партнером
                    </a>
                </div>
            </div>

        </div>
    </section>
    <section class="course tab tab-active " id="course">
        <section class="main">
            <div class="main__wrap">
                <img src="img/course/logo-white.png" alt="FRP" class="main__logo">
                <span class="main__name">Образовательный курс</span>
                <div class="main__img-wrap">
                    <img src="img/course/main.svg" alt="Товарный бизнес с 0 до 100000 руб." class="main__img main__img--desc">
                    <img src="img/course/main-tel.svg" alt="Товарный бизнес с 0 до 100000 руб." class="main__img main__img--tel">
                </div>
                <div class="main__titles">
                    <h1 class="main__title main__title--head">Запусти свой собственный бизнес</h1>
                    <h1 class="main__title main__title--bottom">С доходом от 100 000 ₽</h1>
                </div>
                <div class="main__price">
                    <span class="main__price-name">Всего за</span>
                    <span class="main__price-numb">990 ₽</span>
                </div>
                <!--                <a data-fancybox data-src="#hidden-main" data-product="Заказ курса | Header" data-pprice="990" href="javascript:;" class="main__btn btn btn--yellow">Получить доступ к курсу</a>-->
                <a class="goto main__btn btn btn--yellow" href="#order-form"   onclick="yaCounter49725970.reachGoal('pg_translation_knopka_dostyp2'); return true;">Получить доступ к курсу</a>
            </div>
        </section>
        <!-- Блок 2 -->
        <section class="map">
            <h2 class="h2 map__title">В данном курсе вы изучите</h2>
            <div class="map__wrap">
                <div class="map__item map__item--mark">
                    <div class="map__content">
                        <div class="map__header">
                            <div class="map__img-wrap">
                                <img src="img/course/mark.png" alt="Марк" class="map__img">
                            </div>
                            <h3 class="h3 map__name">Основы<br>упаковки бизнеса</h3>
                        </div>
                        <p class="map__text">Узнайте как проводить анализ ниши, выбрать подходящий товар, изучить конкурентов, найти сильные стороны продукта. Даже новичок сможет за один день выбрать несколько интересных ниш.<br>Как правильно оформить сайт, который будет продавать? Составление продающих заголовков, офферов и интересного описания товара или услуги</p>
                    </div>
                    <div class="map__abs map__abs--mark"></div>
                </div>
                <div class="map__item map__item--kirill">
                    <div class="map__abs map__abs--kirill"></div>
                    <div class="map__content">
                        <div class="map__header">
                            <h3 class="h3 map__name">Настройка релевантного трафика и привлечение клиентов</h3>
                            <div class="map__img-wrap">
                                <img src="img/course/kirill.png" alt="Кирилл" class="map__img">
                            </div>
                        </div>
                        <p class="map__text">Как рекламировать свой сайт. Какие эффективные каналы привлечения клиентов использовать. Как правильно настроить rконтекстную рекламу в Яндекс.Директ и Google.Adwords<br>Продажи в социальных сетях - какие лучше использовать. Как настроить рекламу. Авито - составление эффективных и продающих объявлений. Как получать заявки без вложений.</p>
                    </div>
                </div>
                <div class="map__item map__item--max">
                    <div class="map__content">
                        <div class="map__header">
                            <div class="map__img-wrap">
                                <img src="img/course/max.png" alt="Максим" class="map__img">
                            </div>
                            <h3 class="h3 map__name">Продвижение и обработка заказов. Увеличение среднего чека</h3>
                        </div>
                        <p class="map__text">Что нужно знать про клиента. Как написать скрипт подходящий по для вашего продукта. Продажа по технике SPIN. Как не допустить возражений.</p>
                    </div>
                    <div class="map__abs map__abs--max"></div>
                </div>
            </div>
        </section>
        <!-- Блок 3 -->
        <section class="man">
            <h2 class="h2 man__title">Кому подойдет этот курс</h2>
            <div class="man__wrap">
                <div class="man__item">
                    <div class="man__img-wrap">
                        <img src="img/course/man1.svg" alt="Начинающий" class="man__img">
                    </div>
                    <div class="man__text">
                        <h3 class="h3 man__name">Начинающим предпринимателям</h3>
                        <p class="man__desc">Научитесь всем основам заработка в интернете</p>
                    </div>
                </div>
                <div class="man__item">
                    <div class="man__img-wrap">
                        <img src="img/course/man2.svg" alt="Опытный" class="man__img">
                    </div>
                    <div class="man__text">
                        <h3 class="h3 man__name">Владельцам существующего бизнеса</h3>
                        <p class="man__desc">Увеличьте доход своего бизнеса</p>
                    </div>
                </div>
                <div class="man__item">
                    <div class="man__img-wrap">
                        <img src="img/course/man3.svg" alt="Работник" class="man__img">
                    </div>
                    <div class="man__text">
                        <h3 class="h3 man__name">Наемным сотрудникам</h3>
                        <p class="man__desc">Получайте дополнительный доход не тратя много времени</p>
                    </div>
                </div>
                <div class="man__item">
                    <div class="man__img-wrap">
                        <img src="img/course/man4.svg" alt="Студент" class="man__img">
                    </div>
                    <div class="man__text">
                        <h3 class="h3 man__name">Школьникам и студентам</h3>
                        <p class="man__desc">Запустите свой первый бизнес без вложений</p>
                    </div>
                </div>
            </div>
            <!--
            <a data-fancybox data-src="#hidden-main" data-product="Заказ курса | Header" data-pprice="199" href="javascript:;" class="man__btn btn btn--yellow">Мне подходит <span>- Хочу заказать</span></a>-->
            <a class="goto man__btn btn btn--yellow" href="#order-form"   onclick="yaCounter49725970.reachGoal('pg_translation_knopka_zakazat'); return true;">Мне подходит <span>- Хочу заказать</span></a>
        </section>
        <!-- Доп блок -->
        <section class="money">
            <div class="money__wrap">
                <h2 class="h2 money__title">Гарантируем окумаемость стоимости курса!</h2>
                <p class="money__text">Все ученики окупили стоимоть данного курса уже в первую неделю работы.</p>
                <p class="money__text">Если предоставленные нами знания не помогут Вам окупить стоимость курса, мы гарантируем возврат денег!</p>
                <!--                <a data-fancybox data-src="#hidden-main" data-product="Заказ курса" data-pprice="199" href="javascript:;" class="money__btn btn btn--yellow">Получить курс</span></a>-->
                <a   class=" goto money__btn btn btn--yellow" href="#order-form" onclick="yaCounter49725970.reachGoal('pg_translation_knopka_dostyp3'); return true;">Получить курс</span></a>
            </div>
            <img src="img/course/money.svg" alt="Возврат" class="money__img">
        </section>
        <section class="trener">
            <div class="terner__wrap">
                <div class="trener__img-wrap">
                    <img src="/img/course/trener.png" alt="Эксперт" class="trener__img">
                    <div class="terner__inst-wrap">
                        <span class="trener__inst-text">Страница инстаграм:</span>
                        <!--                        <a class="trener__inst-icon" href="https://www.instagram.com/mark_fankuhin/" target="_blank"><img class="trener__inst-img" src="/img/course/instagram.svg" alt="Инстаграм"></a>-->
                        <a class="trener__inst-icon" href="https://www.instagram.com/mark_fankuhin/" target="_blank"><img class="trener__inst-img" src="/img/course/instagram.svg" alt="Инстаграм"></a>
                    </div>
                </div>
                <div class="trener__content">
                    <h2 class="h2 trener__title"><span class="trener__subtitle sub2">Бонус всем участникам курса</span></h2>
                    <h2 class="h2 trener__title">Персональный разбор <span class="trener__subtitle">От основателя Fank Retail Partners</span></h2>
                    <p class="trener__text">Ответит на все вопросы по запуску товарного бизнеса. Работа над ошибками по выполнению заданий. Рекомендации и советы по достижению цели "Доход от 100 000 рублей ежемесячно"</p>
                    <!--                    <a data-fancybox data-src="#hidden-consult" data-product="Консультация" data-pprice="0" href="javascript:;" class="trener__btn btn btn--blue">Получить консультацию</a>-->
                    <a  class=" goto trener__btn btn btn--blue" href="#order-form" onclick="yaCounter49725970.reachGoal('pg_translation_knopka_consultation'); return true;">Получить консультацию</a>
                </div>
            </div>
        </section>
        <!-- Блок 5 -->


        <!-- Тарифы -->


        <div class="tarif__wrap">
            <h2 class="h2 tarif__title">Начните сейчас</h2>
            <div class="tarif">
                <div class="tarif__item tarif__item--a">
                    <h3 class="tarif__name">Стандартный</h3>
                    <ul class="tarif__list">
                        <li class="tarif__text">Доступ к образовательной платформе</li>
                        <li class="tarif__text">Обучающий видеокурс для запуска товарного бизнеса (самостоятельно)</li>
                    </ul>
                    <p class="tarif__price">990<span> ₽</span></p>
                    <a href="#order-form" class="tarif__btn btn" data-pprice="990" data-target="1" onclick="yaCounter49725970.reachGoal('pg_translation_knopka_course990'); return true;">Выбрать</a>
                </div>
                <div class="tarif__item tarif__item--b tarif__item--active">
                    <h3 class="tarif__name">Персональньный</h3>
                    <ul class="tarif__list tarif__list--active">
                        <li class="tarif__text">Доступ к обучающей платформе</li>
                        <li class="tarif__text">Обучающий видеокурс для запуска товарного бизнеса</li>
                        <li class="tarif__text"><b>Домашние задания</b></li>
                        <li class="tarif__text"><b>Проверка домашних заданий</b></li>
                        <li class="tarif__text"><b>Рекомендации по улучшению сайта, рекламных кампаний, скриптов продаж</b></li>
                    </ul>
                    <p class="tarif__price">2990<span> ₽</span></p>
                    <a href="#order-form" class="tarif__btn btn tarif__btn--active" data-pprice="2990" data-target="2" onclick="yaCounter49725970.reachGoal('pg_translation_knopka_course2990'); return true;">Выбран</a>
                </div>
                <div class="tarif__item tarif__item--c">
                    <h3 class="tarif__name">VIP</h3>
                    <ul class="tarif__list">
                        <li class="tarif__text">Доступ к образовательной платформе</li>
                        <li class="tarif__text">Обучающий видеокурс для запуска товарного бизнеса</li>
                        <li class="tarif__text">Домашние задания</li>
                        <li class="tarif__text">Проверка домашних заданий</li>
                        <li class="tarif__text">Рекомендации по улучшению сайта, рекламных кампаний, скриптов продаж</li>
                        <li class="tarif__text"><b>Персональный разбор от основателя FRP, Марка Фанкухина</b></li>
                        <li class="tarif__text"><b>Персональное ведение до результата 5 продаж</b></li>
                    </ul>
                    <p class="tarif__price">9990<span> ₽</span></p>
                    <a href="#order-form" class="tarif__btn btn" onclick="yaCounter49725970.reachGoal('pg_translation_knopka_course9990'); return true;" data-pprice="9990" data-target="3">Выбрать</a>
                </div>
            </div>
        </div>

        <section class="footer" id="order-form">
            <div class="footer__wrap">
                <h2 class="h2 footer__title">Запишись на курс<span class="footer__subtitle">и начни зарабатывать на своем бизнесе от 100000 ₽</span></h2>




                <?php $form_order = ActiveForm::begin(['id'=>'course-form','action'=>'/webinar/add-course', 'fieldConfig' => [
                    'template' => "{input}",
                    'options' => [
                        'tag'=>'span'
                    ]
                ]]); ?>
                <div class="footer__input-cont">
                    <?$course->course = 2;?>
                    <div class="footer__input-wrap">
                        <?= $form_order->field($course, 'name', [
                            'options' => [
                                'tag' => false, // Don't wrap with "form-group" div
                            ],
                        ])->textInput(['class'=>'footer__input','placeholder'=>'Введите Имя...', 'required'=>'required'])->label(false) ?>

                    </div>
                    <div class="footer__input-wrap">


                        <?= $form_order->field($course, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '+7 (999) 999-99-99',
                            'options' => [
                                'class' => 'footer__input mask',
                                'id' => 'phone2',
                                'placeholder' => ('Введите телефон...'),
                                'type'=>'phone',
                                'required'=>'required',
                                'tag'=>false
                            ],
                            'clientOptions' => [
                                'clearIncomplete' => true
                            ]
                        ])->label(false) ?>


                    </div>
                    <div class="footer__input-wrap">
                        <?= $form_order->field($course, 'email', [
                            'options' => [
                                'tag' => false, // Don't wrap with "form-group" div
                            ],
                        ])->textInput(['class'=>'footer__input', 'type'=>'email','placeholder'=>'Введите Емайл...', 'required'=>'required'])->label(false) ?>
                    </div>

                </div>

                <input type="hidden" id="type_course" class="type_course" name="OrderCourseForm[course]" value="2">
                <button class="footer__btn btn btn--yellow" onclick="yaCounter49725970.reachGoal('pg_translation_knopka_form_footer'); return true;">Начать обучение за 2990 ₽</button>

                <?php ActiveForm::end(); ?>


            </div>
        </section>
        <div>


        </div>

        <!-- Доступ к курсу -->


        <div style="display: none;" id="hidden-main" class="h-form">
            <h3 class="h3 h-form__title">Для получения доступа к курсу заполните форму ниже</h3>

            <form class="h-form__form" action="/send.php" method="post">
                <input class="h-form__input" type="text" name='name' required placeholder="Введите Имя...">
                <input class="h-form__input mask" type="text" name='phone' required placeholder="Введите Номер телефона...">
                <input type="hidden" name='variant' value="">
                <input type="hidden" name='landing' value="Видеоглазки" >
                <input type="hidden" name='category_title' value="Заказ">
                <input type="hidden" name='product_title' value="Видеоглазки">
                <button class="btn btn--yellow h-form__btn">Получить курс</button>
            </form>

        </div>


        <!-- Консультация -->


        <div style="display: none;" id="hidden-consult" class="h-form">
            <h3 class="h3 h-form__title">Для получения консультации заполните форму ниже</h3>

            <form class="h-form__form" action="/send.php" method="post">
                <input class="h-form__input" type="text" name='name' required placeholder="Введите Имя...">
                <input class="h-form__input mask" type="text" name='phone' required placeholder="Введите Номер телефона...">
                <input type="hidden" name='variant' value="">
                <input type="hidden" name='landing' value="Видеоглазки" >
                <input type="hidden" name='category_title' value="Заказ">
                <input type="hidden" name='product_title' value="Видеоглазки">
                <button class="btn btn--blue h-form__btn">Получить консультацию</button>
            </form>

        </div>
    </section>

    <div class="tab " id="reg">
        <section class="register " >
            <div class="register__content">
                <h1 class="register__title">Стань партнером успешного бизнес сообщества и зарабатывай <b>от 100 000 ₽ в месяц</b></h1>
            </div>
            <div class="register__form-block">
                <div class="register__form-wrap">
                    <p class="register__action">Зарегистрируйтесь прямо сейчас</p>



                    <?php $form_partner = ActiveForm::begin(['id'=>'partner-form', 'action'=>'/webinar/add-partner','fieldConfig' => [

                        'template' => "{input}",
                        'options' => [
                            'tag'=>'span',
                            'action'=>'/webinar/course',
                        ]
                    ]]); ?>

                    <div class="register__radio">

                        <input type="radio" id="drop" name="PartnerCourseForm[type]" value="1" checked>
                        <label for="drop">Дропшиппинг</label>
                        <input type="radio" id="opt" name="PartnerCourseForm[type]" value="2">
                        <label for="opt">ОПТ</label>
                    </div>

                    <?= $form_partner->field($partner, 'name', [
                        'options' => [
                            'tag' => false, // Don't wrap with "form-group" div
                        ],
                    ])->textInput(['class'=>'form__input register__input','placeholder'=>'Введите Имя...', 'required'=>'required'])->label(false) ?>

                    <?= $form_partner->field($partner, 'email', [
                        'options' => [
                            'tag' => false, // Don't wrap with "form-group" div
                        ],
                    ])->textInput(['class'=>'form__input register__input', 'type'=>'email','placeholder'=>'Введите Е-майл...', 'required'=>'required'])->label(false) ?>

                    <?= $form_partner->field($partner, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7 (999) 999-99-99',
                        'options' => [
                            'class' => 'form__input register__input',
                            'placeholder' => ('Введите телефон...'),
                            'type'=>'phone',
                            'required'=>'required',
                            'tag'=>false,

                        ],
                        'clientOptions' => [
                            'clearIncomplete' => true
                        ]
                    ])->label(false) ?>

                    <button class="register__btn">Зарегистрироваться</button>
                    <?php ActiveForm::end(); ?>


                </div>
            </div>
        </section>
    </div>

</div>
<!--


<?php /*$form_partner = ActiveForm::begin(['class'=>'form register__form', 'fieldConfig' => [
    'template' => "{input}",
    'options' => [
        'tag'=>'span'
    ]
]]); */?>

<div class="register__radio">
    <?/*=$form_partner->field($partner, 'type', ['options'=>['tag' => false,]])
        ->radio([
            'id'=>'drop',
            'tag' => false,

        ])->label('Дропшиппинг',['for'=>'drop']);*/?>
    <?/*=$form_partner->field($partner, 'type', ['options'=>['tag' => false,]])
        ->radio([
            'id'=>'drop',
            'tag' => false,
        ])->label('ОПТ',['for'=>'drop']);*/?>
</div>


<?/*= $form_partner->field($partner, 'name', [
    'options' => [ 'tag' => false, ],
])->textInput(['class'=>'form__input register__input','placeholder'=>'Введите Имя...', 'required'=>'required'])->label(false) */?>



--><?php /*ActiveForm::end(); */?>