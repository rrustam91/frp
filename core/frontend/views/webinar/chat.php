<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.08.2018
 * Time: 12:47
 */

?>

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $chat \common\models\forms\ChatMessagesForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$chat->webinar = 1;
?>


<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id'=>'send-msg']); ?>
            <div class="form-group">

                <?= $form->field($chat, 'msg')->textInput(['class'=>'chat__message-input','autofocus' => true, 'placeholder'=>'Введите сообщение'])->label(false) ?>
                <?= $form->field($chat, 'webinar')->textInput(['type'=>'hidden'])->label(false) ?>

                <button class="submit chat__message-btn"><i class="fa fa-paper-plane" aria-hidden="true"></i>Отправить</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>