
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $chat \common\models\forms\ChatMessagesForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;



?>


<div  id="reg" style="display:block;">
    <section class="register " >
        <div class="register__content">
            <h1 class="register__title">Стань партнером успешного бизнес сообщества и зарабатывай <b>от 100 000 ₽ в месяц</b></h1>
        </div>
        <div class="register__form-block">
            <div class="register__form-wrap">
                <p class="register__action">Зарегистрируйтесь прямо сейчас</p>



                <?php $form_partner = ActiveForm::begin(['id'=>'partner-form', 'action'=>'/webinar/add-partner','fieldConfig' => [

                    'template' => "{input}",
                    'options' => [
                        'tag'=>'span',
                        'action'=>'/webinar/course',
                    ]
                ]]); ?>

                <div class="register__radio">

                    <input type="radio" id="drop" name="PartnerCourseForm[type]" value="1" checked>
                    <label for="drop">Дропшиппинг</label>
                    <input type="radio" id="opt" name="PartnerCourseForm[type]" value="2">
                    <label for="opt">ОПТ</label>
                </div>

                <?= $form_partner->field($partner, 'name', [
                    'options' => [
                        'tag' => false, // Don't wrap with "form-group" div
                    ],
                ])->textInput(['class'=>'form__input register__input','placeholder'=>'Введите Имя...', 'required'=>'required'])->label(false) ?>

                <?= $form_partner->field($partner, 'email', [
                    'options' => [
                        'tag' => false, // Don't wrap with "form-group" div
                    ],
                ])->textInput(['class'=>'form__input register__input', 'type'=>'email','placeholder'=>'Введите Е-майл...', 'required'=>'required'])->label(false) ?>

                <?= $form_partner->field($partner, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+7 (999) 999-99-99',
                    'options' => [
                        'class' => 'form__input register__input',
                        'placeholder' => ('Введите телефон...'),
                        'type'=>'phone',
                        'required'=>'required',
                        'tag'=>false,

                    ],
                    'clientOptions' => [
                        'clearIncomplete' => true
                    ]
                ])->label(false) ?>

                <button class="register__btn">Зарегистрироваться</button>
                <?php ActiveForm::end(); ?>


            </div>
        </div>
    </section>
</div>
