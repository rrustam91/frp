<?php
$link = 'http://fankretailpartners.ru/webinar';
$this->title = 'Спасибо за регистрацию';
?>
<style>
    .thanks{
        width: 100%;
        max-width: 800px;
        margin: 20px auto;
        text-align: center;
        padding: 20px;
    }
    .thanks__title{
        margin: 0 auto;
    }
    .thanks__desc{

    }
    .thanks__link{
        display: block;
        width: 100%;
        min-height: 50px;
        padding: 20px;
        color: #333;
        font-size: 24px;
        background: #e5e5e5;
        border-radius: 50px;
    }
    .thanks__link:hover,.thanks__link:active, .thanks__link:focus  {
        color: #333;
        text-decoration: none;
    }
    .thanks-social{
        margin: 30px auto;
    }
    .thanks-social__wrap{
        max-width: 300px;
        margin: 0 auto;
        display: flex;
        flex-direction: row;


    }
    .thanks-social__item{
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 10px;
        background: #5546ba;
        margin: 0 auto;
        min-width: 50px;
        min-height: 50px;
        color: white;
        transition: .5s all ease;

    }
    .thanks-social__item:hover,.thanks-social__item:active,.thanks-social__item:focus {
        text-decoration: none;
        color: white;
        background: #7867ec;
    }
</style>

<!-- Главная -->

<section class="section head">
    <header class="head__header">
        <div class="head__img-wrap">
            <a href="/"><img src="/img/logo.jpg" alt="FRP" class="head__img">
                <img src="/img/logo_m.png" alt="FRP" class="head__img head__img--mobile"></a>
        </div>
        <p class="head__live">Онлайн трансляция по всему миру</p>
    </header>
    <div class="thanks">
        <div>
            <h1 class="thanks__title">Спасибо за регистрацию</h1>
            <p class="thanks__desc">Ваша ссылка на вебинар:</p>
            <a href="<?= $link ?>" class="thanks__link" >
                <?= $link ?>
            </a>
            <hr>

            <p class="thanks__desc">
                <span>
                    Теперь вы можете скачать PDF
                </span>
            </p>
            <h1 class="thanks__title" style="margin: 20px auto; font-weight: bold;">
                ТОП-10 Прибыльных и актуальных<br>
                ниш для дропшиппинга </h1>
            <div class="btnCont" align="center">
                <a href="/upload/top10.pdf" target="_blank" class="main-content__button btn btn--blue scroll ripplelink grey scroll" onclick="yaCounter49725970.reachGoal('pg_reg_save_pdf'); return true;"><span class="ink animate" style="height: 350px; width: 350px; top: -151px; left: 80px;"></span>
                    Cкачать
                </a>
            </div>

        </div>
        <div class="thanks-social">
            <p class="thanks-social__title">Всю актуальную информацию вы можете узнать здесь:</p>
            <div class="thanks-social__wrap">
                <a href="#" class="thanks-social__item"><i class="fab fa-vk"></i></a>
                <a href="#" class="thanks-social__item"><i class="fab fa-telegram-plane"></i></a>
                <a href="#" class="thanks-social__item"><i class="fab fa-whatsapp"></i></a>
                <a href="#" class="thanks-social__item"><i class="fab fa-facebook"></i></a>
            </div>
        </div>
    </div>
    <div class="thanks-social">

    </div>
</section>
