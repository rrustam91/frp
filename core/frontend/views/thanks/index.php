
<div class="wrap">
    <img src="img/logo-white.png" alt="">
    <h1>Спасибо, ваш платеж получен</h1>
    <p class="desc">Доступ к первому модулю будет открыт <span>21 августа 2018 года</span>.</p>
    <p class="desc">Все данные мы отправим вам на указанный электронный адрес.</p>
    <div class="bon">
        <p class="bonus">Ваш дополнительный бонус</p>
        <a href="/upload/top10.pdf" class="btn">Получить бонус</a>
    </div>
    <p class="contact">
        Почта для обратной связи:
        <span>web@fankretailpartners.ru</span>
    </p>
</div>
