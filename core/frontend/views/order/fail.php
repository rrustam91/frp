<style>
    .wrap{
        background: white;
        color: #333;
        max-width: 600px;
        margin: 40px auto;
        border-radius: 20px;
        box-shadow: 0px 10px 20px rgba(0,0,0,.3);
        padding: 25px;
    }
    .desc{
        font-size: 16px;
    }
</style>
<div class="wrap" style="">
    <img src="img/logo-white.png" alt="">
    <h1 style="font-size: 24px; font-weight: bold;">Платеж не прошел! <br>Что-то пошло не так</h1>
    <p class="desc">Не беспокойтесь, наши технические специалисты,<br> уже знают о проблеме.</p>
    <br><br>

    <div class="btnCont" align="center">
        <a href="/" class="main-content__button btn btn--blue scroll ripplelink grey scroll" style="font-size: 16px; padding: 10px; background: #7a6ef4; color: white; box-shadow: 0 0 0 0 ; " >
            Вернуться на главную
        </a>
        <br>
    </div>
    <p class="desc">
        Почта для обратной связи:
        <span>web@fankretailpartners.ru</span>
    </p>

</div>
