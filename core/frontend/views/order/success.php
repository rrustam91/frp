
<style>
    .wrap{
        background: white;
        color: #333;
        max-width: 600px;
        margin: 40px auto;
        border-radius: 20px;
        box-shadow: 0px 10px 20px rgba(0,0,0,.3);
        padding: 25px;
    }
    .desc{
        font-size: 16px;
    }
</style>
<div class="wrap" style="">
    <img src="img/logo-white.png" alt="">
    <h1 style="font-size: 24px; font-weight: bold;">
        Платеж прошел успешно!<br>
        Доступ к первому модулю открыт
    </h1>
    <p class="desc">
        Данные для входа в кабине:
    </p>
    <p class="desc">
        <b>Логин:</b>
        <?=$user->email?>
    </p>
    <p class="desc">
        <b>Пароль:</b>
        <?=$user->username?>
    </p>
    <br><br>

    <div class="btnCont" align="center">
        <a href="https://cp.fankretailpartners.ru" class="main-content__button btn btn--blue scroll ripplelink grey scroll" style="font-size: 16px; padding: 10px; background: #7a6ef4; color: white; box-shadow: 0 0 0 0 ; " >
           Начать обучение
        </a>
        <br>
    </div>
    <p class="desc">
        Почта для обратной связи:
        <span style="color: darkblue">web@fankretailpartners.ru</span>
    </p>

</div>

