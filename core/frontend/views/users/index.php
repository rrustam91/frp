<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Users'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'username',
                'email:email',
                'name',
                'phone',
                // 'role',
                // 'auth_key',
                // 'password_hash',
                // 'password_reset_token',
                // 'status',
                // 'created_at',
                // 'updated_at',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'visibleButtons' => [
                        'update' => function ($model) {
                            return \Yii::$app->user->can('update', ['post' => $model]) ;
                        },
                        'delete' => function ($model) {
                            return \Yii::$app->user->can('delete', ['post' => $model]);
                        },
                    ]
                ],

            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
