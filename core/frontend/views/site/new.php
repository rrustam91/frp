<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Начни зарабатывать от  100.000 ₽ с первого месяца на продаже товаров через интернет';
?>

<section class="header-new">
    <div class="header-new__container container">
        <div class="header-new__row row">
            <div class="col-12">
                <div class="header-new__logo " align="right" >
                    <img src="new_page/img/logo-head.png" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-12">

                <div class="header-new__title-block " >
                    <h1 class="header-new__title-up" >
                        Открой свой
                    </h1>
                    <h1 class="header-new__title" >
                        интернет-магазин
                    </h1>
                    <h1 class="header-new__title-sub" >
                        за 14 дней
                    </h1>
                    <div class="header-new__plus">
                        <div class="header-new__plus-bl">
                            <div class="header-new__plus-blc">
                                Без закупки товаров
                            </div>
                            <div class="header-new__plus-blc">
                                Без склада
                            </div>

                        </div>

                        <div class="header-new__plus-bl">
                            <div class="header-new__plus-blc">
                                Без сотрудников
                            </div>
                            <div class="header-new__plus-blc">
                                Без офиса
                            </div>

                        </div>
                        <div class="header-new__plus-bl">
                            <div class="header-new__plus-blc">
                                Без бухгалтерии
                            </div>
                            <div class="header-new__plus-blc">
                                Без нервов
                            </div>

                        </div>

                    </div>
                    <p class="header-new__sub ">
							<span>
								Научись на ошибках успешного предпринимателя
								с оборотом более 300 млн в год
							</span>
                    </p>
                    <div class="wow fadeIn">
                        <a class="btn btn--main shadow--theme shadow-shine pulse shine goto " href="#about">
							<span class="btn__inner  ">
								Узнать как
							</span>
                        </a>
                    </div >

                </div>
            </div>
        </div>
    </div>
</section>
<section class="about" id="about">
    <div class="container about__container">
        <div class="about__row row">
            <div class="col-md-12 ovr-h" align="center" style="overflow: hidden">
                <h1 class="about__title title   m-0 p-0 " data-aos="fade-down" data-aos-duration="1000"  data-aos-delay="0">
                    Но перед тем как узнать, <span class="colored">давай познакомимся</span>
                </h1>
                <h3 class="about__title-sub title-sub " data-aos="fade-up" data-aos-duration="1000"  data-aos-delay="100" style="margin-top: 20px;">
                    Меня зовут Фанкухин Марк.
                </h3>
                <p class="text"  data-aos="fade-up" data-aos-duration="1000"  data-aos-delay="300">
                    Я основатель крупнейшего в России товарного сообщества FankRetaiPartners
                </p>
            </div>
        </div>
        <div class="about__row about__row-f row">
            <div class="col-md-12 col-lg-12" style="">
                <div class="about__cart about__cart-f left"  data-aos="fade-right" data-aos-duration="1000"  data-aos-delay="0">
                    <div class="about__cart--wrap ovr-h" >
                        <div class="text-sub"  data-aos="fade-left-down" data-aos-duration="1000"  data-aos-delay="100">
                            Мне
                        </div>
                        <div class="text-big"  data-aos="fade-left" data-aos-duration="1000"  data-aos-delay="200">
                            27
                        </div>
                        <div class="text-sub-d"  data-aos="fade-up" data-aos-duration="1000"  data-aos-delay="300">
                            лет
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-12">

                <div class="about__cart about__cart-s right"  data-aos="fade-left" data-aos-duration="1000"  data-aos-delay="0">
                    <div class="about__cart--wrap">
                        <div class="text-sub"  data-aos="fade-left" data-aos-duration="1000"  data-aos-delay="100">
                            более
                        </div>
                        <div class="text-big"  data-aos="fade-right" data-aos-duration="1000"  data-aos-delay="200">
                            5<span class="revert">Лет</span>
                        </div>
                        <div class="text-sub-d"  data-aos="fade-up" data-aos-duration="1000"  data-aos-delay="300" align="right">
                            в товарном <br>
                            бизнесе с нуля!
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-12 ">
                <div class="about__cart about__cart-th left"  data-aos="fade-right" data-aos-duration="1000"  data-aos-delay="0">
                    <div class="about__cart--wrap">
                        <div class="text-sub"   data-aos="fade-left" data-aos-duration="1000"  data-aos-delay="100">
                            Точка А:
                        </div>
                        <div class="text-big"   data-aos="fade-up" data-aos-duration="1000"  data-aos-delay="200">
                            45 000<span class="revert">руб</span>
                        </div>
                        <div class="text-sub-d"   data-aos="fade-left" data-aos-duration="1000"  data-aos-delay="300">
                            в месяц
                        </div>
                        <div class="text-sub-ds"   data-aos="fade-left" data-aos-duration="1000"  data-aos-delay="300">
                            Работая барменом, не имея представления о бизнесе
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 ">

                <div class="about__cart about__cart-fr right"  data-aos="fade-left" data-aos-duration="1000"  data-aos-delay="0">
                    <div class="about__cart-w">
                        <div class="txt-1 txt"  data-aos="fade-down" data-aos-duration="1000"  data-aos-delay="100">
                            Точка Б:
                        </div>
                        <div class="txt-2  txt"  data-aos="fade-left" data-aos-duration="1000"  data-aos-delay="200">
                            <span class="num_mask">100000000</span>
                        </div>
                        <div class="txt-3  txt"  data-aos="fade-down" data-aos-duration="1000"  data-aos-delay="300">
                            Рублей в год
                        </div>
                        <div class="txt-4  txt"  data-aos="fade-up" data-aos-duration="1000"  data-aos-delay="400">
                            цель к 27 годам
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 ovr-h " style="padding-bottom: 15px;">

                <div class="about__cart about__cart-fr about__cart-frr left"  data-aos="fade-left" data-aos-duration="1000"  data-aos-delay="0">
                    <div class="about__cart-w">
                        <div class="txt-1 txt"  data-aos="fade-down" data-aos-duration="1000"  data-aos-delay="100">
                            Точка Б по факту:
                        </div>
                        <div class="txt-2  txt "  data-aos="fade-down" data-aos-duration="1000"  data-aos-delay="200">
                            Оборот рублей в год
                        </div>
                        <div class="txt-3  txt"  data-aos="fade-left" data-aos-duration="1000"  data-aos-delay="200">
                            <span class="num_mask ">300000000</span>
                        </div>
                        <div class="txt-4  txt"  data-aos="fade-up" data-aos-duration="1000"  data-aos-delay="300">
                            перевыполнена на
                        </div>
                        <div class="txt-5  txt"  data-aos="fade-up" data-aos-duration="1000"  data-aos-delay="300">
                            200 000 000 руб
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about__row about__nish-blc row">
            <div class="col-md-4 about__nish-block ovr-h" align="right">
                <div class="about__nish-list nish nish__1" data-aos="fade-left" data-aos-duration="1400"  data-aos-delay="250">

                    <div class="d-none">
                        <img src="new_page/img/nish__1.jpg" alt="" class="img-responsive about__nish-img">
                    </div>

                    <div align="right">
							<span data-aos="fade-left" data-aos-duration="1400"  data-aos-delay="350">
								Товарный бизнес
							</span>

                        <p data-aos="fade-left" data-aos-duration="1400"  data-aos-delay="450" align="right" class="" style="margin: 0;">
                            Продаем товары через интернет в больших объемах в розницу и оптом по всей России и СНГ
                        </p>
                    </div>
                </div>
                <div class="about__nish-list nish  nish__2" data-aos="fade-left" data-aos-duration="1400"  data-aos-delay="500">
                    <div class="d-none">
                        <img src="new_page/img/nish__2.jpg" alt="" class="img-responsive about__nish-img">
                    </div>

                    <div>
							<span data-aos="fade-left" data-aos-duration="1400"  data-aos-delay="350">
								Логистическое предприятие
							</span>

                        <p data-aos="fade-left" data-aos-duration="1400"  data-aos-delay="450" align="right">
                            Мы возим товары с фабрик из Китая, доставляем в кратчайшие сроки
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 about__nish-title "  data-aos="fade" data-aos-duration="2000"  data-aos-delay="0">
                <h1 class="about__nish about__nish-title about__title title " align="center">
                    <div data-aos="fade-down" data-aos-duration="2500"  data-aos-delay="0" class="ovr-h">
                        Ниши<span class="colored blue">:</span>
                    </div>
                </h1>

            </div>
            <div class="col-md-4 about__nish-block ovr-h">

                <div class="about__nish-list nish  nish__3" data-aos="fade-right" data-aos-duration="1400"  data-aos-delay="250">

                    <div class="d-none">
                        <img src="new_page/img/nish__3.jpg" alt="" class="img-responsive about__nish-img">
                    </div>

                    <div align="left">
							<span data-aos="fade-right" data-aos-duration="1400"  data-aos-delay="350">
								Онлайн образование
							</span>
                        <p data-aos="fade-right" data-aos-duration="1400"  data-aos-delay="450" align="left">
                            Разработали и запустили собственную образовательную платформу FRP, в которой каждый может научиться запуску бизнеса и стать успешным предпринимателем
                        </p>
                    </div>
                </div>
                <div class="about__nish-list nish nish__4" data-aos="fade-right" data-aos-duration="1400"  data-aos-delay="500">

                    <div class="d-none">
                        <img src="new_page/img/nish__4.jpg" alt="" class="img-responsive about__nish-img">
                    </div>
                    <div>
							<span data-aos="fade-left" data-aos-duration="1400"  data-aos-delay="350">
								Производство портативного видеонаблюдения
							</span>
                        <p data-aos="fade-right" data-aos-duration="1400"  data-aos-delay="450" align="left">
                            Снабжаем крупшейшие банки, гос.структуры своим собственным оборудованием, позволяющем контролировать работу всех сотрудников
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <div class="" data-aos="fade-up"   data-aos-delay="500" data-aos-duration="1500"  >
                    <a class="d-flex btn btn--main shadow--theme shadow-shine pulse shine goto m-0-auto" href="#solution">
									<span class="btn__inner  ">
										Хочу начать зарабатывать
									</span>
                    </a>
                </div >
            </div>
        </div>
        <div class="about__row row about__brands ovr-h">
            <div class="col-md-12" align="center" data-aos="fade-down" data-aos-duration="1400"  data-aos-delay="0">
                <h1 class="about__title title" align="center">
                    Бренды<span class="colored blue">:</span>
                </h1>
                <p class="about__title-sub "  style="max-width: 600px; margin: 20px auto;">
                    Благодаря работе напрямую с заводами и большим объемам закупок мы разработали свои бренды
                </p>
            </div>
            <div class="col-6 col-sm-6 col-md-4 ovr-h" align="center" >
                <img src="new_page/img/brand/1.png" alt="about__logos" class="about__logos img-responsive" data-aos="fade-up" data-aos-duration="1400"  data-aos-delay="250">
            </div>
            <div class="col-6 col-sm-6 col-md-4 ovr-h" align="center">
                <img src="new_page/img/brand/2.png" alt="about__logos" class="about__logos img-responsive" data-aos="fade-up" data-aos-duration="1400"  data-aos-delay="500" >
            </div>
            <div class="col-6 col-sm-6 col-md-4 ovr-h" align="center">
                <img src="new_page/img/brand/3.png" alt="about__logos" class="about__logos img-responsive" data-aos="fade-up" data-aos-duration="1400"  data-aos-delay="750">
            </div>
            <div class="col-6 col-sm-6 col-md-4 ovr-h" align="center">
                <img src="new_page/img/brand/6.png" alt="about__logos" class="about__logos img-responsive" data-aos="fade-down" data-aos-duration="1400"  data-aos-delay="250">
            </div>
            <div class="col-6 col-sm-6 col-md-4 ovr-h" align="center">
                <img src="new_page/img/brand/4.png" alt="about__logos" class="about__logos img-responsive" data-aos="fade-down" data-aos-duration="1400"  data-aos-delay="500" >
            </div>
            <div class="col-6 col-sm-6 col-md-4 ovr-h" align="center">
                <img src="new_page/img/brand/5.png" alt="about__logos" class="about__logos img-responsive" data-aos="fade-down" data-aos-duration="1400"  data-aos-delay="750">
            </div>
        </div>
        <div class="about__row row">
            <div class="col-md-12 ovr-h" align="center">

                <div class="about__cart about__cart-fv "  data-aos="fade-up"  data-aos-duration="1000"  data-aos-delay="0">
                    <div class="about__cart--wrap">
                        <div class="text-sub"  data-aos="fade-down"  data-aos-duration="1000"  data-aos-delay="250">
                            Обучил более
                        </div>
                        <div class="text-big"  data-aos="fade"  data-aos-duration="3000"  data-aos-delay="250">
                            700
                        </div>
                        <div class="text-sub-d"  data-aos="fade-up"  data-aos-duration="1000"  data-aos-delay="500">
                            человек
                        </div>
                        <div class="text-anth-f"  data-aos="fade-up"  data-aos-duration="1000"  data-aos-delay="350">
                            Как запустить

                        </div>
                        <div class="ovr-h">
                            <div class="text-anth-s"   data-aos="fade-up"  data-aos-duration="1000"  data-aos-delay="450">
                                свой интернет магазин с нуля
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="trouble">
        <div class="container trouble__container">
            <div class="row trouble__row" >

                <div class="col-md-12">
                    <h1 class="trouble__title title   m-0 p-0 " >
						<span data-aos="fade-up"  data-aos-duration="2500"  data-aos-delay="0">
							С какими
						</span>
                        <span class="colored-r"  data-aos="fade-up"  data-aos-duration="2500"  data-aos-delay="250">
							ошибками
						</span>

                        <span data-aos="fade-up"  data-aos-duration="2500"  data-aos-delay="500">
						и
						</span>
                        <span class="colored-r" data-aos="fade-up"  data-aos-duration="2500"  data-aos-delay="750">
							сложностями
						</span>

                        <span data-aos="fade-up"  data-aos-duration="2500"  data-aos-delay="1000">
							я столкнулся за
						</span>
                        <span class="colored-r"  data-aos="fade-up"  data-aos-duration="2500"  data-aos-delay="1250">
							5 лет
						</span>

                        <span data-aos="fade-up"  data-aos-duration="2500"  data-aos-delay="1500">
							в товарном бизнесе:
						</span>
                    </h1>
                </div>

                <div class="col-md-12 ovr-h" >
                    <div class="trouble__list left"   data-aos="fade-down"  data-aos-duration="1000"  data-aos-delay="500">

                        <span class="colored-r">Потратил</span> на тестирование и гипотезы более <span class="colored-r">6 000 000 руб</span>
                    </div>
                </div>
                <div class="col-md-12 ovr-h">
                    <div class="trouble__list right"   data-aos="fade-down"  data-aos-duration="1000"  data-aos-delay="700">
                        <span class="colored-r">Слил более 15 000</span> заявок из-за отсутствия продающего скрипта продаж
                    </div>
                </div>

                <div class="col-md-12 ovr-h">
                    <div class="trouble__list center"  data-aos="fade-down"  data-aos-duration="1000"  data-aos-delay="700">
                        Столкнулся <span class="colored-r">с безответственными поставщиками</span>, которые задерживали сроки поставки товаров
                    </div>
                </div>
                <div class="col-md-12 ovr-h">
                    <div class="trouble__list left"  data-aos="fade-down"  data-aos-duration="1000"  data-aos-delay="500" >


                        Не рассчитал <span class="colored-r">процент невыкупаемости</span> почтовых отправлений, что существенно сокращало оборотные средства
                    </div>
                </div>

                <div class="col-md-12 ovr-h">
                    <div class="trouble__list right"  data-aos="fade-down"  data-aos-duration="1000"  data-aos-delay="700">

                        <span class="colored-r">Задержки выплат</span> наложенного плажета от курьерских компаний
                    </div>
                </div>
                <div class="col-md-12 ovr-h">
                    <div class="trouble__list center"  data-aos="fade-down"  data-aos-duration="1500"  data-aos-delay="500">
                        <span class="colored-r">20% брака</span>, из за которого приходилось переотправлять товар клиентам, что тоже <span class="colored-r">увеличивает расходы</span>
                    </div>
                </div>

                <div class="col-md-12 ovr-h">
                    <div class="trouble__list left"  data-aos="fade-down"  data-aos-duration="1500"  data-aos-delay="700">
                        Потратил <span class="colored-r">более 100</span> часов на собеседования в поисках персонала (менеджеры, логисты)
                    </div>
                </div>

                <div class="col-md-12 ovr-h">
                    <div class="trouble__list right" data-aos="fade-down"  data-aos-duration="1500"  data-aos-delay="500">
                        Более <span class="colored-r">50 часов на обучение</span> менеджеров по продажам
                    </div>
                </div>

                <div class="col-md-12 ovr-h">
                    <div class="trouble__list center" data-aos="fade-down"  data-aos-duration="1500"  data-aos-delay="700" >
                        Привлек более <span class="colored-r num_mask">200000000 руб</span> от сторонних инсвесторов для закупки товара
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container solution__container">
        <div class="row solution__row" data-aos="fade"   data-aos-delay="200" data-aos-duration="1500"  >

            <div class="col-md-12 ovr-h">
                <br><br>
                <div class="solution__text-down">

                    <h2 class="solution__text-title" align="center" data-aos="fade-down"   data-aos-delay="100" data-aos-duration="1500"  >
                        Пройдя весь путь проб и ошибок я решил создать сообщество <span class="colored-b f-bold">FankRetailPartners</span>, где я и ведущие эксперты в области товарного бизнеса делимся накопленным опытом, помогаем быстро и успешно запустить свой интернет магазин с нуля


                    </h2>
                    <div class="header-new__plus">
                        <div class="header-new__plus-bl">
                            <div class="header-new__plus-blc" data-aos="fade"   data-aos-delay="300" data-aos-duration="1500"  >
                                Без закупки товаров
                            </div>
                            <div class="header-new__plus-blc" data-aos="fade"   data-aos-delay="400" data-aos-duration="1500"  >
                                Без склада
                            </div>

                        </div>

                        <div class="header-new__plus-bl">
                            <div class="header-new__plus-blc" data-aos="fade"   data-aos-delay="300" data-aos-duration="1500"  >
                                Без сотрудников
                            </div>
                            <div class="header-new__plus-blc" data-aos="fade"   data-aos-delay="400" data-aos-duration="1500"  >
                                Без офиса
                            </div>

                        </div>
                        <div class="header-new__plus-bl">
                            <div class="header-new__plus-blc" data-aos="fade"   data-aos-delay="300" data-aos-duration="1500"  >
                                Без бухгалтерии
                            </div>
                            <div class="header-new__plus-blc" data-aos="fade"   data-aos-delay="400" data-aos-duration="1500"  >
                                Без нервов
                            </div>

                        </div>

                    </div>
                    <div class="" data-aos="fade-up"   data-aos-delay="500" data-aos-duration="1500"  >
                        <a class="d-flex btn btn--main shadow--theme shadow-shine pulse shine goto m-0-auto" href="#solution">
									<span class="btn__inner  ">
										Узнать как
									</span>
                        </a>
                    </div >
                </div>
                <br><br>

            </div>

        </div>
    </div>
</section>
<section class="solution" id="solution">
    <div class="solution__container container">
        <div class="solution__row row">
            <div class="col-12" align="center">
                <h1 class="solution__title title" align="center">
                    Как это <span class="colored">работает!</span>
                </h1>

            </div>
        </div>
        <br><br>
    </div>
    <div class="system">
        <div class="system__container container">
            <div class="system__row row">
                <div class="col-md-6 order-1 order-md-2">
                    <div class="system__block-img" data-aos="fade-up"  data-aos-duration="1500"  data-aos-delay="300">
                        <div data-aos="fade-down"  data-aos-duration="1500"  data-aos-delay="600">
                            <img src="/new_page/img/sys_01.png" alt="" class="system__img  img-responsive" >
                        </div>
                    </div>
                </div>
                <div class="col-md-6 order-2 order-md-1">
                    <div class="system__block-text">
                        <h1 class="system__title">
                            Вступите в наше  <span class="colored">торговое сообщество</span>


                        </h1>
                        <p class="system__text">
                            Eсли Вы <b>новичок </b>и не знаете как привлекать клиентов, пройдите быстрое обучение, после которого запустите свой интернет-магазин, находясь в любой точке мира<br><br>
                            ИЛИ <br><br>
                            Eсли Вы <b>уже знакомы с товарным бизнесом </b>и привлечением клиентов, становитесь нашим парнером и начинайте зарабатывать<br><br>
                            <a href="#price" class="btn btn--system btn--green goto" data-target=".system__full">
                                Пройти обучение
                            </a>&nbsp;&nbsp;&nbsp;
                            <a href="/partner" class="btn btn--system" data-target=".system__full">
                                Стать партнером
                            </a>
                            <br><br><br>
                        </p>
                    </div>

                </div>
            </div>
            <div class="system__row row">
                <div class="col-md-6 order-1 order-md-1">
                    <div class="system__block-img" data-aos="fade-up"  data-aos-duration="1500"  data-aos-delay="300">
                        <div data-aos="fade-down"  data-aos-duration="1500"  data-aos-delay="600">
                            <img src="/new_page/img/sys_1.png" alt="" class="system__img lvt img-responsive" >
                        </div>
                    </div>
                </div>
                <div class="col-md-6 order-2 order-md-2">
                    <div class="system__block-text">
                        <h1 class="system__title">
                            Вы <span class="colored">отправляете заявку</span>
                            в нашу систему

                        </h1>
                        <p class="system__text">
                            Отслеживаете свой заказ на всех этапах обработки, доставки. Ежедневное обновление

                        </p>

                    </div>

                </div>
            </div>
            <div class="system__row row">
                <div class="col-md-6 order-1 order-md-2">
                    <div class="system__block-img" data-aos="fade-up"  data-aos-duration="1500"  data-aos-delay="300">
                        <div data-aos="fade-down"  data-aos-duration="1500"  data-aos-delay="600">
                            <img src="/new_page/img/sys_2.png" alt="" class="system__img lvt img-responsive">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 order-2 order-md-1">
                    <div class="system__block-text">
                        <h1 class="system__title">
                            Наша система <span class="colored">обрабатывает заказ</span>

                        </h1>
                        <p class="system__test">
                            Свяжемся с клиентом, обработаем заказ, доставим в кратчайшие сроки.
                            И проведем всю работу за вас, избавляя вас от хлопот.
                        </p>
                        <a href="#" class="btn btn--system sys__btn" data-target=".system__full">подробнее</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="system__container system__full  container">
            <!-- about system -->
            <div class="system__row active row">
                <div class="col-md-6 order-1 order-md-1">
                    <div class="system__block-img" data-aos="fade-up"  data-aos-duration="1500"  data-aos-delay="300">
                        <div data-aos="fade"  data-aos-duration="1500"  data-aos-delay="600">
                            <img src="/new_page/img/sys__2.png" alt="" class="system__img img-responsive">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 order-2 order-md-2">
                    <div class="system__block-text">
                        <h1 class="system__title" >
                            Наши менеджеры <span class="colored">работают с клиентом </span> от начала до конца

                        </h1>
                        <p class="system__test">
                            Обученные менеджеры связываются с клиентом
                            подтверждают заказ, уточняют детали
                            передают в отдел логистики
                        </p>
                    </div>
                </div>
            </div>
            <div class="system__row active row">
                <div class="col-md-6 order-1 order-md-2">
                    <div class="system__block-img" data-aos="fade-up"  data-aos-duration="1500"  data-aos-delay="300">
                        <div data-aos="fade"  data-aos-duration="1500"  data-aos-delay="600">
                            <img src="/new_page/img/sys__1.png" alt="" class="system__img img-responsive">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 order-2 order-md-1">
                    <div class="system__block-text">
                        <h1 class="system__title">
                            Комплектация <span class="colored">заказа </span>
                        </h1>
                        <p class="system__test">
                            Упаковываем в надежную защитную упаковку
                            передаем в курьерскую/почтовую службу, контролируем весь процесс доставки
                            напоминаем клиенту о заказе, мотивируем забрать и оплатить заказ
                        </p>
                    </div>
                </div>
            </div>
            <div class="system__row active row">
                <div class="col-md-6 order-1 order-md-1">
                    <div class="system__block-img" data-aos="fade-up"  data-aos-duration="1500"  data-aos-delay="300">
                        <div data-aos="fade"  data-aos-duration="1500"  data-aos-delay="600">
                            <img src="/new_page/img/sys__3.png" alt="" class="system__img img-responsive">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 order-2 order-md-2">
                    <div class="system__block-text">
                        <h1 class="system__title">
                            Берем все <span class="colored">расходы и обязательства</span> на себя!
                        </h1>
                        <p class="system__test">
                            Мы платим налоги, соблюдая законодательство РФ,
                            выплачиваем зарплату логистам, менеджерам, бухгалтерам, it-специалистам
                            платим за аренду склада, закупку товара, доставку, представителям в Китае
                        </p>
                    </div>
                </div>
            </div>
            <!-- about system -->

        </div>

        <div class="system__container container">
            <div class="system__row row">
                <div class="col-md-6 order-1 order-md-1 sys_end_img">
                    <div class="system__block-img" data-aos="fade-up"  data-aos-duration="1500"  data-aos-delay="300">
                        <div data-aos="fade-down"  data-aos-duration="1500"  data-aos-delay="600">
                            <img src="/new_page/img/sys_3.png" alt="" class="system__img lvt img-responsive">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 order-2 order-md-2 sys_end_txt">
                    <div class="system__block-text">
                        <h1 class="system__title">
                            Вы получаете <span class="colored">прибыль!</span>

                        </h1>
                        <p class="system__test">
                            Как только мы получаем оплату от клиента, Вы получаете свою коммисию
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="solution__container container">
        <div class="solution__row row">
            <div class="col-12" align="left">
                <h1 class="solution__titles-sub title f-black" align="left"  style="max-width: 820px;font-size: 2em; margin: 20px auto">
                    Ведущие менторы <span class="colored">поделятся с Вами опытом </span> в построении успешного интернет-магазина
                </h1>

            </div>
        </div>
        <div class="solution__row row">
            <div class="col-12">
                <div class="solution__about-block ">
                    <div class="solution__about-photo">
                        <img src="new_page/img/slt_1.jpg" alt="" class="solution__about-img ">
                    </div>
                    <div class="solution__about-desc b1">
                        <h1 class="solution__about-name">
                            Марк Фанкухин
                        </h1>
                        <div class="solution__about-prof">

                            <h3 class="solution__about-prof-title"  >
                                знает как: <span class="colored">УПАКОВать БИЗНЕС</span>
                            </h3>
                            <div class="solution__about-prof-w def"  >
                                <div class="solution__about-prof-b def"  >
                                    <div class="solution__about-prof-list">
                                        Как выбрать нишу, провести точный анализ
                                    </div>
                                    <div class="solution__about-prof-list">
                                        Составить лучшее предложение среди конкурентов
                                    </div>
                                    <div class="solution__about-prof-list">
                                        Сделать продающий лендинг самому за один вечер
                                    </div>
                                    <div class="solution__about-prof-list">
                                        Составляем продающие заголовки
                                    </div>
                                    <div class="solution__about-prof-list">
                                        Повышаем конверсию сайта Х2
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="solution__about-list list1">
                            <ul class="solution__about-ul">
                                <li class="solution__about-li">
                                    Основатель компании <b>Fank Retail Partners</b>
                                </li>
                                <li class="solution__about-li">
                                    Бизнес-коуч и наставник более <b>20 стартапов в сфере товарного бизнеса</b>.
                                </li>
                                <li class="solution__about-li">
                                    Совладелец <b>4 успешных интернет магазинов</b> в России
                                </li>
                                <li class="solution__about-li">
                                    Основатель <b>Body-cam - крупнейшей компании в СНГ</b> по снабжению нагрудных видеорегистраторов
                                </li>
                                <li class="solution__about-li">
                                    Выпускник института Бизнес Молодость в 2011 году
                                </li>
                                <li class="solution__about-li">
                                    Ежемесячно <b>проводит более 20 вебинаров</b> с сфере построения товарного бизнеса
                                </li>
                            </ul>
                        </div>
                        <div align="center">
                            <a class="btn-about" data-target="list1">
                                <span class="btn-about-text">Подробнее</span> <span class="arrow">▼</span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="solution__about-block">
                    <div class="solution__about-photo">
                        <img src="new_page/img/slt_2.jpg" alt="" class="solution__about-img ">
                    </div>
                    <div class="solution__about-desc  b2">
                        <h1 class="solution__about-name">
                            Кирилл Низамов
                        </h1>
                        <div class="solution__about-prof">

                            <h3 class="solution__about-prof-title"  >
                                знает как: <span class="colored">работать с ТРАФИКом</span>
                            </h3>
                            <div class="solution__about-prof-w def"  >
                                <div class="solution__about-prof-b rev"  >
                                    <div class="solution__about-prof-list">
                                        Как привлекать клиентов, какую рекламу использовать
                                    </div>
                                    <div class="solution__about-prof-list">
                                        Настройка рекламы, как не слить рекламный бюджет
                                    </div>
                                    <div class="solution__about-prof-list">
                                        Доски объявлений - Авито, Юла
                                    </div>
                                    <div class="solution__about-prof-list">
                                        Контекстная реклама - Яндекс Директ, Google ADS
                                    </div>
                                    <div class="solution__about-prof-list">
                                        Соц.сети - Facebook, Instagram
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="solution__about-list list2">
                            <ul class="solution__about-ul">
                                <li class="solution__about-li">
                                    Привлек заказов через интернет-рекламу более чем на 1 млрд. рублей оборота
                                </li>
                                <li class="solution__about-li">
                                    Сотрудничество с известными брендами Photo-Hunter, Body-Cam, Билайн, Альфа Банк, СДЭК и д.р
                                </li>
                                <li class="solution__about-li">
                                    Опыт в настройке и оптимизации рекламных каналов более чем в 20 сферах бизнеса
                                </li>
                                <li class="solution__about-li">
                                    Сертифицированный специалист Яндекс.Директ и Google Adwords
                                </li>

                            </ul>
                        </div>
                        <div align="center">
                            <a class="btn-about" data-target="list2">
                                <span class="btn-about-text">Подробнее</span> <span class="arrow">▼</span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="solution__about-block ">
                    <div class="solution__about-photo">
                        <img src="new_page/img/slt_3.jpg" alt="" class="solution__about-img ">
                    </div>
                    <div class="solution__about-desc b3">
                        <h1 class="solution__about-name">
                            Максим Коленцев
                        </h1>
                        <div class="solution__about-prof">

                            <h3 class="solution__about-prof-title"  >
                                знает как: <span class="colored">Делать продажи</span>
                            </h3>
                            <div class="solution__about-prof-w def"  >
                                <div class="solution__about-prof-b def"  >
                                    <div class="solution__about-prof-list">
                                        Как обработать полученную заявку
                                    </div>
                                    <div class="solution__about-prof-list">
                                        Увеличиваем средний чек, продаем апсейлы
                                    </div>
                                    <div class="solution__about-prof-list">
                                        Отработка возражений
                                    </div>
                                    <div class="solution__about-prof-list">
                                        Логистика товаров
                                    </div>
                                    <div class="solution__about-prof-list">
                                        Бухгалтерия, законы и прочие бумажные волокиты
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="solution__about-list list3">
                            <ul class="solution__about-ul">
                                <li class="solution__about-li">
                                    Построил с "0" более 10 отделов продаж
                                </li>
                                <li class="solution__about-li">
                                    Провел обучение более 200 менеджеров по продажам(с ростом показателей менеджеров минимум в двое)
                                </li>
                                <li class="solution__about-li">
                                    Разработал и написал скрипты и пособия по работе менеджеров отдела продаж и системы мотивации для ОП для более 20 компаний малого и среднего бизнеса
                                </li>

                            </ul>
                        </div>
                        <div align="center">
                            <a class="btn-about" data-target="list3">
                                <span class="btn-about-text">Подробнее</span> <span class="arrow">▼</span>
                            </a>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="solution__row row">
            <div class="col-12" align="center">
                <br>
                <h3 class="solution__title-sub title-sub">
                    Вместе мы можем гораздо больше, чем один!
                </h3>
            </div>
        </div>
    </div>
</section>

<section class="price" id="price">
    <div class="price__container container">
        <div class="price__row row">
            <div class="col-md-10">
                <h1 class="about__title title   m-0 p-0" >
                    Давай <span class="colored"> начнем?</span>
                </h1>
                <h3 class="about__title-sub title-sub   m-0 p-0">
                    Выберите один из пакетов, и начнем обучение!
                </h3>

            </div>
        </div>
        <div class="price__row  row  p-0 " >
            <div class=" price__block price__block_1  ">
                <h3 class="price__title">
                    Стандарт
                </h3>
                <div class="price__desc">
                    <ul class="price__list">
                        <li class="price__li">Доступ к образовательной платформе</li>
                        <li class="price__li">Обучающий видеокурс для запуска товарного бизнеса (самостоятельно)</li>
                        <li class="price__li">Домашние задания к каждому уроку
                            (без проверки)</li>
                        <li class="price__li">
                            Доступ к маржинальных базе товаров для продаж</li>
                    </ul>
                </div>
                <div class="price__price">
                    <div class="price__old price__price-f">
                        <span class="left">Старая цена:</span>
                        <span class="right  num_mask">4 990 ₽</span>
                    </div>
                    <div class="price__new price__price-f">
                        <span class="left">Новая цена:</span>
                        <span class="right num_mask"><?=$courses[0]['price']?> ₽</span>
                    </div>
                </div>
                <div class="price__btn">
                    <a class="btn btn--main shadow--dark   btn__order open__modal" data-modal="price__modal" data-name="<?=$courses[0]['name']?>" data-price="<?=$courses[0]['price']?>" data-type="<?=$courses[0]['id']?>">
						<span class="btn__inner   shine">
							Начать обучение
						</span>
                    </a>
                </div>
            </div>
            <div class="price__block  price__block_2 active  ">
                <h3 class="price__title">
                    Професиональный
                </h3>
                <div class="price__desc">
                    <ul class="price__list ">
                        <li class="price__li">Домашние задания к каждому уроку
                            (без проверки)</li>
                        <li class="price__li">
                            Доступ к маржинальных базе товаров для продаж</li>
                        <li class="price__li">Доступ к обучающей платформе</li>
                        <li class="price__li">Обучающий видеокурс для запуска товарного бизнеса</li>
                        <li class="price__li"><b>Домашние задания</b></li>
                        <li class="price__li"><b>Проверка домашних заданий</b></li>
                        <li class="price__li"><b>Рекомендации по улучшению сайта, рекламных кампаний, скриптов продаж</b></li>

                    </ul>
                </div>
                <div class="price__price">
                    <div class="price__old price__price-f">
                        <span class="left">Старая цена:</span>
                        <span class="right  num_mask">11 990 ₽</span>
                    </div>
                    <div class="price__new price__price-f">
                        <span class="left">Новая цена:</span>
                        <span class="right  num_mask"><?=$courses[1]['price']?> ₽</span>
                    </div>
                </div>
                <div class="price__btn">
                    <a class="btn btn--main shadow--dark  btn__order  btn__order open__modal" data-modal="price__modal" data-name="<?=$courses[1]['name']?>" data-price="<?=$courses[1]['price']?>" data-type="<?=$courses[1]['id']?>">
						<span class="btn__inner   shine">
							Начать обучение
						</span>
                    </a>
                </div>
            </div>
            <div class="price__block  price__block_3  ">
                <h3 class="price__title">
                    VIP
                </h3>
                <div class="price__desc">
                    <ul class="price__list">
                        <li class="price__li">
                            Домашние задания к каждому уроку
                        </li>

                        <li class="price__li">Проверка домашних заданий</li>
                        <li class="price__li">
                            Доступ к маржинальных базе товаров для продаж
                        </li>
                        <li class="price__li">Доступ к образовательной платформе</li>
                        <li class="price__li">Обучающий видеокурс для запуска товарного бизнеса</li>
                        <li class="price__li">Рекомендации по улучшению сайта, рекламных кампаний, скриптов продаж</li>
                        <li class="price__li"><b>Персональный разбор от основателя FRP, Марка Фанкухина</b></li>
                        <li class="price__li"><b>Персональное ведение до результата 5 продаж</b></li>
                    </ul>
                </div>
                <div class="price__price">
                    <div class="price__old price__price-f">
                        <span class="left">Старая цена:</span>
                        <span class="right  num_mask ">19 990 ₽</span>
                    </div>
                    <div class="price__new price__price-f">
                        <span class="left">Новая цена:</span>
                        <span class="right  num_mask"><?=$courses[2]['price']?> ₽</span>
                    </div>
                </div>
                <div class="price__btn">

                    <a class="btn btn--main shadow--dark  btn__order open__modal" data-modal="price__modal" data-name="<?=$courses[2]['name']?>" data-price="<?=$courses[2]['price']?>" data-type="<?=$courses[2]['id']?>">
							<span class="btn__inner   shine" >
								Начать обучение
							</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="reviews" id="">
    <div class="reviews__container container">
        <div class="reviews__row row" >
            <div class="col-md-12" align="center">
                <h1 class="reviews__title title   m-0 p-0" >
                    Результаты  <span class="colored"> наших партнеров</span>
                </h1>
                <h3 class="reviews__title-sub title-sub   m-0 p-0">
                    Выберите один из пакетов, и начем обучение!
                </h3>
                <div class="reviews__video " style="max-width: 800px; margin: 20px auto; border-radius: 20px; overflow: hidden">
                    <div class="thumb-wrap">

                        <iframe width="640" height="360" src="https://www.youtube.com/embed/aBPjAmhkKcw?disablekb=1&rel=0&showinfo=0&mute=0&loop=0&playlist=" frameborder="0" allowfullscreen></iframe>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="dare" style="padding-bottom: 20px">
    <div class="dare__container container">
        <div class="dare__row row" align="center">
            <div class="col-md-12" >
                <h1 class="dare__title title" align="center">

                    Еще <span class="colored">не решился?</span>
                </h1>
                <h3 class="dare__title-sub title-sub">
                    Думаешь, стоит ли начинать все это?
                </h3>
                <p class="dare__text text">
                    Предлагаю протестировать один единственный инструмент нашего курса,<br>
                    после которого твое сознание поменяется!<br>
                    <br>
                </p>
            </div>

            <div class="col-md-12">
                <div class="dare__video" align="center">
                    <div>

                        <h3 align="center" class="dare__title">
                            <br>
                            Получи доступ всего за <span class="colored-r"><?=$up_c->price?> руб!</span>
                            <br><br><br><br><br>

                        </h3>

                    </div>
                    <div class="dare__video-btn">
                        <br><br><br><br>
                        <a class="btn btn--main shadow--dark  btn__order open__modal" data-modal="price__modal" data-name="Поток лидов ВКонтакте <br>за 20-50 руб!" data-price="<?=$up_c->price?>" data-type="<?=$up_c->id?>">
							<span class="btn__inner   shine" >
								Получить доступ
							</span>
                        </a>


                        <div align="center" class="dare__title-sub" >
                            Предложение действительно до <span class="date_day">30</span> <span class="date_month">Октября</span>

                        </div>
                    </div>


                </div>
            </div>
            <div class="col-md-12">
                <br><br>
                <p class="dare__text text">
                    Видео - "Поток лидов ВКонтакте за 20-50 руб!" - показываем наглядно как настроить рекламу в
                    VK за 20 минут и получить большой поток заявок уже через час"
                </p>
            </div>
        </div>
    </div>
</section>

<section class="partner bg--grey d-none">
    <div class="partner__container container">
        <div class="partner__row row">
            <div class="col-md-12" align="center">
                <h1 class="partner__title title">
                    Стань нашим <span class="colored">партнером!</span>
                </h1>
                <p class="partner__text text">
                    Если ты уже сделал первые шаги, то мы с радостью примем тебя в нашу бизнес семью.<br>
                    Мы предлагаем следующее партнерство:
                </p>
            </div>
            <div class="col-lg-6">

                <div class="partner__block active  ">
                    <h3 class="partner__title" align="center">
                        Стать <span class="colored">дропшипером</span>
                    </h3>
                    <div class="partner__desc">
                        <p class="partner__desc ">
                            Если ты только начинаешь развивать свой бизнес, но у тебя возникают сложности в доступности товаров, то ты можешь стать нашим партнером в лице "Дропшипера". Мы предоставляем большую базу товаров, информацию и улучшение введение бизенса.
                        </p>
                    </div>

                    <div class="partner__btn">
                        <a class="btn btn--main shadow--dark  btn__order " href="/dropshiping">
								<span class="btn__inner   shine">
									Стать дропшипером
								</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">

                <div class="partner__block active  ">
                    <h3 class="partner__title" align="center">
                        Стать <span class="colored-r">оптовиком</span>
                    </h3>
                    <div class="partner__desc">
                        <p class="partner__desc ">
                            Если ты уже идешь большими шагами в бизнесе, но у тебя возникают сложности в доступности товара, или закупка выхывает много сложности, в логистике, доставкию Мы можем помочь тебе стать нашим партнером в Оптовых продуктов. Мы предоставляем большие склады, логистическую компанию, прямые постави и многое другое
                        </p>
                    </div>

                    <div class="partner__btn">
                        <a class="btn btn--main shadow--dark  btn__order " href="/opt">
								<span class="btn__inner   shine">
									Стать оптовиком
								</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="answer">
    <div class="answer__container container">
        <div class="answer__row row">
            <div class="col-12">
                <div class="mark__up">
                    <img src="/new_page/img/mark_up.png" alt="" class="img-responsive">
                </div>
                <div class="mark__center" >
                    <div class="answer__mark-form">
                        <h1 class="answer__title title   m-0 p-0">
                            Остались <span class="colored-r">вопросы?</span>
                        </h1>
                        <h3 class="answer__title-sub title-sub   m-0 p-0" style="max-width: 520px;margin: 0 auto;">
                            Оставь заявку. Давай обсудим их лично. Я и моя команда готовы тебе помочь.
                        </h3>
                        <br>
                        <div style="width: 100%;">
                            <form action="" class="form form--center form--inline m-0-auto">
                                <div class="form__block form__block--error">
                                    <input type="text" class="form__input " placeholder="+7 (999) 999-99-99">
                                </div>


                                <div class="form__block " align="center">
                                    <button type="submit" class=" btn btn--main pulse">
											<span class="btn__inner  ">
												оставить заявку
											</span>
                                    </button>
                                </div>

                            </form>
                        </div>
                        <a class="d-flex " href="#about">

                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<section class="footer">
    <div class="footer__container container">
        <div class="footer__row row">
            <div class="col-md-4">
                &nbsp;
            </div>
            <div class="col-md-4">
                &nbsp;
            </div>
            <div class="col-md-4">
                &nbsp;
            </div>
        </div>
    </div>
</section>
<div class="modal modal-price" id="price__modal">

    <div class="modal-back"></div>
    <div class="modal-wrap price__form-block">
        <?php $form_order = ActiveForm::begin([
            'id'=>'course-form',
            'action'=>'/courses/add-course',
            'options' => ['class' => 'form price__form'],
            'fieldConfig' => [
                 'template' => "{input}",
                    'options' => [
                        'tag'=>'span'
                    ]
                ]
            ]);
        ?>
        <form action="/" class="form price__form" >
            <div class="form__title">
                Вы выбрали курс:
                <h3 class="price__product-name m-0 p-0">
                    Профессионал
                </h3>
            </div>
            <div class="form__block form__block--error">
                <?= $form_order->field($course, 'name', [
                    'options' => [
                        'tag' => false, // Don't wrap with "form-group" div
                    ],
                ])->textInput(['class'=>'form__input','placeholder'=>'Введите Имя...', 'required'=>'required'])->label(false) ?>

            </div>

            <div class="form__block form__block--error">
                <?= $form_order->field($course, 'email', [
                    'options' => [
                        'tag' => false, // Don't wrap with "form-group" div
                    ],
                ])->textInput(['class'=>'form__input', 'type'=>'email','placeholder'=>'Введите Емайл...', 'required'=>'required'])->label(false) ?>

            </div>
            <div class="form__block form__block--error">
                <?= $form_order->field($course, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+7 (999) 999-99-99',
                    'options' => [
                        'class' => 'form__input mask',
                        'id' => 'phone2',
                        'placeholder' => ('Введите телефон...'),
                        'type'=>'phone',
                        'required'=>'required',
                        'tag'=>false
                    ],
                    'clientOptions' => [
                        'clearIncomplete' => true
                    ]
                ])->label(false) ?>

            </div>
            <div class="form__block ">
                <button type="submit" class="form__btn btn btn--main pulse shine ">
                    <span class="btn__inner  ">
                        Начать обучение
                    </span>
                </button>
                <input type="hidden" id="type_course" class="type_course" name="OrderCourseForm[course]" value="<?=$courses[1]['id']?>">
            </div>

        </form>

        <?php ActiveForm::end(); ?>

    </div>
</div>