<?php
$this->title = 'Fank Retail Partners';
?>
<!-- Главная -->

<section class="section head">
    <header class="head__header">
        <div class="head__img-wrap">
            <a href="/"><img src="img/logo.png" alt="FRP" class="head__img  ">
            </a>
        </div>
        <nav class="head__nav">
            <a class="head__nav-item scroll" href="#map">Программа</a>
            <a class="head__nav-item scroll" href="#we">О нас</a>
            <a class="head__nav-item scroll" href="#speakers">Спикеры</a>
            <a class="head__nav-item scroll" href="#action">Регистрация</a>
        </nav>
        <p class="head__live">Онлайн трансляция по всему миру</p>
        <a href="#form" class="btn btn--blue head__hidden-btn  ripplelink grey scroll">Участвовать</a>
    </header>
    <div class="mobile_titles">
        <span class="main-content__desc">Бесплатный мастер-класс</span>
        <h1 class="main-content__title">Научись зарабатывать<br>от 100 000 рублей</h1>
        <p class="main-content__subtitle-xs">С первого месяца без вложений на продаже товаров через интернет</p>
    </div>
    <div class="head__body main-content">
        <div class="main-content__text">
            <span class="main-content__desc desctop-titles">Бесплатный мастер-класс</span>
            <div class="main-content__title-sub hidden-md">
                <h1 class="main-content__title desctop-titles">Научись зарабатывать<br>от 100 000 рублей</h1>
                <p class="main-content__subtitle hidden-md">С первого месяца без вложений на продаже товаров через интернет</p>
            </div>
            <div class="main-content__action">

                <div class="btnCont">
                    <a href="#form" class="main-content__button btn btn--blue scroll ripplelink grey scroll" onclick="yaCounter49725970.reachGoal('main_click_knopka1'); return true;">
                        Зарегистрироваться сейчас
                    </a>
                </div>

                <p class="main-content__utp">Зарегистрируйся на вебинар сейчас и стань партнером успешного бизнеса</p>
            </div>
        </div>
        <a data-fancybox href="https://www.youtube.com/watch?v=gQOldSVwgH8" class="main-content__video">
            <img src="/img/head.png" alt="preview" class="main-content__img">
        </a>
        <div class="main-content__instagram" >
            <p class="main-content__inst-text">Инстаграм основателя FRP:</p>
            <a href="https://www.instagram.com/mark_fankuhin/" target="_blank" class="main-content__icon">
                <img src="img/instagram.svg" alt="Инстаграм">
            </a>
        </div>
    </div>
    <footer class="head__footer">
        <p class="head__adv-item"><span>Дата</span><d class="date_start ">6 августа</d></p>
        <p class="head__adv-item"><span>Время</span><d class="time_start">20:00 по Мск</d></p>
        <p class="head__adv-item"><span>Воды</span>0%</p>
        <p class="head__adv-item"><span>Знаний</span>100%</p>
        <p class="head__adv-item"><span>Вложений</span>0 руб.</p>
        <p class="head__adv-item"><span>Чистой прибыли</span>от 100 тыс. руб.</p>
    </footer>
</section>

<!-- Карта -->

<section id="map" class="section map">
    <h2 class="sec-title map__title" style="line-height: 1.2">
        На этом вебинаре вы получите формулу из
        <br>
        <span style="">
				5 шагов к заработку от 100 000 руб.
			</span>
    </h2>

    <div class="map__wrap-new">
        <div class="map__line">
            <img src="/img/ygline.png" alt="">
        </div>
        <div class="map__item map__item--purple">
            <div class="number">
                01
            </div>
            <div class="map__item-content map__item-content">
                <div class="map__img-wrap map__img-wrap--purple ">
                    <img src="/img/icon1.svg" alt="analice" class="map__img">
                </div>
                <div class="map__text">
                    <h3 class="map__item-title item-title">Анализ</h3>
                    <p class="map__desc">Как правильно выбрать нишу и избежать ошибок? <br>Как проводить анализ ниши, выбрать подходящий товар, изучить конкурентов, найти сильные стороны продукта. Даже новичок сможет за один день выбрать несколько интересных ниш</p>
                </div>
            </div>
        </div>
        <div class="map__item map__item--green">
            <div class="number">
                02
            </div>
            <div class="map__item-content map__item-content">
                <div class="map__img-wrap map__img-wrap--green ">
                    <img src="/img/icon2.svg" alt="analice" class="map__img">
                </div>
                <div class="map__text">
                    <h3 class="map__item-title item-title">Оформление</h3>
                    <p class="map__desc">Как правильно оформить сайт,  который будет продавать?<br> Какие важные конверсионные блоки не добавляют на сайт 90% предпринимателей? Как вызвать доверие и удержать посетителя на странице?</p>
                </div>
            </div>
        </div>
        <div class="map__item map__item--red">
            <div class="number">
                03
            </div>
            <div class="map__item-content map__item-content">
                <div class="map__img-wrap map__img-wrap--red">
                    <img src="/img/icon3.svg" alt="analice" class="map__img">
                </div>
                <div class="map__text">
                    <h3 class="map__item-title item-title">Контент</h3>
                    <p class="map__desc">Как правильно подобрать контент для сайта?<br>
                        Составление продающих заголовков, офферов и интересного описания товара или услуги. Какие изображения лучше использовать? </p>
                </div>
            </div>
        </div>
        <div class="map__item map__item--orange">
            <div class="number">
                04
            </div>
            <div class="map__item-content map__item-content">
                <div class="map__img-wrap map__img-wrap--orange ">
                    <img src="/img/icon4.svg" alt="analice" class="map__img">
                </div>
                <div class="map__text">
                    <h3 class="map__item-title item-title">Маркетинг</h3>
                    <p class="map__desc">Как рекламировать свой сайт? <br>
                        Какие эффективные каналы привлечения клиентов использовать? <br>
                        Как не слить рекламный бюджет в первые дни? <br>
                        Первые заявки без вложений - как это сделать?</p>
                </div>
            </div>
        </div>
        <div class="map__item map__item--blue ">
            <div class="number">
                05
            </div>
            <div class="map__item-content map__item-content">
                <div class="map__img-wrap map__img-wrap--blue ripplelink">
                    <img src="/img/icon5.svg" alt="analice" class="map__img">
                </div>
                <div class="map__text">
                    <h3 class="map__item-title item-title">Продажа</h3>
                    <p class="map__desc">Как обработать полученную заявку и довести ее до продажи? <br>
                        Как увеличить средний чек с помощью продажи дополнительных товаров?<br>
                        Отработка возражений. Повторные продажи.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="map__call" style="   ">

        <h1 align="center" style="padding: 0;margin: 0;margin-top: 10px;">
            Вебинар стартует
        </h1>
        <h2 align="center" style=" margin: 5px auto; border-top: 6px solid  #7a6ef4; max-width: 400px;  padding: 15px 0 20px ">
            <span class="date_start">6 августа</span> в <span class="time_start">20:00 по Мск</span>

        </h2>

        <div class="btnCont" align="center">
            <a href="#form" class="main-content__button btn btn--blue scroll ripplelink grey " onclick="yaCounter49725970.reachGoal('main_click_knopka2'); return true;">
                Зарегистрироваться сейчас
            </a>
        </div>
    </div>
</section>

<!-- О нас -->

<section id="we" class="wich-me">
    <div class="section width-me__wrap">
        <div class="wich-me__title">
            <h1 align="center" class="wich-me__money-title" style="   max-width: 600px; margin: 0 auto 40px; ">
                <b>О нас</b>
            </h1>
        </div>
        <div class="wich-me__text">
            <div class="wich-me__fank">
                <img src="/img/logo-white.png" alt="FRP" class="wich-me__logo">
                <p class="wich-me__fank-text">С 2013 года крупная сеть интернет-магазинов в России по оптовой и розничной продаже цифровой электроники</p>
            </div>
            <div class="wich-me__money">
                <p class="wich-me__money-title">более <b>300 000 000</b> рублей</p>
                <p class="wich-me__money-text">Денежный оборот нас и наших партнеров за 2018 год<br>Мы обладаем практическим опытом в сфере заработка через онлайн продажи и готовы поделиться им с вами!</p>
            </div>
        </div>
        <div class="wich-me__partners partners">
            <div class="partners__img-wrap">
                <img src="/img/p1.png" alt="partner" class="partners__img">
                <img src="/img/p2.png" alt="partner" class="partners__img">
                <img src="/img/p3.png" alt="partner" class="partners__img">
                <img src="/img/p4.png" alt="partner" class="partners__img">
                <img src="/img/p5.png" alt="partner" class="partners__img">
            </div>
        </div>
    </div>
</section>

<!-- Наш сервис -->

<section class="section service">
    <h2 class="sec-title service__title">Наш сервис</h2>
    <div class="service__wrap">
        <div class="service__item service__item--blue">
            <img src="/img/team-01.jpg" alt="team" class="service__img">
            <p class="service__item-desc">Товар всегда в наличии, на собственном складе</p>
        </div>
        <div class="service__item service__item--green">
            <img src="/img/team-02.jpg" alt="team" class="service__img">
            <p class="service__item-desc">Центральный офис в г. Санкт-Петербург</p>
        </div>
        <div class="service__item service__item--red">
            <img src="/img/team-03.jpg" alt="team" class="service__img">
            <p class="service__item-desc">Четкий контроль отправки всех заказов</p>
        </div>
        <div class="service__item service__item--orange">
            <img src="/img/team-04.jpg" alt="team" class="service__img">
            <p class="service__item-desc">Внедрение новых инструментов для выгодного сотрудничества</p>
        </div>
    </div>
</section>

<!-- Наши спикеры -->

<section id="speakers" class="section speakers">
    <h2 class="sec-title speakers__title">Наши спикеры</h2>
    <div class="speakers__wrap">
        <div class="speakers__item">
            <div class="speakers__img-wrap speakers__img-wrap--left"><img src="/img/mark.png" alt="mark" class="speakers__img speakers__img--purple"></div>
            <div class="speakers__text">
                <h3 class="speakers__item-title item-title">Марк Фанкухин</h3>
                <ul class="speakers__desc">
                    <li> Основатель компании <b>Fank Retail Partners</b></li>
                    <li> Бизнес-коуч и наставник более <b>20 стартапов в сфере товарного бизнеса</b>.</li>
                    <li> Совладелец <b>4 успешных интернет магазинов</b> в России</li>
                    <li> Основатель <b>Body-cam - крупнейшей компании в СНГ</b> по снабжению нагрудных видеорегистраторов</li>
                    <li> Выпускник института Бизнес Молодость в 2011 году</li>
                    <li> Ежемесячно <b>проводит более 20 вебинаров</b> с сфере построения товарного бизнеса</li>
                </ul>
            </div>
        </div>
        <div class="speakers__item">
            <div class="speakers__img-wrap speakers__img-wrap--left"><img src="/img/kirill.png" alt="kirill" class="speakers__img speakers__img--blue"></div>
            <div class="speakers__text">
                <h3 class="speakers__item-title item-title">Кирилл Низамов</h3>
                <ul class="speakers__desc speakers__desc--blue">
                    <li> Привлек заказов через интернет-рекламу более чем на <b>1 млрд. рублей</b> оборота</li>
                    <li>Сотрудничество с известными брендами <b>Photo-Hunter, Body-Cam, Билайн, Альфа Банк, СДЭК и д.р</b></li>
                    <li>Опыт в настройке и оптимизации рекламных каналов более чем<b> в 20 сферах бизнеса</b></li>
                    <li>Сертифицированный специалист <b>Яндекс.Директ и Google Adwords</b></li>
                </ul>
            </div>
        </div>




        <div class="speakers__item">
            <div class="speakers__img-wrap speakers__img-wrap--left"><img src="/img/max.png" alt="max" class="speakers__img speakers__img--red"></div>
            <div class="speakers__text">
                <h3 class="speakers__item-title item-title">Максим Коленцев</h3>
                <ul class="speakers__desc speakers__desc--red">
                    <li> Построил <b>с "0" более 10 отделов продаж</b></li>
                    <li>Провел обучение более <b>200 менеджеров по продажам</b>(с ростом показателей менеджеров минимум в двое)</li>
                    <li><b> Разработал и написал скрипты и пособия по работе менеджеров отдела продаж </b>и  системы мотивации для ОП  для более 20 компаний малого и среднего бизнеса</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Футер -->
<section id="form" class=" footer footer--new" >
    <div class="footer__title">

        <h2 align="center" style="margin: 0 auto;">
            <span style="font-weight: 300; font-size: 28px;">Вебинар стартует <span class="date_start" style="display: inline-block;font-weight: 500;">6 августа</span> в <span class="time_start"  style="display: inline-block;font-weight: 500;">20:00 по Мск</span></span>
        </h2>
        <hr style="max-width: 400px; margin: 20px auto;">
        <h1 align="center" style="margin: 0 auto;">
            <span style="font-weight: 300; font-size: 40px;">Первый бонус для участников <d>вебинара</d></span>
        </h1>
    </div>
    <div class="section footer__wrap">

        <div class="footer__img-wrap">
            <img src="/img/mockup.png" alt="pdf" class="footer__img">
        </div>
        <div class="footer__content">
            <h2 class="sec-title footer__title footer__title--new" style="
">
                <span>Зарегистрируйся на вебинар  и получи</span>
                ТОП-10 Прибыльных <br>и актуальных ниш для дропшиппинга с чек-листом
            </h2>

            <form class="form footer__form" action="/site/subscribe" method="post" onsubmit="yaCounter49725970.reachGoal('main_click_subscribe'); return true;">
                <input class="form__input footer__input" type="text" name='name' required placeholder="Введите Имя...">
                <input class="form__input footer__input email" type="email" name='email' required placeholder="Введите E-mail...">
                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                <input class="form__input footer__input mask" type="tel" name='phone' required placeholder="Введите Номер телефона...">
                <input type="hidden" name='landing' value="Мастер-Класс Вариант 2 - Рустам" >
                <input type="hidden" name='category_title' value="Мастер-Класс">
                <input type="hidden" name='product_title' value="Мастер-Класс">
                <button class="btn btn--white footer__btn  ripplelink purple">Зарегистрироваться</button>
                <div style="color: white; margin: 10px auto;" align="center">и получить PDF</div>
            </form>
            <p id="controls" class="footer__checkbox">
                <input type="checkbox" checked> Нажимая на кнопку, я даю согласие на обработку персональных данных и соглашаюсь c пользовательским соглашением и политикой конфиденциальности<br/>
                <input type="checkbox" checked> Я согласен на получение рекламной информации от Fank Retail Partners. <br/>
            </p>

        </div>
    </div>

</section>

<script>
    _r = false;
</script>