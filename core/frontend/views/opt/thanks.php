<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Спасибо за заявку';
?>

<section class="nav nav--fixed ">
    <div class="container nav__container ">



        <section class="nav">
            <div class="container nav__container">
                <div class="row nav__row align-items-center">
                    <div class=" col-11  col-sm-5 p-0">
                        <div class="nav__logo-block">
                            <img src="img/logo-head.png" alt="" class="nav__logo img-responsive">
                            <p  class="f-light nav__logo-text" align="right">
                                Поставка товаров оптом
                            </p>
                        </div>
                    </div>
                    <div class="col-3 col-sm-7  d-none d-sm-inline-block">
                        <h3 class="nav__phone d-none d-sm-block" align="right">
                            8 (800) 999-99-99
                            <br>
                            <span style="font-size: 1em">+7 (999) 034-22-10</span>
                        </h3>
                    </div>
                </div>
            </div>

        </section>
    </div>
</section>

<section class="header cover">
    <div class="container header__container">
        <div class="row header__row ">
                <div class="header__content" align="center" style="max-width: 100%;width:100%;">
                    <h1 class="header__title">
                        Спасибо за заявку!
                    </h1>
                    <h3 class="header__text">
                        Наши менеджеры скоро свяжутся с вами
                    </h3>
                </div>
        </div>
    </div>
</section>
