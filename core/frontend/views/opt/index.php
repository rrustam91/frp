<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Покупай оптом в России по заводским ценам производителей из Китая'

?>

<section class="menu menu__block ">

    <div class="container nav__container">
        <div class="row nav__row nav__items align-items-center">
            <div class="col-12" >
                <div style="">
                    <a href="#about" class="btn--nav goto">О НАС</a>
                    <a href="#preim" class="btn--nav goto">ПРЕИМУЩЕСТВА</a>
                    <a href="#clients" class="btn--nav goto">КЛИЕНТЫ</a>
                    <a href="#company" class="btn--nav goto">Компания в цифрах</a>
                    <a href="#deliv" class="btn--nav goto">ДОСТАВКА</a>
                    <a href="#contact-block" class="btn--nav goto">КОНТАКТЫ</a>
                    <a href="#reviews" class="btn--nav goto">Отзывы</a>

                    <a  class="btn btn--nav btn--round  goto" data-fancybox href="#contact-modal">Связаться</a>
                    <h3 class="nav__phone d-block d-sm-none " align="right">
                        8 (800) 500-69-43
                        <br>
                        <span style="font-size: 1em">+7 (999) 034-22-10</span>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="nav nav--fixed ">
    <div class="container nav__container ">



        <section class="nav">
            <div class="container nav__container">
                <div class="row nav__row align-items-center">
                    <div class=" col-11  col-sm-4 p-0">
                        <div class="nav__logo-block">
                            <img src="img/logo-head.png" alt="" class="nav__logo img-responsive">
                            <p  class="f-light nav__logo-text" align="right">
                                Поставка товаров оптом
                            </p>
                        </div>
                    </div>
                    <div class="col-3 col-sm-7  d-none d-sm-inline-block">
                        <h3 class="nav__phone d-none d-sm-block" align="right">
                            8 (800) 500-69-43
                            <br>
                            <span style="font-size: 1em">+7 (999) 034-22-10</span>
                        </h3>
                    </div>
                    <div class="col-1  col-sm-1 col-lg-1 ">

                        <div class="" align="center">
						<span class="menu--toggle ">
							Menu
						</span>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</section>

<section class="header cover">
    <div class="container header__container">
        <div class="row header__row ">
            <div class="col-sm-12 col-md-12   col-lg-7  col-xl-8 align-self-center">
                <div class="header__content">
                    <h1 class="header__title">
                        Покупай оптом в России <span class="d-inline">по заводским ценам </span>производителей из Китая
                    </h1>
                    <h3 class="header__text">
                        С гарантией доставки в срок и наценкой <span class="d-inline d-lg-block">всего 1.5% от цены завода</span>
                    </h3>
                </div>
            </div>
            <div class="col-sm-12 col-md-12  col-lg-5 col-xl-4 p-0 align-self-end">

                <?php $form = ActiveForm::begin(['id'=>'header__form','options' => [ 'class'=>'header__form form form--center form--inline'], 'fieldConfig' => [
                    'template' => "{input}",
                    'options' => [
                        'tag'=>'span',

                    ]
                ]]); ?>
                    <div class="form__title">
                        <h2 class="form__title--text">
                            Оставьте заявку
                        </h2>
                        <p class="form__text">
                            И мы подготовим прайс-лист с оптовыми ценами на Вашу продукцию
                        </p>
                    </div>
                    <div class="form__block form__block--error">
                        <?= $form->field($model, 'email',  [
                            'options' => [
                                'tag' => false, // Don't wrap with "form-group" div
                            ],
                        ])->textInput(['class'=>'form__input','type'=>'email','placeholder'=>'Ваш емайл','required'=>'required'])->label(false) ?>

                    </div>
                    <div class="form__block form__block--error">
                        <?= $form->field($model, 'phone',  [
                            'options' => [
                                'tag' => false, // Don't wrap with "form-group" div
                            ],
                        ])->textInput(['class'=>'form__input','type'=>'tel','placeholder'=>'+7 (999) 999-99-99','required'=>'required'])->label(false) ?>

                    </div>

                    <div class="form__block ">
                        <button class="form__btn btn pulse shine ">ОСТАВИТЬ ЗАЯВКУ</button>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div class="col-lg-1"></div>
            <div class="col-6 col-sm-4 col-md-4 col-lg-2 align-self-end">
                <div class="header__pain">
                    <div class="header__pain-icon">
                        <span class="fi flaticon-hat"></span>
                    </div>
                    <div class="header__pain-text">
                        Мы не агенты!
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-4 col-md-4 col-lg-2 align-self-end">
                <div class="header__pain">
                    <div class="header__pain-icon">
                        <span class="fi flaticon-pallet"></span>
                    </div>
                    <div class="header__pain-text">
                        Свои склады в Китае
                    </div>
                </div>
            </div>

            <div class="col-6 col-sm-4 col-md-6 col-lg-2 align-self-end">
                <div class="header__pain">
                    <div class="header__pain-icon">
                        <span class="fi flaticon-delivery"></span>
                    </div>
                    <div class="header__pain-text ">
                        Сроки доставки<br> от 10 часов
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-2 align-self-end">
                <div class="header__pain">
                    <div class="header__pain-icon">
                        <span class="fi flaticon-handshake"></span>
                    </div>
                    <div class="header__pain-text">
                        100% замена заводского брака
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-2 align-self-end">
                <div class="header__pain">
                    <div class="header__pain-icon">
                        <span class="fi flaticon-project"></span>
                    </div>
                    <div class="header__pain-text">
                        Вернем до 20% в случае нелеквидности
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="about section pb-0" id="about">
    <div class="container about__container">
        <div class="row about__row about__title-block ">
            <div class="col-md-7">
                <div class="about__content">

                </div>
            </div>
        </div>
        <div class="row about__row about__desc-block ">
            <div class="col-md-7">
                <div class="about__content">
                    <h1 class="title--border about__title">
                        О нас
                    </h1>
                    <p class="about__text about__author-desc desc-1 active">
                        Наша компания занимается оптовой поставкой товара с фабрик Китая в Россию и страны СНГ уже с 2014 года. Мы имеем собственные склады на территории Китая, сотрудничаем с более 130 производителями. Наша главная задача - предоставить вам качественный товар, напрямую с завода по лучшим ценам.
                    </p>
                    <a  data-fancybox href="#contact-modal" class="btn btn--round pulse">
                        Получить предложение
                    </a>
                </div>
            </div>
            <div class="col-md-5 ">
                <div class="about__video-block about__author-desc active desc-1">
                    <!--<div class="about__video-block">-->
                    <!--<div class="about__video&#45;&#45;video">-->
                    <!--<video  id="video_about_1" >-->
                    <!--<source src="http://9-shop.ru/freshfood5img/pomidor.mp4" type="video/mp4" >-->
                    <!--Ваш браузер не поддерживает тег Video.-->
                    <!--</video>-->
                    <!--</div>-->
                    <!--</div>-->

                    <!--<div class="about__video&#45;&#45;thumb" data-target="video_about_1">-->
                    <div class="about__video--thumb" data-target="">
                        <img src="img/video_2.jpg" alt="" class="img-responsive">

                    </div>
                </div>


            </div>

        </div>
        <div class="about__row row about__author-block d-none d-md-flex">
            <div class="col-12 col-md-6 ">
                <div class="about__author-blocks active" data-target="desc-1">
                    <div class="about__author-img">
                        <img src="img/auth1.jpg" alt="" class="img-responsive">
                    </div>

                    <div class="about__author-text">

                        <div class="about__author-dsc">
                            Основатель компании Fank Retail Partners
                        </div>
                        <div class="about__author-name">
                            Марк фанкухин
                        </div>
                        <div class="about__author-dsc">

                            "Я владею крупнейшим интернет-магазином в России. За 7 лет своей деятельности построил хорошие отношения со сногими заводами-производителями в Китае, Германии и России."
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 ">
                <div class="about__author-blocks "  data-target="desc-1">

                    <div class="about__author-img">
                        <img src="img/auth2.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="about__author-text">
                        <div class="about__author-dsc">
                            Комерческий директор компании
                        </div>
                        <div class="about__author-name">
                            максим каленцев
                        </div>
                        <div class="about__author-dsc">

                            "Наша главная задача - предоставить клиентам максимально выгодные условия, начиная от поиска фабрики и заканчивая своевременной доставки до склада заказчика"
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about__row row about__author-block  d-flex d-md-none  owl-carousel about__carousel">
            <div class="col-12 col-md-6 ">
                <div class="about__author-blocks active" data-target="desc-1">
                    <div class="about__author-img">
                        <img src="img/auth1.jpg" alt="" class="img-responsive">
                    </div>

                    <div class="about__author-text d-block">

                        <div class="about__author-dsc">
                            Основатель компании Fank Retail Partners
                        </div>
                        <div class="about__author-name">
                            Марк фанкухин
                        </div>
                        <div class="about__author-dsc">

                            "Я владею крупнейшим интернет-магазином в России. За 7 лет своей деятельности построил хорошие отношения со сногими заводами-производителями в Китае, Германии и России."
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 ">
                <div class="about__author-blocks active"  data-target="desc-1">

                    <div class="about__author-img">
                        <img src="img/auth2.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="about__author-text  d-block">
                        <div class="about__author-dsc">
                            Комерческий директор компании
                        </div>
                        <div class="about__author-name">
                            максим каленцев
                        </div>
                        <div class="about__author-dsc">

                            "Наша главная задача - предоставить клиентам максимально выгодные условия, начиная от поиска фабрики и заканчивая своевременной доставки до склада заказчика"
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about__row row about__gallery-block ">
            <div class="col-1  d-none d-sm-flex align-items-center justify-content-center">
                <div class="prev btn btn--round btn--xs" style="max-height: 110px;"><</div>
            </div>
            <div class="col-12 col-sm-10">
                <div class="owl-carousel carousel">
                    <div class="carousel__items item">

                        <a href="img/about_us2.jpg" data-fancybox data-caption="Благодарственное письмо" class="img-responsive fancybox">
                            <img src="img/about_us2.jpg" alt="" class=" img-responsive" />
                        </a>
                    </div>
                    <div class="carousel__items item">

                        <a href="img/about_us4.jpg" data-fancybox data-caption="Благодарственное письмо" class="img-responsive fancybox">
                            <img src="img/about_us4.jpg" alt="" class=" img-responsive" />
                        </a>
                    </div>
                    <div class="carousel__items item">

                        <a href="img/about_us3.jpg" data-fancybox data-caption="Благодарственное письмо" class="img-responsive fancybox">
                            <img src="img/about_us3.jpg" alt="" class=" img-responsive" />
                        </a>
                    </div>
                    <div class="carousel__items item">

                        <a href="img/comp2.jpg" data-fancybox data-caption="Благодарственное письмо" class="img-responsive fancybox">
                            <img src="img/comp2.jpg" alt="" class=" img-responsive" />
                        </a>

                    </div>
                    <div class="carousel__items item">

                        <a href="img/comp3.jpg" data-fancybox data-caption="Благодарственное письмо" class="img-responsive fancybox">
                            <img src="img/comp3.jpg" alt="" class=" img-responsive" />
                        </a>
                    </div>
                    <div class="carousel__items item">

                        <a href="img/comp4.jpg" data-fancybox data-caption="Благодарственное письмо" class="img-responsive fancybox">
                            <img src="img/comp4.jpg" alt="" class=" img-responsive" />
                        </a>
                    </div>
                    <div class="carousel__items item">

                        <a href="img/comp5.jpg" data-fancybox data-caption="Благодарственное письмо" class="img-responsive fancybox">
                            <img src="img/comp5.jpg" alt="" class=" img-responsive" />
                        </a>
                    </div>
                    <div class="carousel__items item">

                        <a href="img/comp6.jpg" data-fancybox data-caption="Благодарственное письмо" class="img-responsive fancybox">
                            <img src="img/comp6.jpg" alt="" class=" img-responsive" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-1 d-none d-sm-flex align-items-center justify-content-center" style="max-height: 110px;">
                <div class="next btn btn--round btn--xs" > > </div>
            </div>
        </div>

    </div>

</section>
<section class="how-we grey section" id="preim" >
    <div class="container how-we__container">
        <div class="row how-we__row">
            <div class="col-md-12">
                <h1 class="title--border">
                    Наши главные преимущества
                </h1>
                <p class="how-we__text">
                    Мы начали заниматься оптовыми продажами в 2014 году. Отличное знание рынка и опыт
                    работы позволяют сделать самое выгодное для Вас предложение.
                </p>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="how-we__map-desc  desc desc1">
                            Весь ассортимент
                            в наличие на складе
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="how-we__map-desc  desc  desc2">
                            Прямые поставки с выстроенной
                            логистической стратегией
                        </div>
                    </div>
                </div>
                <div class="how-we__map d-none d-md-block" >
                    <img src="img/map.jpg" alt=" " class="img-responsive">
                    <div class="how-we__map-point d-none d-lg-block">
                        <img src="img/map-point.png" alt=" " class="img-responsive">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="how-we__map-desc desc   desc3">
                            Собственная логистическая компания

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="how-we__map-desc  desc  desc4">
                            15-ти летние связи
                            по всему миру
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section >
<section class="company section" id="company">
    <div class="container company__container">
        <div class="row company__row">
            <div class="col-12">
                <h1 class="company__title" align="center">
                    Компания в цифрах
                </h1>
                <p class="company__subtitle" align="center">
                    Наши клиенты доверяют нам и поэтому зарабатывают вместе с нами.
                </p>
            </div>
            <div class="col-6 col-md-3" align="center">
                <div class="company__circle-block">
                    <div class="circle" data-num="7" data-text="Лет" data-value="0.9">
                        <div class="num_mask"></div>
                    </div>
                    <div>
                        <h4 class="company__circle-title">
                            Успешной работы
                        </h4>
                        <p class="company__circle-text">

                        </p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3" align="center">
                <div class="company__circle-block">
                    <div class="circle" data-num="19"  data-text="Складов" data-value="0.9">
                        <div class="num_mask"></div>
                    </div>
                    <div>
                        <h4 class="company__circle-title">
                            В Китае и России
                        </h4>
                        <p class="company__circle-text"></p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3" align="center">
                <div class="company__circle-block">
                    <div class="circle" data-num="30"  data-value="0.6" data-append="+">
                        <div class="num_mask"></div>
                    </div>
                    <div>

                        <h4 class="company__circle-title">
                            Сотрудников в компании
                        </h4>
                        <p class="company__circle-text"></p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3" align="center">
                <div class="company__circle-block">
                    <div class="circle" data-num="794" data-value="1">
                        <div class="num_mask"></div>
                    </div>
                    <div>
                        <h4 class="company__circle-title">
                            Постоянных клиентов
                        </h4>
                        <p class="company__circle-text"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="clients section" id="clients">
    <div class="container clients__container">
        <div class="row clients__row">
            <div class="col-12">
                <h1 class="title--border clients__title">
                    Наши постоянные клиенты
                </h1>
                <p class="clients__subtitle">
                    За годы работы к нам обращались за помощью физические
                    и юридические лица, среди которых широко известные личности
                    и компании. Мы ценим доверие — это дорогого стоит.
                </p>
            </div>
        </div>
    </div>
    <div class="container-fluid clients__container">
        <div class="row clients__row">
            <div class="col-6 col-sm-3  p-0 clients__item c1 m1">
                <div class="clients__block">
                    <div class="clients__logo">
                        <img src="img/clients/c1.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-3  p-0 clients__item c2 m2">
                <div class="clients__block">
                    <div class="clients__logo">
                        <img src="img/clients/c2.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-3 p-0 clients__item c2 m1">
                <div class="clients__block">
                    <div class="clients__logo">
                        <img src="img/clients/c3.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-3 p-0 clients__item c1">
                <div class="clients__block">
                    <div class="clients__logo">
                        <img src="img/clients/c4.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-3  p-0 clients__item c1">
                <div class="clients__block">
                    <div class="clients__logo">
                        <img src="img/clients/c5.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-3 p-0 clients__item c2">
                <div class="clients__block">
                    <div class="clients__logo">
                        <img src="img/clients/c6.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-3 p-0 clients__item c2">
                <div class="clients__block">
                    <div class="clients__logo">
                        <img src="img/clients/c7.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-3 p-0 clients__item c1">
                <div class="clients__block">
                    <div class="clients__logo">
                        <img src="img/clients/c8.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="reviews section" id="reviews">
    <div class="container reviews__container">
        <div class="row reviews__row">
            <div class="col-12">
                <h1 class="reviews__title" align="center">
                    Отзывы наших клиентов
                </h1>
            </div>
            <div class="col-6 col-md-3 reviews__block">

                <img src="img/m1.jpg" alt="" class=" img-responsive reviews__img" />

            </div>
            <div class="col-6 col-md-3 reviews__block">

                <img src="img/m2.jpg" alt="" class=" img-responsive reviews__img" />
            </div>
            <div class="col-6 col-md-3 reviews__block">
                <img src="img/m3.jpg" alt="" class=" img-responsive reviews__img" />
                <!--<a href="img/m3.jpg" data-fancybox data-caption="Благодарственное письмо GADGET GROUP" class="img-responsive reviews__img fancybox">
                    <img src="img/m3.jpg" alt="" class=" img-responsive" />
                </a>-->
            </div>
            <div class="col-6 col-md-3 reviews__block">
                <img src="img/m4.jpg" alt="" class=" img-responsive reviews__img" />
            </div>
        </div>
    </div>
</section>

<section class="contact videobg ">
    <video class="videobg__video" autoplay="" loop="" muted="">
        <source src="img/videobg.mp4" type="video/mp4">
    </video>
    <div class="contact__wrapper videobg__wrapper">
        <div class="container contact__wrapper">
            <div class="row contact__row">
                <div class="col-sm-12 col-md-12   col-lg-7  col-xs-8 align-self-center">
                    <div class="contact__content">
                        <h1 class="contact__title">
                            Получите прайс<span class="d-sm-inline d-lg-block"> на необходимый товар от производителя из Китая </span>
                        </h1>
                        <p class="contact__text">
                            И наши менеджеры предоставят коммерческое предложение с цифрами в течение 24 часов
                        </p>

                    </div>
                </div>
                <div class="col-sm-12 col-md-12  col-lg-5 col-xs-4 p-0 align-self-end">
                    <?php $form = ActiveForm::begin(['id'=>'header__form','options' => [ 'class'=>'contact__form form form--center '], 'fieldConfig' => [
                        'template' => "{input}",
                        'options' => [
                            'tag'=>'span',

                        ]
                    ]]); ?>
                        <div class="form__title">
                            <h2 class="form__title--text" style="font-size: 26px;">
                                Отправьте ссылку<span class="d-sm-inline d-lg-block"> на требуемый товар</span>
                            </h2>
                            <p class="form__text">
                                И мы подготовим прайс-лист с оптовыми ценами на Вашу продукцию
                            </p>
                        </div>
                        <div class="form__block form__block--error">
                            <?= $form->field($model, 'link',  [
                                'options' => [
                                    'tag' => false, // Don't wrap with "form-group" div
                                ],
                            ])->textInput(['class'=>'form__input','placeholder'=>'Ссылка на товар'])->label(false) ?>

                        </div>
                        <div class="form__block form__block--error">
                            <?= $form->field($model, 'email',  [
                                'options' => [
                                    'tag' => false, // Don't wrap with "form-group" div
                                ],
                            ])->textInput(['class'=>'form__input','type'=>'email','placeholder'=>'Ваш емайл','required'=>'required'])->label(false) ?>

                        </div>
                        <div class="form__block form__block--error">
                            <?= $form->field($model, 'phone',  [
                                'options' => [
                                    'tag' => false, // Don't wrap with "form-group" div
                                ],
                            ])->textInput(['class'=>'form__input','type'=>'tel','placeholder'=>'+7 (999) 999-99-99','required'=>'required'])->label(false) ?>

                        </div>

                        <div class="form__block ">
                            <button class="form__btn btn pulse shine ">Получить прайс</button>
                        </div>
                    <?php ActiveForm::end(); ?>

                </div>

                <div class="col-lg-1"></div>
            </div>
        </div>
    </div>
</section>

<section class="sertificate section">
    <div class="container sertificate__container">
        <div class="row sertificate__row" align="center">
            <div class="col-12">
                <h2 class="sertificate__title">Наши лицензии и сертификаты</h2>
            </div>
            <div class="col-md-6">
                <div class="sertificate__block"><img src="img/cert1.jpg" alt="" class="sertificate__img img-responsive"></div>
            </div>
            <div class="col-md-6">
                <div class="sertificate__block"><img src="img/cert2.jpg" alt="" class="sertificate__img img-responsive"></div>
            </div>
        </div>
    </div>
</section>
<section class="map-block" id="contact-block">

    <div class="map-block__map  ">
        <div class="container">
            <div class="row  map__contact">
                <div class="col-md-12">
                    <h1 class="contact__title">
                        Наши контакты
                    </h1>
                </div>
                <div class="col-sm-12 col-md-12   col-lg-7 align-self-center">
                    <div class="map__content" >
                        <h2 class="map-subhead">
                            Поддержка покупателей
                        </h2>
                        <h1 class="map-txt">
                            8 (800) 500-69-43
                        </h1>
                        <h2 class="map-subhead">
                            Отдел продаж в Санкт-Петербурге
                        </h2>
                        <h1 class="map-txt">
                            8 (812) 748-26-69
                        </h1>

                        <h2 class="map-subhead">
                            График работы
                        </h2>
                        <h1 class="map-txt">
                            10:00 - 22:00
                        </h1>

                        <p class="map-address">
                            Центральный офис в Санкт-Петербурге
                            Лиговский пр.50 корпус 16, офис 121
                            <br><br>
                            <a class=" map--href" data-fancybox data-type="iframe" data-src="https://yandex.ru/map-widget/v1/?um=constructor%3A427be5b7d49b3766a6c94c094258305359af327c4a888148df2ef17e2808299f&amp;source=constructor" href="javascript:;"> <span class="fa fa-map-marker f-color"></span> Показать на карте  </a>
                        </p>



                    </div>
                </div>
                <div class="col-sm-12 col-md-12  col-lg-5 p-0 align-self-end">


                    <?php $form = ActiveForm::begin(['id'=>'header__form','options' => [ 'class'=>'header__form form form--center'], 'fieldConfig' => [
                        'template' => "{input}",
                        'options' => [
                            'tag'=>'span',

                        ]
                    ]]); ?>
                    <div class="form__title">
                        <h2 class="form__title--text" style="font-size: 24px; max-width: 100%;">
                            Остались вопросы?
                        </h2>
                        <p class="form__text">
                            Оставьте ваши контакты, и мы свяжемся с вами
                        </p>
                    </div>
                    <div class="form__block form__block--error">
                        <?= $form->field($model, 'email',  [
                            'options' => [
                                'tag' => false, // Don't wrap with "form-group" div
                            ],
                        ])->textInput(['class'=>'form__input','type'=>'email','placeholder'=>'Ваш емайл','required'=>'required'])->label(false) ?>

                    </div>
                    <div class="form__block form__block--error">
                        <?= $form->field($model, 'phone',  [
                            'options' => [
                                'tag' => false, // Don't wrap with "form-group" div
                            ],
                        ])->textInput(['class'=>'form__input','type'=>'tel','placeholder'=>'+7 (999) 999-99-99','required'=>'required'])->label(false) ?>

                    </div>

                    <div class="form__block ">
                        <button class="form__btn btn pulse shine ">Свяжитесь со мной</button>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="footer">
    <div class="footer__container container">
        <div class="footer__row row">
            <div class="col-sm-6 col-md-4">
                <div class="footer__content">
                    <div class="footer__logo" >
                        <div class="nav__logo-block">
                            <img src="img/logo-head.png" alt="" class="nav__logo img-responsive">
                            <p  class="f-light nav__logo-text" align="left">
                                Поставка товаров оптом
                            </p>
                        </div>
                        <div>
                            <p>
                                <i class="fa fa-envelope-o"></i> info@fankretailpartner.ru
                            </p>
                        </div>
                        <div>
                            <p>
                                <i class="fa fa-phone"></i> 8 (800) 500-69-43 <br>
                                &nbsp;&nbsp;&nbsp; 8 (812) 748-26-69<br>
                                &nbsp;&nbsp;&nbsp; 8 (999) 034-22-10<br>
                            </p>
                            <p >
                                <i class="fa fa-clock-o"></i> Время работы каждый день 10:00 - 22:00
                            </p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="d-none d-md-block col-sm-6 col-md-4">
                <div class="footer__content"></div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="footer__content">
                    <h4 class="m-0 p-0">
                        Техническая поддержка
                    </h4>
                    <p>
                        По всем техническим вопросам напишите в отдел технической поддержки
                    </p>

                    <?php $form = ActiveForm::begin(['id'=>'header__form','options' => [ 'class'=>'form form--nostyle m-0 p-0'], 'fieldConfig' => [
                        'template' => "{input}",
                        'options' => [
                            'tag'=>'span',

                        ]
                    ]]); ?>
                    <div class="form__block form__block--error">
                        <?= $form->field($model, 'email',  [
                            'options' => [
                                'tag' => false, // Don't wrap with "form-group" div
                            ],
                        ])->textInput(['type'=>'email','placeholder'=>'Ваш емайл','required'=>'required'])->label(false) ?>

                    </div>

                    <div class="form__block " align="center">
                        <button class="form__btn btn pulse shine ">Свяжитесь со мной</button>
                    </div>

                    <div class="form__block form__block--error">
                        <?= $form->field($model, 'phone',  [
                            'options' => [
                                'tag' => false, // Don't wrap with "form-group" div
                            ],
                        ])->textInput(['type'=>'hidden','value'=>'0','class'=>'form__input','placeholder'=>'+7 (999) 999-99-99','required'=>'required'])->label(false) ?>

                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="col-12">
                <p>

                </p>
            </div>
        </div>
    </div>
</section>

<div id="contact-modal"  style="display: none">
    <?php $form = ActiveForm::begin(['id'=>'header__form','options' => [ 'class'=>'contact__form form form--center'], 'fieldConfig' => [
        'template' => "{input}",
        'options' => [
            'tag'=>'span',

        ]
    ]]); ?>
    <div class="form__title">
        <h2 class="form__title--text" style="font-size: 24px; max-width: 100%;">
            ОСТАВЬТЕ ЗАЯВКУ
        </h2>
        <p class="form__text">
            И мы подготовим прайс-лист с оптовыми ценами на Вашу продукцию
        </p>
    </div>

    <div class="form__block form__block--error">
        <?= $form->field($model, 'link',  [
            'options' => [
                'tag' => false, // Don't wrap with "form-group" div
            ],
        ])->textInput(['class'=>'form__input','placeholder'=>'Ссылка на товар'])->label(false) ?>

    </div>
    <div class="form__block form__block--error">
        <?= $form->field($model, 'email',  [
            'options' => [
                'tag' => false, // Don't wrap with "form-group" div
            ],
        ])->textInput(['class'=>'form__input','type'=>'email','placeholder'=>'Ваш емайл','required'=>'required'])->label(false) ?>

    </div>
    <div class="form__block form__block--error">
        <?= $form->field($model, 'phone',  [
            'options' => [
                'tag' => false, // Don't wrap with "form-group" div
            ],
        ])->textInput(['class'=>'form__input','type'=>'tel','placeholder'=>'+7 (999) 999-99-99','required'=>'required'])->label(false) ?>

    </div>

    <div class="form__block ">
        <button class="form__btn btn pulse shine ">Свяжитесь со мной</button>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<div id="map" style="display: none; max-width: 800px; ">
    <iframe src="" width="100%" height="100%" frameborder="0" ></iframe>
</div>
