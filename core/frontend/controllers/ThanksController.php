<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.08.2018
 * Time: 11:51
 */

namespace frontend\controllers;


use common\helpers\myHellpers;
use Yii;

class ThanksController extends MainController{
    public $layout = 'thanks';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [ ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionIndex(){
        return $this->render('index');
    }
    public function actionDrop(){

        if(Yii::$app->request->get('pass')){
            $pass = Yii::$app->request->get('pass');
        }
        Yii::$app->user->logout();
        return $this->render('drop', compact('pass'));
    }
    public function actionWebinar(){
        $this->layout = 'main';
        return $this->render('webinar');
    }

}