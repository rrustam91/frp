<?php
namespace frontend\controllers;


use common\helpers\myHellpers;
use common\models\courses\Courses;
use common\models\forms\OrderCourseForm;
use common\models\forms\PartnerCourseForm;
use common\models\user\User;
use common\models\user\UserAuth;
use Yii;

use yii\web\Controller;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class DrophipController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [ ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }



    public function actionIndex() {

        $this->layout = 'dropship';
        echo 'dropship';
        return $this->render('index', [

        ]);
    }

    public function actionPartner(){
        Yii::$app->user->logout();
        $this->layout = 'webinar';
        $partner = new PartnerCourseForm();
        return $this->render('partner-page',[
            'partner'=>$partner,
        ]);
    }


    public function actionAddPartner(){
        if(Yii::$app->request->isPost){
            $partner = new PartnerCourseForm();
            if ($partner->load(Yii::$app->request->post()) && $partner->addPartner()) {

                $this->redirect(['/thanks/drop','pass'=>$partner->username]);
            }
        }else{
            $this->redirect('/webinar');
        }

    }

}
