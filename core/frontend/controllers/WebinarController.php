<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.08.2018
 * Time: 11:51
 */

namespace frontend\controllers;


use common\helpers\myHellpers;
use common\models\courses\Courses;
use common\models\courses\CoursesOrder;
use common\models\forms\WebinarForm;
use common\models\forms\ChatMessagesForm;
use common\models\webinars\WebinarAds;
use common\models\webinars\Webinars;
use common\models\forms\OrderCourseForm;
use common\models\forms\PartnerCourseForm;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class WebinarController extends MainController
{
    public $layout = 'webinar';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [ ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionTest(){
        $this->render('test');
    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex(){
        $this->redirect("/webinar/wait");
        
    }



    public function actionRoom(){
        if(Yii::$app->user->isGuest){
            $this->redirect('/webinar/auth');
        }
        $chat_form = new ChatMessagesForm();
        $cours_form = new OrderCourseForm();
        $partner_form = new PartnerCourseForm();
        //$webinar = new Webinars();
        //$ad = new WebinarAds();
        $webinar = Webinars::find(1)->one();
        $ads = WebinarAds::find()->where(['webinar_id'=>$webinar->id])->all();


        return $this->render('index',[
            'chat'=>$chat_form,
            'course'=>$cours_form,
            'partner'=>$partner_form,
            'webinar'=>$webinar,
        ]);
    }


    public function actionWait(){
        $this->layout = 'wait';
        return $this->render('wait');
    }

    public function actionAuth(){
        if(Yii::$app->user->isGuest) {
            $model = new WebinarForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $this->redirect('/webinar/room');
            } else {
                return $this->render('auth', [
                    'model' => $model
                ]);
            }
        }else{
            $this->redirect('/webinar/room');
        }

    }

    public function actionSendMsg(){
        if(Yii::$app->request->isAjax){
            $chat_form = new ChatMessagesForm();
            if ($chat_form->load(Yii::$app->request->post()) && $chat_form->addMsg()) {
                return myHellpers::encodeJson(['success'=>'true']);
            } 
            return myHellpers::encodeJson(['success'=>'error']);
        }else{
            $this->redirect('/webinar');
        }

    }
    public function actionAddCourse(){
        if(Yii::$app->request->isPost){
            $course_form = new OrderCourseForm();
            if ($course_form->load(Yii::$app->request->post()) && $order = $course_form->addOrder()) {
                $url = CoursesOrder::getUrlPay($order,$course_form);
                return $this->redirect($url);
            }
        }else{
            $this->redirect('/webinar');
        }

    }

    public function actionAddPartner(){
        if(Yii::$app->request->isPost){
            $partner = new PartnerCourseForm();
            if ($partner->load(Yii::$app->request->post()) && $partner->addPartner()) {
 
                $this->redirect(['/thanks/drop','pass'=>$partner->username]);
            }
        }else{
            $this->redirect('/webinar');
        }

    }

    public function actionCourse(){
        //$this->redirect('/webinar');

        if(Yii::$app->request->get()){
            //myHellpers::show(Yii::$app->request->get('cr'));
            $y = Yii::$app->request->get('cr');
            return $this->render('course' );

        }
        return $this->redirect('/courses');
    }
    public function actionPartnerPage(){

        $partner = new PartnerCourseForm();
        return $this->render('partner-page',[
            'partner'=>$partner,
        ]);
    }

    public function actionCoursePage(){
        $course_form = new OrderCourseForm();

        return $this->render('course-page',[
            'course'=>$course_form,
        ]);
    }



}