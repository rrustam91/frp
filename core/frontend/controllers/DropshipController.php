<?php

namespace frontend\controllers;

use Yii;
use common\models\forms\PartnerCourseForm;

class DropshipController extends \yii\web\Controller
{
    //public $layout = 'dropship';
    public function actionIndex()
    {
        $model = new PartnerCourseForm();
        if($model->load(Yii::$app->request->post()) && $model->addPartner()){
            return $this->redirect('/opt/thanks');
        }
        return $this->render('index',[
            'model'=>$model,
        ]);
    }

}
