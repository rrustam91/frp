<?php

namespace frontend\controllers;
use common\helpers\myHellpers;
use Yii;
use common\models\forms\OptForm;

class OptController extends \yii\web\Controller
{
    public $layout = 'opt';
    public function actionIndex()
    {
        $model = new OptForm();
        if($model->load(Yii::$app->request->post()) && $model->addOptSubscribe()){
            return $this->redirect('/opt/thanks');
        }else{
            return $this->render('index',[
                'model'=>$model,
            ]);
        }
    }
    public function actionThanks(){
        return $this->render('thanks');
    }
}
