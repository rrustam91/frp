<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.08.2018
 * Time: 12:38
 */

namespace frontend\controllers;

use common\models\MainModel;
use Yii;
use common\helpers\myHellpers;
use common\models\dropship\DropOrder;
use RetailCrm\Methods\V3\Orders;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class HobaController extends Controller
{

    protected $retailCrm;
    protected const STATUS_LIST = MainModel::STATUS_LIST_DROP_ORDERS;

    public function __construct(string $id, $module, array $config = []){
        $this->retailCrm = Yii::$app->retailCrm;
        parent::__construct($id, $module, $config);
    }


    public function actionTest(){
        $l = $this->retailCrm->client->request->sitesList();
        myHellpers::show($l);
    }

    public function actionCheckOrders(){
        $filter['numbers'] = self::getIds(self::getOrders(),'crm_id','A');
        $_orders = $this->retailCrm->client->request->ordersList($filter,1,100);

        foreach ($_orders['orders'] as $_order){
            if(!$order = self::getOrder($_order['id'])){    continue; }
            $order->delivery_price = $_order['delivery']['cost'];
            $order->track_number = $_order['delivery']['data']['trackNumber'];

            $order->status_crm = $_order['status'];
            $order->status_crm_drop = self::getStatus($_order['status']);
            $order->comment_manager = $_order['managerComment'];
            $order->commission_full = $order->getCommission();
            $order->save();
        }
    }


    protected static function getStatus($status){
        if($status != 'spam') return (self::STATUS_LIST[$status]) ? $status : 'v-procc';
        return 'spam';
    }

    public function checkProducts(){

    }
    public function checkProductsCategory(){

    }

    public function actionGetUsers(){
        $_orders = $this->retailCrm->client->request->usersGroups();
        myHellpers::show($_orders);
        die();
    }

    /** Models connectors */

    protected static function getOrders(){
        return DropOrder::find()
            ->where(['not in','status_crm',['dv','provierien','vozvrat','spam']])
            ->all();
    }
    protected static function getOrdersIds(){
        return DropOrder::find()
            ->select('crm_id')
            ->where(['not in','status_crm',['dv','provierien','vozvrat','spam']])
            ->asArray()
            ->all();
    }

    protected static function getOrder($crm_id){
        return DropOrder::find()->where(['crm_id'=>$crm_id])->one();
    }


    protected static function getIds($data, $field = 'id', $prf = '') {
        $result = [];
        foreach ($data as $key => $val) {
            $result[] = (string)$val[$field].$prf;
        }
        return $result;
    }



}