<?php
namespace frontend\controllers;


use common\helpers\myHellpers;
use common\models\courses\Courses;
use common\models\forms\OrderCourseForm;
use common\models\forms\PartnerCourseForm;
use common\models\user\User;
use common\models\user\UserAuth;
use Yii;

use yii\web\Controller;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [ ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }



    public function actionIndex() {
        $ab = rand(1,4);
        $page = 'index';
        $this->layout = 'main';
        return $this->render($page, [
            'page' => $page,
        ]);
    }


    public function actionNew() {
        $course_form = new OrderCourseForm();
        $courses =  Courses::getCoursesList(4);
        $up_c =  Courses::findOne(5); 
        $page = 'new';
        $this->layout = 'new';
        return $this->render($page,[
            'course'=>$course_form,
            'courses'=>$courses,
            'up_c'=>$up_c,
        ]);
    }


    public function actionOld() {
        $ab = rand(1,4);
        $page = 'index';
        $this->layout = 'main';
        return $this->render($page, [
            'page' => $page,
        ]);
    }


    public function actionPay(){
        $this->layout = 'page';

        return $this->render('pay');
    }


    public function actionPartner(){
        Yii::$app->user->logout();
        $this->layout = 'webinar';
        $partner = new PartnerCourseForm();
        return $this->render('partner-page',[
            'partner'=>$partner,
        ]);
    }


    public function actionAddPartner(){
        if(Yii::$app->request->isPost){
            $partner = new PartnerCourseForm();
            if ($partner->load(Yii::$app->request->post()) && $partner->addPartner()) {

                $this->redirect(['/thanks/drop','pass'=>$partner->username]);
            }
        }else{
            $this->redirect('/webinar');
        }

    }
    public function actionSubscribe(){

        $ref = myHellpers::getUtm(Yii::$app->request->getReferrer());
        $ref['landing'] = Yii::$app->request->post('landing');
        $ref['category_title'] = Yii::$app->request->post('category_title');
        $ref['product_title'] = Yii::$app->request->post('product_title');

        $data['phone'] = Yii::$app->request->post('phone');
        $data['name'] = Yii::$app->request->post('name');
        $data['email'] = Yii::$app->request->post('email');
        $data['utm'] = myHellpers::encodeJson($ref);


        $user = new UserAuth();

        if($user->addSubscribe($data)){
            $emails = [
                [
                    'email' => $data['email'],
                    'variables' => [
                        'phone' => $data['phone'],
                        'name' => $data['name'],
                    ]
                ]
            ];
            Yii::$app->sendpulse->addEmails('1852164', $emails);

        };

        $chats = [
            461704559,
            302980258,
            396497806,
        ];
        Yii::$app->telegram->sendMessage([
                'chat_id' => $chats[0],
                'text' => "Имя: {$data['name']}\n Тел: {$data['phone']}\n Емайл: {$data['email']}\n ",
            ]
        );
        Yii::$app->telegram->sendMessage([
                'chat_id' => $chats[1],
                'text' => "Имя: {$data['name']}\n Тел: {$data['phone']}\n Емайл: {$data['email']}\n ",
            ]
        );
        Yii::$app->telegram->sendMessage([
                'chat_id' => $chats[2],
                'text' => "Имя: {$data['name']}\n Тел: {$data['phone']}\n Емайл: {$data['email']}\n ",
            ]
        );
        return $this->redirect('/thanks/webinar');


    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionTest()
    {
        $token = '368b621bb64ff91c44524aacf0e62bdd';
        $v = @file_get_contents(
            'https://api.r7k12.ru/'.$token.'/addLeads',
            0,
            stream_context_create(
                [
                    'http' => [
                        'method' => "POST",
                        'header' => "Content-Type: application/json\r\n",
                        'content' => json_encode(
                            [
                                [
                                    'type'=> "Form", //тип заявки

                                    'title' => "Заявка с сайта", //Заголовок заявка
                                    'comment'=> "Коментарий к сделке",//Комментарий к сделке (НЕ ОБЯЗАТЕЛЬНО)
                                    'name'=> "Имя клиента",//Название контакта (НЕ ОБЯЗАТЕЛЬНО)
                                    'email' => "test@example.com", //E-mail адрес контакта (ОБЯЗАТЕЛЕН ЕСЛИ НЕ УКАЗАН ТЕЛЕФОН КОНТАКТА)
                                    'phone' => "8-912-345-67-89", //Телефон контакта (ОБЯЗАТЕЛЕН ЕСЛИ НЕ УКАЗАН E-MAIL КОНТАКТА)
                                    'create_new_lead' => "0",
                                    //'0' - новая сделка создается только если нет сделки или предыдущая в статусе "успешно реализовано" или "возврат"; '1' - новая сделка создается в любом случае

                                    'fields' => [//доп. поля

                                        // Массив дополнительных полей, если нужны.

                                        'lead' =>[//Поля для сделок

                                            // Примеры использования:

                                            'price' => 453, // Сумма заказа





                                            'orderMethod'=> 'landing-page',
                                        ],



                                    ]

                                ],
                            ]
                        )
                    ]
                ]
            )
        );

        myHellpers::show($v);
    }

}
