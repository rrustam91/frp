<?php
namespace frontend\controllers;


use common\helpers\myHellpers;
use common\models\courses\CoursesOrder;
use common\models\forms\OrderCourseForm;
use common\models\user\User;
use common\models\user\UserAuth;
use Yii;

use yii\helpers\ArrayHelper;
use yii\web\Controller;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class CoursesController extends Controller
{
    public $layout ='courses';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [ ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }



    public function actionIndex() {
        $course_form = new OrderCourseForm();

        return $this->render('index',[
            'course'=>$course_form,
        ]);
    }

    public function actionAddCourse(){

        if(Yii::$app->request->isPost){
            $course_form = new OrderCourseForm();
            if ($course_form->load(Yii::$app->request->post()) && $order = $course_form->addOrder()) {
                $url = CoursesOrder::getUrlPay($order,$course_form);
                return $this->redirect($url);
            }
        }else{
            $this->redirect('/');
        }


    }


}