<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.08.2018
 * Time: 11:54
 */

namespace frontend\controllers;

use common\models\courses\CoursesOrder;
use common\models\MainModel;
use common\models\user\User;
use Yii;
use common\helpers\myHellpers;
use yii\web\Controller;


/**
 *
 *  demo
 *  shopId (идентификатор магазина, демо) = 532238
 *  scid (номер витрины, демо) = 562488
 *  war
 *  shopId (идентификатор магазина, демо) = 532238
 *  scid (номер витрины, демо) = 789758
 *
 * pass afyrhtnfqkgfhnythc
 *
 **/

class OrderController extends Controller
{

    /**
     * afyrhtntqkgfhnythc
     * фанкретайлпартнерс
     * afyrhtnfqkgfhnythc

     **/

    /** Check **/

    public $enableCsrfValidation = false;
    public $configs = [
        'shopId'=>'532238',
        'scId'=>'789758',
        'ShopPassword'=>'afyrhtnfqkgfhnythc'
    ];

    public $layout = 'order-check';


    public function actionCheck(){
        $text = '';
        if(Yii::$app->request->post()){
            $hash = md5($_POST['action'].';'.$_POST['orderSumAmount'].';'.$_POST['orderSumCurrencyPaycash'].';'.$_POST['orderSumBankPaycash'].';'.$this->configs['shopId'].';'.$_POST['invoiceId'].';'.$_POST['customerNumber'].';'.$this->configs['ShopPassword']);
            if (strtolower($hash) != strtolower($_POST['md5'])){
                $code = 1;
            }
            else {
                $code = 0;
            }
            $text = '<?xml version="1.0" encoding="UTF-8"?><checkOrderResponse performedDatetime="'. $_POST['requestDatetime'] .'" code="'.$code.'"'. ' invoiceId="'. $_POST['invoiceId'] .'" shopId="'. $this->configs['shopId'] .'"/>';
        }

        return $this->render('order',['text'=>$text]);

    }
    public function actionTestCheck(){

        if(Yii::$app->request->post()) {
            $hash = md5($_POST['action'] . ';' . $_POST['orderSumAmount'] . ';' . $_POST['orderSumCurrencyPaycash'] . ';' . $_POST['orderSumBankPaycash'] . ';' . $this->configs['shopId'] . ';' . $_POST['invoiceId'] . ';' . $_POST['customerNumber'] . ';' . $this->configs['ShopPassword']);
            if (strtolower($hash) != strtolower($_POST['md5'])) {
                $code = 1;
            } else {
                $code = 0;
            }
            $text = '<?xml version="1.0" encoding="UTF-8"?><checkOrderResponse performedDatetime="' . $_POST['requestDatetime'] . '" code="' . $code . '"' . ' invoiceId="' . $_POST['invoiceId'] . '" shopId="' . $this->configs['shopId'] . '"/>';
        }
        return $this->render('order',['text'=>$text]);

    }

    /** payment aviso url **/
    public function actionPaymentAvisoUrl(){
        if(Yii::$app->request->post()) {
            $hash = md5($_POST['action'] . ';' . $_POST['orderSumAmount'] . ';' . $_POST['orderSumCurrencyPaycash'] . ';' . $_POST['orderSumBankPaycash'] . ';' . $this->configs['shopId'] . ';' . $_POST['invoiceId'] . ';' . $_POST['customerNumber'] . ';' . $this->configs['ShopPassword']);
            if (strtolower($hash) != strtolower($_POST['md5'])) {
                $code = 1;
            } else {
                $code = 0;
            }
            $text = '<?xml version="1.0" encoding="UTF-8"?><paymentAvisoResponse performedDatetime="' . $_POST['requestDatetime'] . '" code="' . $code . '" invoiceId="' . $_POST['invoiceId'] . '" shopId="' . $this->configs['shopId'] . '"/>';
        }
        return $this->render('order',['text'=>$text]);
    }
    public function actionTestPaymentAvisoUrl(){
        if(Yii::$app->request->post()) {
            $hash = md5($_POST['action'].';'.$_POST['orderSumAmount'].';'.$_POST['orderSumCurrencyPaycash'].';'.$_POST['orderSumBankPaycash'].';'.$this->configs['shopId'].';'.$_POST['invoiceId'].';'.$_POST['customerNumber'].';'.$this->configs['ShopPassword']);
            if (strtolower($hash) != strtolower($_POST['md5'])){
                $code = 1;
            }
            else {
                $code = 0;
            }
            $text = '<?xml version="1.0" encoding="UTF-8"?><paymentAvisoResponse performedDatetime="'. $_POST['requestDatetime'] .'" code="'.$code.'" invoiceId="'. $_POST['invoiceId'] .'" shopId="'. $this->configs['shopId'] .'"/>';
        }
        return $this->render('order',['text'=>$text]);
    }


    /** payment success url **/
    public function actionSuccessPay(){

        $this->layout = 'thanks';
        if($request = Yii::$app->request->get()) {
            $user = explode(' ', $request['customerNumber']);
            $order['id'] = substr($request['orderNumber'], 10);
            $order['date'] = substr($request['orderNumber'], 0, 10);
            if (CoursesOrder::changeStatus($user[1], $order, MainModel::STATUS_PAY)) {
                $user = User::find()->where(['id'=>$user[1]])->one();
                return $this->render('success',['user'=>$user]);
            }
        }
        return $this->redirect('/order/fail');
    }
    public function actionFailPay(){
        $this->layout = 'thanks';
        if($request = Yii::$app->request->get()) {
            $user = explode(' ', $request['customerNumber']);
            $order['id'] = substr($request['orderNumber'], 10);
            $order['date'] = substr($request['orderNumber'], 0, 10);
            CoursesOrder::changeStatus($user[1], $order, MainModel::STATUS_EDIT);
        }
        return $this->render('fail');
    }


    /** payment success url **/
    public function actionTestSuccessPay(){
        if($request = Yii::$app->request->get()) {
            $user = explode(' ', $request['customerNumber']);
            $order['id'] = substr($request['orderNumber'], 10);
            $order['date'] = substr($request['orderNumber'], 0, 10);
            if (CoursesOrder::changeStatus($user[1], $order, MainModel::STATUS_PAY)) {
                return true;
            }
        }
        return false;
    }
    public function actionTestFailPay(){
        if($request = Yii::$app->request->get()) {
            $user = explode(' ', $request['customerNumber']);
            $order['id'] = substr($request['orderNumber'], 10);
            $order['date'] = substr($request['orderNumber'], 0, 10);
            if (CoursesOrder::changeStatus($user[1], $order, MainModel::STATUS_EDIT)) {
                return true;
            }
        }
        return false;
    }

    public function actionCurl(){
        $URL="https://demomoney.yandex.ru/eshop.xml";
//https://money.yandex.ru/payments/external?requestid=d0b81ce2-0e02-4d20-9b0d-563307cc16be
        //https://money.yandex.ru/payments/external?requestid=35c3c426-7073-4ac3-8dcd-ef54cfbf71df
        $fields=array(
            'shopId'=>'532238',
            //'scid'=>'789758',
            'scid'=>'562488',
            'sum'=>'2',
            'customerNumber'=>'abc'.rand(0,1000),
            'paymentType'=>'AC',
            'orderNumber'=>time().'1',
            'cps_phone'=>79111111111,
            'cps_email'=>'test@test.ru',

        );

        $data= $this->sendCurl($URL,$fields);

        $string = $data;
        preg_match('/htt(.*?)$/', $string,$smod);
        //echo $string;
        $sm = 'htt'. $smod[1];
        return $this->redirect($sm);
    }



    private function sendCurl($url,$fields){
        $fields_str = http_build_query($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_str);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

}