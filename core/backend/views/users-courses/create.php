<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\user\UserCourses */

$this->title = Yii::t('app', 'Create User Courses');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Courses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-courses-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
