<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\user\search\UserCoursesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Courses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-courses-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create User Courses'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute'=>'user_id',
                    'value' => function($data){


                        if(!empty($_data)){
//                            foreach($_data as $k=>$v){
//                                $_rdr .= "{$k} : {$v}<br>";
//                            }
                        }

                        return '';
                    },
                    'format'=>'html',
                    'label'=>'Метки'
                ],
                //'user_id',
                //'course_id',
                [
                    'attribute'=>'user_id',
                    'value' => function($data){
                        return $data->name;
                    },
                    'format'=>'html',
                    'label'=>'Курс'
                ],
                'status',
                'created_at:datetime',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
