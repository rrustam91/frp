<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\user\UserCourses */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'User Courses',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Courses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-courses-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
