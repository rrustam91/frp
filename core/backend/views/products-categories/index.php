<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\dropship\search\ProductsCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-categories-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Products Categories'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute'=>'name',
                    'value' => function($model){
                        return Html::a($model->name, ['products/category', 'category_id' => $model->id]);
                    },
                    'format'=>'html',
                    'label'=>'Название'
                ],
                [
                    'attribute'=>'status',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->status, \common\models\MainModel::STATUS_SIMPLE_LIST);
                    },
                    'label'=>'Статус',
                    'format'=>'html'

                ],

                [
                    'attribute'=>'status_drop',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->status_drop, \common\models\MainModel::STATUS_SIMPLE_LIST);
                    },

                    'filter' => \common\models\MainModel::STATUS_SIMPLE_LIST,
                    'filterInputOptions' => ['class' => 'form-control form-control-sm'],
                    'label'=>'Статус Дропа',
                    'format'=>'html'

                ],

                [
                    'attribute'=>'parent_id',
                    'value' => function($model){
                        return $model->parent->name;
                    },
                    'format'=>'html',
                    'label'=>'Родительская'
                ],
                // 'status',
                // 'xml_id',
                // 'xml_parent_id',
                // 'xml_name',
                // 'seo_keywords',
                // 'seo_description',
                // 'img',
                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
