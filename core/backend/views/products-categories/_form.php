<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\products\ProductsCategories;

/* @var $this yii\web\View */
/* @var $model common\models\products\ProductsCategories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-categories-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?=$form->field($model, 'parent_id')->widget(Select2::classname(), [

            'data' => $model::getSelectList(ProductsCategories::className()),
            'options' => ['placeholder' => 'Выберите категорию' ],
            'pluginOptions' => [
                'allowClear' => true
            ],
            'theme'=>Select2::THEME_DEFAULT,
        ]); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>



        <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>


        <?= $form->field($model, 'status')->dropDownList(\common\models\MainModel::STATUS_DEFAULT_LIST) ?>

        <?= $form->field($model, 'status_drop')->dropDownList(\common\models\MainModel::STATUS_SIMPLE_LIST) ?>

        <?= $form->field($model, 'status_opt')->dropDownList(\common\models\MainModel::STATUS_SIMPLE_LIST) ?>




    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
