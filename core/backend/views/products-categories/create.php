<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\products\ProductsCategories */

$this->title = Yii::t('app', 'Create Products Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-categories-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
