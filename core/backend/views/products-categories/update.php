<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\products\ProductsCategories */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Products Categories',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="products-categories-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
