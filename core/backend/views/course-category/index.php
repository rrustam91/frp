<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\courses\search\CourseCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Course Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-category-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Course Category'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'name',
                'alias',
                'content:ntext',

                [
                    'attribute'=>'course_id',
                    'value' => function($model){

                        return $model->course->name;
                    },
                    'label'=>'Курс',
                    'filter' => \common\helpers\modelHellpers::getSelectList(\common\models\courses\Courses::className()),
                    'filterInputOptions' => ['class' => 'form-control form-control-sm'],
                ],
                // 'status',
                // 'opened',
                // 'step_by',
                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
