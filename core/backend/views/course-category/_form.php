<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\courses\Courses;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\courses\CourseCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-category-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?=$form->field($model, 'course_id')->widget(Select2::classname(), [

                'data' => \common\helpers\modelHellpers::getSelectList(Courses::className()),
                'options' => ['placeholder' => 'Выберите курс' ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'theme'=>Select2::THEME_DEFAULT,
            ]); ?>
            <?= $form->field($model, 'step_by')->textInput(['type'=>'number','value'=>'1']) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'status')->dropDownList(\common\models\MainModel::STATUS_SIMPLE_LIST) ?>
            <?= $form->field($model, 'opened')->dropDownList(\common\models\MainModel::STATUS_SIMPLE_LIST) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'basic'
            ]) ?>
        </div>







        <?//= $form->field($model, 'created_at')->textInput() ?>

        <?//= $form->field($model, 'updated_at')->textInput() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
