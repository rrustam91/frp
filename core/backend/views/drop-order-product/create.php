<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\dropship\DropOrderProduct */

$this->title = Yii::t('app', 'Create Drop Order Product');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drop Order Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drop-order-product-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
