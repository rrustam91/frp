<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\dropship\DropOrder */

$this->title = Yii::t('app', 'Create Drop Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drop Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drop-order-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
