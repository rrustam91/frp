<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\products\Products;

use common\models\user\User;
use kartik\select2\Select2;

/* @var $this yii\web\View */

/* @var $model common\models\dropship\DropOrder */
/* @var $client common\models\user\User */
/* @var $drop_delivery common\models\dropship\DropDeliver */
/* @var $model_form common\models\forms\DropOrderForm */
/* @var $form yii\widgets\ActiveForm */
$drop_product = Products::getDrop();
?>

<div class="drop-order-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>

    <div class="box-body table-responsive row">

        <div class="col-md-6">
            <h3 class="m-0 p-0">Покупатель</h3>

            <?= $form->field($model_form, 'name')->textInput() ?>


            <?= $form->field($model_form, 'phone')->textInput() ?>

        </div>

        <div class="col-md-6">
            <h3 class="m-0 p-0">Прочее</h3>
            <?= $form->field($model_form, 'comment')->textarea(['rows' => 6]) ?>

        </div>
        <div class="col-md-12">
            <h3 class="m-0 ">Товар</h3>
            <hr/>
        </div>
        <div class="col-md-2 " style="padding: 7px 20px;" align="left">
            Выберите товар:
        </div>
        <div class="col-md-6">
            <div id="products_select">

            </div>

        </div>

        <div class="col-md-4">
            <button type="button" class="btn bg-green btn-flat addDropList">
                <i class="fa fa-plus"></i> Добавить
            </button>
        </div>

        <div class="col-md-12">
            <hr/>
            <div class="table-responsive">
                <table class="table table-hover product__table product__table--drop " id="listProducts">
                    <thead class="thead-dark">
                    <tr>
                        <th>Наименование товара</th>
                        <th>Артикул</th>

                        <th>Мин. цена</th>
                        <th align="center">Кол-во</th>
                        <th align="center">Озвученная цена</th>
                        <th>Сумма заказа</th>
                        <th align="center">Действия</th>
                    </tr>
                    </thead>


                    <tbody class="dropProductListTable">
                        <tr >
                            <td >
                                <div class="empty-table">
                                    Товар не выбран
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>


        </div>
        <hr/>
    </div>

    <div class="box-body table-responsive row">
        <div class="col-md-12">

            <h3 class="m-0 p-0">Доставка</h3>

        </div>
        <div class="col-md-6">

            <?= $form->field($model_form, 'delivery_type_id')->widget(Select2::classname(), [

                'data' => \common\models\MainModel::getSelectList(\common\models\delivery\DeliveryType::className()),
                'options' => ['placeholder' => 'Выберите способ доставки'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'theme' => Select2::THEME_DEFAULT,
            ]); ?>
            <?= $form->field($model_form, 'region')->textInput() ?>
            <?= $form->field($model_form, 'city')->textInput() ?>
            <?= $form->field($model_form, 'subway')->textInput() ?>
            <?= $form->field($model_form, 'street')->textInput() ?>
            <div class="row p-0">

                <div class="col-md-4">
                    <?= $form->field($model_form, 'zipcode')->textInput() ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model_form, 'building')->textInput() ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model_form, 'flat')->textInput() ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model_form, 'block')->textInput() ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model_form, 'house')->textInput() ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model_form, 'floor')->textInput() ?>
                </div>

            </div>

        </div>

        <div class="col-md-6">

            <?= $form->field($model_form, 'note')->textarea(['rows' => 6]) ?>

            <?= $form->field($model_form, 'cost')->textInput(['type'=>'number','min'=>'0','value'=>0]) ?>

        </div>


    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat bg-blue']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>


<script>
    var _products = <?=json_encode($drop_product); ?>;
</script>