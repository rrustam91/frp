<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\dropship\search\DropOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Drop Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drop-order-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Drop Order'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider'=>$dataProvider,

            'showPageSummary'=>true,
            'pjax'=>false,
            'striped'=>true,
            'hover'=>true,
            'panel'=>true,
            'export'=>false,
            'panel'=>['type'=>'info', 'heading'=>'Таблица ваших заявок'],
            'columns'=>[

                [

                    'class' => '\kartik\grid\ActionColumn',
                    'hiddenFromExport' => true,

                ],
 
                [
                    'attribute'=>'crm_id',
                    'width'=> '50px',
                    'filter'=>false,
                    'label'=>'Номер заказа в CRM',
                    'hAlign'=>'center',
                ],

                [
                    'attribute'=>'user_id',
                    'value' => function($model){
                        return Html::a($model->user->name, ['users/view', 'id' => $model->user->id]);
                    },


                    'format'=>'html',
                    'label'=>'Дропшипер',

                    'width'=>'200px',
                ],
                [
                    'attribute'=>'products',
                    'value'=>function ($model, $key, $index, $widget) {
                        $items = $model->list;
                        $list = '';
                        foreach ($items as $item){
                            $list .= $item->product_name . ', кол-во:'.$item->quantity.'<br>';
                        }
                        return $list;

                    },

                    'format'=>'html',
                    'filter'=>false,
                    'filterInputOptions'=>['placeholder'=>'Название продукта'],
                    'label'=>'Список товаров',

                    'width'=>'300px',
                ],
                [
                    'attribute'=>'quantity',
                    'value'=>function ($model, $key, $index, $widget) {
                        $items = $model->list;
                        $q = 0;
                        foreach ($items as $item){
                            $q += $item->quantity;
                        }
                        return $q;

                    },

                    'hAlign'=>'center',
                    'width'=>'70px',
                    'format'=>'html',
                    'label'=>'Общ. кол-во'
                ],
                [
                    'attribute'=>'total_price',
                    'value'=>function ($model, $key, $index, $widget) {
                        $items = $model->list;
                        $price = 0;
                        foreach ($items as $item){ $price += $item->price_total; }
                        return $price;
                    },

                    'width'=>'200px',
                    'label'=>'Общая стоимость',
                    'width'=>'150px',
                    'hAlign'=>'right',
                    'pageSummary'=>true,
                    'format'=>['decimal'],
                ],
                [
                    'attribute'=>'track_number',
                    'label'=>'трек номер',
                    'width'=>'100px',
                ],
                [
                    'attribute'=>'status_crm_drop',
                    'value'=> function ($model){

                        return $model->statusCrmDrop->name;
                        //return \common\helpers\myHellpers::getStatusLabelDropOrder($model->status_crm_drop);
                    },
                    'format'=>'html',
                    'filter'=>false,
                    'width'=>'50px',
                    'hAlign'=>'center',
                    'label'=> "Статус ЦРМ"

                ],
                [
                    'attribute'=>'status_crm',
                    'value'=> function ($model){

                        return $model->statusCrm->name;
                    },
                    'format'=>'html',
                    'filter'=>false,
                    'width'=>'50px',
                    'hAlign'=>'center',
                    'label'=> "Статус для дроперов"
                ],
                [
                    'attribute'=>'created_at',
                    'format'=>'datetime',
                    'width'=>'150px',

                    'hAlign'=>'center'
                ],


            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
