<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\dropship\DropOrder */
/* @var $client common\models\user\User */
/* @var $drop_delivery common\models\dropship\DropDeliver */
/* @var $model_form common\models\forms\DropOrderForm*/

$this->title = Yii::t('app', 'Create Drop Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drop Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drop-order-create">

    <?= $this->render('_form-sample', [
        'model_form' => $model_form,
        'client' => $client,
        'drop_delivery' => $drop_delivery
    ]) ?>

</div>
