<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\webinars\Webinars;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\webinars\WebinarAds */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="webinar-ads-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive row">
        <div class="col-md-4">
            <?=$form->field($model, 'webinar_id')->widget(Select2::classname(), [
                'data' => $model::getSelectList(Webinars::className()),
                'options' => ['placeholder' => Yii::t('app', 'Select Webinar') ],
                'pluginOptions' => [ 'allowClear' => true ],
                'theme'=>Select2::THEME_DEFAULT,
            ])->label( Yii::t('app', 'Select Webinar')); ?>

            <?= $form->field($model, 'status')->dropDownList( $model::STATUS_SIMPLE_LIST) ?>

            <?= $form->field($model, 'img')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'text')->textarea(['maxlength' => true]) ?>

            <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'time_start')->textInput(['type'=>'number']) ?>

        </div>




    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
