<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\webinars\WebinarAds */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Webinar Ads',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Webinar Ads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="webinar-ads-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
