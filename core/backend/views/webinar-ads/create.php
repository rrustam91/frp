<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\webinars\WebinarAds */

$this->title = Yii::t('app', 'Create Webinar Ads');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Webinar Ads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="webinar-ads-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
