<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use common\helpers\myHellpers;
/* @var $this yii\web\View */
/* @var $model common\models\user\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
    'class' => 'btn btn-danger btn-flat',
    'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
    ],
]) ?>
<section class="content" style="display: none">

    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <!--<img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">-->

                    <h3 class="profile-username text-center"><?=$model->name?></h3>

                    <p class="text-muted text-center"><?=$model->role?></p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Телефон</b> <a class="pull-right"><?=$model->phone?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Email</b> <a class="pull-right"><?=$model->email?></a>
                        </li>
                        <li class="list-group-item">
                            <b>CRM Имя</b> <a class="pull-right"><?=$model->crm_name?></a>
                        </li>
                        <li class="list-group-item">
                        </li>
                        <li class="list-group-item">
                            <b>Friends</b> <a class="pull-right">13,287</a>
                        </li>
                    </ul>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">About Me</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <strong><i class="fa fa-book margin-r-5"></i> История</strong>


                    <p class="text-muted">

                    </p>

                    <hr>

                    <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

                    <p class="text-muted">Malibu, California</p>

                    <hr>

                    <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

                    <p>
                        <span class="label label-danger">UI Design</span>
                        <span class="label label-success">Coding</span>
                        <span class="label label-info">Javascript</span>
                        <span class="label label-warning">PHP</span>
                        <span class="label label-primary">Node.js</span>
                    </p>

                    <hr>

                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="false">Активность</a></li>
                    <li class=""><a href="#timeline" data-toggle="tab" aria-expanded="true">Timeline</a></li>
                    <li class=""><a href="#settings" data-toggle="tab" aria-expanded="false">Settings</a></li>
                </ul>
                <div class="tab-content ">
                    <div class="tab-pane active" id="activity">
                        <p>
                            <?
                            $roles = '';
                            if($model->roles){
                                $roles = str_replace(',', ', <br>',$model->roles);
                            }
                            echo $roles;
                            ?>

                        </p>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane " id="timeline">
                        <!-- The timeline -->
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="settings">
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

</section>
<div class="user-view">
    <div class="box-header">

    </div>
    <div class="box-body table-responsive no-padding">


        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                
                'email:email',
                'crm_name',
                'name',
                'phone',
                'role',


                [
                    'attribute'=>'roles',
                    'value' => function($model){
                        $roles = '';
                        if($model->roles){
                            $roles = str_replace(',', ', <br>',$model->roles);
                        }
                        return $roles;
                    },


                    'headerOptions' => ['width' => '200'],
                    'format'=>'html',
                    'label'=>'Шаги'
                ],
                [
                    'attribute'=>'UTM',
                    'value' => function($data){

                        $_rdr = '';
                        if($_data = myHellpers::decodeJson(unserialize($data->utm))){
                            foreach($_data as $k=>$v){
                                $_rdr .= "{$k} : {$v}<br>";
                            }
                        }

                        return $_rdr;
                    },
                    'format'=>'html',
                    'label'=>'Метки'
                ],

                'created_at:datetime',
            ],
        ]) ?>
    </div>
</div>
