<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\user\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [

                ['class' => 'yii\grid\ActionColumn'],

                'id',
                //'login',

                [
                    'attribute'=>'name',
                    'value' => function($model){
                        return Html::a($model->name, ['users/view', 'id' => $model->id]);
                    },
                    'format'=>'html',
                    'label'=>'Имя'
                ],
                'email:email',
                'phone',



                [
                    'attribute'=>'roles',
                    'value' => function($model){
                        $roles = '';
                        if($model->roles){
                            $roles = str_replace(',', ', <br>',$model->roles);
                        }
                        return $roles;
                    },


                    'headerOptions' => ['width' => '200'],
                    'format'=>'html',
                    'label'=>'Шаги'
                ],
                //'auth_key',
                //'password_hash',
                //'password_reset_token',
                [
                    'attribute'=>'utm',
                    'value' => function($data){
                        $_data = \common\helpers\myHellpers::decodeJson(unserialize($data->utm));
                        $_rdr = '';
                        if(!empty($_data)){
                            foreach($_data as $k=>$v){
                                $_rdr .= "{$k} : {$v}<br>";
                            }
                        }

                        return $_rdr;
                    },
                    'format'=>'html',
                    'label'=>'Метки'
                ],
                'created_at:datetime',
                //'updated_at',
            ],
            'layout' => "{items}\n{summary}\n{pager}",
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
