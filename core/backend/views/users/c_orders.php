<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\user\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-primary">

    <div class="box-body table-responsive no-padding">
        <div class="col-lg-12">
            <?
            foreach ($model as $order){?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?=
                        $order->course->name
                        ?>
                    </div>
                    <div class="panel-body">
                        <p >
                            Имя: <?=
                            $order->user->name
                            ?> <br>
                            Телефон: <?=
                            $order->user->phone
                            ?>  <br>
                            Email: <?=
                            $order->user->email
                            ?>  <br>
                            Курс: <?=
                            $order->course->name
                            ?>
                        </p>
                    </div>
                </div>


            <?
            }
            ?>
        </div>
    </div>

</div>
