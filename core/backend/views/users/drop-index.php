<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\user\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding ">
        <div class="row">
            <div class="col-md-12">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


        <? try {
            echo  GridView::widget([
                'dataProvider'=> $dataProvider,
                'filterModel' => $searchModel,

                'toolbar'=>[
                    '{export}',
                    '{toggleData}'
                ],

                'showPageSummary'=>true,
                'pjax'=>false,
                'striped'=>true,
                'hover'=>true,

                'panel'=>['type'=>'info', 'heading'=>'Таблица дропшиперов'],
                'exportConfig' => [
                    GridView::CSV => ['label' => 'Save as CSV'],
                    GridView::EXCEL => [],
                        GridView::PDF => [],
                        ],
                'columns'=>[

                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'visibleButtons' => [
                            'update' => function ($model) {
                                return \Yii::$app->user->can('manager', ['post' => $model]);
                            },
                            'delete' => function ($model) {
                                return \Yii::$app->user->can('supermanager', ['post' => $model]);
                            },
                        ]
                    ],

                    [
                        'attribute'=>'status_drop',
                        'value' => function($data){
                            return \common\helpers\myHellpers::getStatusLabel(
                                $data->status_drop,
                                \common\models\MainModel::STATUS_MODERATE,
                                \common\models\MainModel::STATUS_BADGE_USER);
                        },
                        'filter' => \common\models\MainModel::STATUS_MODERATE,
                        'filterInputOptions' => ['class' => 'form-control form-control-sm'],

                        'width'=>'120px',

                        'label'=>'Статус',
                        'format'=>'html'

                    ], 
                    [
                        'attribute'=>'name',
                        'value' => function($model){
                            return Html::a($model->name, ['users/view', 'id' => $model->id]);
                        },


                        'format'=>'html',
                        'label'=>'Имя',

                        'width'=>'200px',
                    ],
                    [
                        'attribute' => 'crm_name',
                        'value' => function ($model) {
                            return $model->name .' '.$model->id.'-дроп';
                        },
                        'format' => 'html',
                        'label' => 'Логин дропера',
                    ],
                    'email:email',
                    'phone',
                    [
                        'attribute' => 'created_at',

                        'format' => 'datetime',
                        'label' => 'Время заявки'
                    ],
                ],
                'layout' => "{items}\n{summary}\n{pager}",
            ]);
        } catch (Exception $e) {
        } ?>
            </div>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
