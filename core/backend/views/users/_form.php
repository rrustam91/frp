<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\user\User */
/* @var $form yii\widgets\ActiveForm */
$role_list = \common\models\rbac\AuthItem::getRolesList();
?>

<div class="user-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>


        <?= $form->field($model, 'status_role')->dropDownList($role_list) ?>
        <?= $form->field($model, 'status_drop')->dropDownList(\common\models\MainModel::STATUS_MODERATE) ?>
        <hr>
        <?
            if(Yii::$app->user->can('supermanager')){
        ?>
        <?= $form->field($model, 'password_new')->textInput(['type'=>'password']) ?>
        <?= $form->field($model, 'password_confirm')->textInput(['type'=>'password']) ?>
        <?
            }
        ?>


    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
