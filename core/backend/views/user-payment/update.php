<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\user\UserClientPayment */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'User Client Payment',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Client Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-client-payment-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
