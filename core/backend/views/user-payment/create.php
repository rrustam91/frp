<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\user\UserClientPayment */

$this->title = Yii::t('app', 'Create User Client Payment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Client Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-client-payment-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
