<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\user\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <? try {
          echo  GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'status_drop',
                        'value' => function ($data) {

                            return \common\helpers\myHellpers::getStatusLabel(
                                $data->status_drop,
                                \common\models\MainModel::STATUS_MODERATE,
                                \common\models\MainModel::STATUS_BADGE_USER);
                        },
                        'format' => 'html',
                        'label' => 'Статус',
                    ],
                    'name',
                    'email:email',
                    'phone',
                    [
                        'attribute' => 'created_at',

                        'format' => 'datetime',
                        'label' => 'Время заявки'
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{leadView} {leadUpdate} {leadDelete}',
                        'buttons' => [
                            'leadView' => function ($url, $model) {
                                $url = Url::to(['users/view', 'id' => $model->id]);
                                return Html::a('<span class="fa fa-eye"></span>', $url, ['title' => 'view']);
                            },
                            'leadUpdate' => function ($url, $model) {
                                $url = Url::to(['users/update', 'id' => $model->id]);
                                return Html::a('<span class="fa fa-pencil"></span>', $url, ['title' => 'update']);
                            },
                            'leadDelete' => function ($url, $model) {
                                $url = Url::to(['users/delete', 'id' => $model->id]);
                                return Html::a('<span class="fa fa-trash"></span>', $url, [
                                    'title' => 'delete',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                                ]);
                            },
                        ],
                        'visibleButtons' => [
                            'update' => function ($model) {
                                return '';
                            },
                            'delete' => function ($model) {
                                return '';
                            },
                        ]
                    ],
                ],
                'layout' => "{items}\n{summary}\n{pager}",
            ]);
        } catch (Exception $e) {
        } ?>
    </div>
    <?php Pjax::end(); ?>
</div>
