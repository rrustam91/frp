<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\dropship\Dropship */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Dropship',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dropships'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dropship-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
