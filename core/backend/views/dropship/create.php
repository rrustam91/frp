<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\dropship\Dropship */

$this->title = Yii::t('app', 'Create Dropship');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dropships'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dropship-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
