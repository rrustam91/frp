<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\delivery\DeliveryType */

$this->title = Yii::t('app', 'Create Delivery Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Delivery Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-type-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
