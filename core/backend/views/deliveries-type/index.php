<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\delivery\search\DeliveryTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Delivery Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-type-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Delivery Type'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                [
                    'attribute'=>'status_drop',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->status_drop, \common\models\MainModel::STATUS_SIMPLE_LIST);
                    },
                    'filter' => \common\models\MainModel::STATUS_SIMPLE_LIST,
                    'filterInputOptions' => ['class' => 'form-control form-control-sm'],
                    'label'=>'Статус для Дроперов',
                    'format'=>'html'

                ],
                [
                    'attribute'=>'status_opt',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->status_opt, \common\models\MainModel::STATUS_SIMPLE_LIST);
                    },
                    'filter' => \common\models\MainModel::STATUS_SIMPLE_LIST,
                    'filterInputOptions' => ['class' => 'form-control form-control-sm'],
                    'label'=>'Статус для опта',
                    'format'=>'html'

                ],
                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
