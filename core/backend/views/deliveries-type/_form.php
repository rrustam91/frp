<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\delivery\DeliveryType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-type-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'delivery_price')->textInput(['type' => 'number']) ?>

        <?= $form->field($model, 'status')->dropDownList(
                common\models\MainModel::STATUS_SIMPLE_LIST
        ) ?>
        <?= $form->field($model, 'status_drop')->dropDownList(
                common\models\MainModel::STATUS_SIMPLE_LIST
        ) ?>
        <?= $form->field($model, 'status_opt')->dropDownList(\common\models\MainModel::STATUS_SIMPLE_LIST) ?>

        <?//= $form->field($model, 'created_at')->textInput() ?>

        <?//= $form->field($model, 'updated_at')->textInput() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
