<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\courses\CoursesOrder */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Courses Order',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Courses Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="courses-order-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
