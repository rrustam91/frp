<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\helpers\modelHellpers;
/* @var $this yii\web\View */
/* @var $model common\models\courses\CoursesOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="courses-order-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <div class="col-md-6">
             <?//= $form->field($model, 'user_id')->textInput() ?>

            <?//= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
             <?=$form->field($model, 'user_id')->widget(Select2::classname(), [

                 'data' => modelHellpers::getSelectList(\common\models\user\User::className(),'id','email'),
                 'options' => ['placeholder' => 'Выберите курс' ],
                 'pluginOptions' => [
                     'allowClear' => true
                 ],
                 'theme'=>Select2::THEME_DEFAULT,
             ]); ?>
            <?=$form->field($model, 'course_id')->widget(Select2::classname(), [

                'data' => modelHellpers::getSelectList(\common\models\courses\Courses::className()),
                'options' => ['placeholder' => 'Выберите курс' ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'theme'=>Select2::THEME_DEFAULT,
            ]); ?>
             <?=$form->field($model, 'p_course_id')->widget(Select2::classname(), [
                 'data' => modelHellpers::getSelectList(\common\models\courses\Courses::className()),
                 'options' => ['placeholder' => 'Выберите курс' ],
                 'pluginOptions' => [
                     'allowClear' => true
                 ],
                 'theme'=>Select2::THEME_DEFAULT,
             ]); ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'price')->textInput() ?>

            <?= $form->field($model, 'status')->dropDownList(\common\models\MainModel::STATUS_ORDER_USER__LIST) ?>

            <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
        </div>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
