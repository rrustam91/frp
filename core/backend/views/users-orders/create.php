<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\courses\CoursesOrder */

$this->title = Yii::t('app', 'Create Courses Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Courses Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="courses-order-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
