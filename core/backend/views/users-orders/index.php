<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\courses\search\CoursesOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="courses-order-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Courses Order'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="table-responsive">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",

            'options' => [
                'style'=>'overflow: auto; word-wrap: break-word;'
            ],
            'columns' => [

                ['class' => 'yii\grid\ActionColumn'],

                [
                    'attribute'=>'user_name',
                    'value' => function($model){

                        return Html::a($model->user->name, ['users/view', 'id' => $model->user->id]);
                    },
                    'format'=>'html',
                    'label'=>'ФИО'
                ],
                [
                    'attribute'=>'user_email',
                    'value' => function($model){
                        return $model->getUserEmail();
                    },
                    'label'=>'Email'
                ],
                [
                    'attribute'=>'user_phone',
                    'value' => function($model){
                        return $model->user->phone;
                    },
                    'label'=>'Телефон'
                ],
                [
                    'attribute'=>'course_id',
                    'value' => function($model){

                        return $model->course->name;
                    },
                    'label'=>'Курс',
                    'filter' => \common\helpers\modelHellpers::getSelectList(\common\models\courses\Courses::className()),
                    'filterInputOptions' => ['class' => 'form-control form-control-sm'],
                ],
                [
                    'attribute'=>'comment',

                    'label'=>'Коменты',
                    'value' => function($data){
                        return $data->comment ;
                    },

                    'contentOptions'=>['style'=>'max-width: 240px; min-height:100px; overflow: auto; word-wrap: break-word;height: auto'],
                    'format'=>'raw',
                ],

                'price',


                [
                    'attribute'=>'status',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->status, \common\models\MainModel::STATUS_ORDER_USER__LIST);
                    },
                    'filter' => \common\models\MainModel::STATUS_ORDER_USER__LIST,
                    'filterInputOptions' => ['class' => 'form-control form-control-sm'],
                    'label'=>'Статус',
                    'format'=>'html'

                ],
                // 'comment:ntext',
                 'created_at:datetime',
                // 'updated_at',

            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
    </div>
</div>
