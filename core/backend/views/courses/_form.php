<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\courses\Courses;
use kartik\select2\Select2;
use \dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\courses\Courses */
/* @var $form yii\widgets\ActiveForm */
if(empty($model->price)){
    $model->price = 0;
}
?>

<div class="courses-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">
        <div class="col-md-3">

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?//= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

            <?=$form->field($model, 'parent_id')->widget(Select2::classname(), [

                'data' => \common\helpers\modelHellpers::getSelectList(Courses::className()),
                'options' => ['placeholder' => 'Выберите курс' ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'theme'=>Select2::THEME_DEFAULT,
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'price')->textInput(['type'=>'number','min'=>0]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList(\common\models\MainModel::STATUS_DEFAULT_LIST) ?>

        </div>

        <div class="col-md-12">

            <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'basic'
            ]) ?>

        </div>
        <?//= $form->field($model, 'date_start')->textInput() ?>

        <?//= $form->field($model, 'date_end')->textInput() ?>



        <?/*= $form->field($model, 'status')->textInput() */?><!--

        <?/*= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) */?>

        <?/*= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) */?>

        --><?/*= $form->field($model, 'seo_description')->textInput(['maxlength' => true]) */?>
<!--
        <?/*= $form->field($model, 'created_at')->textInput() */?>

        --><?/*= $form->field($model, 'updated_at')->textInput() */?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
