<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\courses\search\CoursesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="courses-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Courses'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => [
                'class' => 'table table-striped table-bordered'
            ],

            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'name',

                [
                    'attribute'=>'name',
                    'value' => function($model){

                        return $model->name;
                    },
                    'label'=>'Название курса',

                ],
                [
                    'attribute'=>'parent_id',
                    'value' => function($model){

                        return $model->name;
                    },
                    'label'=>'Основной курс',
                    'filter' => \common\helpers\modelHellpers::getSelectList(\common\models\courses\Courses::className()),
                    'filterInputOptions' => ['class' => 'form-control form-control-sm'],
                ],
                //'alias',
                'price',
                'date_start',
                // 'date_end',
                // 'content:ntext',
                // 'status',
                // 'seo_title',
                // 'seo_keywords',
                // 'seo_description',
                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],

        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
