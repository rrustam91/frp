<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\products\ProductsStock */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Products Stock',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products Stocks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="products-stock-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
