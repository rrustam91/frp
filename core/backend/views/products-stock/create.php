<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\products\ProductsStock */

$this->title = Yii::t('app', 'Create Products Stock');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products Stocks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-stock-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
