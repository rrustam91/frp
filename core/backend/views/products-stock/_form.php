<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\products\ProductsStock */
/* @var $form yii\widgets\ActiveForm */

if(empty($model->count)){
    $model->count = 1;
}
?>

<div class="products-stock-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?=$form->field($model, 'product_id')->widget(Select2::classname(), [

            'data' => $model::getSelectList(\common\models\products\Products::className()),
            'options' => ['placeholder' => 'Выберите продукт' ],
            'pluginOptions' => [
                'allowClear' => true
            ],
            'theme'=>Select2::THEME_DEFAULT,
        ]); ?>
        <?= $form->field($model, 'count')->textInput(['type'=>'number','min'=>'0']) ?>


    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
