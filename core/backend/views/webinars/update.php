<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\webinars\Webinars */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Webinars',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Webinars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="webinars-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
