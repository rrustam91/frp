<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\webinars\Webinars */

$this->title = Yii::t('app', 'Create Webinars');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Webinars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="webinars-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
