<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\webinars\Webinars */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="webinars-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'status')->dropDownList($model::STATUS_DEFAULT_LIST) ?>
            <?= $form->field($model, 'date_start')->textInput(['class'=>'datepick form-control','maxlength' => true]) ?>
            <?= $form->field($model, 'date_end')->textInput(['class'=>'datepick form-control','maxlength' => true]) ?>
        </div>

        <div class="col-md-8">
            <?= $form->field($model, 'video_url')->textInput(['maxlength' => true]) ?>
        </div>





    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
