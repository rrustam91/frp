<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\webinars\WebinarComments */

$this->title = Yii::t('app', 'Create Webinar Comments');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Webinar Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="webinar-comments-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>

