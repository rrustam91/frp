<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\webinars\search\WebinarCommentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Webinar Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="webinar-comments-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Webinar Comments'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
                [
                    'attribute'=>'name',
                    'label'=>Yii::t('app', 'User name viewer')
                ],
                [
                    'attribute'=>'text',
                    'label'=>Yii::t('app', 'Text chat')
                ],
                'time_send',
                [
                    'attribute'=>'webinar_id',
                    'value' => function($data){
                        return $data->webinar->name;
                    },
                    'label'=>'Вебинар'
                ],
                //'created_at',
                //'updated_at',

                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
