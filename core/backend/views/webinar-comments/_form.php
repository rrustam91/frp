<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\webinars\Webinars;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\webinars\WebinarComments */
/* @var $form yii\widgets\ActiveForm */
$model->webinar_id = 1;
$model->time_send = 0;
?>

<div class="webinar-comments-form box box-primary">

    <?php $form = ActiveForm::begin([
        'action' => 'save',
        'enableAjaxValidation' => true,
        'validationUrl' => 'valid',
    ]); ?>


    <div class="box-body table-responsive row">

        <div class="col-md-6">

            <?=$form->field($model, 'webinar_id')->widget(Select2::classname(), [

                'data' => $model::getSelectList(Webinars::className()),
                'options' => ['placeholder' => 'Выберите категорию' ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'theme'=>Select2::THEME_DEFAULT,
            ]); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Имя') ?>

            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'time_send')->textInput(['class'=>'time_send form-control', 'type'=>'number']) ?>

        </div>

        <div class="col-md-6" style="padding: 0">
            <div style="min-height:400px; padding-right: 20px; position: relative; ">
                <video id="player" playsinline controls preload="auto" class="video-js vjs-default-skin vjs-big-play-centered" style="height: 100%; width: 100%; position: absolute;"></video>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>



