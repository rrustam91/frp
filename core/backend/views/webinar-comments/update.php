<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\webinars\WebinarComments */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Webinar Comments',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Webinar Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="webinar-comments-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
