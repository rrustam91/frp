<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\dropship\DropDeliver */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="drop-deliver-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'status')->textInput() ?>

        <?= $form->field($model, 'drop_order_id')->textInput() ?>

        <?= $form->field($model, 'delivery_type_id')->textInput() ?>

        <?= $form->field($model, 'delivery_type_code')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'subway')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'zipcode')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'building')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'block')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'house')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'floor')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'cost')->textInput() ?>

        <?= $form->field($model, 'delivery_price')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'created_at')->textInput() ?>

        <?= $form->field($model, 'updated_at')->textInput() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
