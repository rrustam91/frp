<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\dropship\DropDeliver */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Drop Deliver',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drop Delivers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="drop-deliver-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
