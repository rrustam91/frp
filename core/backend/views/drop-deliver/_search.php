<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\dropship\search\DropDeliverSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="drop-deliver-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'drop_order_id') ?>

    <?= $form->field($model, 'delivery_type_id') ?>

    <?= $form->field($model, 'delivery_type_code') ?>

    <?php // echo $form->field($model, 'region') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'subway') ?>

    <?php // echo $form->field($model, 'street') ?>

    <?php // echo $form->field($model, 'zipcode') ?>

    <?php // echo $form->field($model, 'building') ?>

    <?php // echo $form->field($model, 'block') ?>

    <?php // echo $form->field($model, 'house') ?>

    <?php // echo $form->field($model, 'floor') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'cost') ?>

    <?php // echo $form->field($model, 'delivery_price') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
