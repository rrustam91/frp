<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\dropship\DropDeliver */

$this->title = Yii::t('app', 'Create Drop Deliver');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drop Delivers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drop-deliver-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
