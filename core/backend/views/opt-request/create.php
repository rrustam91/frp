<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\opt\OptRequest */

$this->title = Yii::t('app', 'Create Opt Request');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opt Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opt-request-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
