<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\opt\OptRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Opt Requests');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opt-request-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Opt Request'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [

                ['class' => 'yii\grid\ActionColumn'],

                //'id',
                [
                    'attribute'=>'email',
                    'value' => function($model){
                        return Html::a($model->user->email, ['users/view', 'id' => $model->user->id]);
                    },
                    'format'=>'html',
                    'label'=>'Название курса',

                ],
                [
                    'attribute'=>'phone',
                    'value' => function($model){

                        return $model->user->phone;
                    },
                    'label'=>'Телефон',

                ],
                'link',
//
//                [
//                    'attribute'=>'status',
//                    'value' => function($data){
//                        return \common\helpers\myHellpers::getStatusLabel($data->status, \common\models\MainModel::STATUS_SIMPLE_LIST);
//                    },
//                    'filter' => \common\models\MainModel::STATUS_SIMPLE_LIST,
//                    'filterInputOptions' => ['class' => 'form-control form-control-sm'],
//                    'label'=>'Статус',
//                    'format'=>'html'
//
//                ],
                'append:ntext',
                 'created_at:datetime',
                // 'updated_at',

            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
