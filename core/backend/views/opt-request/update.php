<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\opt\OptRequest */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Opt Request',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opt Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="opt-request-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
