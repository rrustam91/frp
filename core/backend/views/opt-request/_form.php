<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\opt\OptRequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="opt-request-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive row">

        <div class="col-sm-6">
            <?= $form->field($model, 'email')->textInput() ?>
            <?= $form->field($model, 'phone')->textInput() ?>
        </div>
        <?//= $form->field($model, 'user_id')->textInput() ?>

        <div class="col-sm-6">
            <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropDownList(\common\models\MainModel::STATUS_SIMPLE_LIST)?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($model, 'append')->textarea(['rows' => 6]) ?>
        </div>






    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
