<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\user\search\UserHomeworkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Homeworks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-homework-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create User Homework'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [

                ['class' => 'yii\grid\ActionColumn'],
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute'=>'user_name',
                    'value' => function($model){

                        return Html::a($model->user->name, ['users/view', 'id' => $model->user->id]);
                    },
                    'format'=>'html',
                    'label'=>'ФИО'
                ],
                [
                    'attribute'=>'lesson_id',
                    'value' => function($model){

                        return $model->lesson->name;
                    },
                    'format'=>'html',
                    'label'=>'ФИО'
                ],
                'text:html',
                // 'comment_teach:ntext',
                 'created_at:datetime',
                // 'updated_at',

            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
