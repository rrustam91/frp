<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\dropship\OrdersStatusGroup */

$this->title = Yii::t('app', 'Create Orders Status Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders Status Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-status-group-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
