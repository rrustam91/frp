<aside class="main-sidebar">

    <section class="sidebar">


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [

                    ['label' => 'Проекты', 'options' => ['class' => 'header']],
                    ['label' => 'Заявки по курсам ', 'icon' => 'file-code-o', 'url' => ['/users-orders']],
                    ['label' => 'Заявки по дропам ', 'icon' => 'file-code-o', 'url' => ['/drop-order']],
                    ['label' => 'Заявки по опту ', 'icon' => 'file-code-o', 'url' => ['/opt-request']],

                    ['label' => 'Сообщения', 'icon' => 'comments-o', 'url' => ['/users-messages']],
                    [
                        'label' => 'Вебинары модули',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [

                            ['label' => 'Вебинары ', 'icon' => 'file-code-o', 'url' => ['/webinars']],
                            ['label' => 'Вопросы от пользователей', 'icon' => 'commenting-o', 'url' => ['/webinar-chats']],
                            ['label' => 'Реклама/Баннеры', 'icon' => 'clone', 'url' => ['/webinar-ads']],
                            ['label' => 'Авто-сообщения в чате', 'icon' => 'comments-o', 'url' => ['/webinar-comments']],

                        ],
                    ],
                    [
                        'label' => 'Курсы',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Курсы', 'icon' => 'file-code-o', 'url' => ['/courses']],
                            ['label' => 'Уроки к курсам', 'icon' => 'commenting-o', 'url' => ['/courses-lessons']],
                            ['label' => 'Категории ', 'icon' => 'comments-o', 'url' => ['/course-category']],
                            ['label' => 'Заказы по курсам', 'icon' => 'clone', 'url' => ['/courses-order']],
                            ['label' => 'Ученики', 'icon' => 'comments-o', 'url' => ['/courses/users']],
                            ['label' => 'Дом. задания', 'icon' => 'comments-o', 'url' => ['/users-homework']],

                        ],
                    ],
                    ['label' => 'Пользователи', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Все пользователи',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Пользователи ', 'icon' => 'file-code-o', 'url' => ['/users']],
                            ['label' => 'Выплаты ', 'icon' => 'file-code-o', 'url' => ['/user-payment']],
                            ['label' => 'Оптовики', 'icon' => 'clone', 'url' => ['/users/opt']],
                            ['label' => 'Ученики', 'icon' => 'clone', 'url' => ['/users/student']],
                            ['label' => 'Клиенты с заявок', 'icon' => 'comments-o', 'url' => ['/users/clients']],
                            ['label' => 'Сообщения', 'icon' => 'comments-o', 'url' => ['/users-messages']],

                        ],
                    ],
                    [
                        'label' => 'Дропшиперы',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [

                            ['label' => 'Дропшиперы', 'icon' => 'commenting-o', 'url' => ['/users/dropship']],
                            ['label' => 'Заявки', 'icon' => 'commenting-o', 'url' => ['/drop-order']],
                            ['label' => 'Товары', 'icon' => 'clone', 'url' => ['/products/drop']],
                            ['label' => 'Клиенты с дропшипа', 'icon' => 'comments-o', 'url' => ['/drop/clients']],

                        ],
                    ],
                    [
                        'label' => 'Оптовики',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Дропшиперы', 'icon' => 'commenting-o', 'url' => ['/users/opt']],
                            ['label' => 'Заявки', 'icon' => 'commenting-o', 'url' => ['/opt/orders']],
                            ['label' => 'Товары', 'icon' => 'clone', 'url' => ['/products/opt']],

                        ],
                    ],
                    [
                        'label' => 'Доп. параметры',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Способы доставки ', 'icon' => 'file-code-o', 'url' => ['/deliveries-type']],
                            ['label' => 'Продукты ', 'icon' => 'file-code-o', 'url' => ['/products']],
                            ['label' => 'Категории продуктов', 'icon' => 'file-code-o', 'url' => ['/products-categories']],
                            ['label' => 'Склад продуктов', 'icon' => 'file-code-o', 'url' => ['/products-stock']],

                        ],
                    ],
                    ['label' => 'Страницы', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Страницы',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Главная', 'icon' => 'file-code-o', 'url' => ['/pages/main']],
                            ['label' => 'Блог ', 'icon' => 'file-code-o', 'url' => ['/pages/blog']],
                            ['label' => 'Новости', 'icon' => 'commenting-o', 'url' => ['/pages/news']],

                        ],
                    ],

                    ['label' => 'Выйти', 'url' => ['/logout'] ],
                ],
            ]
        ) ?>

    </section>

</aside>
