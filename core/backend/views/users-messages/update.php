<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\user\UserMessages */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'User Messages',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-messages-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
