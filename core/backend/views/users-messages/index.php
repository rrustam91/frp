<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\user\search\UserMessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-messages-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create User Messages'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute'=>'user_id',
                    'value' => function($model){
                        return Html::a($model->user->name, ['users/view', 'id' => $model->user->id]);
                    },
                    'format'=>'html',
                    'label'=>'Имя'
                ],

                [
                    'attribute'=>'type',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->type, \common\models\MainModel::STATUS_MESSAGES_LIST);
                    },
                    'label'=>'Тип сообщения',
                    'format'=>'html'

                ],
                'text:ntext',
                'status',
                 'created_at:datetime',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
