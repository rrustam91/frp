<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\products\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name:ntext',
                [
                    'attribute'=>'article',

                    'label'=>'Ариткул',
                    'value' => function($model){
                        return $model->article ;
                    },

                ],
                [
                    'attribute' => 'category_id',
                    'value' => function ($model) {

                        return $model->category->name;
                    },
                    'format' => 'html',
                    'label' => 'Категория',
                ],

                'alias',

                'price',

                'price_drop',

                'price_opt',

                [
                    'attribute'=>'status',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->status, \common\models\MainModel::STATUS_SIMPLE_LIST);
                    },
                    'label'=>'Статус',
                    'format'=>'html'

                ],
                [
                    'attribute'=>'status_drop',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->status_drop, \common\models\MainModel::STATUS_SIMPLE_LIST);
                    },
                    'label'=>'Статус для Дроперов',
                    'format'=>'html'

                ],
                'created_at:datetime',
                'updated_at:datetime',
            ],
        ]) ?>
        <div class="col-md-12">
            <h4> Описание</h4>
            <p>
                <?=$model->info->text?>
            </p>
        </div>
        <div class="col-md-12">
            <h4> Изображения</h4>
            <?
            if(!empty($model->imgs) && isset($model->imgs)){
                foreach ($model->imgs as $img){
                    ?>
                    <div class="col-md-1">
                        <img src="/uploads/<?=$img->url?>" alt="" class="img-responsive">
                    </div>
                    <?
                }

            }
            ?>
            <br clear="all">
            <br><br>
        </div>
    </div>
</div>
