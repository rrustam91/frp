<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\products\search\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Products'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",

            'options' => [
                'style'=>'overflow: auto; word-wrap: break-word;'
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute'=>'article',

                    'label'=>'Ариткул',
                    'value' => function($model){
                        return $model->article ;
                    },

                    'contentOptions'=>['style'=>'width: 40px; '],
                    'format'=>'raw',
                ],

                'name:ntext',

                [
                    'attribute' => 'category_id',
                    'value' => function ($model) {

                        return $model->category->name;
                    },
                    'format' => 'html',
                    'label' => 'Категория',
                ],
                [
                    'attribute'=>'status',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->status, \common\models\MainModel::STATUS_SIMPLE_LIST);
                    },
                    'filter' => \common\models\MainModel::STATUS_SIMPLE_LIST,
                    'filterInputOptions' => ['class' => 'form-control form-control-sm'],
                    'label'=>'Статус',
                    'format'=>'html'

                ],
                [
                    'attribute'=>'status_drop',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->status_drop, \common\models\MainModel::STATUS_SIMPLE_LIST);
                    },
                    'label'=>'Статус для Дроперов',
                    'filter' => \common\models\MainModel::STATUS_SIMPLE_LIST,
                    'filterInputOptions' => ['class' => 'form-control form-control-sm'],
                    'format'=>'html'

                ],
                // 'price',
                // 'status',
                // 'status_drop',
                // 'xml_id',
                // 'xml_name',
                // 'xml_category',
                // 'xml_price',
                // 'xml_article',
                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
