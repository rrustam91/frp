<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\products\ProductsCategories;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\forms\ProductsForm */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="products-form box box-primary">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="box-body table-responsive">
        <div class="col-md-6">

            <?= $form->field($model, 'name')->textInput() ?>

            <?=$form->field($model, 'category_id')->widget(Select2::classname(), [

                'data' => \common\helpers\modelHellpers::getSelectList(ProductsCategories::className()),
                'options' => ['placeholder' => 'Выберите категорию' ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'theme'=>Select2::THEME_DEFAULT,
            ]); ?>

            <?= $form->field($model, 'price')->textInput(['type'=>'number']) ?>
            <?= $form->field($model, 'price_drop')->textInput(['type'=>'number']) ?>
            <?//= $form->field($model, 'price_opt')->textInput(['type'=>'number']) ?>
            <?
            if(!empty($model->id) && isset($model->id)){
                echo $form->field($model, 'product_id')->textInput(['type'=>'hidden', 'value'=>$model->id])->label(false);
            }
            ?>




        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'status')->dropDownList(\common\models\MainModel::STATUS_DEFAULT_LIST) ?>

            <?= $form->field($model, 'status_drop')->dropDownList(\common\models\MainModel::STATUS_SIMPLE_LIST) ?>
            <?= $form->field($model, 'status_opt')->dropDownList(\common\models\MainModel::STATUS_SIMPLE_LIST) ?>
        </div>
        <div class="col-md-12">
            <?
            if(!empty($model->imgs) && isset($model->imgs)){
                foreach ($model->imgs as $img){
                    ?>
                    <div class="col-md-1">
                        <img src="/uploads/<?=$img->url?>" alt="" class="img-responsive">
                    </div>
                    <?
                }

            }
            ?>
            <br clear="all">
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'basic'
            ]) ?>
        </div>




    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
