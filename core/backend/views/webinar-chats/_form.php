<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\user\User;
use common\models\webinars\Webinars;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\webinars\WebinarChats */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="webinar-chats-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">



        <?=$form->field($model, 'user_id')->widget(Select2::classname(), [
            'data' => $model::getSelectList(User::className()),
            'options' => ['placeholder' => Yii::t('app', 'Select user') ],
            'pluginOptions' => [ 'allowClear' => true ],
            'theme'=>Select2::THEME_DEFAULT,
        ])->label( Yii::t('app', 'Select user')); ?>

        <?=$form->field($model, 'webinar_id')->widget(Select2::classname(), [

            'data' => $model::getSelectList(Webinars::className()),
            'options' => ['placeholder' => Yii::t('app', 'Select Webinar') ],
            'pluginOptions' => [ 'allowClear' => true ],
            'theme'=>Select2::THEME_DEFAULT,
        ])->label( Yii::t('app', 'Select Webinar')); ?>

        <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'time_video')->textInput() ?>


    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
