<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\webinars\WebinarChats */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Webinar Chats',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Webinar Chats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="webinar-chats-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
