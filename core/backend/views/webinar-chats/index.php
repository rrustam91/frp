<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\webinars\search\WebinarChatsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Webinar Chats');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="webinar-chats-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Webinar Chats'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                //'user_id',

                [
                    'attribute'=>'user_id',
                    'value' => function($model){

                        return Html::a($model->user->name, ['users/view', 'id' => $model->user_id]);
                    },
                    'format'=>'html',
                    'label'=>'ФИО'
                ], 
                [
                    'attribute'=>'webinar_id',
                    'value' => function($data){
                        return $data->webinar->name;
                    },
                    'label'=>'Вебинар'
                ],
                'text:ntext',
                'time_video',
                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
