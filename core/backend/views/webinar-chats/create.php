<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\webinars\WebinarChats */

$this->title = Yii::t('app', 'Create Webinar Chats');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Webinar Chats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="webinar-chats-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
