<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\courses\CoursesLessons */

$this->title = Yii::t('app', 'Create Courses Lessons');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Courses Lessons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="courses-lessons-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
