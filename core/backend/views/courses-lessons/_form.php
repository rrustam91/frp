<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\courses\CoursesLessons */
/* @var $form yii\widgets\ActiveForm */

if(empty($model->step_by)){
    $model->step_by = 0;
}
?>

<div class="courses-lessons-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">
        <div class="col-md-6">

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?=$form->field($model, 'course_id')->widget(Select2::classname(), [

                'data' => \common\helpers\modelHellpers::getSelectList(\common\models\courses\Courses::className()),
                'options' => ['placeholder' => 'Выберите курс' ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'theme'=>Select2::THEME_DEFAULT,
            ]); ?>
            <?=$form->field($model, 'category_id')->widget(Select2::classname(), [

                'data' => \common\helpers\modelHellpers::getSelectList(\common\models\courses\CourseCategory::className()),
                'options' => ['placeholder' => 'Выберите курс' ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'theme'=>Select2::THEME_DEFAULT,
            ]); ?>
        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'status')->dropDownList(\common\models\MainModel::STATUS_DEFAULT_LIST) ?>

            <?= $form->field($model, 'opened')->dropDownList(\common\models\MainModel::STATUS_SIMPLE_LIST) ?>

            <?= $form->field($model, 'step_by')->textInput(['type'=>'number']) ?>

        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'content')->textArea(['rows' => 6,  'class' => 'tinymce_editor']); ?>
        </div>


    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
