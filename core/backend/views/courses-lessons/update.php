<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\courses\CoursesLessons */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Courses Lessons',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Courses Lessons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="courses-lessons-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
