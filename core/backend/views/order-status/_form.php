<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\dropship\OrdersStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-status-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">


        <?=$form->field($model, 'group')->widget(Select2::classname(), [

            'data' => $model::getSelectList(\common\models\dropship\OrdersStatusGroup::className()),
            'options' => ['placeholder' => 'Выберите категорию' ],
            'pluginOptions' => [
                'allowClear' => true
            ],
            'theme'=>Select2::THEME_DEFAULT,
        ]); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'drop_name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'status_drop')->dropDownList(\common\models\MainModel::STATUS_SIMPLE_LIST) ?>


    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
