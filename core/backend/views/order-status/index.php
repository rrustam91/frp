<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\dropship\search\OrdersStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders Statuses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-status-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Orders Status'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                'drop_name',

                'code',
                [
                    'attribute'=>'status_drop',
                    'value' => function($data){
                        return \common\helpers\myHellpers::getStatusLabel($data->status_drop, \common\models\MainModel::STATUS_SIMPLE_LIST);
                    },
                    'label'=>'Статус для Дроперов',
                    'filter' => \common\models\MainModel::STATUS_SIMPLE_LIST,
                    'filterInputOptions' => ['class' => 'form-control form-control-sm'],
                    'format'=>'html'

                ],
                // 'active',
                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
