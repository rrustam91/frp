<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\dropship\OrdersStatus */

$this->title = Yii::t('app', 'Create Orders Status');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-status-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
