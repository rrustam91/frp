<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\dropship\OrdersStatus */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Orders Status',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="orders-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
