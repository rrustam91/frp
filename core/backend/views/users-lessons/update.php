<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\user\UserLessons */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'User Lessons',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Lessons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-lessons-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
