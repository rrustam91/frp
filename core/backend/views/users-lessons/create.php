<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\user\UserLessons */

$this->title = Yii::t('app', 'Create User Lessons');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Lessons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-lessons-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
