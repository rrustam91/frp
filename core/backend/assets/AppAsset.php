<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'libs/datetimepicker/jquery.datetimepicker.min.css',

        'libs/videojs/video-js.css',
    ];
    public $js = [
        'libs/datetimepicker/jquery.datetimepicker.full.min.js',
        'libs/videojs/video.js',
        'libs/videojs/lang/ru.js',

        'js/TinyEditor.js',
        //'libs/videojs/ie8/videojs-ie8.min.js',
        //'libs/videojs/youtubePlugin.js',
        //'libs/videojs/hls/videojs-hlsjs.min.js',
        'js/scripts.js'
    ];
    public $depends = [

        'yii\web\YiiAsset',

        'dosamigos\tinymce\TinyMceAsset',
        'yii\bootstrap\BootstrapAsset',

    ];
}