<?php

namespace backend\controllers;

use common\helpers\myHellpers;
use common\models\dropship\DropDeliver;
use common\models\forms\DropOrderForm;
use Yii;
use common\models\dropship\DropOrder;
use common\models\dropship\search\DropOrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DropOrderController implements the CRUD actions for DropOrder model.
 */
class DropOrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DropOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DropOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DropOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' =>$this->findModel($id)
        ]);
    }

    /**
     * Creates a new DropOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DropOrder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Creates a new DropOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateSample()
    {
        $model_form = new DropOrderForm();
        $drop_delivery = new DropDeliver();

        if(Yii::$app->request->isPost){
            if($model_form->load(Yii::$app->request->post())){
                $data = Yii::$app->request->post();
                $drop_order = new CreateDropOrder(Yii::$app->user, $data['DropOrderForm']);
                $drop_order->createOrder();
                echo '<pre>';
                //      myHellpers::show(Yii::$app->request->post(), 'data post');
                echo '</pre>';

            }
        }
        if ($model_form->load(Yii::$app->request->post()) && $model_form->save()) {
            return $this->redirect(['view', 'id' => $model_form->id]);
        } else {
            return $this->render('create-sample', [
                'model_form' => $model_form,
                'drop_delivery' => $drop_delivery
            ]);
        }
    }

    /**
     * Updates an existing DropOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DropOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DropOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DropOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        if (($model = DropOrder::find()->where(['id'=>$id])->with('products')->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
