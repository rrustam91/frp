<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.08.2018
 * Time: 13:04
 */

namespace backend\controllers\rbac;


use common\models\rbac\AuthItem;
use backend\controllers\rbac\UserProfileOwnerRule;
use Yii;
use yii\console\Controller;
use backend\controllers\rbac\UserGroupRule;

class RbacController extends Controller
{
    public function actionIndex(){
        $auth_item = AuthItem::find()->where(['type'=>2])->all();
        //$auth_item::find()->where(['type'=>2])->all();
        print_r($auth_item);

    }
    public function actionInit()
    {
        $authManager = \Yii::$app->authManager;
        $authManager->removeAll();
        // Create roles
        $guest  = $authManager->createRole('guest');
        $client  = $authManager->createRole('client');
        $student  = $authManager->createRole('student');
        $drop  = $authManager->createRole('drop');
        $opt  = $authManager->createRole('opt');
        $manager = $authManager->createRole('manager');
        $super_manager = $authManager->createRole('supermanager');
        $admin  = $authManager->createRole('admin');
        $superadmin  = $authManager->createRole('SUPERADMIN');

        // Create simple, based on action{$NAME} permissions
        $login  = $authManager->createPermission('login');
        $logout = $authManager->createPermission('logout');
        $error  = $authManager->createPermission('error');
        $signUp = $authManager->createPermission('sign-up');
        $index  = $authManager->createPermission('index');
        $view   = $authManager->createPermission('view');
        $update = $authManager->createPermission('update');
        $create = $authManager->createPermission('create');
        $delete = $authManager->createPermission('delete');

        // Add permissions in Yii::$app->authManager
        $authManager->add($login);
        $authManager->add($logout);
        $authManager->add($error);
        $authManager->add($signUp);
        $authManager->add($index);
        $authManager->add($view);
        $authManager->add($update);
        $authManager->add($create);
        $authManager->add($delete);

/*
        // Add rule, based on UserExt->group === $user->group
        $userGroupRule = new UserGroupRule();
        $authManager->add($userGroupRule);

        // Add rule "UserGroupRule" in roles
        $guest->ruleName  = $userGroupRule->name;
        $client->ruleName  = $userGroupRule->name;
        $manager->ruleName = $userGroupRule->name;
        $admin->ruleName  = $userGroupRule->name;
*/
        // Add roles in Yii::$app->authManager
        $authManager->add($guest);
        $authManager->add($client);
        $authManager->add($student);
        $authManager->add($drop);
        $authManager->add($opt);
        $authManager->add($manager);
        $authManager->add($super_manager);
        $authManager->add($admin);
        $authManager->add($superadmin);

        // Add permission-per-role in Yii::$app->authManager
        // Guest
        $authManager->addChild($guest, $login);
        $authManager->addChild($guest, $logout);
        $authManager->addChild($guest, $error);
        $authManager->addChild($guest, $signUp);
        $authManager->addChild($guest, $index);
        $authManager->addChild($guest, $view);


        // Drop / Opt / Client
        $authManager->addChild($client, $create);
        $authManager->addChild($client, $guest);
        $authManager->addChild($opt, $client);
        $authManager->addChild($drop, $client);
        $authManager->addChild($student, $client);


        // manager

        $authManager->addChild($manager, $update);

        $authManager->addChild($manager, $opt);
        $authManager->addChild($manager, $drop);
        $authManager->addChild($manager, $client);

        // super-manager
        $authManager->addChild($super_manager, $delete);
        $authManager->addChild($super_manager, $manager);


        // Admin
        $authManager->addChild($admin, $super_manager);


        // SUPERADMIN
        $authManager->addChild($superadmin, $admin );

        // add the rule
        $userProfileOwnerRule = new UserProfileOwnerRule();
        $authManager->add($userProfileOwnerRule);

        $updateOwnProfile = $authManager->createPermission('updateOwnProfile');
        $updateOwnProfile->ruleName = $userProfileOwnerRule->name;
        $authManager->add($updateOwnProfile);

        //$authManager->addChild($guest, $updateOwnProfile);
        $authManager->addChild($client, $updateOwnProfile);

        $guest  = $authManager->getRole('guest');
        $student  = $authManager->getRole('student');

        $drop  = $authManager->getRole('drop');
        $opt  = $authManager->getRole('opt');
        $manager = $authManager->getRole('manager');
        $super_manager  = $authManager->getRole('supermanager');
        $superadmin  = $authManager->getRole('superadmin');
        $authManager->assign($superadmin, '984');

        echo 'rbac complete';
    }
}