<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.08.2018
 * Time: 13:08
 */

namespace backend\controllers\rbac;


use yii\rbac\Rule;
use yii\rbac\Item;

class UserProfileOwnerRule extends Rule
{
    public $name = 'isProfileOwner';

    /**
     * @param string|integer $user   the user ID.
     * @param Item           $item   the role or permission that this rule is associated with
     * @param array          $params parameters passed to ManagerInterface::checkAccess().
     *
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (\Yii::$app->user->can('admin' )|| \Yii::$app->user->can('manager' )) {
            return true;
        }
        return isset($params['id']) ? \Yii::$app->user->id == $params['id'] : false;
    }
}