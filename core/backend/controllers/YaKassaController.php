<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.09.2018
 * Time: 10:16
 */

namespace backend\controllers;
use common\helpers\myHellpers;
use common\models\OrderTest; 
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use YandexCheckout\Client;

class YaKassaController extends Controller
{
    public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'order-check' => ['post'],
                    'payment-notification' => ['post'],
                ],
            ]
        ];
    }
    public function actions()
    {
        return [
            'order-check' => [
                'class' => 'kroshilin\yakassa\actions\CheckOrderAction',
                'beforeResponse' => function ($request) {
                    /**
                     * @var \yii\web\Request $request
                     */
                    $invoice_id = (int) $request->post('orderNumber');
                    Yii::warning("Кто-то хотел купить несуществующую подписку! InvoiceId: {$invoice_id}", Yii::$app->yakassa->logCategory);
                    return false;
                }
            ],
            'payment-notification' => [
                'class' => 'kroshilin\yakassa\actions\PaymentAvisoAction',
                'beforeResponse' => function ($request) {
                    /**
                     * @var \yii\web\Request $request
                     */
                }
            ],
        ];
    }

    public function actionIndex(){

        $order = new OrderTest();

        $this->enableCsrfValidation = false;
        return $this->render('index', [
            'order' => $order
        ]);
    }
}