<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.08.2018
 * Time: 16:07
 */

namespace backend\controllers;

use common\models\delivery\DeliveryType;
use common\models\dropship\OrdersStatus;
use common\models\dropship\OrdersStatusGroup;
use Yii;
use bobchengbin\Yii2XmlRequestParser\XmlRequestParser;
use common\helpers\myHellpers;
use common\models\MainModel;
use common\models\products\Products;
use common\models\products\ProductsCategories;
use common\models\products\ProductsStock;
use yii\gii\Module;
use yii\web\Controller;

class ImportXmlController extends Controller
{

    public function actionN(){
        $retailCrm = Yii::$app->retailCrm;

        $sl = $retailCrm->client->request->statusGroupsList();
        //myHellpers::show($sl);
        //$sl = $retailCrm->client->request->ordersList();
        $sl = $retailCrm->client->request->ordersGet('38934','id');
        /*$sl = $retailCrm->client->request->ordersStatuses(['38934','38921','38587']);
        myHellpers::show($sl);*/
        //$sl = $retailCrm->client->request->statusesList();
        myHellpers::show($sl);
    }
    public function actionA(){
        $c = ProductsCategories::find()->all();
        foreach ($c as $_c){
            $_c->status_drop =5;
            $_c->update();
        }
    }

    public function actionStatuses(){
        $retailCrm = Yii::$app->retailCrm;
        $stsG = $retailCrm->client->request->statusGroupsList();
        $stsL = $retailCrm->client->request->statusesList();
        if(self::importStatusGroup($stsG)){
            self::importStatus($stsL);
        }

    }

    protected static function importStatusGroup($data){
        foreach ($data['statusGroups'] as $g=>$v){
            if(!$g = self::getStatusGroup($v['code'])) $g = new OrdersStatusGroup();
            $g->alias = $v['code'];
            $g->name = $v['name'];
            $g->code = $v['code'];
            if(!$g->save()){ myHellpers::show($g->errors);}
        }
        return true;
    }

    protected static function getStatusGroup($code){
        return OrdersStatusGroup::find()->where(['code'=>$code])->one();
    }
    protected static function importStatus($data){
        myHellpers::show($data);
        foreach ($data['statuses'] as $_g=>$v){
            myHellpers::show($_g);
            if(!$s = self::getStatus($v['code'])) $s = new OrdersStatus();
            $gr = self::getStatusGroup($v['group']);
            $s->alias = $v['code'];
            $s->name = $v['name'];
            $s->code = $v['code'];
            $s->group = $gr->id;
            if(!$s->save()){ myHellpers::show($s->errors);}

        }
        return true;
    }

    protected static function getStatus($code){
        return OrdersStatus::find()->where(['code'=>$code])->one();
    }



    public function actionDelivery(){
        $retailCrm = Yii::$app->retailCrm;
        $del = $retailCrm->client->request->deliveryTypesList();

        foreach ($del->deliveryTypes as $k=>$v){
            if(!$d = DeliveryType::find()->where(['alias'=>$k])->one()){
                $d = new DeliveryType();
            }
            $d->name = $v['name'];
            $d->alias = $k;
            $d->delivery_price = $v['defaultCost'];
            $d->status = MainModel::STATUS_DROP_ACTIVE;
            $d->status_drop = MainModel::STATUS_DROP_INACTIVE;
            $d->status_opt = MainModel::STATUS_DROP_INACTIVE;
            if(!$d->save()){
                myHellpers::show([],'delivery import complete');
            }
            myHellpers::show($d->errors);
        }

    }
    public function actionIndex(){
        $xml = new XmlRequestParser;
        //$url = 'http://retailcrm.smart-microcam.com/moysklad/microcam.catalog.xml';
        $url = 'http://warehouse.smart-microcam.com/microcam.catalog.xml';
        $xml_d =  file_get_contents($url);
        $data = $xml->parse($xml_d, '');

        $_cat = $data['shop']['categories']['category'];
        $_prd = $data['shop']['offers']['offer'];

        $cat = $this->parseCat($_cat);
        self::importCategory($cat);


        $prd = $this->parsePrd($_prd);

        self::importProducts($prd);
    }

    private static function importCategory($_data){
        //myHellpers::show($_data, 'category');
        //ProductsCategories::deleteAll(['status'=>'10']);
        foreach ($_data as $data){
            $model = (ProductsCategories::findXmlCategory($data['xml_id'])) ? ProductsCategories::findXmlCategory($data['xml_id']) : new ProductsCategories();
            $prn = ($model::findXmlCategory($data['parentId'])->id) ? $model::findXmlCategory($data['parentId'])->id : 1;
            $model->name = $data['name'];
            $model->parent_id = 0;
            $model->xml_name = $data['xml_name'];
            $model->xml_id = $data['xml_id'];
            $model->xml_parent_id = $data['xml_parent_id'];
            if($model->status_drop == MainModel::STATUS_PRODUCT_DROP_ACTIVE){
                $model->status_drop = MainModel::STATUS_PRODUCT_DROP_ACTIVE;
            }else{
                $model->status_drop = MainModel::STATUS_PRODUCT_DROP_INACTIVE;
            }

            if(!$model->save()){
                myHellpers::show($model->errors,'Category Import Errors');
            }
        }
        echo 'Cat import!';
    }
    private static function importProducts($_data){
        //ProductsStock::deleteAll();
        foreach ($_data as $data){
            $model = (Products::findProductByXml($data['xml_id'])) ? Products::findProductByXml($data['xml_id']) : new Products();
            $ctg = (ProductsCategories::findXmlCategory($data['xml_category'])->id) ? ProductsCategories::findXmlCategory($data['xml_category'])->id : 0;
            $model->xml_id = $data['xml_id'];
            $model->name = $data['name'];
            $model->xml_name = $data['xml_name'];
            $model->category_id = $ctg;
            $model->xml_category = $data['xml_category'];
            $model->price = $data['price'];
            $model->xml_price = $data['xml_price'];
            $model->article = $data['article'];
            $model->xml_article = $data['xml_article'];
            $model->status = $data['status'];
            if($model->status_drop == MainModel::STATUS_PRODUCT_DROP_ACTIVE){
                $model->status_drop = MainModel::STATUS_PRODUCT_DROP_ACTIVE;
            }else{
                $model->status_drop = MainModel::STATUS_PRODUCT_DROP_INACTIVE;
            }

            if($model->save()){
                $stock = (ProductsStock::findProductId($model->id)) ? ProductsStock::findProductId($model->id) : new ProductsStock();
                $stock->product_id = $model->id;
                $stock->count = 1;
                if(!$stock->save()){
                    myHellpers::show($model->errors,'Product Import Errors');
                }
            }
        }
        echo 'Product   import!';


    }
    private function parseCat($_cat){
        $cat = [];
        for($i = 0 ; $i<count($_cat); $i++){
            if(!empty($_cat[$i])){
                $cat[$i]['name'] = $_cat[$i];
                $cat[$i]['xml_name'] = $_cat[$i];
                $cat[$i]['xml_id'] = $_cat[$i.'_attr']['id'];
                $cat[$i]['xml_parent_id'] = ($_cat[$i.'_attr']['parentId']) ? $_cat[$i.'_attr']['parentId'] : '0';

                $data['status'] = MainModel::STATUS_ACTIVE;
            }
        }
        return $cat;
    }
    private function parsePrd($_prd){
        $prd = [];
        for($i=0; $i<count($_prd); $i++){
            if(!empty($_prd[$i])) {
                $prd[$i]['name'] = $_prd[$i]['name'];
                $prd[$i]['price'] = $_prd[$i]['price'];
                $prd[$i]['article'] = $_prd[$i]['param'][0];
                $prd[$i]['xml_id'] = $_prd[$i]['xmlId'];
                $prd[$i]['xml_name'] = $_prd[$i]['name'];
                $prd[$i]['xml_price'] = $_prd[$i]['price'];
                $prd[$i]['xml_article'] = $_prd[$i]['param'][0];
                $prd[$i]['xml_category'] = $_prd[$i]['categoryId'];

                $prd[$i]['status'] = MainModel::STATUS_PRODUCT_ACTIVE;
                $prd[$i]['status_drop'] = MainModel::STATUS_PRODUCT_DROP_INACTIVE;
            }
        }
        return $prd;
    }

}