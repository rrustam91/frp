<?php
return [
    'cookieValidationKey' => '',
    'cookieDomain' => '.fankretailpartners.net',
    'frontendHostInfo' => 'http://fankretailpartners.net',
    'backendHostInfo' => 'http://admin.fankretailpartners.net',
    'cpHostInfo' => 'http://cp.fankretailpartners.net',
    'staticHostInfo' => 'http://static.fankretailpartners.net',
    'mailChimpKey' => '',
    'mailChimpListId' => '',
    'smsRuKey' => '',
];
