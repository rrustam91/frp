
$(document).ready(function() {

    $curl = document.location.pathname;
    $('.lesson__sidebar-list').each(function(i,elem) {
        if ($(this).data("href") == $curl) {
            $(this).addClass('active');
            $(this).find('i').addClass('fa fa-circle');
        } else {
            $(this).removeClass('active');
        }
    });

})