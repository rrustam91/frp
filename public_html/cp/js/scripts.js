
$(document).ready(function() {

    $curl = document.location.pathname;
    $('.lesson__sidebar-list').each(function(i,elem) {
        if ($(this).data("href") == $curl) {
            $(this).addClass('active');
            $(this).find('i').addClass('fa fa-circle');
        } else {
            $(this).removeClass('active');
        }
    });


    function initActions() {
        $('.dropQuantity').on('keyup change mouseup', function (e) {
            $min = $(this).attr('min');
            $min_price = $(this).data('min-price');
            $target = $(this).data('target');
            $new_price = $('#prc-' + $target);
            $qnt = $(this).val();
            if ($qnt < $min) {
                $(this).val($min);
                $qnt = $min
            }
            ;
            if ($new_price.val() > $min_price) {
                $min_price = $new_price.val();
            }
            $price = $min_price * $qnt;
            $('#hQnt' + $target).val($qnt);
            $('#hFpr' + $target).val($price);
            changeDropPrice($target, $price);

        });



        $('.dropPrice').on('change mouseup', function (e) {
            $min = parseInt($(this).attr('min'));
            $target = $(this).data('target');
            $qnt = parseInt($('#qnt-' + $target).val());
            $price = parseInt($(this).val());
            if ($price <= $min) {
                $price = $min
                $(this).val($min);
            }
            $fprice = $qnt * $price;
            $('#hQnt' + $target).val($qnt);
            $('#hApr' + $target).val($price);
            $('#hFpr' + $target).val($fprice);

            changeDropPrice($target, $fprice);
        });

        $('.removeDropList').on('click', function (e) {
            $target = $('#dropListProduct' + $(this).data('id'));
            $('#op-' + $(this).data('id')).removeAttr('disabled')
            removeElem($target);
            $('.dropdown').select2();
        });

        checkTable();
        priceMask();

        function removeElem($elem) {
            $elem.fadeOut(250, function () {
                $elem.remove();
                checkTable()
            });
        }
        $('.delivery__cost').on('change mouseup', function (e) {checkTable()});
    }

    function getNum(_n){
        return parseInt(_n.replace(/\s/g, ''), 10)
    }
    function checkFullPrice(){
        var _ts=0;
        var _td=0;
        var qnt = 1;
        var _dc = getNum( $('.delivery__cost').val());
        if($('.price_prd').length>0){
            $('.price_prd ').each(function(i,elem) {
                _ts += getNum($(this).text());
            });
            $('.price_drp ').each(function(i,elem) {
                qnt = '.qnt_'+$(this).data('qnt');
                qnt = getNum($(qnt).val());
                _td += getNum($(this).text());
            });
        }
        $('.full__com').text((((_ts + _dc)*0.97)-(_td * qnt)).toFixed(2));

        priceMask();
    }
    function checkTable() {
        checkFullPrice();
        $count = $('.dropProductListTable tr.dropListProductTr').length;
        if ($count <= 0) {
            $('.empty-table').removeClass('hidden');
            return true;
        }
        $('.empty-table').addClass('hidden');
    }

    function changeDropPrice($trg, $prc) {
        $('#end-' + $trg).html($prc);
        checkTable();
        priceMask();
    }
    function initSelect(select){
        select = '#'+select
        $('.dropdown').select2({
            theme: "default",
            placeholder: "Выбрать",
            language: {
                noResults: function (term) {
                    return "Не найден";
                }
            },

        });


        $(select).on('select2:select', function (e) {
            var data = e.params.data;
            $name = data.element.text;
            $trg = getVal(data, 'target');
            $price = Math.floor(getVal(data, 'price'));
            $price_drop = Math.floor(getVal(data, 'price-drop'));
            $article = getVal(data, 'article');
            $pid = getVal(data, 'pid');
            $('.dropProductListTable').append(renderTbl($trg, $name, $price,  $price_drop, $article, $pid));
            initActions();

            var element = e.params.data.element;
            var $element = $(element);
            $element.attr('disabled', 'disabled');
            $(select).select2();

        });

        initActions()
    }


    function renderSelect(select_box, data) {
        if(typeof(data) != "undefined" && data !== null) {
            var select_id = 'select_'+Math.floor((Math.random() * 10) + 1);
            var opt = '<select class="dropdown form-control" id="'+select_id+'" name="DropOrderForm[products]"" style="width:100%">';
            opt += '<option value="0" disabled selected>- Выберите товар -</option>';
            $.each(data, function () {
                opt += '<option id="op-' + this.xml_id + '" value="' + this.xml_id + '" data-target="' + this.xml_id + '" data-price="' + this.price + '"  data-price-drop="' + this.price_drop + '" data-article="' + this.article + '" data-pid="' + this.id + '" >' + this.name + '</option>\n'

            });
            opt += '</select>';
            if($(select_box)){
                $(select_box).append(opt);
                initSelect(select_id);
            }


            ;
        }else{
            console.log('bad');
        }
    }

    renderSelect('#products_select',_products);
    renderSelect('#products_select2',_products_app);
    function renderTbl(pxid, pname, pprc,  pprd, part, pid, qnt) {
        var tb_template = '<tr id="dropListProduct' + pxid + '" class="dropListProductTr"><td style="vertical-align: middle;" class="text-nowrap">' +
            '<a href="/cp/products/'+pid+'" target="_blank" class="btn btn-flat bg-green"> <i class="fa fa-info"></i></a>&nbsp; &nbsp;' + pname + '</td><td style="vertical-align: middle;"><div align="center"> ' + part + '</div></td><td style="vertical-align: middle;"><div align="center" > <span class="price_drp" data-qnt="'+pxid+'">' + pprd + '</span> р.</div></td><td style="vertical-align: middle;"><div align="center"> ' + pprc + ' р.</div></td><td><div align="center" style="min-width: 60px; max-width: 120px;"> <input type="number" class="dropQuantity form-control qnt_' + pxid + '" data-target="' + pxid + '" data-min-price="' + pprc + '" id="qnt-' + pxid + '" min="1" value="1"></div></td><td><div align="center" style="min-width: 100px; max-width: 150px;"> <input type="number" class="dropPrice form-control prc' + pxid + '" id="prc-' + pxid + '" data-target="' + pxid + '" min="' + pprc + '" value="' + pprc + '"></div></td><td style="vertical-align: middle;"><div align="left" style="vertical-align: center"> <span class="dropDeliveryPrice price-num price_prd " id="end-' + pxid + '"> ' + pprc + ' </span> р.</div></td><td style="vertical-align: middle;"><div align="center">  <button type="button" class="btn bg-red btn-flat removeDropList" data-id="' + pxid + '"> <i class="fa fa-trash-o"></i> </button></div></td>';
        tb_template += '<input id="hId' + pid + '" type="hidden" name="DropOrderForm[products_h][' + pxid + '][id]" value="' + pid + '">';
        tb_template += '<input id="hxId' + pxid + '" type="hidden" name="DropOrderForm[products_h][' + pxid + '][xid]" value="' + pxid + '">';
        tb_template += '<input id="hName' + pxid + '" type="hidden" name="DropOrderForm[products_h][' + pxid + '][name]" value="' + pname + '">';
        tb_template += '<input id="hPr' + pxid + '"  type="hidden" name="DropOrderForm[products_h][' + pxid + '][price]" value="' + pprc + '">';
        tb_template += '<input id="hPrD' + pxid + '"  type="hidden" name="DropOrderForm[products_h][' + pxid + '][price_drop]" value="' + pprd + '">';
        tb_template += '<input id="hApr' + pxid + '"  type="hidden" name="DropOrderForm[products_h][' + pxid + '][ap_price]" value="' + pprc + '">';
        tb_template += '<input id="hArt' + pxid + '"  type="hidden" name="DropOrderForm[products_h][' + pxid + '][article]" value="' + part + '">';
        tb_template += '<input id="hQnt' + pxid + '"  type="hidden" name="DropOrderForm[products_h][' + pxid + '][qnt]" value="1">';
        tb_template += '<input id="hFpr' + pxid + '"  type="hidden" name="DropOrderForm[products_h][' + pxid + '][full_price]" value="1">';
        tb_template += '</tr>';
        return tb_template;

    }


    function getVal(data, trg) {
        if (data.element.attributes['data-' + trg]) {
            return data.element.attributes['data-' + trg].value;
        }
        return '';
    }


    function priceMask(){
        var prices = document.querySelectorAll(".price-num");
        Array.from(prices).forEach(function(elem){
            elem.innerHTML = elem.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        });
    }
    priceMask();




    $('.lesson__sidebar-header').on('click', function (e) {
        var trg = $(this);
        $_trg = trg.data('target');
        $('#'+$_trg).slideToggle(100, '', function(){ // отображаем, или скрываем элементы <div> в документе
            trg.parent().toggleClass('open');
        });
    });

    $('.form__drop-order').on('submit',function (e) {
        if($('.dropListProductTr').length > 0){
            return true;
        }else{
            alert('Вы забыли добавить товары!');
            return false;
        }
    })

});