$(".nav_content li").on("click", "a", function (e) {
    e.preventDefault();
    if (!$(this).hasClass('add-contact')) {
        $(this).tab('show');
    };
})
    .on("click", "span", function () {
        var anchor = $(this).parent();
        $(anchor.attr('href')).remove();
        $(anchor).parent().remove();
        $(".nav_content li").children('a').first().click();
    });

$('#products-name').on('input', function() {
    var send = $(this).val();
    $.ajax({
        url: '/site/alias-translate',
        data: {name: send},
        type: 'POST',
        success: function (res){
            $('#products-alias').val(res);
        },
        error: function (er){
            //console.log(er);
        }
    });
});