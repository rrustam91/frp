


$('.add_content').click(function (e) {
    e.preventDefault();
    var id = ($(".tab_content .tab_box").length); //think about it ;)
    var lid = $(".nav_content li").length; //think about it ;)
    var tabId = 'desc_tab_' + id;
    $(this).closest('li').before('<li ><a href="#desc_tab_'+id+' " data-toggle="tab" aria-expanded="false">Новая вкладка '+(id+1)+' <span class="fa fa-close"></span></a></li>');
    $('.tab_content').append('<div class="tab-pane tab_box" id="' + tabId + '"><div class="box-body"><div class="form-group field-products-product_content-'+id+'-name"><label class="control-label" for="products-product_content-'+id+'-name">Заголовок описания</label><input type="text" id="products-product_content-'+id+'-name" class="form-control" name="Products[product_content]['+id+'][name]" value="Название вкладки '+(id+1)+'" placeholder="Заголовок вкладки"><div class="help-block"></div></div><div class="form-group field-products-product_content-'+id+'-content"><label class="control-label" for="products-product_content-'+id+'-content">Текст</label><textarea id="products-product_content-content" class="form-control tinymce_editor" name="Products[product_content]['+id+'][content]" >Тут будет текст для вкладки '+(id+1)+'</textarea><div class="help-block"></div></div></div></div>');

    tinyRemove();
    setTimeout(function(){
        tinyInit();
        $(this).parent().removeClass('active');
        $('.nav_content li:nth-child(' + (lid) + ') a').click();
    },500)




});
function tinyInit(){
    tinymce.init({
        selector: '.tinymce_editor',
        language: 'ru',
        height: '350',
        branding: false,
        browser_spellcheck: true,

        fontsize_formats: '16px 18px 20px 24px 28px 30px 40px',
        language_url : '/plugins/responsivefilemanager/filemanager/lang/langs/ru.js',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help',
            'image responsivefilemanager filemanager',
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic forecolor fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  responsivefilemanager link image media',
        //toolbar2: 'print preview media |  backcolor     ',
        external_filemanager_path: '/plugins/responsivefilemanager/filemanager/',
        filemanager_title : 'Responsive Filemanager',
        external_plugins  : {
            'filemanager':'/plugins/responsivefilemanager/filemanager/plugin.min.js',
            'responsivefilemanager': '/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
        },

    });
}
function tinyRemove(){
    tinymce.remove();
}
function tinyReinit(){

}
tinyInit();