
$(document).ready(function() {
    $('#w0').on('submit', function (e) {

        e.preventDefault();
        var $yiiform = $(this);
        // отправляем данные на сервер
        $.ajax({
                type: $yiiform.attr('method'),
                url: $yiiform.attr('action'),
                data: $yiiform.serializeArray()
            }
        )
            .done(function (data) {
                if (data.success) {
                    console.log('succes');
                    $('#webinarcomments-name').val('');
                    $('#webinarcomments-text').text('');
                    $('#w0 #webinarcomments-name').focus();
                    // данные сохранены
                } else {
                    console.log(data);
                    // сервер вернул ошибку и не сохранил наши данные
                }
            })
            .fail(function (data) {

                console.log(data);
                // не удалось выполнить запрос к серверу
            })
        return false; // отменяем отправку данных формы
    })

} );