
$(document).ready(function() {
    $('.datepick').datetimepicker( {
        format:'Y-m-d H:i',
        allowTimes:[
            '10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30',
            '14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30',
            '18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30',
            '22:00','22:30','23:00','23:30','00:00','00:30','01:00','01:30',
            '02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30'
        ],
        lang:'ru'
    });

    function checkElem($elem){
        return $('*').is($elem);
    }
    function initActions(){
        $('.dropQuantity').on('keyup change mouseup', function (e) {
            $min = $(this).attr('min');
            $min_price = $(this).data('min-price');
            $target = $(this).data('target');
            $new_price = $('#prc-'+$target);
            $qnt = $(this).val();
            if($qnt<$min){$(this).val($min); $qnt = $min};
            if($new_price.val() > $min_price){ $min_price = $new_price.val(); }
            $price = $min_price*$qnt;
            $('#hQnt'+$target).val($qnt);
            $('#hFpr'+$target).val($price);
            changeDropPrice($target, $price);
        });

        $('.dropPrice').on('keyup change mouseup', function (e) {
            $min = $(this).attr('min');
            $target = $(this).data('target');
            $qnt = $('#qnt-'+$target).val();
            $price = $(this).val();

            if($price<$min){$(this).val($min); $price = $min};
            $fprice = $qnt*$price;

            $('#hQnt'+$target).val($qnt);
            $('#hApr'+$target).val($price);
            $('#hFpr'+$target).val($fprice);
            changeDropPrice($target, $fprice);
        });

        $('.removeDropList').on('click', function (e) {
            $target = $('#dropListProduct'+$(this).data('id'));
            $('#op-'+$(this).data('id')).removeAttr('disabled')
            removeElem($target);

            $('.dropdown').select2();
        });

        $('.addDropList').on('click', function (e) {
            //$('.dropProductListTable').append(renderTbl());
            //console.log($(".dropdown option:selected").data('target'))
            initActions();
        });
        checkTable();
        $(".dropdown" ).select2({
            theme: "default",
            placeholder: "Выбрать",
            language: {
                noResults: function(term) {
                    return "Не найден";
                }
            },

        });


        $('.dropdown').on('select2:select', function (e) {
            var data = e.params.data;
            console.log(data);
            $name = data.element.text;
            $trg = getVal(data,'target');
            $price = Math.floor(getVal(data,'price'));
            $article = getVal(data,'article');
            $pid = getVal(data,'pid');
            $('.dropProductListTable').append(renderTbl($trg,$name, $price, $article,$pid));
            initActions();

            var element = e.params.data.element;
            var $element = $(element);
            $element.attr('disabled','disabled');
            $('.dropdown').select2();

        });


        function removeElem($elem){
            $elem.fadeOut( 250, function() {
                $elem.remove();
                checkTable()
            });
        }

    }







    function checkTable(){
        $count = $('.dropProductListTable tr.dropListProductTr').length;
        if($count <= 0) { $('.empty-table').removeClass('hidden'); return true; }
        $('.empty-table').addClass('hidden');
    }
    function changeDropPrice($trg, $prc){
        $('#end-'+$trg).html($prc);
    }



    renderSelect();
    function renderSelect(){
        var opt = '<select class="dropdown form-control" name="DropOrderForm[products]"" style="width:100%">';
        opt += '<option value="0" disabled selected>- Выберите товар -</option>';
        $.each(_products, function(){
            opt += '<option id="op-'+this.xml_id+'" value="'+this.xml_id+'" data-target="'+this.xml_id+'" data-price="'+this.price+'" data-article="'+this.article+'" data-pid="'+this.id+'" >'+this.name+'</option>\n'

        });
        opt += '</select>';
        $('#products_select').append(opt);
        initActions();
    }

    function renderTbl(pxid, pname, pprc, part, pid, qnt ){
        var tb_template = '<tr id="dropListProduct'+pxid+'" class="dropListProductTr"><td style="vertical-align: middle;" class="text-nowrap">'+pname+'</td><td style="vertical-align: middle;"><div align="center"> '+part+'</div></td><td style="vertical-align: middle;"><div align="center"> '+pprc+' р.</div></td><td><div align="center" style="min-width: 60px; max-width: 120px;"> <input type="number" class="dropQuantity form-control qnt'+pxid+'" data-target="'+pxid+'" data-min-price="'+pprc+'" id="qnt-'+pxid+'" min="1" value="1"></div></td><td><div align="center" style="min-width: 100px; max-width: 150px;"> <input type="number" class="dropPrice form-control prc'+pxid+'" id="prc-'+pxid+'" data-target="'+pxid+'" min="'+pprc+'" value="'+pprc+'"></div></td><td style="vertical-align: middle;"><div align="left" style="vertical-align: center"> <span class="dropDeliveryPrice " id="end-'+pxid+'"> '+pprc+' </span> р.</div></td><td style="vertical-align: middle;"><div align="center">  <button type="button" class="btn bg-red btn-flat removeDropList" data-id="'+pxid+'"> <i class="fa fa-trash-o"></i> </button></div></td></tr>';
        tb_template += '<input id="hId'+pid+'" type="hidden" name="DropOrderForm[products_h]['+pxid+'][id]" value="'+pid+'">';
        tb_template += '<input id="hxId'+pxid+'" type="hidden" name="DropOrderForm[products_h]['+pxid+'][xid]" value="'+pxid+'">';
        tb_template += '<input id="hName'+pxid+'" type="hidden" name="DropOrderForm[products_h]['+pxid+'][name]" value="'+pname+'">';
        tb_template += '<input id="hPr'+pxid+'"  type="hidden" name="DropOrderForm[products_h]['+pxid+'][price]" value="'+pprc+'">';
        tb_template += '<input id="hApr'+pxid+'"  type="hidden" name="DropOrderForm[products_h]['+pxid+'][ap_price]" value="'+pprc+'">';
        tb_template += '<input id="hArt'+pxid+'"  type="hidden" name="DropOrderForm[products_h]['+pxid+'][article]" value="'+part+'">';
        tb_template += '<input id="hQnt'+pxid+'"  type="hidden" name="DropOrderForm[products_h]['+pxid+'][qnt]" value="1">';
        tb_template += '<input id="hFpr'+pxid+'"  type="hidden" name="DropOrderForm[products_h]['+pxid+'][full_price]" value="1">';
        return tb_template;

    }






    function getVal(data,trg){
        if(data.element.attributes['data-'+trg]) {
            return data.element.attributes['data-' + trg].value;
        }
        return '';
    }

    if(checkElem('#player')){
        var myPlayer = videojs('#player');

        myPlayer.src('http://fankretailpartners.ru/upload/g1Lsx094.mp4');

        myPlayer.on('timeupdate', function() {
            $('.time_send').val(Math.round(this.currentTime()));
        });
        myPlayer.ready(function() {
            var language = 'ru';
            // get the current time, should be 120 seconds
            var whereYouAt = myPlayer.currentTime();
        });
    }







});