$(function() {

	// Custom JS
    pulse();

    $('.fancybox, .call__form').fancybox({ });
    var owl2 = $('.about__carousel');


        owl2.owlCarousel({
            loop:true,
            stagePadding: 15,
            margin:0,
            nav: false,

            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:2
                }
            },

        });

    var owl = $('.carousel');
    owl.owlCarousel({
        loop:true,
        stagePadding: 50,
        margin:10,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        },

    });

    // Custom Navigation Events
    $(".next").click(function() {
        owl.trigger('next.owl.carousel');
        console.log('asd')
    });

    $(".prev").click(function() {
        owl.trigger('prev.owl.carousel');
        console.log('zxc')
    });

    $('.goto').on('click', function(e){
        goto($(this).attr('href'));
    });

    // Custom Navigation Events
    $(".menu--toggle").click(function() {
        $(this).toggleClass(function() {
            if ($(this).hasClass('active')) {
                closeMenu();
                return $(this).removeClass('active');
            } else {
                openMenu();
                //$(".menu--toggle").addClass('active');
                return 'active';

            }
        });
    });


    function openMenu(){
        $('.menu__block').addClass('active');
    }
    function closeMenu(){
        $(".menu--toggle").removeClass('active');
        $('.menu__block').removeClass('active');
    }



    $('.btn_video, .about__video--thumb').on('click', function (object) {
       /*var target = '#'+$(this).data('target');
       // $('.about__video--thumb').fadeOut();
        if ($(target).get(0).paused) {
            $(target).get(0).play();
        } else {
            $(target).get(0).pause();
        }
        $(target).get(0).play()*/
    });


    $('.about__author-blocks').on('click', function (e) {
        $('.about__author-blocks').removeClass('active');
        $(this).addClass('active');
        var target = '.'+$(this).data('target');
        $('.about__author-desc').removeClass('active');
        $(target).addClass('active');

        return false;

    })


    /** Circle donut chart**/
    $('.circle').circleProgress({
        size: 150,
        thickness: 3,
        emptyFill: 'rgba(180,180,180,.3)',
        fill:  'rgba(100,50,150,.8)',
    }).on('circle-animation-progress', function(event, progress) {
        var txt = ($(this).data('text')) ? $(this).data('text') : '';
        var apnd = ($(this).data('append')) ? $(this).data('append') : '';
        var num = $(this).data('num');
        $(this).find('.num_mask').html('<span class="circle__num">'+Math.round(num * progress) + apnd +'</span><br><i>'+ txt +'</i>');
        pureNum()
    });



    /** Num mask 99 999 **/
    function pureNum(){
        var prices = document.querySelectorAll(".num_mask");
        Array.from(prices).forEach(function(elem){
            elem.innerHTML = elem.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        });
    }


    /** Btn pulse animation **/
    function pulse() {
        $('.pulse').each(function () {
            var $this = $(this);
            var ink, d, x, y;

            setInterval(function () {
                if ($this.find(".ink").length === 0) {
                    $this.prepend("<span class='ink'></span>");
                }

                ink = $this.find(".ink");
                ink.removeClass("animate");

                if (!ink.height() && !ink.width()) {
                    d = Math.max($this.outerWidth(), $this.outerHeight());
                    ink.css({ height: d, width: d });
                }

                x = Math.round(Math.random() * ink.width() - ink.width() / 2);
                y = Math.round(Math.random() * ink.height() - ink.height() / 2);
                // y = 0;
                // x = e.pageX - $this.offset().left - ink.width()/2;
                // y = e.pageY - $this.offset().top - ink.height()/2;

                ink.css({ top: y + 'px', left: x + 'px' }).addClass("animate");
            }, 1600);
        });
    };
    /**  Goto scroll **/
    function goto(to){
        console.log(to)
        if($(to).length){
            $('body, html').animate({ scrollTop: $(to).offset().top }, 1000);
        }
        closeMenu();

    }

});
