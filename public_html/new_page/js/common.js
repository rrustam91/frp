$(function() {

    AOS.init();

    $('.price__block').on('click', function(){
        if(!$(this).hasClass('active')){
            $('.price__block').removeClass('active');
            $(this).addClass('active');
            $('.price__block').find('.btn').removeClass('active');
            $(this).find('.btn').addClass('active');
        }

    })

    $('.btn__order').on('click', function(){

        $price = $(this).data('price');
        $type = $(this).data('target');
        $info = $price +' руб.';
        console.log($price);
        console.log($type);
        $('.type_course').val($type);
        $('.form__btn ').find('.btn__inner').html('Начать обучение за &nbsp; <span class="num_mask">'+$price +'</span> ₽');
        $('.price__product-name').html($(this).data('name'));
        pureNum();
    });


    $('.open__modal').on('click', function(){
        var target = $(this).data('modal');
        open_modal('#'+target);
    });
    $('.close').on('click', function(){
        close_modal();
    });
    function open_modal(target){
        var trg = $(target);
        if(!$('body').hasClass('modal-opened')){    $('body, section').addClass('modal-opened'); }

        if(!trg.hasClass('open')){
            trg.fadeIn('slow',function(){
                trg.addClass("open");
            })
        }

    }
    $('.modal-back').on('click', function(e){
        close_modal();
    })
    function close_modal(){
        $('body, section').removeClass('modal-opened');
        $('.modal').fadeOut('open', function () {
            $(this).removeClass('open');
        });
    }

    $('.btn-about').on('click', function(e){
        trg = '.'+$(this).data('target');
        if(!$(this).hasClass('active')){
            $(this).addClass('active');

            $(trg).slideDown()
        }else{
            $(this).removeClass('active');
            $(trg).slideUp()
        }
        e.preventDefault();
    });
    function toggleBlock(trg,prn){
        trg = '.'+trg;
        console.log(trg);
        if(!$(trg).hasClass('active')){
            $(trg).addClass('active');
            $(trg).slideUp()
        }else{
            $(trg).removeClass('active');

        }
    }
    /**

    var wow = new WOW(
        {
            boxClass:     'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset:       0,          // distance to the element when triggering the animation (default is 0)
            mobile:       true,       // trigger animations on mobile devices (default is true)
            live:         true,       // act on asynchronously loaded content (default is true)
            callback:     function(box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null,    // optional scroll container selector, otherwise use window,
            resetAnimation: true,     // reset animation on end (default is true)
        }
    );
    wow.init();
     */
	// Custom JS
    pulse();
    pureNum();

    /** Num mask 99 999 **/
    function pureNum(){
        var prices = document.querySelectorAll(".num_mask");
        Array.from(prices).forEach(function(elem){
            elem.innerHTML = elem.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        });
    }
    $('.goto').on('click', function (e) {
        goto($(this).attr('href'));
        e.preventDefault();
    })
    function goto(to){
        if($(to).length){
            $('body, html').animate({ scrollTop: $(to).offset().top }, 1000);
        }
    }


    /** Btn pulse animation **/
    function pulse() {
        $('.pulse').each(function () {
            var $this = $(this);
            var ink, d, x, y;

            setInterval(function () {
                if ($this.find(".ink").length === 0) {
                    $this.prepend("<span class='ink'></span>");
                }

                ink = $this.find(".ink");
                ink.removeClass("animate");

                if (!ink.height() && !ink.width()) {
                    d = Math.max($this.outerWidth(), $this.outerHeight());
                    ink.css({ height: d, width: d });
                }

                x = Math.round(Math.random() * ink.width() - ink.width() / 2);
                y = Math.round(Math.random() * ink.height() - ink.height() / 2);
                // y = 0;
                // x = e.pageX - $this.offset().left - ink.width()/2;
                // y = e.pageY - $this.offset().top - ink.height()/2;

                ink.css({ top: y + 'px', left: x + 'px' }).addClass("animate");
            }, 1600);
        });
    };


});
