$(function() {

    function time() {
        var x = new Date();
        var currentTime = (180 + x.getTimezoneOffset())*60*1000;
        var now = new Date(x.getTime()+currentTime);
        var minutes = now.getMinutes();
        var nextHour = false;
        var nextDay = false;
        var monthName = "";
        var output = new Date(now.getYear(), now.getMonth(), now.getDate(), now.getHours(),now.getMinutes());
        var hours = output.getHours();
        switch (true){
            case minutes < 15:
                output.setMinutes(15);
                break;
            case (minutes > 15 && minutes < 30) || minutes === 15:
                output.setMinutes(30);
                break;
            case (minutes > 30 && minutes < 45) || minutes === 30:
                output.setMinutes(45);
                break;
            case (minutes > 15 && minutes < 60) || minutes === 45:
                output.setMinutes(0);
                nextHour = true;
                break;
            default:
                break;
        }
        if(nextHour){
            if(hours === 23){
                nextDay = true;
                output.setDate(now.getDate() + 1);
            }
            else
                output.setHours(output.getHours() + 1);
        }
        switch (output.getMonth()) {
            case 0:
                monthName = "Января";
                break;
            case 1:
                monthName = "Февраля";
                break;
            case 2:
                monthName = "Марта";
                break;
            case 3:
                monthName = "Апреля";
                break;
            case 4:
                monthName = "Мая";
                break;
            case 5:
                monthName = "Июня";
                break;
            case 6:
                monthName = "Июля";
                break;
            case 7:
                monthName = "Августа";
                break;
            case 8:
                monthName = "Сентября";
                break;
            case 9:
                monthName = "Октября";
                break;
            case 10:
                monthName = "Ноября";
                break;
            case 11:
                monthName = "Декабря";
                break;
            default:
                monthName = "Установите правильное время на своем компьютере!"
        }

        if(nextHour){
            minutes = "00";
            if(nextDay){
                var _time = " 00:" + minutes + " по Мск";
                var _date = output.getDate() + " " + monthName;
                $('.date_start').text(_date);
                $('.time_start').text(_time);
                if(_r) {simpleTimer(output, nextDay);}
            } else{
                var _time = output.getHours() + ":" + minutes + " по Мск";
                var _date = output.getDate() + " " + monthName;
                $('.date_start').text(_date);
                $('.time_start').text(_time);
                if(_r) {simpleTimer(output);}
            }
        }
        else{
            var _time = output.getHours() + ":" + output.getMinutes() + " по Мск";
            var _date = output.getDate() + " " + monthName;
            $('.date_start').text(_date);
            $('.time_start').text(_time);
            if(_r) {simpleTimer(output);}
        }
    }

    time();
    function simpleTimer(endDate, nextDay = null) {
        function getTimeRemaining(endtime) {
            var t = Date.parse(endtime) - Date.parse(new Date().toISOString());
            var seconds = Math.floor(t / 1000 % 60);
            var minutes = Math.floor(t / 1000 / 60 % 60);
            var hours = Math.floor(t / (1000 * 60 * 60) % 24);
            var days = Math.floor(t / (1000 * 60 * 60 * 24));
            return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        }

        function initializeClock(id, endtime) {

            var clock = document.getElementById(id);
            var daysSpan = $('._td');
            var hoursSpan = $('._th');
            var minutesSpan = $('._tm');
            var secondsSpan = $('._ts');

            function updateClock() {
                var t = getTimeRemaining(endtime);
                daysSpan.innerHTML = t.days;
                hoursSpan.text(('0' + t.hours).slice(-2));
                minutesSpan.text(('0' + t.minutes).slice(-2));
                secondsSpan.text(('0' + t.seconds).slice(-2));

                if(t.days === 0 || t.days < 0){
                    $("#clockdiv > div:first-of-type").hide();
                }
                if (t.total <= 0) {
                    clearInterval(timeinterval);
                    daysSpan.innerHTML = "00";
                    hoursSpan.innerHTML = "00";
                    minutesSpan.innerHTML = "00";
                    secondsSpan.innerHTML = "00";
                    redirect();
                }
            }

            updateClock();
            var timeinterval = setInterval(updateClock, 1000);
        }
        if(nextDay)
            endDate.setDate(endDate.getDate()-1);
        endDate.setMonth(endDate.getMonth() + 1);
        var minutes = endDate.getMinutes() == 0 ?  "00" : endDate.getMinutes();
        minutes = minutes+"";
        minutes = minutes.length > 1 ? minutes : "0" + minutes;
        var month = endDate.getMonth() >= 10 ? endDate.getMonth() : "0" + endDate.getMonth();
        var hours = endDate.getHours() + "";
        hours = hours.length > 1 ? hours : "0" + hours;
        hours = hours+"";
        hours = hours.length > 1 ? hours : "0" + hours;
        var seconds = "";
        var dateEnd = endDate.getDate() >= 10 ? endDate.getDate() : "0" + endDate.getDate();
        if(nextDay){
            minutes = "59";
            hours = "23";
            seconds = "59";
        }else{
            seconds = "00";
        }
        var deadlineDate = new Date(Date.parse("2018-"+month+"-"+dateEnd+"T"+hours+":"+minutes+":"+seconds+".000+03:00")); //Month Days, Year HH:MM:SS
        initializeClock('clockdiv', deadlineDate);
    }
    function redirect(){
        window.location.replace('/webinar/room');
    }



});
